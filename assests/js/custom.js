$(document).ready(function () {
    var base_url = 'http://landwindtech.com/';
    var validationErro = 'animated shake errors';
    $('#btnRequestCall').click(function (event) {
event.preventDefault();
        var contName = $('#contName').val();
        var contEmail = $('#contEmail').val();
        var contPhone = $('#contPhone').val();
        var contMessage = $('#contMessage').val();

        if ($.trim(contName) == '') {
            $("#contName").notify("Please Enter Your Name").focus();
            $('#contName').addClass(validationErro);
            setTimeout(function () {
                $('#contName').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(contEmail) == '') {
            $("#contEmail").notify("Please Enter Your Email").focus();
            $('#contEmail').addClass(validationErro);
            setTimeout(function () {
                $('#contEmail').removeClass(validationErro);
            }, 2000);
            return false;
        }
        var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!filter.test(contEmail)) {
            $("#contEmail").notify("Your Email Invalid").focus();
            $('#contEmail').addClass(validationErro);
            setTimeout(function () {
                $('#contEmail').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(contPhone) == '') {
            $("#contPhone").notify("Please Enter Your Phone").focus();
            $('#contPhone').addClass(validationErro);
            setTimeout(function () {
                $('#contPhone').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(contMessage) == '') {
            $("#contMessage").notify("Please Enter Your Message").focus();
            $('#contMessage').addClass(validationErro);
            setTimeout(function () {
                $('#contMessage').removeClass(validationErro);
            }, 2000);
            return false;
        } else {
            var dataForm = {
                contName: contName,
                contEmail: contEmail,
                contPhone: contPhone,
                contMessage: contMessage
            }
            $.ajax({
                url: base_url + 'callbackMessage',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    data = jQuery.parseJSON(data);
                    if (data.status == 1) {
                        swal({
                            type: 'success',
                            title: 'Thank You',
                            text: data.message
                        });
                        $('#contName').val('');
                        $('#contEmail').val('');
                        $('#contPhone').val('');
                        $('#contMessage').val('');
                        $("#callbackModal").modal("hide");
                        //$('#callbackModal').dialog('close');   
                    } else {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: data.message
                        });
                    }
                }
            });
        }
    });
    $(".number").keypress(function (evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode
    
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    
            $(this).notify("Only Number");
    
            return false;
    
        }
    
    });
    //====NEW===
    $(".bars").click(function () {
        $(this).toggleClass("active-bar");
        $(this).next(".navbar").toggleClass("active");
    });
    //MENU TABS====
    $(".tabed li a").click(function () {
        var DATAR = $(this).data("rel");
        $(this).addClass("active").parents("li").siblings("li").children("a").removeClass("active");
        $(".submenuinner").find(DATAR).addClass("active").siblings(".programs").removeClass("active");

    });


    var $output = $(".requestcallbackstickey"),
            scrolling = $(".requestcallbackstickey").addClass("scrolling"),
            stopped = $(".requestcallbackstickey").removeClass("scrolling");
    var lastScrollTop = 0;
    if ($(window).width() > 640) {
        $(window).scroll(function () {
            var st = $(this).scrollTop();
            var scroll = $(window).scrollTop();
            if (st > lastScrollTop) {
                $(".requestcallbackstickey").addClass("scrolling");
                clearTimeout($.data(this, "scrollCheck"));
                $.data(this, "scrollCheck", setTimeout(function () {
                    $(".requestcallbackstickey").removeClass("scrolling");
                }, 250));
            } else {
                $(".requestcallbackstickey").removeClass("scrolling");
            }
            lastScrollTop = st;
        });
    }//Shams
    //Responsive Menu====
    if ($(window).width() < 1025) {

        $(".menu-item-has-children .down-angle").click(function () {
             $(this).toggleClass("arrow-down");
            $(".submenu").slideToggle();
        });

    }



    
    //====END NEW===
});
$(window).scroll(function () {
    var sticky = $('header'),
            bodySt = $('body'),
            scroll = $(window).scrollTop();

    if (scroll >= 1) {
        sticky.addClass('sticky');
        bodySt.addClass('stickyHeader');
    } else {
        sticky.removeClass('sticky');
        bodySt.removeClass('stickyHeader');
    }
    if (scroll >= 400) {
        sticky.addClass('down');
    } else {
        sticky.removeClass('down');
    }
});
$(window).on("load", function () {
    if ($(window).width() < 1025) {
        $(".navmenu").mCustomScrollbar();
    }
});

//
$(window).on("load", function () {
    $(".bannerimage").css("opacity", 1);
});
$(document).ready(function () {
    $('#owl-carousel').owlCarousel({
        margin: 0,
        loop: true,
        items: 1,
        singleItem: true,
        dots: true,
        dot: true,
    });

    //

    var sections = $('.tabsbyid')
            , nav = $('.tabscustom')
            , nav_height = nav.outerHeight();
    var lastScrollTop = 0;
    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop();
        //Responsive if scroll Top====
        if (cur_pos > lastScrollTop) {
            $(".requestcallbackstickey").addClass("scrolltop");

        } else {
            $(".requestcallbackstickey").removeClass("scrolltop");
        }
        if ($(window).width() < 768) {
            if (cur_pos > lastScrollTop) {

                if (cur_pos > 500) {
                    $(".tabscustom").fadeOut(400);
                }
            } else {
                $(".tabscustom").fadeIn(400);
            }

        }
        lastScrollTop = cur_pos;

        //


        sections.each(function () {
            var top = $(this).offset().top - 200,
                    bottom = top + $(this).outerHeight();
            if (cur_pos >= top && cur_pos <= bottom) {
                nav.find('li').removeClass('active');
                nav.find('li[data-rel="#' + $(this).attr('id') + '"]').addClass('active');
            } else {
            }
        });
        setTimeout(function () {
            $(".tabscustom li").removeClass("cactive");
        }, 500);

        //
        //
        var tabscustomOffset = $(".tabstype").offset().top;
        var tabscustomOffsetTotal = tabscustomOffset - 66;
        if ($(this).scrollTop() > tabscustomOffsetTotal) {
            //console.log(tabscustomOffset);
            $(".tabscustom").addClass("stickey");
            $(".tabscustom").css({
                position: 'fixed',
                top: '66px',
                left: 'auto'
            });
        } else {
            //console.log(tabscustomOffset + "else");
            $(".tabscustom").removeClass("stickey");
            $(".tabscustom").css({
                position: 'absolute',
                top: '0px'
            });
        }
        //Remove class if less or more scroll
        var tabstypeoffsettop = $(".tabstype").offset().top;
        var tabstypeoffsettop1 = tabstypeoffsettop - 100;
        var tabstypeheight = $(".tabstype").height();
        $(".tabstype").each(function () {
            var top1 = $(this).offset().top - 200,
                    bottom1 = (top1 + 200) + $(this).outerHeight();
            if (cur_pos >= tabstypeoffsettop1 && cur_pos <= bottom1) {
                //console.log("Under");
            } else {
                $(".tabscustom.container li").removeClass("active");
                //If need to hide tabs after all tabscontent scrolled top uncoment below
                /* $(".tabscustom").removeClass("stickey");
                 $(".tabscustom").css({
                 position: 'absolute',
                 top: '0px'
                 }); */
            }
        });
    });
    //End Scroll
    //TABS====
    $(".tabscustom li").click(function () {
        var DATA = $(this).data("rel");
        $(this).addClass("cactive").siblings("li").removeClass("cactive");
        setTimeout(function () {
            $("body , html").animate({
                scrollTop: $(DATA).offset().top - 150
            }, 500);

        }, 50);
    });
});



$(document).ready(function () {
$('#owl-demo').owlCarousel({
    loop:true,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1200:{
            items:2
        },
        1300:{
            items:4
        },
           1920:{
            items:4
        }
    }
});





$('.partner-slides').owlCarousel({
    items: 5, 
    margin:30,
    loop:true,
    nav:false,
    dots:false,
    autoplay: true,
    // autoplayTimeout: 1515,
    smartSpeed: 1500,
    animateIn: 'linear',
    animateOut: 'linear',
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2
        },
        1000:{
            items:5
        }
    }
});





});


  // var owl = $z("#owl-demo");
// $('#owl-demo').owlCarousel({
//       items : 4, 
//       itemsDesktop : [1199,3], 
//       itemsDesktopSmall : [991,2], 
//       itemsTablet: [639,1], 
//       itemsMobile : false, 
//       autoPlay : true,
//       autoPlaySpeed : 500  
//   });


/***************** /Testimonial Carousel Jquery *******************/