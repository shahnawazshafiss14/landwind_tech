var abc = 0;      // Declaring and defining global increment variable.

//ADD MORE file js
$(document).ready(function() {
$('#add_more').click(function() {
	$(this).before($("<div/>", {
		id: 'filediv'
		})
		.fadeIn('slow').append($("<input/>", {
			name: 'file[]',
			type: 'file',
			id: 'file'
			}), $("<br/>")));
		});


// Following function will executes on change event of file input to select different file.
$('body').on('change', '#file', function() {
	if (this.files && this.files[0]) {
	abc += 1; // Incrementing global variable by 1.
	var z = abc - 1;
	var x = $(this).parent().find('#previewimg' + z).remove();
	$(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
	var reader = new FileReader();
	reader.onload = imageIsLoaded;
	reader.readAsDataURL(this.files[0]);

$(this).hide();
$("#abcd" + abc).append($("<img/>", {
	id: 'img',
	src: 'https://playmyart.com/assests/dist/img/close.png',
	alt: 'delete'
	}).click(function() {
		$(this).parent().parent().remove();
	})); 
}
});

// To Preview Image
function imageIsLoaded(e) {
$('#previewimg' + abc).attr('src', e.target.result);
};
$('#upload').click(function(e) {
var name = $(":file").val();
if (!name) {
alert("First Image Must Be Selected");
e.preventDefault();
}
});
});

/*Devlopment query*/

$('.add_carts').click(function () {
    var main_image = $(this).data("main_image");
    var product_id = $(this).data("productid");
    var cartid = $(this).data("cartid");
    var product_name = $(this).data("productname");
    var sessionid_email = $(this).data("sessionid_email");
    var product_price = $(this).data("price");
    var quantity = $('#' + product_id).val();
    if (quantity != '' && quantity > 0) {
	$.ajax({
	    url: base_url + "add",
	    method: "POST",
	    data: {main_image: main_image, sessionid_email: sessionid_email, cartid: cartid, product_id: product_id, product_name: product_name, product_price: product_price, quantity: quantity},
	    success: function (data) {
	        $('#notification').addClass('shownotification'); 
	        setTimeout(function(){
	             $('#notification').removeClass('shownotification'); 
	        },3000,'swing');
	        
		$('#cart_details').html(data);
		$('.cart_count').load(base_url + 're_view_count');
		$('#' + product_id).val();
	    }
	}); 
    } else { 
	        $('#notification1').addClass('shownotification'); 
	        setTimeout(function(){
	             $('#notification1').removeClass('shownotification'); 
	        },3000,'swing'); 
    }
});

$('#cart_details').load(base_url + "load");
$('.cart_count').load(base_url + 're_view_count');