var base_url = 'http://landwindtech.com/admin/';
var baseassest = 'http://landwindtech.com/assests/';
var validationErro = 'animated shake errors';
 
$(document).ready(function () {
 
    $('.add').on('click', function () {

        var $qty = $(this).closest('.input-group').find('.qty');
        var currentVal = parseInt($qty.val());
        //  alert(currentVal)
        if (!isNaN(currentVal)) {
            $qty.val(currentVal + 1);
        }
    });
    $('.minus').on('click', function () {
        var $qty = $(this).closest('.input-group').find('.qty');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal) && currentVal > 50) {
            $qty.val(currentVal - 1);
        }
    });
});
$(window).on('load', function () {
    var grand = $('#grandtotal').val().toUpperCase();
    //var bac = convertNumberToWords(grand);
    $('#amountinword').html(grand);
});
$('body').on('click', '#btnreset', function () {
    window.location.href = '/admin/invoices/invoice_fillter';
    
});
$('body').on('click', '#btnresetexp', function () {
    window.location.href = '/admin/expense/lists';
    
});
$(".datechange").change(function () {
    var date1 = this.value;
    var shop = $(this).data('shop');
    var vehicle = $(this).data('vehicle');
    var sptdate = String(date1).split("/");
    var myMonth = sptdate[1];
    var myDay = sptdate[0];
    var myYear = sptdate[2];
    var date = myYear + '-' + myMonth + '-' + myDay;
    $.ajax({
        url: base_url + "items/addItemsss",
        method: "GET",
        data: {items_dos: date, shop: shop, vehicle: vehicle},
        dataType: "json",
        success: function (data) {
            window.location.href = '/admin/items/addItems/?shop=' + data.shop + '&items_dos=' + data.dos + '&items_vehicle=' + data.vehicle;
        }
    });

});
$(".datechangesales").change(function () {
    var date1 = this.value;
    var shop = $(this).data('shop');
    var vehicle = $(this).data('vehicle');
    var sptdate = String(date1).split("/");
    var myMonth = sptdate[1];
    var myDay = sptdate[0];
    var myYear = sptdate[2];
    var date = myYear + '-' + myMonth + '-' + myDay;
    $.ajax({
        url: base_url + "salesitem/addItemsss",
        method: "GET",
        data: {items_dos: date, shop: shop, vehicle: vehicle},
        dataType: "json",
        success: function (data) {
            window.location.href = '/admin/salesitem/addItems/?shop=' + data.shop + '&items_dos=' + data.dos + '&items_vehicle=' + data.vehicle;
        }
    });

});
function reset() {
    $("#toggleCSS").attr("href", "alertify.default.css");
    alertify.set({
        labels: {
            ok: "OK",
            cancel: "Cancel"
        },
        delay: 3000,
        buttonReverse: false,
        buttonFocus: "ok"
    });
}

$(document).on('change', '#artclassevent', function () {
    var get_id = $(this).find(':selected').val();
    var dela = {
        del_id: get_id
    };
    $.ajax({
        url: base_url + "artists/artclass",
        method: "POST",
        data: dela,
        success: function (data) {
            $('#artclasseventp').html(data);
        }
    });
});
$(document).on('click', '#sendemailaction', function () {
    $('#sentm').show();
    $('#actonw').hide();
});
$(document).on('click', '#emailcancel', function () {
    location.reload();
});
$(document).on('click', '#btnitemrefere', function () {
    location.reload();
});
$(document).on('click', '#tractitemupadd', function () {
    var sid = $(this).data('id');
    //var particuler_id = $('#parst_id').val();
    var transaction_type = $('#transaction_type').val();
    var t_amount = $('#t_amount').val();
    var t_desc = $('#t_desc').val();
    if ($.trim(transaction_type) == "") {
        alert('Select Type');
    } else if ($.trim(t_amount) == "") {
        alert('Enter Amount');
    } else {
        $.ajax({
            url: base_url + "items/itemTrans",
            method: "POST",
            data: {tractitemupadd: transaction_type, t_amount: t_amount, sid: sid, t_desc: t_desc},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});
$(document).on('click', '#tractitemupaddsales', function () {
    var sid = $(this).data('id');
    var transaction_type = $('#transaction_type').val();
    var t_amount = $('#t_amount').val();
    var t_desc = $('#t_desc').val();
    if ($.trim(transaction_type) == "") {
        alert('Select Type');
    } else if ($.trim(t_amount) == "") {
        alert('Enter Amount');
    } else {
        $.ajax({
            url: base_url + "salesitem/itemTrans",
            method: "POST",
            data: {tractitemupadd: transaction_type, t_amount: t_amount, sid: sid, t_desc: t_desc},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});
$(".deleteRcordaddItemt").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "items/deleteItemtran",
                method: "POST",
                data: {del_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteRcordaddItemtP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".deleteRcordaddItemtsales").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "salesitem/deleteItemtran",
                method: "POST",
                data: {del_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteRcordaddItemtP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(document).on('click', '#itemsitemupadd', function () {
    var sid = $(this).data('id');
    var particuler_id = $('#parst_id').val();
    var items_rate = $('#items_rate').val();
    var items_unit = $('#items_unit').val();
    if ($.trim(particuler_id) == "") {
        $('#particuler_id').focus();
        $("#particuler_id").notify("Enter particuler name");
        $('#particuler_id').addClass(validationErro);
        setTimeout(function () {
            $('#particuler_id').removeClass(validationErro);
        }, 2000);

    } else if ($.trim(items_rate) == "") {
        $('#items_rate').focus();
        $("#items_rate").notify("Enter items rate");
        $('#items_rate').addClass(validationErro);
        setTimeout(function () {
            $('#items_rate').removeClass(validationErro);
        }, 2000);

    } else if ($.trim(items_unit) == "") {
        $('#items_unit').focus();
        $("#items_unit").notify("Enter items unit");
        $('#items_unit').addClass(validationErro);
        setTimeout(function () {
            $('#items_unit').removeClass(validationErro);
        }, 2000);
    } else {
        $.ajax({
            url: base_url + "items/itemAdd",
            method: "POST",
            data: {particuler_id: particuler_id, items_rate: items_rate, items_unit: items_unit, sid: sid},
            success: function (data) {
                alertify.success("Items has been added!");
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        });
    }
});
$(document).on('click', '#itemsitemupaddsales', function () {
    var sid = $(this).data('id');
    var particuler_id = $('#parst_id').val();
    var items_rate = $('#items_rate').val();
    var items_unit = $('#items_unit').val();
    if ($.trim(particuler_id) == "") {
        $('#particuler_id').focus();
        $("#particuler_id").notify("Enter particuler name");
        $('#particuler_id').addClass(validationErro);
        setTimeout(function () {
            $('#particuler_id').removeClass(validationErro);
        }, 2000);

    } else if ($.trim(items_rate) == "") {
        $('#items_rate').focus();
        $("#items_rate").notify("Enter items rate");
        $('#items_rate').addClass(validationErro);
        setTimeout(function () {
            $('#items_rate').removeClass(validationErro);
        }, 2000);

    } else if ($.trim(items_unit) == "") {
        $('#items_unit').focus();
        $("#items_unit").notify("Enter items unit");
        $('#items_unit').addClass(validationErro);
        setTimeout(function () {
            $('#items_unit').removeClass(validationErro);
        }, 2000);
    } else {
        $.ajax({
            url: base_url + "salesitem/itemAdd",
            method: "POST",
            data: {particuler_id: particuler_id, items_rate: items_rate, items_unit: items_unit, sid: sid},
            success: function (data) {
                alertify.success("Items has been added!");
                setTimeout(function () {
                    location.reload();
                }, 2000);
            }
        });
    }
});
$(".deleteRcordaddItem").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "items/deleteItem",
                method: "POST",
                data: {del_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteRcordaddItemP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".deletetransitem").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "transport/deleteItem",
                method: "POST",
                data: {del_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteRcordaddItemP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});

$(".deleteRcordaddItemsales").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "salesitem/deleteItem",
                method: "POST",
                data: {del_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteRcordaddItemP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(document).on('change', '#particuler_id', function () {
    var get_id = $(this).find(':selected').val();
    $('#parst_id').val(get_id);
});
$('#btnsendEmail').click(function () {
    var slug_id = $(this).data('slug');
    var sendemail = $('#sendEmail').val();
    if ($.trim(sendemail) == "") {
        alert('Enter email id');
    } else {
        $.ajax({
            url: base_url + "invoices/sendInvoice",
            method: "POST",
            data: {sendemail: sendemail, slug_id: slug_id},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});
$('#btnsendEmailcon').click(function () {
    var slug_id = $(this).data('slug');
    var sendemail = $('#sendEmail').val();
    var from = $('#from').val();

    var to = $('#to').val();
    if ($.trim(sendemail) == "") {
        alert('Enter email id');
    } else if ($.trim(from) == "") {
        alert('Enter date from');
    } else if ($.trim(to) == "") {
        alert('Enter date to');
    } else {
        $.ajax({
            url: base_url + "consignee/sendSales",
            method: "POST",
            data: {sendemail: sendemail, slug_id: slug_id, from: from, to: to},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});

$('#btnsendEmailinlist').click(function () {
    var slug_id = $(this).data('slug');
    var sendemail = $('#sendEmail').val();
    var from = $('#from').val();

    var to = $('#to').val();
    if ($.trim(sendemail) == "") {
        alert('Enter email id');
    } else if ($.trim(from) == "") {
        alert('Enter date from');
    } else if ($.trim(to) == "") {
        alert('Enter date to');
    } else {
        $.ajax({
            url: base_url + "consignee/sendInvoic",
            method: "POST",
            data: {sendemail: sendemail, slug_id: slug_id, from: from, to: to},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});


$('#btnsendConsigmo').click(function () {
    var slug_id = $(this).data('slug');
    var month_id = $(this).data('month');
    var sendemail = $('#sendEmail').val();
    if ($.trim(sendemail) == "") {
        alert('Enter email id');
    } else {
        $.ajax({
            url: base_url + "consignee/sendConsigmo",
            method: "POST",
            data: {sendemail: sendemail, slug_id: slug_id, month_id: month_id},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});
$('#btnsendEmailbill').click(function () {
    var slug_id = $(this).data('slug');
    var sendemail = $('#sendEmail').val();
    if ($.trim(sendemail) == "") {
        alert('Enter email id');
    } else {
        $.ajax({
            url: base_url + "transport/sendBill",
            method: "POST",
            data: {sendemail: sendemail, slug_id: slug_id},
            success: function (data) {
                alert(data);
                location.reload();
            }
        });
    }
});
$(document).on('change', '#categoryevent', function () {
    var get_id = $(this).find(':selected').val();
    $('#categoryartclass_id').val(get_id);
});
$('#btnArtist').click(function () {
    var artist_name = $('#artist_name').val();
    var artist_lname = $('#artist_lname').val();
    var artist_email = $('#artist_email').val();
    var artist_pwd = $('#artist_pwd').val();
    var artist_phone = $('#artist_phone').val();
    var artist_country = $('#userState').val();
    var artist_state = $('#hidCity').val();
    var artist_city = $('#artist_city').val();
    var artist_zipcode = $('#artist_zipcode').val();
    var artist_image = $('#artist_images').val();
    var artist_description = $('#artist_description').val();
    var ext = $('#artist_images').val().split('.').pop().toLowerCase();
    if ($.trim(artist_name) == "") {
        $("#artist_name").notify("Artist First Name Required").focus();
        $('#artist_name').addClass(validationErro);
        setTimeout(function () {
            $('#artist_name').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(artist_lname) == "") {
        $('#artist_lname').focus();
        $("#artist_lname").notify("Artist Last Name Required");
        $('#artist_lname').addClass(validationErro);
        setTimeout(function () {
            $('#artist_lname').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(artist_email) == "") {
        $("#artist_email").notify("Artist Email Required");
        return false;
    }
    var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (!filter.test(artist_email)) {
        $("#artist_email").notify("Email Invalid");
        return false;
    } else if ($.trim(artist_phone) == "") {
        $("#artist_phone").notify("Artist Phone Required");
        return false;
    }
    if (artist_phone.length <= 9) {
        $("#artist_phone").notify("Mobile should be min 9 character");
        return false;
    } else if ($.trim(artist_country) == "") {
        $("#userState").notify("Artist Country Required");
        return false;
    } else if ($.trim(artist_state) == "") {
        $("#hidCity").notify("Artist State Required");
        return false;
    } else if ($.trim(artist_city) == "") {
        $("#artist_city").notify("Artist City Required");
        return false;
    } else if ($.trim(artist_zipcode) == "") {
        $("#artist_zipcode").notify("Artist Zipcode Required");
        return false;
    } else if ($.trim(artist_description) == "") {
        $("#artist_description").notify("About Artist Required");
        return false;
    } else if ($.trim(artist_image) == '') {
        $("#artist_images").notify("About Artist Required");
        return false;
    }
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#artist_images").notify("invalid extension!");
        return false;
    } else {
        return true;
    }
});
$('#btnParticular').click(function () {
    var eral = 'animated shake error';
    var item_particular_name = $('#item_particular_name').val();
    var item_hsn = $('#item_hsn').val();
    var item_unit = $('#item_unit').val();
    if ($.trim(item_particular_name) == "") {
        $("#item_particular_name").notify("Particulars Name Required").focus();
        $('#item_particular_name').addClass(eral);
        setTimeout(function () {
            $('#item_particular_name').removeClass(eral);
        }, 2000);
        return false;
    } else if ($.trim(item_hsn) == "") {
        $('#item_hsn').focus();
        $("#item_hsn").notify("HSN Address Required");
        $('#item_hsn').addClass(eral);
        setTimeout(function () {
            $('#item_hsn').removeClass(eral);
        }, 2000);
        return false;
    } else if ($.trim(item_unit) == "") {
        $('#item_unit').focus();
        $("#item_unit").notify("KG / Weight Required");
        $('#item_unit').addClass(eral);
        setTimeout(function () {
            $('#item_unit').removeClass(eral);
        }, 2000);
        return false;
    } else {
        return true;
    }
});
$('#btnItemssales').click(function () {
    var eral = 'animated shake error';
    var item_dor_id = $('#dor_id').val();
    var item_dos = $('#items_dos').val();
    var item_vehicle = $('#items_vehicle').val();
    if ($.trim(item_dor_id) == "") {
        $("#dor_receiver_id").notify("Select Consignee Name").focus();
        $('#dor_receiver_id').addClass(eral);
        setTimeout(function () {
            $('#dor_receiver_id').removeClass(eral);
        }, 2000);
        return false;
    } else if ($.trim(item_dos) == "") {
        $("#items_dos").notify("Select date of supply").focus();
        $('#items_dos').addClass(eral);
        setTimeout(function () {
            $('#items_dos').removeClass(eral);
        }, 2000);
        return false;
    } else if ($.trim(item_vehicle) == "") {
        $("#items_vehicle").notify("Enter Vehicle Number").focus();
        $('#items_vehicle').addClass(eral);
        setTimeout(function () {
            $('#items_vehicle').removeClass(eral);
        }, 2000);
        return false;
    } else {
        return true;
    }
});
$('#btnItems').click(function () {
    var eral = 'animated shake error';
    var item_dor_id = $('#dor_id').val();
    var item_dos = $('#items_dos').val();
    var item_vehicle = $('#items_vehicle').val();
    if ($.trim(item_dor_id) == "") {
        $("#dor_receiver_id").notify("Select Consignee Name").focus();
        $('#dor_receiver_id').addClass(eral);
        setTimeout(function () {
            $('#dor_receiver_id').removeClass(eral);
        }, 2000);
        return false;
    } else if ($.trim(item_dos) == "") {
        $("#items_dos").notify("Select date of supply").focus();
        $('#items_dos').addClass(eral);
        setTimeout(function () {
            $('#items_dos').removeClass(eral);
        }, 2000);
        return false;
    } else if ($.trim(item_vehicle) == "") {
        $("#items_vehicle").notify("Enter Vehicle Number").focus();
        $('#items_vehicle').addClass(eral);
        setTimeout(function () {
            $('#items_vehicle').removeClass(eral);
        }, 2000);
        return false;
    } else {
        return true;
    }
});


$('#btn_items_remartks').click(function () {
    var items_remartks = $('#items_remartks').val();
    var items_billno = $('#items_billno').val();
    var tid = $(this).data('tid');
    if ($.trim(items_remartks) == '') {
        $("#items_remartks").notify("Remarks Required").focus();
        $('#items_remartks').keyup(function () {
            $('#items_remartks').removeClass(validationErro);
        });
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('items_remartks', items_remartks);
        form_data.append('items_billno', items_billno);
        form_data.append('tid', tid);
        $.ajax({
            url: base_url + "salesitem/updateremarks",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                alertify.success("Remarks/Bill has been updated successfully");
            }
        });
    }
});

$('#btn_items_pur_remartks').click(function () {
    var items_remartks = $('#items_remartks').val();
    var items_billno = $('#items_billno').val();
    var tid = $(this).data('tid');
    if ($.trim(items_remartks) == '') {
        $("#items_remartks").notify("Remarks Required").focus();
        $('#items_remartks').keyup(function () {
            $('#items_remartks').removeClass(validationErro);
        });
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('items_remartks', items_remartks);
        form_data.append('items_billno', items_billno);
        form_data.append('tid', tid);
        $.ajax({
            url: base_url + "items/updateremarks",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                alertify.success("Remarks/Bill has been updated successfully");
            }
        });
    }
});

$(".menumiss").change(function () {
    var useid = $('#useris').val();
    var chageval = $(this).val();
    var form_data = new FormData();
    form_data.append('useid', useid);
    form_data.append('chageval', chageval);
    $.ajax({
        url: base_url + "users/addme",
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            //alert(data);
            location.reload();
        }
    });
});
$(".menumis").change(function () {
    var useid = $('#useris').val();
    var chageval = $(this).val();
    var chai = $(this).data('i');
    var datav = $(this).data('val');
    var form_data = new FormData();
    form_data.append('useid', useid);
    form_data.append('chai', chageval);
    form_data.append('chageval', chai);
    form_data.append('datav', datav);
    $.ajax({
        url: base_url + "users/addmev",
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            //alert(data);
            location.reload();
        }
    });
});
$(".generateinvoice").click(function () { 
var proj_client = $('#proj_client').val();
var proj_id = $('#proj_id').val();
var searchIDs = $("input[name=selservices]:checked").map(function(){
      return $(this).val();
    }).get();
  if ($.trim(proj_client) == "") {
        $.notify("Client ID Required"); 
        return false;
    } else if ($.trim(proj_id) == "") { 
        $.notify("Project Name Required");
         return false;
    } else if ($.trim(searchIDs) == "") { 
        $.notify("Select Services Required");
        return false;
    } else{
	    $.ajax({
        	url: base_url + "project/generateInvoice",
        	method: "POST",
        	data: {proj_client:proj_client, proj_id:proj_id, searchIDs:searchIDs}, 
        	success: function (data) {
			console.log(data);
            		//alert(data);
            		//location.reload();
        	}
    		});
	}
});
$('#ubtnConsignee').click(function () {
    var eral = 'animated shake error';
    var consignee_name = $('#consignee_name').val();
    var consignee_address = $('#consignee_address').val();
    var consignee_gstin = $('#consignee_gstin').val();
    var consignee_state = $('#consignee_state').val();
    var consignee_city = $('#consignee_city').val();
    var consignee_zipcode = $('#consignee_zipcode').val();
    var consignee_description = $('#consignee_description').val();
    var isChecked = $("input[name=consignee_type]:checked").val();
    if ($.trim(consignee_name) == "") {
        $("#consignee_name").notify("Consignee Name Required").focus();
        $('#consignee_name').addClass(validationErro);
        setTimeout(function () {
            $('#consignee_name').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(consignee_address) == "") {
        $('#consignee_address').focus();
        $("#consignee_address").notify("Consignee Address Required");
        $('#consignee_address').addClass(validationErro);
        setTimeout(function () {
            $('#consignee_address').removeClass(validationErro);
        }, 2000);
        return false;
    } else if (!isChecked) {
        $('#consignee_type').focus();
        $("#consignee_type").notify("Consignee Type Required");
        return false;
    } else if ($.trim(consignee_gstin) == "") {
        $('#consignee_gstin').focus();
        $("#consignee_gstin").notify("Consignee GSTIN Required");
        $('#consignee_gstin').addClass(validationErro);
        setTimeout(function () {
            $('#consignee_gstin').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(consignee_state) == "") {
        $('#consignee_state').focus();
        $("#consignee_state").notify("Consignee State Required");
        $('#consignee_state').addClass(validationErro);
        setTimeout(function () {
            $('#consignee_state').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(consignee_city) == "") {
        $("#consignee_city").notify("Consignee City Required");
        return false;
    } else {
        return true;
    }
});
$('#updatebtn_consig_img').click(function () {
    var ext = $('#update_consig_img').val().split('.').pop().toLowerCase();
    var cateimagec1 = document.getElementById("update_consig_img").files[0];
    var cateEdit = $('#consigneeid').val();
    if ($.trim(cateimagec1) == '') {
        $("#update_consig_img").notify("Image Required").focus();
        $('#update_consig_img').keyup(function () {
            $('#update_consig_img').removeClass(validationErro);
        });
        return false;
    }
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#update_consig_img").notify("invalid extension!");
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('consigneeImage', cateimagec1);
        form_data.append('consigneeid', cateEdit);
        $.ajax({
            url: base_url + "consignee/updateImageConsignee",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#response').html('<img src="' + baseassest + 'img/loading.gif" />');
            },
            success: function (data) {
                var message = "Image upload successfully";
                $('#successMessage').modal('show');
                $('#essmessage').text(message)

                //location.reload();
                /*if (data == "1") {
                 $.notify("record has been successfully");
                 location.reload();
                 } 
                 */
            }
        });
    }
});
$('#ubtnShop').click(function () {
    var eral = 'animated shake error';
    var shop_name = $('#shop_name').val();
    var shop_address = $('#shop_address').val();
    var shop_gstin = $('#shop_gstin').val();
    var shop_state = $('#shop_state').val();
    var shop_city = $('#shop_city').val();
    var shop_zipcode = $('#shop_zipcode').val();
    var shop_description = $('#shop_description').val();
    var shop_area = $('#shop_area').val();
    if ($.trim(shop_name) == "") {
        $("#shop_name").notify("Shop Name Required").focus();
        $('#shop_name').addClass(validationErro);
        setTimeout(function () {
            $('#shop_name').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(shop_address) == "") {
        $('#shop_address').focus();
        $("#shop_address").notify("Shop Address Required");
        $('#shop_address').addClass(validationErro);
        setTimeout(function () {
            $('#shop_address').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(shop_gstin) == "") {
        $('#shop_gstin').focus();
        $("#shop_gstin").notify("Shop Aadhar Number Required");
        $('#shop_gstin').addClass(validationErro);
        setTimeout(function () {
            $('#shop_gstin').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(shop_state) == "") {
        $('#shop_state').focus();
        $("#shop_state").notify("Shop State Required");
        $('#shop_state').addClass(validationErro);
        setTimeout(function () {
            $('#shop_state').removeClass(validationErro);
        }, 2000);
        return false;
    } else if ($.trim(shop_city) == "") {
        $("#shop_city").notify("Shop City Required");
        return false;
    } else if ($.trim(shop_area) == "") {
        $("#shop_area").notify("Shop Area Required");
        return false;
    } else {
        return true;
    }
});
$('#updatebtn_venue_img').click(function () {
    var ext = $('#update_venue_img').val().split('.').pop().toLowerCase();
    var cateimagec1 = document.getElementById("update_venue_img").files[0];
    var cateEdit = $('#venueid').val();
    if ($.trim(cateimagec1) == '') {
        $("#update_venue_img").notify("Image Required").focus();
        $('#update_venue_img').keyup(function () {
            $('#update_venue_img').removeClass(validationErro);
        });
        return false;
    }
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#update_venue_img").notify("invalid extension!");
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('venueImage', cateimagec1);
        form_data.append('venueid', cateEdit);
        $.ajax({
            url: base_url + "venues/updateImageVenue",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                location.reload();
                /*if (data == "1") {
                 $.notify("record has been successfully");
                 location.reload();
                 } 
                 */
            }
        });
    }
});

$(".deleteRcordConsignee").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "consignee/deleteConsignee",
                method: "POST",
                data: {del_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteConsigneetrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$('#btnCategoryextraimg').click(function () {
    var ext = $('#category_image').val().split('.').pop().toLowerCase();
    var eventimagec1 = document.getElementById("category_image").files[0];
    if ($.trim(eventimagec1) == '') {
        $("#category_image").notify("Image Required");
        return false;
    }
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#category_image").notify("invalid extension!");
        return false;
    } else {
        return true;
    }
});
$('.add_carts').click(function () {
    var product_id = $(this).data("productid");
    var sessionid_email = $(this).data("sessionid_email");
    var product_price = $(this).data("price");
    var quantity = $('#' + product_id).val();
    if (quantity != '' && quantity > 0) {
        $.ajax({
            url: base_url + "cart/add",
            method: "POST",
            data: {sessionid_email: sessionid_email, product_id: product_id, product_price: product_price, quantity: quantity},
            success: function (data) {
                $("#cartadd").notify("Item added in cart", {position: 'left', type: 'success'});
                $('.cart_count').load(base_url + 'cart/re_view_count');
                $('#' + product_id).val();
            }
        });
    } else {
        $('#notification1').addClass('shownotification');
        setTimeout(function () {
            $('#notification1').removeClass('shownotification');
        }, 3000, 'swing');
    }
});
$('.add_notifys').click(function () {
    var product_id = $(this).data("productid");
    var sessionid_email = $(this).data("sessionid_email");
    $.ajax({
        url: base_url + "orders/notifyadd",
        method: "POST",
        data: {sessionid_email: sessionid_email, product_id: product_id},
        success: function (data) {
            $("#cartadd").notify("Thanks for notify me", {position: 'left', type: 'success'});
            location.reload();
        }
    });
});
$('.addmorecontent').click(function () {
    var product_id = $(this).data("producid");
    var extraid = $(this).data("extraid");
    $.ajax({
        url: base_url + 'product/fetchextra',
        method: "POST",
        data: {extraid: extraid},
        dataType: "json",
        success: function (data) {
            $('#p_extra_con_title').val(data.p_extra_title);
            CKEDITOR.instances['p_extra_con_desc'].setData(data.p_extra_desc);
        }
    });
    $('#p_extra_con_btn').click(function () {
        var p_extra_con_desc1 = CKEDITOR.instances['p_extra_con_desc'].getData().replace(/<[^>]*>/gi, '').length;
        var p_extra_con_desc = CKEDITOR.instances['p_extra_con_desc'].getData();
        var p_extra_con_title = $('#p_extra_con_title').val();
        if ($.trim(p_extra_con_title) == '') {
            $('#p_extra_con_title').addClass(validationErro).focus();
            $("#p_extra_con_title").notify("Title Name Required");
            $('#p_extra_con_title').keyup(function () {
                $('#p_extra_con_title').removeClass(validationErro);
            });
        } else if (!p_extra_con_desc1) {
            $("#p_extra_con_desc").notify("Description Required");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('p_extra_con_title', p_extra_con_title);
            form_data.append('product_id', product_id);
            form_data.append('extraid', extraid);
            form_data.append('p_extra_con_desc', p_extra_con_desc);
            $.ajax({
                url: base_url + "product/extraproductadd",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
});
$('#e_extra_con_btn').click(function () {
    var eventid = $('#eventid').val();
    var eextraid = $('#eextraid').val();
    var ext = $('#event_con_image').val().split('.').pop().toLowerCase();
    var eventimagec1 = document.getElementById("event_con_image").files[0];
    var event_con_desc1 = CKEDITOR.instances['event_con_desc'].getData().replace(/<[^>]*>/gi, '').length;
    var event_con_desc = CKEDITOR.instances['event_con_desc'].getData();
    var event_con_title = $('#event_con_title').val();
    //var eeventid = $('.eaddmorecontent').data('eextraid');
    if ($.trim(event_con_title) == '') {
        $("#event_con_title").notify("Title Name Required");
    } else if (!event_con_desc1) {
        $("#event_con_desc").notify("Description Required");
        return false;
    } else if ($.trim(eventimagec1) == '') {
        $("#event_con_image").notify("Image Required");
        return false;
    }
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#event_con_image").notify("invalid extension!");
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('event_con_title', event_con_title);
        form_data.append('eventimage', eventimagec1);
        form_data.append('eventid', eventid);
        form_data.append('eextraid', eextraid);
        form_data.append('event_con_desc', event_con_desc);
        $.ajax({
            url: base_url + "events/extraeventadd",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data == "1") {
                    reset();
                    alertify.alert("record has been successfully");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                } else {
                    $('#rock').addClass('showpop');
                    $('#product_success').html(data);
                    $('form').trigger('reset');
                    setTimeout(function () {
                        $('#rock').removeClass('showpop');
                    }, 2000);
                }
            }
        });
    }
});
$('#e_extra_con_btnup').click(function () {
    var eventid = $('#eventid').val();
    var eextraid = $('#eextraid').val();
    var event_con_desc1 = CKEDITOR.instances['event_con_desc'].getData().replace(/<[^>]*>/gi, '').length;
    var event_con_desc = CKEDITOR.instances['event_con_desc'].getData();
    var event_con_title = $('#event_con_title').val();
    //var eeventid = $('.eaddmorecontent').data('eextraid');
    if ($.trim(event_con_title) == '') {
        $("#event_con_title").notify("Title Name Required");
    } else if (!event_con_desc1) {
        $("#event_con_desc").notify("Description Required");
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('event_con_title', event_con_title);
        form_data.append('eventid', eventid);
        form_data.append('eextraid', eextraid);
        form_data.append('event_con_desc', event_con_desc);
        $.ajax({
            url: base_url + "events/extraeventadd",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                location.href = base_url + data;
                /*if (data == "1") {
                 reset();
                 alertify.alert("record has been successfully");
                 setTimeout(function () {
                 location.reload();
                 }, 2000);
                 } else {
                 $('#rock').addClass('showpop');
                 $('#product_success').html(data);
                 $('form').trigger('reset');
                 setTimeout(function () {
                 $('#rock').removeClass('showpop');
                 }, 2000);
                 }*/

            }
        });
    }
});
$('#categoryupdated').click(function () {
    var editName = $('#editName').val();
    var cateDescription1 = CKEDITOR.instances['cateDescription'].getData().replace(/<[^>]*>/gi, '').length;
    var cateDescription = CKEDITOR.instances['cateDescription'].getData();
    var cateVediourl = $('#cateVediourl').val();
    if ($.trim(cateVediourl) == '') {
        $("#cateVediourl").notify("Video Url Required");
    } else if (!cateDescription1) {
        $("#cateDescription").notify("Description Required");
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('cateVediourl', cateVediourl);
        form_data.append('editName', editName);
        form_data.append('cateDescription', cateDescription);
        $.ajax({
            url: base_url + "category/adddetails",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                location.href = base_url + data;
            }
        });
    }
});
/*
 $(".quantity").on("keypress", function (evt) {
 var regex = /^\d+(?:\.\d{0,2})$/;
 var numStr = $(this);
 if (regex.test(numStr))
 alert("Number is valid");
 var keycode = evt.charCode || evt.keyCode;
 
 //    if (keycode == 46 || keycode == 45) {
 //        return false;
 //    }
 if (keycode == 48 || keycode == 57) {
 return true;
 }else{
 return false;
 }
 
 });
 */
$(".number").keypress(function (e) {
    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) {
        $(this).notify("Only Number");
        return false;
    }
});
$(".strnum").keypress(function (e) {
    if (String.fromCharCode(e.keyCode).match(/[^A-Za-z0-9 ]/g)) {
        $(this).notify("Only Alphabets, Number");
        return false;
    }
});
$(".str").keypress(function (e) {
    if (String.fromCharCode(e.keyCode).match(/[^A-Za-z ]/g)) {
        $(this).notify("Only alphabets");
        return false;
    }
});
$(".price").keypress(function (e) {
    if (String.fromCharCode(e.keyCode).match(/[^0-9.]/g)) {
        $(this).notify("Only price pattern");
        return false;
    }
});
$('.cart_count').load(base_url + 'cart/re_view_count');
$(".deleteRcordTemp").on('click', function () {
    var row_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "cart/remove",
                method: "POST",
                data: {row_id: row_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteTemptrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".extraeventdelete").on('click', function () {
    var del_id = $(this).data('delid');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "events/deleteExtractevent",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteTemptrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".extraproddelete").on('click', function () {
    var del_id = $(this).data('delid');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "product/deleteExtractproduct",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteTemptrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".categorydeleteextra").on('click', function () {
    var del_id = $(this).data('delid');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "category/deleteImageExtra",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteTemptrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".speakerdelete").on('click', function () {
    var del_id = $(this).data('delid');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "events/deleteSpeaker",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteTemptrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$("#addspeaker").on('click', function () {
    $('#formhinde').toggle();
});
$("#addcategory").on('click', function () {
    $('#formhinde').toggle();
});
$(document).on('click', '.btnupdatecart', function () {

    var qty = $(this).parents('tr').find('.txtqtycat').val();
    if ($.trim(qty) == "") {
        $('#update_qty').addClass(allerror);
        $('#update_qty').focus();
        $('#htmlerror').html('Minimun 1 one quantity required');
        return false;
    }
    var rowid = $(this).attr("id");
    $.ajax({
        url: base_url + "cart/update",
        method: "POST",
        data: {rowid: rowid, qty: qty},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).ready(function () {
    colSum();
});
function colSum() {
    var sum = 0;
    $('.subtotla').each(function () {
        sum += parseFloat($(this).val());
    });
    $('.totalAmount').html(sum);
}

$(document).ready(function () {
    var validationErro = 'animated shake errors';
    $('#menuForm').on('submit', function (e) {
        e.preventDefault();
        var menuName = $('#menuName').val();
        var menuParent = $('#menuParent').val();
        if ($.trim(menuName) == "") {
            $('#menuName').addClass(validationErro).focus();
            $("#menuName").notify("Menu Name Required");
            $('#menuName').keyup(function () {
                $('#menuName').removeClass(validationErro);
            });
        } else {
            $.ajax({
                url: base_url + "menu_top/addMenu",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "menu_top/viewtopMenu", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $(".deleteRcord").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "menu/deleteMenu",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteMenutr' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $('#modelForm').on('submit', function (e) {
        e.preventDefault();
        var modelName = $('#modelName').val();
        if ($.trim(modelName) == "") {
            $('#modelName').addClass(validationErro).focus();
            $("#modelName").notify("Model Name Required");
            $('#modelName').keyup(function () {
                $('#modelName').removeClass(validationErro);
            });
        } else {
            $.ajax({
                url: base_url + "models/addModel",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "models/index", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $(document).on('change', '#dor_receiver_id', function () {
        var event_product_id = $(this).val();
        $('#dor_id').val(event_product_id);
    });
    $(document).on('change', '#doc_receiver_id', function () {
        var event_product_id = $(this).val();
        $('#doc_id').val(event_product_id);
    });
    $(".deleteParticulartruuuu").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "particular/deleteParticular",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteParticularsstrU' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $('#colorForm').on('submit', function (e) {
        e.preventDefault();
        var colorName = $('#colorName').val();
        if ($.trim(colorName) == "") {
            $('#colorName').addClass(validationErro).focus();
            $("#colorName").notify("Color Name Required");
            $('#colorName').keyup(function () {
                $('#colorName').removeClass(validationErro);
            });
        } else {
            $.ajax({
                url: base_url + "colors/addColor",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "colors/index", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $(".deleteColortruuuu").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "colors/deleteColor",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteColorsstrU' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deletenewsLettertr").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "users/deleteNews",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deletenewsLettertrL' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $('.checks input').change(function () {
        $('.checks input').not(this).prop('checked', false)
    });
    $('#btnSpeaker').click(function () {
        var ext = $('#speaker_image').val().split('.').pop().toLowerCase();
        var speaker_image = document.getElementById("speaker_image").files[0];
        var speaker_name = $('#speaker_name').val();
        var speaker_expri = $('#speaker_expri').val();
        if ($.trim(speaker_image) == '') {
            $('#speaker_image').addClass(validationErro).focus();
            $("#speaker_image").notify("Image Required");
            $('#speaker_image').keyup(function () {
                $('#speaker_image').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#speaker_image").notify("invalid extension!");
            return false;
        } else if ($.trim(speaker_name) == '') {
            $('#speaker_name').addClass(validationErro).focus();
            $("#speaker_name").notify("Name Required");
            $('#speaker_name').keyup(function () {
                $('#speaker_name').removeClass(validationErro);
            });
            return false;
        } else if ($.trim(speaker_expri) == '') {
            $('#speaker_expri').addClass(validationErro).focus();
            $("#speaker_expri").notify("Experience Required");
            $('#speaker_expri').keyup(function () {
                $('#speaker_expri').removeClass(validationErro);
            });
            return false;
        } else {
            return true;
        }
    });
    $('#uploadimagechange').click(function () {
        var ext = $('#cateimagechange').val().split('.').pop().toLowerCase();
        var cateimagec1 = document.getElementById("cateimagechange").files[0];
        var cateEdit = $('#editName').val();
        if ($.trim(cateimagec1) == '') {
            $('#cateimagechange').addClass(validationErro).focus();
            $("#cateimagechange").notify("Image Required");
            $('#cateimagechange').keyup(function () {
                $('#cateimagechange').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#cateimagechange").notify("invalid extension!");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('cateimage', cateimagec1);
            form_data.append('cateEdit', cateEdit);
            $.ajax({
                url: base_url + "category/addCategoryc",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('#uploadimagechange').append('<div class="loader"></div>');
                },
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        location.reload();
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $('#uploadproductImage').click(function () {
        var ext = $('#productImage').val().split('.').pop().toLowerCase();
        var prodimagec1 = document.getElementById("productImage").files[0];
        var productid = $('#productid').val();
        if ($.trim(prodimagec1) == '') {
            $('#productImage').addClass(validationErro).focus();
            $("#productImage").notify("Image Required");
            $('#productImage').keyup(function () {
                $('#productImage').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#productImage").notify("invalid extension!");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('productImage', prodimagec1);
            form_data.append('prodEdit', productid);
            $.ajax({
                url: base_url + "product/addProductyc",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        location.reload();
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $('#uploadeventImage').click(function () {
        var ext = $('#eventImage').val().split('.').pop().toLowerCase();
        var eventImage1 = document.getElementById("eventImage").files[0];
        var eventid = $('#eventid').val();
        if ($.trim(eventImage1) == '') {
            $('#eventImage').addClass(validationErro).focus();
            $("#eventImage").notify("Image Required");
            $('#eventImage').keyup(function () {
                $('#eventImage').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#eventImage").notify("invalid extension!");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('eventImage', eventImage1);
            form_data.append('eventid', eventid);
            $.ajax({
                url: base_url + "events/addProductyc",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        location.reload();
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    /*---Category*/
    $('#btnCategory').click(function () {
        var cateName = $('#categoryName').val();
        var cateParent = $('.checks input:checked').val() ? $('.checks input:checked').val() : '0';
        var selectMenu = $('#selectMenu').val();
        var cateTitle = $('#cateTitle').val();
        var cateEdit = $('#editName').val();
        if ($.trim(cateName) == '') {
            $('#categoryName').addClass(validationErro).focus();
            $("#categoryName").notify("Category Name Required");
            $('#categoryName').keyup(function () {
                $('#categoryName').removeClass(validationErro);
            });
        } else if ($.trim(cateTitle) == '') {
            $('#cateTitle').addClass(validationErro).focus();
            $("#cateTitle").notify("Category Title Name Required");
            $('#cateTitle').keyup(function () {
                $('#cateTitle').removeClass(validationErro);
            });
        } else {
            var conte_id = {
                cateName: cateName,
                cateParent: cateParent,
                cateEdit: cateEdit,
                cateTitle: cateTitle,
                selectMenu: selectMenu
            };
            $.ajax({
                url: base_url + "category/addCategory",
                method: "POST",
                data: conte_id,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        //alertify.alert("record has been successfully");
                        $('#rock').addClass('showpop');
                        $('#product_success').html('Record has been successfully');
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $('#btnCategoryupdate').click(function () {
        var cateDescription1 = CKEDITOR.instances['cateDescription'].getData().replace(/<[^>]*>/gi, '').length;
        var cateDescription = CKEDITOR.instances['cateDescription'].getData();
        var cateName = $('#categoryName').val();
        var imagename = $('#imagename').val();
        var cateTitle = $('#cateTitle').val();
        var cateEdit = $('#editName').val();
        if ($.trim(cateName) == '') {
            $('#categoryName').addClass(validationErro).focus();
            $("#categoryName").notify("Category Name Required");
            $('#categoryName').keyup(function () {
                $('#categoryName').removeClass(validationErro);
            });
        } else if ($.trim(cateTitle) == '') {
            $('#cateTitle').addClass(validationErro).focus();
            $("#cateTitle").notify("Category Title Name Required");
            $('#cateTitle').keyup(function () {
                $('#cateTitle').removeClass(validationErro);
            });
        } else if (!cateDescription1) {
            $("#cateDescription").notify("Category Description Required");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('cateName', cateName);
            form_data.append('cateEdit', cateEdit);
            form_data.append('cateTitle', cateTitle);
            form_data.append('imagename', imagename);
            form_data.append('cateDescription', cateDescription);
            $.ajax({
                url: base_url + "category/addCategory",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "category/viewtopCategory", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    /*
     Services add
     */
    $('#btnServices').click(function () {
        var ext = $('#serimage').val().split('.').pop().toLowerCase();
        var serimage1 = document.getElementById("serimage").files[0];
        var serDescription1 = CKEDITOR.instances['serDescription'].getData().replace(/<[^>]*>/gi, '').length;
        var serimage = $('#serimage').val();
        var serDescription = CKEDITOR.instances['serDescription'].getData();
        var servicesName = $('#servicesName').val();
        var menu_id = $('#menu_id').val();
        var serTitle = $('#serTitle').val();
        var imagenameservic = $('#imagenameservic').val();
        var serEdit = $('#editName').val();
        if ($.trim(servicesName) == '') {
            $('#servicesName').addClass(validationErro).focus();
            $("#servicesName").notify("Service Name Required");
        } else if ($.trim(serTitle) == '') {
            $('#serTitle').addClass(validationErro).focus();
            $("#serTitle").notify("Category Title Name Required");
        } else if ($.trim(menu_id) == '') {
            $('#menu_id').addClass(validationErro).focus();
            $("#menu_id").notify("Select Menu");
        } else if ($.trim(serimage) == '') {
            $('#serimage').addClass(validationErro).focus();
            $("#serimage").notify("Image Required");
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#serimage").notify("invalid extension!");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('serimage', serimage1);
            form_data.append('servicesName', servicesName);
            form_data.append('serEdit', serEdit);
            form_data.append('serTitle', serTitle);
            form_data.append('menu_id', menu_id);
            form_data.append('imagenameservic', imagenameservic);
            form_data.append('serDescription', serDescription);
            $.ajax({
                url: base_url + "services/topServices",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "services/index", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $('#btnServicesupdate').click(function () {
        var servicesName = $('#servicesName').val();
        var serDescription = CKEDITOR.instances['serDescription'].getData();
        var selectServices = $('#selectServices').val();
        var serTitle = $('#serTitle').val();
        var menu_id = $('#menu_id').val();
        var imagenameservic = $('#imagenameservic').val();
        var serEdit = $('#editName').val();
        if ($.trim(servicesName) == '') {
            $('#servicesName').addClass(validationErro).focus();
            $("#servicesName").notify("Service Name Required");
        } else if ($.trim(menu_id) == '') {
            $('#menu_id').addClass(validationErro).focus();
            $("#menu_id").notify("Select Menu");
        } else if ($.trim(serTitle) == '') {
            $('#serTitle').addClass(validationErro).focus();
            $("#serTitle").notify("Service Title Required");
        } else {
            var form_data = new FormData();
            form_data.append('serimage', imagenameservic);
            form_data.append('servicesName', servicesName);
            form_data.append('selectServices', selectServices);
            form_data.append('serEdit', serEdit);
            form_data.append('serTitle', serTitle);
            form_data.append('menu_id', menu_id);
            form_data.append('imagenameservic', imagenameservic);
            form_data.append('serDescription', serDescription);
            $.ajax({
                url: base_url + "services/topServices",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "services/index", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $('#seruploadimagechange').click(function () {
        var ext = $('#serimagechange').val().split('.').pop().toLowerCase();
        var cateimagec1 = document.getElementById("serimagechange").files[0];
        var cateEdit = $('#editName').val();
        if ($.trim(cateimagec1) == '') {
            $('#serimagechange').addClass(validationErro).focus();
            $("#serimagechange").notify("Image Required");
            $('#serimagechange').keyup(function () {
                $('#serimagechange').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#serimagechange").notify("invalid extension!");
            return false;
        } else {
            var form_data = new FormData();
            form_data.append('serimage', cateimagec1);
            form_data.append('serEdit', cateEdit);
            $.ajax({
                url: base_url + "services/addServicesc",
                method: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $('#uploadimagechange').append('<div class="loader"></div>');
                },
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        location.reload();
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $(".deleteRcordC").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "category/deleteCategory",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteCategorytrC' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deleteRcordS").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "services/deleteServices",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteservicestrS' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $('#bannerForm').on('submit', function (e) {
        e.preventDefault();
        var ext = $('#image_file').val().split('.').pop().toLowerCase();
        var bTitle = $('#bTitle').val();
        var image_file = $('#image_file').val();
        var bType = $('#bType').val();
        var bMenu = $('#bMenu').val();
        var bCategory = $('#bCategory').val();
        var bOrder = $('#bOrder').val();
        var bannerDescription = $('.ckeditor').val();
        if ($.trim(bTitle) == '') {
            $('#bTitle').addClass(validationErro).focus();
            $("#bTitle").notify("Banner Title Name Required");
            $('#bTitle').keyup(function () {
                $('#bTitle').removeClass(validationErro);
            });
        } else if (image_file == '') {
            $('#image_file').addClass(validationErro).focus();
            $("#image_file").notify("Image field Required");
            $('#image_file').keyup(function () {
                $('#image_file').removeClass(validationErro);
            });
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#image_file").notify("invalid extension!");
            return false;
        } else if (bType == '') {
            $('#bType').addClass(validationErro).focus();
            $("#bType").notify("Select Type");
            $('#bType').keyup(function () {
                $('#bType').removeClass(validationErro);
            });
        } else {
            $.ajax({
                url: base_url + "banner/ajax_upload",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    if (data == "1") {
                        reset();
                        alertify.alert("record has been successfully");
                        setTimeout(function () {
                            window.open(base_url + "banner/bannerlist", '_self');
                        }, 2000);
                    } else {
                        $('#rock').addClass('showpop');
                        $('#product_success').html(data);
                        $('form').trigger('reset');
                        setTimeout(function () {
                            $('#rock').removeClass('showpop');
                        }, 2000);
                    }
                }
            });
        }
    });
    $(".deleteRcordB").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "banner/deleteBanner",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteBannertrB' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $('#btnSubmitUsers').click(function () {
        var allerror = 'animated shake allerror';
        var userName = $('#userName').val();
        var lastName = $('#lastName').val();
        var userEmail = $('#userEmail').val();
        var role_id = $('#role_id').val();
        var userMobile = $('#userMobile').val();
        var userPwd = $('#userPwd').val();
        var userAddress = $('#userAddress').val();
        var userState = $('#userState').val();
        var userCity = $('#userCity').val();
        if ($.trim(userName) == "") {
            $('#userName').focus();
            $("#userName").notify("User Name Required");
            return false;
        } else if ($.trim(lastName) == "") {
            $('#lastName').focus();
            $("#lastName").notify('User Last Name is required');
            return false;
        } else if ($.trim(userEmail) == "") {
            $('#userEmail').notify('User Email is required');
            return false;
        }
        var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!filter.test(userEmail)) {
            $('#userEmail').notify('User Email is invalid');
            return false;
        } else if ($.trim(userPwd) == "") {
            $('#userPwd').notify('User Password is required');
            return false;
        }
        if (userPwd.length <= 7) {
            $('#userPwd').notify('User password minimum 8 characters');
            return false;
        } else if ($.trim(userMobile) == "") {
            $('#userMobile').notify('User Mobile is required');
            return false;
        }
        var intRegex = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (!intRegex.test(userMobile)) {
            $('#userMobile').notify('User mobile invalid');
            return false;
        }
        if (userMobile.length <= 9) {
            $('#userMobile').notify('User mobile minimum 10 characters');
            return false;
        } else if ($.trim(userAddress) == "") {
            $('#userAddress').focus();
            $('#userAddress').notify('User Address is required');
            return false;
        } else if ($.trim(userState) == "") {
            $('#userState').focus();
            $('#userState').notify('User State is required');
            return false;
        } else if ($.trim(userCity) == "") {
            $('#userCity').focus();
            $('#userCity').notify('User City is required');
            return false;
        } else {
            return true;
        }
    });
    //role    
    $(document).on('change', '#role_id', function () {
        $('#disirole').html('<img src="' + base_url + 'assests/img/loader.gif"" />');
        var get_id = $(this).find(':selected').val();
        var dela = {
            del_id: get_id
        };
        $.ajax({
            url: base_url + "role/rolea",
            method: "POST",
            data: dela,
            success: function (data) {
                //alert(data);
                $('#disirole').html(data);
            }
        });
    });
    $(document).on('change', '#role_parent', function () {
        var usercity = $('#role_parent').val();
        $('#hidRole').val(usercity);
    });
    //state
    /*
     $(document).on('change', '#userState', function () {
     $('#disi').html('<img src="' + base_url + 'assests/img/loader.gif"" />');
     var get_id = $(this).find(':selected').val();
     var dela = {
     del_id: get_id
     };
     $.ajax({
     url: base_url + "location/statea",
     method: "POST",
     data: dela,
     success: function (data) {
     //alert(data);
     $('#disi').html(data);
     }
     });
     });
     */
    $(document).on('click', '.parentchecked', function () {
        var get_id = $(this).val();
        $('#categoryParent').val(get_id);
    });
    $(document).on('change', '#userCity', function () {
        var usercity = $('#userCity').val();
        $('#hidCity').val(usercity);
    });
    //Product page
    $(document).on('click', '#btnSubmit', function () {
//e.preventDefault();
        var productName = $('#product_name').val();
        var messageLength = CKEDITOR.instances['product_description'].getData().replace(/<[^>]*>/gi, '').length;
        var ext = $('#product_images').val().split('.').pop().toLowerCase();
        var p_category = $('#categoryParent').val();
        var prdouct_price = $('#prdouct_price').val();
        var prdouct_tax_price = $('#prdouct_tax_price').val();
        var prdouct_ship_price = $('#prdouct_ship_price').val();
        var prdouct_qty = $('#prdouct_qty').val();
        var prdouct_Status = $('#prdouct_Status').val();
        var product_images = $('#product_images').val();
        if (productName == '') {
            $('#product_name').addClass(validationErro).focus();
            alertify.error("product name required!");
            $('#product_name').keyup(function () {
                $('#product_name').removeClass(validationErro);
            });
            return false;
        } else if (!messageLength) {
            alertify.error("product description required!");
            return false;
        } else if (p_category == '') {
            $('#p_category').addClass(validationErro).focus();
            alertify.error("product select category required!");
            $('#p_category').keyup(function () {
                $('#p_category').removeClass(validationErro);
            });
            return false;
        } else if (prdouct_price == '') {
            $('#prdouct_price').addClass(validationErro).focus();
            alertify.error("product Price required!");
            $('#prdouct_price').keyup(function () {
                $('#prdouct_price').removeClass(validationErro);
            });
            return false;
        } else if (prdouct_ship_price == '') {
            $('#prdouct_ship_price').addClass(validationErro).focus();
            alertify.error("product Ship Price required!");
            $('#prdouct_ship_price').keyup(function () {
                $('#prdouct_ship_price').removeClass(validationErro);
            });
            return false;
        } else if (prdouct_tax_price == '') {
            $('#prdouct_tax_price').addClass(validationErro).focus();
            alertify.error("product Tax Price required!");
            $('#prdouct_tax_price').keyup(function () {
                $('#prdouct_tax_price').removeClass(validationErro);
            });
            return false;
        } else if (prdouct_qty == '') {
            $('#prdouct_qty').addClass(validationErro).focus();
            alertify.error("product qty required!");
            $('#prdouct_qty').keyup(function () {
                $('#prdouct_qty').removeClass(validationErro);
            });
            return false;
        } else if (prdouct_Status == '') {
            $('#prdouct_Status').addClass(validationErro).focus();
            alertify.error("product select status required!");
            $('#prdouct_Status').keyup(function () {
                $('#prdouct_Status').removeClass(validationErro);
            });
            return false;
        } else if ($.trim(product_images) == '') {
            $('#product_images').addClass(validationErro).focus();
            alertify.error("product image required!");
            $('#product_images').keyup(function () {
                $('#product_images').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#product_images").notify("invalid extension!");
            return false;
        } else {
            return true;
        }
    });
    $(document).on('click', '#btnEvent', function () {
        var artistsessionid = $('#artistsessionid').val();
        var artclassevent = $('#artclassevent').val();
        var categoryartclass_id = $('#categoryartclass_id').val();
        var event_title_a = $('#event_title_a').val();
        var event_price_a = $('#event_price_a').val();
        var event_ticket_a = $('#event_ticket_a').val();
        var event_slug_a = $('#event_slug_a').val();
        var event_desc_a = CKEDITOR.instances['event_desc_a'].getData().replace(/<[^>]*>/gi, '').length;
        //var event_desc_a = $('#event_desc_a').val();
        var editartistevent = $('#editartistevent').val();
        var event_start_date_a = $('#event_start_date_a').val();
        var event_end_date_a = $('#event_end_date_a').val();
        var event_time = $('#event_time').val();
        var event_images_a = $('#event_images_a').val();
        var event_venue_id = $('#event_venue_id').val();
        var ext = $('#event_images_a').val().split('.').pop().toLowerCase();
        var event_images_a1 = document.getElementById("event_images_a").files[0];
        if ($.trim(artclassevent) == "") {
            $("#artclassevent").notify("Select Art Class Parent").focus();
            $('#artclassevent').addClass(validationErro);
            setTimeout(function () {
                $('#artclassevent').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(categoryartclass_id) == "") {
            $("#categoryartclass_id").notify("Select Main Category").focus();
            $('#categoryartclass_id').addClass(validationErro);
            setTimeout(function () {
                $('#categoryartclass_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_venue_id) == "") {
            $("#event_venue_id").notify("Select venue / address").focus();
            $('#event_venue_id').addClass(validationErro);
            setTimeout(function () {
                $('#event_venue_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(artistsessionid) == "") {
            $("#artistsessionid").notify("Select Event / address").focus();
            $('#artistsessionid').addClass(validationErro);
            setTimeout(function () {
                $('#artistsessionid').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_title_a) == "") {
            $("#event_title_a").notify("Event Title").focus();
            $('#event_title_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_title_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_slug_a) == "") {
            $("#event_slug_a").notify("Event Url required").focus();
            $('#event_slug_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_slug_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_price_a) == "") {
            $("#event_price_a").notify("Event Price required").focus();
            $('#event_price_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_price_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_ticket_a) == "") {
            $("#event_ticket_a").notify("Event Tickets required").focus();
            $('#event_ticket_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_ticket_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if (!event_desc_a) {
            alertify.error("Event description required!");
            return false;
        } else if ($.trim(event_start_date_a) == "") {
            $("#event_start_date_a").notify("Select start date required").focus();
            $('#event_start_date_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_start_date_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_end_date_a) == "") {
            $("#event_end_date_a").notify("Select end date required").focus();
            $('#event_end_date_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_end_date_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_time) == "") {
            $("#event_time").notify("Select Time required").focus();
            $('#event_time').addClass(validationErro);
            setTimeout(function () {
                $('#event_time').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_images_a) == '') {
            $('#event_images_a').addClass(validationErro).focus();
            alertify.error("Image image required!");
            $('#event_images_a').keyup(function () {
                $('#event_images_a').removeClass(validationErro);
            });
            return false;
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#event_images_a").notify("invalid extension!");
            return false;
        } else {
            return true;
        }
    });
    $(document).on('click', '#ubtnEvent', function () {
        var artclassevent = $('#artclassevent').val();
        var categoryartclass_id = $('#categoryartclass_id').val();
        var event_title_a = $('#event_title_a').val();
        var event_slug_a = $('#event_slug_a').val();
        var event_price_a = $('#event_price_a').val();
        var event_ticket_a = $('#event_ticket_a').val();
        //var event_desc_a = CKEDITOR.instances['event_desc_a'].getData().replace(/<[^>]*>/gi, '').length;
        //var editartistevent = $('#editartistevent').val();
        var event_start_date_a = $('#event_start_date_a').val();
        var event_time = $('#event_time').val();
        var event_venue_id = $('#event_venue_id').val();
        var event_end_date_a = $('#event_end_date_a').val();
        if ($.trim(artclassevent) == "") {
            $("#artclassevent").notify("Select Art Class Parent").focus();
            $('#artclassevent').addClass(validationErro);
            setTimeout(function () {
                $('#artclassevent').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(categoryartclass_id) == "") {
            $("#categoryartclass_id").notify("Select Main Category").focus();
            $('#categoryartclass_id').addClass(validationErro);
            setTimeout(function () {
                $('#categoryartclass_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_venue_id) == "") {
            $("#event_venue_id").notify("Select venue / address").focus();
            $('#event_venue_id').addClass(validationErro);
            setTimeout(function () {
                $('#event_venue_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_title_a) == "") {
            $("#event_title_a").notify("Event Title").focus();
            $('#event_title_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_title_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_slug_a) == "") {
            $("#event_slug_a").notify("Event Url required").focus();
            $('#event_slug_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_slug_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_price_a) == "") {
            $("#event_price_a").notify("Event Price required").focus();
            $('#event_price_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_price_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_ticket_a) == "") {
            $("#event_ticket_a").notify("Event Ticket required").focus();
            $('#event_ticket_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_ticket_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_start_date_a) == "") {
            $("#event_start_date_a").notify("Select start date required").focus();
            $('#event_start_date_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_start_date_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_end_date_a) == "") {
            $("#event_end_date_a").notify("Select end date required").focus();
            $('#event_end_date_a').addClass(validationErro);
            setTimeout(function () {
                $('#event_end_date_a').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(event_time) == "") {
            $("#event_time").notify("Select Time required").focus();
            $('#event_time').addClass(validationErro);
            setTimeout(function () {
                $('#event_time').removeClass(validationErro);
            }, 2000);
            return false;
        } else {
            return true;
        }
    });
    $('#btnInvoice').click(function () {
        var invoice_number = $('#invoice_number').val();
        var invoice_vehicle_no = $('#invoice_vehicle_no').val();
        var dor_id = $('#dor_id').val();
        var doc_id = $('#doc_id').val();
        var invoice_cgst = $('#invoice_cgst').val();
        var invoice_sgst = $('#invoice_sgst').val();
        var invoice_igst = $('#invoice_igst').val();
        var invoice_dos = $('#invoice_dos').val();
        var invoice_pos = $('#invoice_pos').val();
        if ($.trim(invoice_number) == "") {
            $("#invoice_number").notify("Enter Invoice number required!").focus();
            $('#invoice_number').addClass(validationErro);
            setTimeout(function () {
                $('#invoice_number').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(invoice_vehicle_no) == '') {
            $("#invoice_vehicle_no").notify("Enter Vehicle number required!").focus();
            $('#invoice_vehicle_no').addClass(validationErro);
            setTimeout(function () {
                $('#invoice_vehicle_no').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(dor_id) == "") {
            $("#dor_receiver_id").notify("Detail of Receiver required!").focus();
            $('#dor_receiver_id').addClass(validationErro);
            setTimeout(function () {
                $('#dor_receiver_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(doc_id) == "") {
            $("#doc_receiver_id").notify("Details of Consignee").focus();
            $('#doc_receiver_id').addClass(validationErro);
            setTimeout(function () {
                $('#doc_receiver_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(invoice_dos) == '') {
            $("#invoice_dos").notify("Date of supply required!").focus();
            $('#invoice_dos').addClass(validationErro);
            setTimeout(function () {
                $('#invoice_dos').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(invoice_pos) == '') {
            $("#invoice_pos").notify("Place of supply rquired!").focus();
            $('#invoice_pos').addClass(validationErro);
            setTimeout(function () {
                $('#invoice_pos').removeClass(validationErro);
            }, 2000);
            return false;
        } else {
            return true;
        }

    });
    $('#btnTrans').click(function () {
        var challan_no = $('#challan_no').val();
        var trans_from = $('#trans_from').val();
        var trans_to = $('#trans_to').val();
        var doc_id = $('#doc_id').val();
        var dor_id = $('#dor_id').val();
        var truck_no = $('#truck_no').val();
        var owner_name = $('#owner_name').val();
        var owner_address = $('#owner_address').val();
        var driver_name = $('#driver_name').val();
        var driver_licence = $('#driver_licence').val();
        var driver_address = $('#driver_address').val();
        var trans_dos = $('#trans_dos').val();
        var trans_pos = $('#trans_pos').val();
        var t_amount = $('#t_amount').val();
        var t_advance = $('#t_advance').val();
        var invoice_dos = $('#invoice_dos').val();
        var invoice_pos = $('#invoice_pos').val();
        if ($.trim(challan_no) == "") {
            $("#challan_no").notify("Enter Challan required!").focus();
            $('#challan_no').addClass(validationErro);
            setTimeout(function () {
                $('#challan_no').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(trans_from) == '') {
            $("#trans_from").notify("Enter From required!").focus();
            $('#trans_from').addClass(validationErro);
            setTimeout(function () {
                $('#trans_from').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(trans_to) == '') {
            $("#trans_to").notify("Enter To required!").focus();
            $('#trans_to').addClass(validationErro);
            setTimeout(function () {
                $('#trans_to').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(doc_id) == "") {
            $("#doc_receiver_id").notify("Details of Consignee").focus();
            $('#doc_receiver_id').addClass(validationErro);
            setTimeout(function () {
                $('#doc_receiver_id').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(truck_no) == "") {
            $("#truck_no").notify("Enter to truck number required!").focus();
            $('#truck_no').addClass(validationErro);
            setTimeout(function () {
                $('#truck_no').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(owner_name) == "") {
            $("#owner_name").notify("Enter to truck Owner Name required!").focus();
            $('#owner_name').addClass(validationErro);
            setTimeout(function () {
                $('#owner_name').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(owner_address) == "") {
            $("#owner_address").notify("Enter to truck address Name required!").focus();
            $('#owner_address').addClass(validationErro);
            setTimeout(function () {
                $('#owner_address').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(driver_name) == "") {
            $("#driver_name").notify("Enter to Driver Name required!").focus();
            $('#driver_name').addClass(validationErro);
            setTimeout(function () {
                $('#driver_name').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(driver_licence) == "") {
            $("#driver_licence").notify("Enter to Driver Licence required!").focus();
            $('#driver_licence').addClass(validationErro);
            setTimeout(function () {
                $('#driver_licence').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(driver_address) == "") {
            $("#driver_address").notify("Enter to Driver Address required!").focus();
            $('#driver_address').addClass(validationErro);
            setTimeout(function () {
                $('#driver_address').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(trans_dos) == '') {
            $("#trans_dos").notify("Date of supply required!").focus();
            $('#trans_dos').addClass(validationErro);
            setTimeout(function () {
                $('#trans_dos').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(trans_pos) == '') {
            $("#trans_pos").notify("Place of supply rquired!").focus();
            $('#trans_pos').addClass(validationErro);
            setTimeout(function () {
                $('#trans_pos').removeClass(validationErro);
            }, 2000);
            return false;
        } else {
            return true;
        }

    });
    $('#ubtnVenue').click(function () {
        var venue_name = $('#venue_name').val();
        var venue_alcohol = $('#venue_alcohol').val();
        var venue_flooring = $('#venue_flooring').val();
        var venue_host_event = $('#venue_host_event').val();
        var venue_hosting_frequency = $('#venue_hosting_frequency').val();
        var venue_eating_capacity = $('#venue_eating_capacity').val();
        var venue_photo = $('#venue_photo').val();
        var workshoplatitude = $('#workshoplatitude').val();
        var workshoplongitude = $('#workshoplongitude').val();
        var workshopplaces = $('#workshopplaces').val();
        var venue_website = $('#venue_website').val();
        var venue_additional = $('#venue_additional').val();
        var wfirstName = $('#wfirstName').val();
        var wlastName = $('#wlastName').val();
        var wtitle = $('#wtitle').val();
        var waddress1 = $('#waddress1').val();
        var waddress2 = $('#waddress2').val();
        var wcity = $('#wcity').val();
        var wstate = $('#wstate').val();
        var wzipcode = $('#wzipcode').val();
        var wcountry = $('#wcountry').val();
        var wemail = $('#wemail').val();
        var wphone = $('#wphone').val();
        var ext = $('#venue_photo').val().split('.').pop().toLowerCase();
        var venue_photo1 = document.getElementById("venue_photo").files[0];
        if ($.trim(venue_name) == "") {
            $("#venue_name").notify("venue Name").focus();
            $('#venue_name').addClass(validationErro);
            setTimeout(function () {
                $('#venue_name').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wtitle) == '') {
            $("#wtitle").notify("Title required").focus();
            $('#wtitle').addClass(validationErro);
            setTimeout(function () {
                $('#wtitle').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(venue_flooring) == "") {
            $("#venue_flooring").notify("venue flooring required").focus();
            $('#venue_flooring').addClass(validationErro);
            setTimeout(function () {
                $('#venue_flooring').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(venue_hosting_frequency) == "") {
            alert('host frequency');
            $("#venue_hosting_frequency").notify("venue hosting frequency required").focus();
            $('#venue_hosting_frequency').addClass(validationErro);
            setTimeout(function () {
                $('#venue_hosting_frequency').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(venue_eating_capacity) == "") {
            $("#venue_eating_capacity").notify("venue seating capacity required").focus();
            $('#venue_eating_capacity').addClass(validationErro);
            setTimeout(function () {
                $('#venue_eating_capacity').removeClass(validationErro);
            }, 2000);
            return false;
        }
        if ($.trim(workshopplaces) == '') {
            $("#workshopplaces").notify("venue location required").focus();
            $('#workshopplaces').addClass(validationErro);
            setTimeout(function () {
                $('#workshopplaces').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(workshoplatitude) == '') {
            $("#workshopplaces").notify("venue location required").focus();
            $('#workshopplaces').addClass(validationErro);
            setTimeout(function () {
                $('#workshopplaces').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(workshoplongitude) == '') {
            $("#workshopplaces").notify("venue location required").focus();
            $('#workshopplaces').addClass(validationErro);
            setTimeout(function () {
                $('#workshopplaces').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(venue_website) == '') {
            $("#venue_website").notify("venue website required").focus();
            $('#venue_website').addClass(validationErro);
            setTimeout(function () {
                $('#venue_website').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(venue_additional) == '') {
            $("#venue_additional").notify("venue description required").focus();
            $('#venue_additional').addClass(validationErro);
            setTimeout(function () {
                $('#venue_additional').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wfirstName) == '') {
            $("#wfirstName").notify("First name required").focus();
            $('#wfirstName').addClass(validationErro);
            setTimeout(function () {
                $('#wfirstName').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wlastName) == '') {
            $("#wlastName").notify("Last name required").focus();
            $('#wlastName').addClass(validationErro);
            setTimeout(function () {
                $('#wlastName').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(waddress1) == '') {
            $("#waddress1").notify("First Address required").focus();
            $('#waddress1').addClass(validationErro);
            setTimeout(function () {
                $('#waddress1').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wcity) == '') {
            $("#wcity").notify("City required").focus();
            $('#wcity').addClass(validationErro);
            setTimeout(function () {
                $('#wcity').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wstate) == '') {
            $("#wstate").notify("State required").focus();
            $('#wstate').addClass(validationErro);
            setTimeout(function () {
                $('#wstate').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wcountry) == '') {
            $("#wcountry").notify("Country required").focus();
            $('#wcountry').addClass(validationErro);
            setTimeout(function () {
                $('#wcountry').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wzipcode) == '') {
            $("#wzipcode").notify("Zipcode required").focus();
            $('#wzipcode').addClass(validationErro);
            setTimeout(function () {
                $('#wzipcode').removeClass(validationErro);
            }, 2000);
            return false;
        } else if ($.trim(wemail) == "") {
            $("#wemail").notify("Email Required");
            return false;
        }
        var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!filter.test(wemail)) {
            $("#wemail").notify("Email Invalid");
            return false;
        } else if ($.trim(wphone) == "") {
            $("#wphone").notify("Phone Required");
            return false;
        } else {
            return true;
        }
    });
    $(document).on('click', '#btnSubmitpage', function () {
//e.preventDefault();
        var page_title = $('#page_title').val();
        var page_description = $('#page_description').val();
        var messageLength = CKEDITOR.instances['page_description'].getData().replace(/<[^>]*>/gi, '').length;
        if (page_title == '') {
            $('#page_title').addClass(validationErro).focus();
            alertify.error("page title required!");
            $('#page_title').keyup(function () {
                $('#page_title').removeClass(validationErro);
            });
            return false;
        } else if (!messageLength) {
            alertify.error("page description required!");
            return false;
        } else {
            return true;
        }
    });
    $(document).on('click', '#btnSubmitcareer', function () {
        var career_type = $('#career_type').val();
        var career_title = $('#career_title').val();
        var career_description = $('#career_description').val();
        var messageLength = CKEDITOR.instances['career_description'].getData().replace(/<[^>]*>/gi, '').length;
        if (career_type == '') {
            $('#career_type').addClass(validationErro).focus();
            alertify.error("Career Type required!");
            $('#career_type').keyup(function () {
                $('#career_type').removeClass(validationErro);
            });
            return false;
        } else if (career_title == '') {
            $('#career_title').addClass(validationErro).focus();
            alertify.error("Career title required!");
            $('#career_title').keyup(function () {
                $('#career_title').removeClass(validationErro);
            });
            return false;
        } else if (!messageLength) {
            alertify.error("Description required!");
            return false;
        } else {
            return true;
        }
    });
    $(document).on('click', '#extraimagesupload', function () {
//e.preventDefault();
        var fileimage = $('#fileimage').val();
        var ext = $('#fileimage').val().split('.').pop().toLowerCase();
        if (fileimage == '') {
            $('#fileimage').addClass(validationErro).focus();
            $("#fileimage").notify("Image field Required");
            $('#fileimage').keyup(function () {
                $('#fileimage').removeClass(validationErro);
            });
        }
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            $("#fileimage").notify("invalid extension!");
            return false;
        } else {
            return true;
        }
    });
    $(document).on('click', '#btncoupon', function () {
//e.preventDefault();
        var couponname = $('#couponname').val();
        var couponprice = $('#couponprice').val();
        if (couponname == '') {
            $('#couponname').addClass(validationErro).focus();
            alertify.error("location name required!");
            return false;
            $('#couponname').keyup(function () {
                $('#couponname').removeClass(validationErro);
            });
        } else if (couponprice == '') {
            $('#couponprice').addClass(validationErro).focus();
            alertify.error("Coupon price required!");
            $('#couponprice').keyup(function () {
                $('#couponprice').removeClass(validationErro);
            });
            return false;
        } else {
            return true;
        }
    });
    $(".deleteRcordconp").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "coupon/delete_coupon",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deletecoupontrCp' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deleteinvoiceitem").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "invoices/delete_invoiceItem",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteinvoiceitemtr' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deleteRcordP").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "product/deleteProduct",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteProducttrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deleteRcordE").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "events/deleteEvent",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteEventtrE' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deleteRcordV").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "invoices/deleteInvoice",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteInvoicetrV' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
    $(".deleteRcordtrans").on('click', function () {
        var del_id = $(this).data('id');
        reset();
        alertify.confirm("You really want to Delete?", function (e) {
            if (e) {
                $.ajax({
                    url: base_url + "transport/deleteTrans",
                    method: "POST",
                    data: {del_id: del_id},
                    success: function (data) {
                    }
                });
                alertify.success("Record has deleted Successfully");
                $('.deleteRcordtransT' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
            } else {
                alertify.error("You've Cancel");
            }
        });
        return false;
    });
});
$('#btnordernow').click(function () {
    var validationErro = 'animated shake errors';
    var cateName = $('#orderby_self').val();
    if ($.trim(cateName) == '') {
        $('#orderby_self').addClass(validationErro).focus();
        $("#orderby_self").notify("Please Enter Retailer/Distributor Email Id");
        alertify.error("Please Enter Retailer/Distributor Email Id!");
        return false;
    }
    var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (!filter.test(cateName)) {
        $('#orderby_self').notify('Please Enter Retailer/Distributor Email Id!');
        return false;
    } else {
        return true;
    }
});
//Location
$('#btnLocation').click(function () {
    var cateName = $('#locationName').val();
    var cateParent = $('#locationParent').val();
    var cateEdit = $('#editName').val();
    if (cateName == '') {
        $('#locationName').addClass(validationErro).focus();
        alertify.error("location name required!");
        $('#locationName').keyup(function () {
            $('#locationName').removeClass(validationErro);
        });
    } else {
        var conte_id = {
            cateName: cateName,
            cateParent: cateParent,
            cateEdit: cateEdit
        };
        $.ajax({
            url: base_url + "location/addLocation",
            method: "POST",
            data: conte_id,
            success: function (data) {
                if (data == "1") {
                    reset();
                    alertify.alert("record has been successfully");
                    setTimeout(function () {
                        window.open(base_url + "location/viewLocation", '_self');
                    }, 2000);
                } else {
                    $('#rock').addClass('showpop');
                    $('#product_success').html(data);
                    $('form').trigger('reset');
                    setTimeout(function () {
                        $('#rock').removeClass('showpop');
                    }, 2000);
                }
            }
        });
    }
});
$('#particuler_idup').change(function () {
    $('#response').html('<img src="' + baseassest + 'img/loading.gif" />');
    var particuler_idup = $('#particuler_idup').val();
    var conte_id = {
        particuler_idup: particuler_idup,
    };
    $.ajax({
        url: base_url + "items/totalqty",
        method: "POST",
        data: conte_id,
        beforeSend: function () {
            $('#response').html('<img src="' + baseassest + 'img/loading.gif" />').show();
        },
        success: function (data) {
            $('.invoice_rate_maxup').val(data);
            $('#response').hide();
        }
    });
});
$('#invoiceitemupadd').click(function () {
    var particuler_idup = $('#particuler_idup').val();
    var invoice_unit_maxup = $('#invoice_unit_maxup').val();
    var availabelqty = $('#availabelqty').val();
    var invoice_unitup = $('#invoice_unitup').val();
    //var countqty = availabelqty < invoice_unitup;
    var invoice_rate_maxup = $('#invoice_rate_maxup').val();
    var invoice_rateup = $('#invoice_rateup').val();
    var cateEdit = $(this).data('id');
    //alert(availabelqty);
    if (particuler_idup == '') {
        $('#particuler_idup').addClass(validationErro).focus();
        alertify.error("Particular name required!");
        $('#particuler_idup').keyup(function () {
            $('#particuler_idup').removeClass(validationErro);
        });
        return false;
    } else if (invoice_unitup == '') {
        $('#invoice_unitup').addClass(validationErro).focus();
        alertify.error("Unit required!");
        $('#invoice_unitup').keyup(function () {
            $('#invoice_unitup').removeClass(validationErro);
        });
        return false;
    } else if (invoice_rateup == '') {
        $('#invoice_rateup').addClass(validationErro).focus();
        alertify.error("Rate required!");
        $('#invoice_rateup').keyup(function () {
            $('#invoice_rateup').removeClass(validationErro);
        });
        return false;
    } else {

        var conte_id = {
            particuler_idup: particuler_idup,
            invoice_unit_maxup: invoice_unit_maxup,
            invoice_unitup: invoice_unitup,
            invoice_rateup: invoice_rateup,
            cateEdit: cateEdit
        };
        $.ajax({
            url: base_url + "invoices/additemtyem",
            method: "POST",
            data: conte_id,
            success: function (data) {
                location.reload();
            }
        });
    }


});
$('#serviceitemupadd').click(function () {
    var term_id = $('#term_id').val();
    var project_id = $('#proj_id').val(); 
    var term_value = $('#term_value').val();
    var particuler_id = $('#particuler_id').val();
    var amount = $('#amount').val(); 
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var proj_client = $('#proj_client').val();

    //var cateEdit = $(this).data('id');
    //alert(availabelqty);
    if (term_id == '') {
        $('#term_id ').addClass(validationErro).focus();
        alertify.error("Term name required!");
        $('#term_id').keyup(function () {
            $('#term_id').removeClass(validationErro);
        });
        return false;
    } else if (term_value == '') {
        $('#term_value').addClass(validationErro).focus();
        alertify.error("Term Value required!");
        $('#term_value').keyup(function () {
            $('#term_value').removeClass(validationErro);
        });
        return false;
    } else if (particuler_id == '') {
        $('#particuler_id').addClass(validationErro).focus();
        alertify.error("Select Item required!");
        $('#particuler_id').keyup(function () {
            $('#particuler_id').removeClass(validationErro);
        });
        return false;
    } else if (amount == '') {
        $('#amount').addClass(validationErro).focus();
        alertify.error("Select Amount required!");
        $('#amount').keyup(function () {
            $('#amount').removeClass(validationErro);
        });
        return false;
    } else {

        var conte_id = {
            term_id: term_id,
            client_id: proj_client,
            project_id: project_id,
            term_value: term_value,
            particuler_id: particuler_id,
            amount: amount,
            start_date: start_date,
            end_date: end_date 
        };
        $.ajax({
            url: base_url + "project/additemtyem",
            method: "POST",
            data: conte_id,
            success: function (data) {
                location.reload();
            }
        });
    }


});


$('#transitemupadd').click(function () {
    var particuler_idup = $('#particuler_idup').val();
    var trans_unit = $('#trans_unit').val();
    var cateEdit = $(this).data('id');
    if (particuler_idup == '') {
        $('#particuler_idup').addClass(validationErro).focus();
        alertify.error("Particular name required!");
        $('#particuler_idup').keyup(function () {
            $('#particuler_idup').removeClass(validationErro);
        });
        return false;
    } else if (trans_unit == '') {
        $('#trans_unit').addClass(validationErro).focus();
        alertify.error("Unit required!");
        $('#trans_unit').keyup(function () {
            $('#trans_unit').removeClass(validationErro);
        });
        return false;
    } else {
        var conte_id = {
            particuler_idup: particuler_idup,
            trans_unit: trans_unit,
            cateEdit: cateEdit
        };
        $.ajax({
            url: base_url + "transport/additemtyem",
            method: "POST",
            data: conte_id,
            success: function (data) {
                location.reload();
            }
        });
    }


});
$(".deleteRcordL").on('click', function () {
    var del_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "location/deleteLocation",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteLocationtrL' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".deleteRcordU").on('click', function () {
    var del_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "users/deleteUser",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteUsertrU' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
function closeshowinvoice() {
    $('.showinvicebg').hide();
}

$('.orderPro').click(function () {
    var udate_id = $(this).data('opro');
    var pid = $(this).data('pid');
    var qty = $(this).data('qty');
    var udate_stut = $(this).data('stut');
    $.ajax({
        url: base_url + 'orders/orderInvoice',
        method: "POST",
        data: {udate_id: udate_id, pid: pid},
        dataType: "json",
        success: function (data) {
            $('#productnameivoice').html(data.p_title);
            $('#productmodelivoice').html(data.p_model);
            $('#productqtyivoice').html(data.order_qty);
            $('#productuniteivoice').html(data.order_price);
            var totalqtyprice1 = data.order_price - (data.order_price * data.order_discount / 100) * 1;
            var totalqtyprice = totalqtyprice1 * data.order_qty;
            var totalshipprice = data.order_qty * data.order_ship_price;
            var totaltax = ((totalqtyprice1 * data.order_tax_price / 100) * data.order_qty);
            var totalshippricetax = (totaltax);
            $('#order_id').html(data.order_id);
            $('#productuniteivoiceinv').html(data.oTransationId);
            $('.productsubtotalivoice').html(totalqtyprice);
            $('#productshipedivoice').html((parseFloat((totalshipprice)).toFixed(2)));
            $('#productsubtaxivoice').html(parseFloat(totalshippricetax).toFixed(2));
            $('#productgrandivoice').html(parseFloat(totalqtyprice + totalshipprice + totalshippricetax).toFixed(2));
            //$('#productgrandivoice').html(((data.order_price * data.order_qty)));
            $('#sName').html(data.sName);
            $('#sEmail').html(data.sEmail);
            $('#sAddress').html(data.sAddress);
            $('#sMobile').html(data.sMobile);
            $('#sCountry').html(data.sCountry);
            $('#sState').html(data.sState);
            $('#sCity').html(data.sCity);
            $('#sZipcode').html(data.sZipcode);
            $('#sLandmark').html(data.sLandmark);
            $('.showinvicebg').show();
            $('.printinvoice').click(function () {
                $.ajax({
                    url: base_url + 'orders/orderInvoiceStatus',
                    method: "POST",
                    data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid, qty: qty},
                    success: function (data) {
                        location.reload();
                    }
                });
            });
        }
    });
});
$('.orderDis').click(function () {
    var udate_id = $(this).data('opro');
    var udate_stut = $(this).data('stut');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + 'orders/orderInvoice',
        method: "POST",
        data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid},
        dataType: "json",
        success: function (data) {
            $('#orderDataProgracee2').html(data.oDate);
            $('#orderDataProgracee3').html(data.odispatDate);
            $('.showinvicebg1').show();
            $('.delivered').click(function () {
                var messageBody = $('#messageBody1').val();
                var messagereg1 = $('#messagereg1').val();
                var messageType1 = $('#messageType1').val();
                $.ajax({
                    url: base_url + 'orders/orderdeliveredStatus',
                    method: "POST",
                    data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid, messagebody: messageBody, messagereg1: messagereg1, messagetype1: messageType1},
                    success: function (data) {
                        location.reload();
                    }
                });
            });
        }
    });
});
//show cancel popup
$(document).on('click', '.cancelOrder', function () {
    var udate_id = $(this).data('opro');
    var udate_stut = $(this).data('stut');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + 'orders/orderInvoice',
        method: "POST",
        data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid},
        dataType: "json",
        success: function (data) {
            $('#orderDataProgracee1').html(data.oDate);
            $('.showinvicebg2').show();
            $('.cancelorderli').click(function () {
                var messageBody = $('#messageBody').val();
                var messagereg = $('#messagereg').val();
                $.ajax({
                    url: base_url + 'orders/orderCancelStatus',
                    method: "POST",
                    data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid, messageBody: messageBody, messagereg: messagereg},
                    success: function (data) {
                        location.reload();
                    }
                });
            });
        }
    });
});
//Show all check error


/*
 $('.orderDis').click(function () {
 var udate_id = $(this).data('opro');
 var udate_stut = $(this).data('stut');
 $.ajax({
 url: base_url + 'admin/orders/orderdispatcheds',
 method: "POST",
 data: {udate_id: udate_id, udate_stut: udate_stut},
 success: function (data) {
 $('.showinvicebg1').show();
 });
 });
 */
//hide all popup
function closeshowe() {
    $('.showinvicebg1').hide();
}
function closeshowe2() {
    $('.showinvicebg2').hide();
}

function hide_obj(val) {
    $(val).hide();
}

$(document).on('click', '#show_invoice', function () {
    $('.showinvicebg1').show();
});
//SHOW ORDER COMPLETE 
$(document).on('click', '.show_complete_order', function () {

    var udate_id = $(this).data('opro');
    var udate_stut = $(this).data('stut');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + 'orders/orderInvoice',
        method: "POST",
        data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid},
        dataType: "json",
        success: function (data) {
            $('#orderDataProgracee7').html(data.oDate);
            $('#orderDataProgracee8').html(data.odispatDate);
            $('#orderDataProgracee9').html(data.ocomDate);
            $('.showinvicebg3').show();
        }
    });
});
//SHOW ORDER cancal 
$(document).on('click', '.show_cancel_order', function () {
    var udate_id = $(this).data('opro');
    var udate_stut = $(this).data('stut');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + 'orders/orderInvoice',
        method: "POST",
        data: {udate_id: udate_id, udate_stut: udate_stut, pid: pid},
        dataType: "json",
        success: function (data) {
            $('#orderDataProgracee10').html(data.oDate);
            $('#orderDataProgracee11').html(data.odispatDate);
            $('#orderDataProgracee12').html(data.ocomDate);
            $('.show_cancel_order_form').show();
        }
    });
});
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}


$(document).on('click', '#addmore', function () {
    $('.first').show('slow');
});
$(".deleteimg").on('click', function () {
    var del_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "product/deleteExtractimages",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                    location.reload();
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteCategorytrC' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$("#p_category").on('change', function () {
    var val_id = $(this).val();
    $.ajax({
        url: base_url + "category/findpartcat",
        method: "POST",
        data: {val_id: val_id},
        success: function (data) {
            //alert(data);
            $('#p_itemType').html(data);
        }
    });
});
$("#p_itemType").on('change', function () {

    var val_id = $(this).val();
    $('#parent_category').val(val_id);
});
$(".deleteRcordPage").on('click', function () {
    var del_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "pages/deletePage",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deleteLocationtrpage' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(".deleteRcordCareer").on('click', function () {
    var del_id = $(this).data('id');
    reset();
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "career/deleteCareer",
                method: "POST",
                data: {del_id: del_id},
                success: function (data) {
                }
            });
            alertify.success("Record has deleted Successfully");
            $('.deletetrcareer' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
$(document).on('change', '#selectdate', function () {
    var val_id = $(this).val();
    $.ajax({
        url: base_url + "reports/findvalyear",
        method: "POST",
        data: {val_id: val_id},
        success: function (data) {
            $('#yearview').html(data);
        }
    });
});
$(document).on('click', '.updateqtyactive', function () {
    $(this).prev("span").show();
});
$('.btnupdate').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "product/updateqty",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).on('click', '.updatepricevenue', function () {
    $(this).prev("span").show();
});
$('.btnupdatevenuePrice').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('venueid');
    $.ajax({
        url: base_url + "venues/updatePrice",
        method: "POST",
        data: {val_id: val_id, venueid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).on('click', '.updatepriceevent', function () {
    $(this).prev("span").show();
});
$('.btnupdateeventPrice').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('eventid');
    $.ajax({
        url: base_url + "events/updatePrice",
        method: "POST",
        data: {val_id: val_id, eventid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).on('click', '.updateslugvenue', function () {
    $(this).prev("span").show();
});
$('.btnupdatevenueSlug').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('venueid');
    $.ajax({
        url: base_url + "venues/updateSlug",
        method: "POST",
        data: {val_id: val_id, venueid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).on('click', '.updatecommisionvenue', function () {
    $(this).prev("span").show();
});
$('.btnupdatevenueCommision').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('venueid');
    $.ajax({
        url: base_url + "venues/updateCommision",
        method: "POST",
        data: {val_id: val_id, venueid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).on('click', '.updatepriceeventcomm', function () {
    $(this).prev("span").show();
});
$('.btnupdateeventcommPrice').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('eventid');
    $.ajax({
        url: base_url + "events/updateCommision",
        method: "POST",
        data: {val_id: val_id, eventid: pid},
        success: function (data) {

            var message = "URL has been updated";
            $('#successMessage').modal('show');
            $('#essmessage').text(message);
            //location.reload();
        }
    });
});
$(document).on('click', '.updateslugevent', function () {
    $(this).prev("span").show();
});
$('.btnupdateeventSlug').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('eventid');
    $.ajax({
        url: base_url + "events/updateSlug",
        method: "POST",
        data: {val_id: val_id, eventid: pid},
        success: function (data) {

            var message = "URL has been updated";
            $('#successMessage').modal('show');
            $('#essmessage').text(message);
            //location.reload();
        }
    });
});
$(document).on('click', '.updatedispriceevent', function () {
    $(this).prev("span").show();
});
$('.btnupdateeventdisprice1').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('eventid');
    $.ajax({
        url: base_url + "events/updateDiscount1",
        method: "POST",
        data: {val_id: val_id, eventid: pid},
        success: function (data) {

            var message = "URL has been updated";
            $('#successMessage').modal('show');
            $('#essmessage').text(message);
            //location.reload();
        }
    });
});
$(document).on('click', '.updatedispriceevent2', function () {
    $(this).prev("span").show();
});
$('.btnupdateeventdisprice2').click(function () {

    var val_id = $(this).prev('input').val();
    var pid = $(this).data('eventid');
    $.ajax({
        url: base_url + "events/updateDiscount2",
        method: "POST",
        data: {val_id: val_id, eventid: pid},
        success: function (data) {

            var message = "URL has been updated";
            $('#successMessage').modal('show');
            $('#essmessage').text(message);
            //location.reload();
        }
    });
});
$('#statuschecked').click(function () {
    var val_id = $(this).data('val');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "project/statuscheck",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$('#paynow').click(function () {
    var client_id = $(this).data('client_id');
    var invoiceno = $(this).data('invoiceno');
    var bank_name = $('#bank_name').val();
    var pid = $(this).data('pid');
    var payamount = $('#payAmount').val();
    $.ajax({
        url: base_url + "project/paynow",
        method: "POST",
        data: {bank_name:bank_name,client_id: client_id, pid: pid,invoiceno:invoiceno,payamount:payamount},
        success: function (data) { 
   location.reload();
        }
    });
});
$('.pstatus').click(function () {
    var val_id = $(this).data('val');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "product/updatestatus",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$('.estatus').click(function () {
    var val_id = $(this).data('val');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "events/updatestatus",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$('.vstatus').click(function () {
    var val_id = $(this).data('val');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "venues/updatestatus",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$('.astatus').click(function () {
    var val_id = $(this).data('val');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "artists/updatestatus",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
$('.outofstock').click(function () {
    var val_id = $(this).data('val');
    var pid = $(this).data('pid');
    $.ajax({
        url: base_url + "product/updateoutofstock",
        method: "POST",
        data: {val_id: val_id, pid: pid},
        success: function (data) {
            location.reload();
        }
    });
});
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
