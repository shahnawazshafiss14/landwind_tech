<?php

class Buy_model extends MY_Model {
    const tableName = 'tblbuy';
    const tableName_pk = 'buy_id';

    public function __construct() {
	parent::__construct();
    }
    public function viewRecordRsum($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->select_sum('buy_qty', 'qty');
	$this->db->select_sum('buy_price', 'bprice');
	$this->db->order_by($this::tableName_pk, 'DESC');
	$query = $this->db->get($this::tableName);
	return $query->row();
    }
    public function viewRecordRsumqtypri($data) {
	$this->db->where($data);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	$query = $querySql->result();
	$output = 0;
	foreach ($query as $sf){
	    $output += percetage($sf->buy_price, $sf->buy_event_discount, $sf->buy_qty);
	}
	return $output;
    }

    public function viewRecordIdProductId($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$this->db->join('tblproduct', 'tblproduct.p_product_id = ordertable.order_product_id');
	$this->db->join('usertable', 'usertable.userEmail = ordertable.order_userName');
	$query = $this->db->get();
	return $query->row();
    }

    public function viewOrderEmailUserEmail($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$this->db->join('usertable', 'usertable.userEmail = ordertable.order_userName');
	$query = $this->db->get();
	return $query->row();
    }

    public function viewsummn($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$query = $this->db->get();
	return $query->num_rows();
    }

    public function viewsumy($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsumw($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (WEEK(oDate) = (WEEK(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsumyn($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$query = $this->db->get();
	return $query->num_rows();
    }

    public function viewsumm() {
	$this->db->select('(SELECT SUM(tblbuy.buy_price) FROM tblbuy WHERE tblbuy.MONTH(buyDate) = (MONTH(NOW())) AND YEAR(buyDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('tblbuy');
	return $query->row();
    }

    public function viewsumd($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsum($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function reprotmonth() {
	$this->db->where('(MONTH(buyDate) = (MONTH(NOW())) AND YEAR(buyDate) = YEAR(NOW()))');
	$this->db->select('*');
	$this->db->from('tblbuy');
	$query = $this->db->get();
	return $query->result();
    }

    public function viewRecordAnyRAll($arrvenue, $start_date, $end_date) {
	$this->db
		->select('*')
		->from('tblbuy')
		->where($arrvenue)
		->where('tblbuy.buyDate >=', $start_date, true)
		->where('tblbuy.buyDate <= ', $end_date, true);
	$querySql = $this->db->get();
	return $querySql->result();
    }

    public function reprotjan($date, $date2) {
	$total = '';
	$totalqty = '';
	$venuesessionid = $this->session->userdata('venuefront');
	$avfetch = array(
	    'venue_type' => $venuesessionid
	);
	$venuefetch = $this->venue_model->viewRecordAny($avfetch);
	$barra1 = array(
	    'venue_id' => $venuefetch->venue_id
	);
	$fetchevnt = $this->event_model->viewRecordAnyR($barra1);
	foreach ($fetchevnt as $se) {
	    $barra = array(
		'buyStatus' => '1',
		'buy_product_id' => $se->event_id,
	    );
	    $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	    foreach ($fetchbuy as $sf) {
		$total += $sf->buy_price;
		$totalqty += $sf->buy_qty;
	    }
	}
	return $totalqty;
    }

    /*
      public function reportartist($date, $date2) {
      $total = '';
      $totalqty = '';
      $venuesessionid = $this->session->userdata('artistfront');
      $barra1 = array(
      'event_status' => '1',
      'artist_id' => $venuesessionid
      );
      $fetchevnt = $this->event_model->viewRecordAnyR($barra1);
      foreach ($fetchevnt as $se) {
      $barra = array(
      'buyStatus' => '1',
      'buy_product_id' => $se->event_id,
      );
      $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
      foreach ($fetchbuy as $sf) {
      $total += $sf->buy_price * $sf->buy_qty;
      }
      }

      return $total;
      }
     *
     */

    public function reportartist($date, $date2) {
	$total = 0;
	$totalqty = '';
	$venuesessionid = $this->session->userdata('artistfront');
	$barra1 = array(
	    'artist_id' => $venuesessionid
	);
	$fetchevnt = $this->event_model->viewRecordAnyR($barra1);
	foreach ($fetchevnt as $se) {
	    $barra = array(
		'buyStatus' => '1',
		'buy_product_id' => $se->event_id,
	    );
	    $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	    foreach ($fetchbuy as $sf) {
		$total += $sf->buy_qty;
	    }
	}

	return $total;
    }

    public function reportartistadmin($date, $date2, $artist_email) {
	$total = 0;
	$totalqty = '';
	$arass = array(
	    'artist_id' => $artist_email
	);
	$fetchevnt = $this->event_model->viewRecordAnyR($arass);
	foreach ($fetchevnt as $se) {
	    $barra = array(
		'buyStatus' => '1',
		'buy_product_id' => $se->event_id,
	    );
	    $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	    foreach ($fetchbuy as $sf) {
		$total += $sf->buy_qty;
	    }
	}

	return $total;
    }

    public function reportartistadminprice($date, $date2, $artist_email) {
	$total = 0;
	$totalqty = '';
	$arass = array(
	    'artist_id' => $artist_email
	);
	$fetchevnt = $this->event_model->viewRecordAnyR($arass);
	foreach ($fetchevnt as $se) {
	    $barra = array(
		'buyStatus' => '1',
		'buy_product_id' => $se->event_id,
	    );
	    $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	    foreach ($fetchbuy as $sf) {
		$total += (($sf->buy_price * $sf->buy_qty) - (($sf->buy_price * $sf->buy_qty) * $sf->buy_event_discount / 100)) * $sf->buy_event_commission / 100;
	    }
	}

	return $total;
    }

    public function reportvenueadmin($date, $date2, $artist_email) {
	$total = 0;
	$totalqty = '';
	$arass = array(
	    'venue_id' => $artist_email
	);
	$fetchevnt = $this->venue_model->viewRecordAnyR($arass);
	foreach ($fetchevnt as $se) {
	    $barra = array(
		'buyStatus' => '1',
		'buy_venue_id' => $se->venue_id,
	    );
	    $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	    foreach ($fetchbuy as $sf) {
		$total += $sf->buy_qty;
	    }
	}
	return $total;
    }

    public function reportvenueadminprice($date, $date2, $artist_email) {
	$total = 0;
	$totalqty = '';
	$arass = array(
	    'venue_id' => $artist_email
	);
	$fetchevnt = $this->venue_model->viewRecordAnyR($arass);
	foreach ($fetchevnt as $se) {
	    $barra = array(
		'buyStatus' => '1',
		'buy_venue_id' => $se->venue_id,
	    );
	    $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	    foreach ($fetchbuy as $sf) {
		$total += (($sf->buy_price * $sf->buy_qty) - (($sf->buy_price * $sf->buy_qty) * $sf->buy_event_discount / 100)) * $sf->buy_venue_commisson / 100;
	    }
	}

	return $total;
    }

}

?>