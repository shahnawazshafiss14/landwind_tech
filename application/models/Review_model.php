<?php

class Review_model extends MY_Model {

    const tableName = 'reviewstart';
    const tableName_pk = 'reviewid';
    public function __construct() {
	parent::__construct();
    }
     public function review3($data){
        $this->db->limit(5);
        $this->db->where($data);
        $this->db->order_by($this::tableName_pk, 'DESC');
        $querySql = $this->db->get($this::tableName);
        return $querySql->result();
    }


}

?>
