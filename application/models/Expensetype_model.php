<?php 
class Expensetype_model extends MY_Model{
	const tableName = 'tbl_expense_type';
	const tableName_pk = 'type_id';
	public function __construct(){
		parent::__construct();
	}
	 
	public function find_by_name($id){
		$this->db->where($this::tableName_pk, $id);
		$querySql = $this->db->get($this::tableName);
		return $querySql->row('type_name');
	}
}
?>