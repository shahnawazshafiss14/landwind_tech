<?php

class Permissionmenu_model extends MY_Model {

    const tableName = 'permession_menu';
    const tableName_pk = 'per_id';

    public function __construct() {
	parent::__construct();
    }

    public function viewRecordIdUserId($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->order_by('menu_left.sort_by', 'ASC');
	$this->db->from('permession_menu');
	$this->db->join('menu_left', 'menu_left.menu_id = permession_menu.menu_id');
	$this->db->join('usertable', 'usertable.u_id = permession_menu.user_id');
	$query = $this->db->get();
	return $query->result();
    }

}

?>
