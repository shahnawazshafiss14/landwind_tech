<?php

class Order_model extends MY_Model {

    const tableName = 'ordertable';
    const tableName_pk = 'order_id';

    public function __construct() {
	parent::__construct();
    }

    public function viewRecordIdProductId($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$this->db->join('tblproduct', 'tblproduct.p_product_id = ordertable.order_product_id');
	$this->db->join('sorderaddress', 'sorderaddress.order_tranction_id = ordertable.order_tranction_id');
	//$this->db->join('messageError', 'messageError.order_tranction_id = ordertable.order_tranction_id');
	$query = $this->db->get();
	return $query->row();
    }

    public function viewOrderEmailUserEmail($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$this->db->join('usertable', 'usertable.userEmail = ordertable.order_userName');
	$query = $this->db->get();
	return $query->row();
    }

    public function viewsummn($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$query = $this->db->get();
	return $query->num_rows();
    }

    public function viewsumy($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsumw($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (WEEK(oDate) = (WEEK(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsumyn($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('ordertable');
	$query = $this->db->get();
	return $query->num_rows();
    }

    public function viewsumm($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsumd($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function viewsum($oStatus) {
	$this->db->select('(SELECT SUM(ordertable.order_price) FROM ordertable WHERE ordertable.' . $oStatus . ' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW())) ) AS amount_paid', FALSE);
	$query = $this->db->get('ordertable');
	return $query->row();
    }

    public function reprotmonth() {
	$this->db->where('(MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))');
	$this->db->select('*');
	$this->db->from('ordertable');
	$query = $this->db->get();
	return $query->result();
    }

}

?>