<?php

class Venue_model extends MY_Model {

    const tableName = 'tblvenue';
    const tableName_pk = 'venue_id';

    public function __construct() {
	parent::__construct();
    }

    public function venueId($data) {
	$this->db->where('venue_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('venue_name');
    }

    public function searchvenue($latitude, $longtitude) {
	$lat = $latitude;
	$lon = $longtitude;
	$radius = 100; // Km
	$angle_radius = $radius / 200; // Every lat|lon degree� is ~ 111Km
	$min_lat = $lat - $angle_radius;
	$max_lat = $lat + $angle_radius;

	$min_lon = $lon - $angle_radius;
	$max_lon = $lon + $angle_radius;
	$querySql = $this->db->query("select * from tblvenue WHERE latitude BETWEEN $min_lat AND $max_lat AND longitude BETWEEN $min_lon AND $max_lon");
	return $querySql->result();
    }
    public function viewRecordRsum($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->select_sum('venue_price', 'venueprice');
	$this->db->order_by($this::tableName_pk, 'DESC');
	$query = $this->db->get($this::tableName);
	return $query->result();
    }

}

?>