<?php

class Category_model extends MY_Model {

    const tableName = 'category_top';
    const tableName_pk = 'category_id';

    private $category = 'category_top';

    public function __construct() {
	parent::__construct();
    }

    public function viewRecordAnyR($data) {
	$this->db->where($data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function categorybyId($data) {
	$this->db->where('category_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('category_name');
    }

    public function category_menu() {
	// Select all entries from the menu table
	$query = $this->db->query("select category_id, category_name, category_link,
            category_parent_id from " . $this->category . " order by category_parent_id, category_link");

	// Create a multidimensional array to contain a list of items and parents
	$cat = array(
	    'items' => array(),
	    'parents' => array()
	);
	// Builds the array lists with data from the menu table
	foreach ($query->result() as $cats) {
	    // Creates entry into items array with current menu item id ie. $menu['items'][1]
	    $cat['items'][$cats->category_id] = $cats;
	    // Creates entry into parents array. Parents array contains a list of all items with children
	    $cat['parents'][$cats->category_parent_id][] = $cats->category_id;
	}

	if ($cat) {
	    $result = $this->build_category_menu(0, $cat);
	    return $result;
	} else {
	    return FALSE;
	}
    }

    // Menu builder function, parentId 0 is the root
    function build_category_menu($parent, $menu) {
	$html = "";
	if (isset($menu['parents'][$parent])) {
	    $html .= "<ul class='checks' style='list-style-type:none'>";
	    //$html .= "<li><input type='checkbox' name='categoryParent' value='0'>Select parent</li>";
	    foreach ($menu['parents'][$parent] as $itemId) {
		if (!isset($menu['parents'][$itemId])) {
		    $html .= "<li><input type='checkbox' name='categoryParent' value='" . $menu['items'][$itemId]->category_id . "'> " . $menu['items'][$itemId]->category_name . " | <a href='javascript:;' class='deleteRcordC' data-id='" . $menu['items'][$itemId]->category_id . "'><i class='fa fa-trash'></i></a> | <a href='" . ADMINC . "category/topCategory/" . $menu['items'][$itemId]->category_id . "'><i class='fa fa-edit'></i></a> | <a href='" . ADMINC . "category/details/" . $menu['items'][$itemId]->category_id . "'><i class='fa fa-book'></i></a></li>";
		}
		if (isset($menu['parents'][$itemId])) {
		    $html .= "<li><input type='checkbox' name='categoryParent' value='" . $menu['items'][$itemId]->category_id . "'> " . $menu['items'][$itemId]->category_name . "";
		    $html .= $this->build_category_menu($itemId, $menu);
		    $html .= "</li>";
		}
	    }
	    $html .= "</ul>";
	}
	return $html;
    }

}

?>