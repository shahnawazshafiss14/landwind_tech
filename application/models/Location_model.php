<?php
class Location_model extends MY_Model{
    const tableName = 'locationtable';
    const tableName_pk = 'location_id';
    public function __construct() {
	parent::__construct();
    }
    public function locationId($data) {
	$this->db->where('location_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('location_name');
    }
}
?>