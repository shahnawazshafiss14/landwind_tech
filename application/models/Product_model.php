<?php

class Product_model extends MY_Model {

    const tableName = 'tblproduct';
    const tableName_pk = 'p_product_id';

    private $category = 'category_top';

    public function __construct() {
	parent::__construct();
    }

    public function searchtop($querytop) {
	//$this->db->where($data);
	$this->db->like('p_title', $querytop);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function viewRecordIdProductId($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('tblproduct');
	$this->db->join('prod_extracon', 'prod_extracon.p_product_id = tblproduct.p_product_id');
	$query = $this->db->get();
	return $query->row();
    }

    public function viewRecordAllr() {
	$this->db->order_by('rand()');
	$this->db->limit(10);
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function viewRecordAllf() {
	$this->db->order_by($this::tableName_pk, 'DESC');
	$this->db->limit(10);
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function category_menu() {
	// Select all entries from the menu table
	$query = $this->db->query("select category_id, category_name, category_link,
            category_parent_id from " . $this->category . " order by category_parent_id, category_link");

	// Create a multidimensional array to contain a list of items and parents
	$cat = array(
	    'items' => array(),
	    'parents' => array()
	);
	// Builds the array lists with data from the menu table
	foreach ($query->result() as $cats) {
	    // Creates entry into items array with current menu item id ie. $menu['items'][1]
	    $cat['items'][$cats->category_id] = $cats;
	    // Creates entry into parents array. Parents array contains a list of all items with children
	    $cat['parents'][$cats->category_parent_id][] = $cats->category_id;
	}

	if ($cat) {
	    $result = $this->build_category_menu(0, $cat);
	    return $result;
	} else {
	    return FALSE;
	}
    }

    // Menu builder function, parentId 0 is the root
    function build_category_menu($parent, $menu) {
	$html = "";
	if (isset($menu['parents'][$parent])) {
	    $html .= "<ul class='checks' style='list-style-type:none'>";
	    foreach ($menu['parents'][$parent] as $itemId) {
		if (!isset($menu['parents'][$itemId])) {
		    $html .= "<li><input type='checkbox' class='parentchecked' name='categoryParent1' value='" . $menu['items'][$itemId]->category_id . "'> " . $menu['items'][$itemId]->category_name . "</li>";
		}
		if (isset($menu['parents'][$itemId])) {
		    $html .= "<li><input type='checkbox' class='parentchecked' name='categoryParent1' value='" . $menu['items'][$itemId]->category_id . "'> " . $menu['items'][$itemId]->category_name . "";
		    $html .= $this->build_category_menu($itemId, $menu);
		    $html .= "</li>";
		}
	    }
	    $html .= "</ul>";
	}
	return $html;
    }

}

?>