<?php

class Salesdownload_model extends MY_Model {

    const tableName = 'tbl_sales_download';
    const tableName_pk = 's_down_id';

    public function __construct() {
	parent::__construct();
    }
    public function consigId($data) {
        $this->db->where('consig_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('consig_name');
    }

    public function viewRecordAnyRAll($arrvenue, $start_date, $end_date) {
	$this->db
		->select('*')
		->from('tble_invoice')
		->where($arrvenue)
		->where('tble_invoice.invoice_dos >=', $start_date, true)
		->where('tble_invoice.invoice_dos <= ', $end_date, true);
	$querySql = $this->db->get();
	return $querySql->result();
    }
    
     public function viewRecordAnyRAllsales($arrvenue, $start_date, $end_date) {
	$this->db
		->select('*')
		->from('tble_items_sales')
		->where($arrvenue)
		->where('tble_items_sales.items_date >=', $start_date, true)
		->where('tble_items_sales.items_date <= ', $end_date, true)
                ->order_by('items_date', 'ASC');
	$querySql = $this->db->get();
	return $querySql->result();
    }

    public function reportvenueadmin($date, $date2, $consig_id) {
	$total = 0;
	$totalqty = '';
	$barra = array(
	    'invoice_status' => '1',
	    'invoice_consig' => $consig_id
	);
	$fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	foreach ($fetchbuy as $sf) {
	    $asge = array(
		'itemInvoice_invoice' => $sf->invoice_id
	    );
	    $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {
		$total += $fech->itemInvoice_max_kg;
	    }
	}
	return $total;
    }
    public function reportvenueadminwithout($date, $date2) {
	$total = 0;
	$totalqty = '';
	$barra = array(
	    'invoice_status' => '1'
	);
	$fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	foreach ($fetchbuy as $sf) {
	    $asge = array(
		'itemInvoice_invoice' => $sf->invoice_id
	    );
	    $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {
		$total += $fech->itemInvoice_max_kg;
	    }
	}
	return $total;
    }

    public function reportvenueadminprice($date, $date2, $consig_id) {
	$total = 0;
	$totalqty = '';
	$barra = array(
	    'invoice_status' => '1',
	    'invoice_consig' => $consig_id
	);
	$fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	foreach ($fetchbuy as $sf) {
	    $asge = array(
		'itemInvoice_invoice' => $sf->invoice_id
	    );
	    $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {

		$taotal += ($fech->itemInvoice_kg * $fech->itemInvoice_rate);
	    }
	    $cgest = $this->gst_model->commongstId($sf->invoice_cgst);
	    $sgest = $this->gst_model->commongstId($sf->invoice_sgst);
	    $igest = $this->gst_model->commongstId($sf->invoice_igst);
	    $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
	    $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
	    $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
	    $total = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
	}

	return $total;
    }
    public function reportvenueadminpricewithout($date, $date2) {
	$total = 0;
	$totalqty = '';
	$barra = array(
	    'invoice_status' => '1'
	);
	$fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
	foreach ($fetchbuy as $sf) {
	    $asge = array(
		'itemInvoice_invoice' => $sf->invoice_id
	    );
	    $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {

		$taotal += ($fech->itemInvoice_kg * $fech->itemInvoice_rate);
	    }
	    $cgest = $this->gst_model->commongstId($sf->invoice_cgst);
	    $sgest = $this->gst_model->commongstId($sf->invoice_sgst);
	    $igest = $this->gst_model->commongstId($sf->invoice_igst);
	    $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
	    $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
	    $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
	    $total = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
	}

	return $total;
    }

}

?>