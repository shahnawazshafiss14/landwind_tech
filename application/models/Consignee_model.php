<?php

class Consignee_model extends MY_Model {

    const tableName = 'tblconsignee';
    const tableName_pk = 'consig_id';

    public function __construct() {
        parent::__construct();
    }

    public function consigId($data) {
        $this->db->where('consig_id', $data);
        $querySql = $this->db->get($this::tableName);
        return $querySql->row('consig_name');
    }

    public function viewRecordAnyRAll($arrvenue, $start_date, $end_date) {
        $this->db
                ->select('*')
                ->from('tble_invoice')
                ->where($arrvenue)
                ->where('tble_invoice.invoice_dos >=', $start_date, true)
                ->where('tble_invoice.invoice_dos <= ', $end_date, true)
                ->order_by('invoice_dos', 'ASC');

        $querySql = $this->db->get();
        return $querySql->result();
    }

    public function viewRecordAnyRAllsales($arrvenue, $start_date, $end_date) {
        $this->db
                ->select('*')
                ->from('tble_items_sales')
                ->where($arrvenue)
                ->where('tble_items_sales.items_date >=', $start_date, true)
                ->where('tble_items_sales.items_date <= ', $end_date, true)
                ->order_by('items_date', 'ASC');
        $querySql = $this->db->get();
        return $querySql->result();
    }

    public function reportvenueadmin($date, $date2, $consig_id) {
        $total = 0;
        $totalqty = '';
        $barra = array(
            'invoice_status' => '1',
            'invoice_consig' => $consig_id
        );
        $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'itemInvoice_invoice' => $sf->invoice_id
            );
            $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {
                $total += $fech->itemInvoice_max_kg;
            }
        }
        return $total;
    }

    public function reportvenueadminwithout($date, $date2) {
        $total = 0;
        $totalqty = '';
        $barra = array(
            'invoice_status' => '1'
        );
        $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'itemInvoice_invoice' => $sf->invoice_id
            );
            $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {
                $total += $fech->itemInvoice_max_kg;
            }
        }
        return $total;
    }

    public function find_date_all_sum($consig_id) {
        $total = 0;
        $toalpurchased = 0;
        $toalpurchased1 = 0;
        $this->db
                ->select('min(items_date) AS mindate, max(items_date) AS maxdate')
                ->from('tble_items_sales')
                ->where($consig_id);
        $querySql = $this->db->get();
        $fethc = $querySql->row();
        $date = $fethc->mindate;
        $date2 = $fethc->maxdate;
        $fetchbuy = $this->viewRecordAnyRAllsales($consig_id, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'purchedItem' => $sf->items_id
            );
            $fietch = $this->pitemssales_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $toalpurchased += ($fech->pitem_unit * $fech->pitem_rate);
            }
            // $total = $taotal;
            $fetchParticlearraydr = array(
                'purchedItem' => $sf->items_id,
                'strans_type' => 1
            );
            $fetchdatat = $this->consigtrans_model->viewRecordAnyR($fetchParticlearraydr);
            foreach ($fetchdatat as $strans) {
                $toalpurchased += $strans->strans_amount;
            }
            $fetchParticlearraycr = array(
                'purchedItem' => $sf->items_id,
                'strans_type' => 2
            );
            $fetchdatatcr = $this->consigtrans_model->viewRecordAnyR($fetchParticlearraycr);
            foreach ($fetchdatatcr as $stranscr) {
                $toalpurchased1 += $stranscr->strans_amount;
            }
        }
        $total = $toalpurchased - $toalpurchased1;
        return $total;
    }

    public function find_blanced($consig_id) {
        $this->db
                ->select('min(items_date) AS mindate, max(items_date) AS maxdate')
                ->from('tble_items_sales')
                ->where($consig_id);
        $querySql = $this->db->get();
        $fethc = $querySql->row();
        $date = $fethc->mindate;
        $date2 = $fethc->maxdate;

        $ara = array(
            '0' => $date,
            '1' => $date2,
        );


        $itemsObjects = $this->viewRecordAnyRAllsales($consig_id, $ara[0], $ara[1]);
        $i = 1;
        $grandtoatal1 = 0;
        $toalpurchased = 0;
        $toalspayment = 0;
        $toalspaymentdr = 0;


        foreach ($itemsObjects as $pO) :
            $fetchParticlearray1 = array(
                'purchedItem' => $pO->items_id
            );
            $fetchParticlegr = $this->pitemssales_model->viewRecordPageRG($fetchParticlearray1, 'purchedItem');
            $fetchParticle1 = $this->pitemssales_model->viewRecordAnyR($fetchParticlearray1);
            if (count($fetchParticle1) > 0):

                $taotal = 0;
                $taotalrate = 0;
                $taotalkg = '';
                $itemunit = '';
                foreach ($fetchParticlegr as $tr1):

                    foreach ($fetchParticle1 as $tr):
                        $fetchParticlearray = array(
                            'item_id' => $tr->pitem_particular
                        );
                        $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                        $taotal += $tr->pitem_unit * $tr->pitem_rate;
                        $toalpurchased += $tr->pitem_unit * $tr->pitem_rate;

                    endforeach;

                endforeach;
            endif;

            $fetchParticlearraycr = array(
                'purchedItem' => $pO->items_id,
                'strans_type' => 1
            );

            $fetchdatat = $this->consigtrans_model->viewRecordAnyR($fetchParticlearraycr);
            foreach ($fetchdatat as $ftt):
                $toalpurchased += $ftt->strans_amount;

            endforeach;
            $fetchParticlearraydr = array(
                'purchedItem' => $pO->items_id,
                'strans_type' => 2
            );

            $fetchdatat = $this->consigtrans_model->viewRecordAnyR($fetchParticlearraydr);
            foreach ($fetchdatat as $ftt):
                $toalspayment += $ftt->strans_amount;

            endforeach;
        endforeach;

        return $toalpurchased - $toalspayment;
    }

    public function find_date_all_sum_trans($consig_id) {
        $total = 0;
        $this->db
                ->select('min(items_date) AS mindate, max(items_date) AS maxdate')
                ->from('consigTrans')
                ->where($consig_id);
        $querySql = $this->db->get();
        $fethc = $querySql->row();
        $date = $fethc->mindate;
        $date2 = $fethc->maxdate;
        $fetchbuy = $this->viewRecordAnyRAllsales($consig_id, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'purchedItem' => $sf->items_id
            );
            $fietch = $this->pitems_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $taotal += ($fech->pitem_unit * $fech->pitem_rate);
            }
            $total = $taotal;
        }

        return $total;
    }

    public function reportvenueadminprice($date, $date2, $consig_id) {
        $total = 0;
        $totalqty = '';
        $barra = array(
            'invoice_status' => '1',
            'invoice_consig' => $consig_id
        );
        $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'itemInvoice_invoice' => $sf->invoice_id
            );
            $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $taotal += ($fech->itemInvoice_kg * $fech->itemInvoice_rate);
            }
            $cgest = $this->gst_model->commongstId($sf->invoice_cgst);
            $sgest = $this->gst_model->commongstId($sf->invoice_sgst);
            $igest = $this->gst_model->commongstId($sf->invoice_igst);
            $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
            $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
            $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
            $total = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
        }

        return $total;
    }

    public function reportvenueadminpricewithout($date, $date2) {
        $total = 0;
        $totalqty = '';
        $barra = array(
            'invoice_status' => '1'
        );
        $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'itemInvoice_invoice' => $sf->invoice_id
            );
            $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $taotal += ($fech->itemInvoice_kg * $fech->itemInvoice_rate);
            }
            $cgest = $this->gst_model->commongstId($sf->invoice_cgst);
            $sgest = $this->gst_model->commongstId($sf->invoice_sgst);
            $igest = $this->gst_model->commongstId($sf->invoice_igst);
            $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
            $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
            $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
            $total = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
        }

        return $total;
    }

}

?>