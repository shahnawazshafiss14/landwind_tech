<?php

class Uvfy_model extends MY_Model {

    const tableName = 'userverfifydoc';
    const tableName_pk = 'uv_id';

    public function __construct() {
	parent::__construct();
    }

    public function uvId($data) {
	$this->db->where('uv_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('uv_doc');
    }

}

?>