<?php

class Menu_model extends MY_Model {

    const tableName = 'menu_left';
    const tableName_pk = 'menu_id';

    public function __construct() {
	parent::__construct();
    }

    public function viewRecordAllMenu() {
	$this->db->limit(8);
	$this->db->order_by($this::tableName_pk, 'ASC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function menubyId($data) {
	$this->db->where('menu_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('menu_name');
    }

}

?>