<?php

class Shop_model extends MY_Model {

    const tableName = 'shopname';
    const tableName_pk = 'shop_id';

    public function __construct() {
        parent::__construct();
    }

    public function shopId($data) {
        $this->db->where('shop_id', $data);
        $querySql = $this->db->get($this::tableName);
        $rewfetch = $querySql->row('shop_name') . ', ' . $querySql->row('shop_area');
        return $rewfetch;
    }

    public function viewRecordAnyRAll($arrvenue, $start_date, $end_date) {
        $this->db
                ->select('*')
                ->from('tble_items')
                ->where($arrvenue)
                ->where('tble_items.items_date >=', $start_date, true)
                ->where('tble_items.items_date <= ', $end_date, true)
                ->order_by('items_date', 'ASC');
        $querySql = $this->db->get();
        return $querySql->result();
    }

    public function reportvenueadminprice($date, $date2, $consig_id) {
        $total = 0;
        $barra = array(
            'items_shop_id' => $consig_id
        );
        $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'purchedItem' => $sf->items_id
            );
            $fietch = $this->pitems_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $taotal += ($fech->pitem_unit * $fech->pitem_rate);
            }
            $total = $taotal;
        }

        return $total;
    }

    public function find_date_all_sum($consig_id) {
        $total = 0;
        $toalpurchased = 0;
        $toalpurchased1 = 0;

        $this->db
                ->select('min(items_date) AS mindate, max(items_date) AS maxdate')
                ->from('tble_items')
                ->where($consig_id);
        $querySql = $this->db->get();
        $fethc = $querySql->row();

        $date = $fethc->mindate;
        $date2 = $fethc->maxdate;

        $fetchbuy = $this->viewRecordAnyRAll($consig_id, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'purchedItem' => $sf->items_id
            );
            $fietch = $this->pitems_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $toalpurchased += ($fech->pitem_unit * $fech->pitem_rate);
            }
            // $total = $taotal;
            $fetchParticlearraydr = array(
                'purchedItem' => $sf->items_id,
                'strans_type' => 2
            );
            $fetchdatat = $this->shoptrans_model->viewRecordAnyR($fetchParticlearraydr);
            foreach ($fetchdatat as $strans) {
                $toalpurchased += $strans->strans_amount;
            }
            $fetchParticlearraycr = array(
                'purchedItem' => $sf->items_id,
                'strans_type' => 1
            );
            $fetchdatatcr = $this->shoptrans_model->viewRecordAnyR($fetchParticlearraycr);
            foreach ($fetchdatatcr as $stranscr) {
                $toalpurchased1 += $stranscr->strans_amount;
            }
        }

        $total = $toalpurchased1 - $toalpurchased;



        return $total;

        //return $total;
    }

    public function find_date_all_sum_trans($consig_id) {
        $total = 0;

        $this->db
                ->select('min(items_date) AS mindate, max(items_date) AS maxdate')
                ->from('shopTrans')
                ->where($consig_id);
        $querySql = $this->db->get();
        $fethc = $querySql->row();

        $date = $fethc->mindate;
        $date2 = $fethc->maxdate;



        $fetchbuy = $this->viewRecordAnyRAll($consig_id, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'purchedItem' => $sf->items_id
            );
            $fietch = $this->pitems_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $taotal += ($fech->pitem_unit * $fech->pitem_rate);
            }
            $total = $taotal;
        }

        return $total;
    }

    public function reportvenueadminpricewithout($date, $date2) {
        $total = 0;
        $totalqty = '';
        $barra = array(
            'invoice_status' => '1'
        );
        $fetchbuy = $this->viewRecordAnyRAll($barra, $date, $date2);
        foreach ($fetchbuy as $sf) {
            $asge = array(
                'itemInvoice_invoice' => $sf->invoice_id
            );
            $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
            foreach ($fietch as $fech) {

                $taotal += ($fech->itemInvoice_kg * $fech->itemInvoice_rate);
            }
            $cgest = $this->gst_model->commongstId($sf->invoice_cgst);
            $sgest = $this->gst_model->commongstId($sf->invoice_sgst);
            $igest = $this->gst_model->commongstId($sf->invoice_igst);
            $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
            $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
            $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
            $total = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
        }

        return $total;
    }

}

?>