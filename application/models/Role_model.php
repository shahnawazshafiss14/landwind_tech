<?php

class Role_model extends MY_Model {

    const tableName = 'role_id';
    const tableName_pk = 'role_id';

    public function __construct() {
	parent::__construct();
    }

    public function roleId($data) {
	$this->db->where('role_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('role_name');
    }

}

?>