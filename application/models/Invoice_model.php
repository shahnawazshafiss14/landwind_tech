<?php

class Invoice_model extends MY_Model {

    const tableName = 'tble_invoice';
    const tableName_pk = 'invoice_id';

    public function __construct() {
        parent::__construct();
    }

    public function taotal($item_id) {
        $total = 0;

        $asge = array(
            'itemInvoice_name' => $item_id
        );
        $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
        foreach ($fietch as $fech) {

            $total += $fech->itemInvoice_max_kg;
        } 
        return !empty($total) ? $total :'0';
    }
    public function subtaotal($consig_id) {
	$total = 0;
	
	    $asge = array(
		'itemInvoice_name' => $consig_id
	    );
	    $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {

		$taotal += $fech->itemInvoice_max_kg;
	    }
	    $total = $taotal;
	

	return $total;
    }
    public function taotalparti($item_id) {
        $total = 0;

        $asge = array(
            'itemInvoice_name' => $item_id
        );
        $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
        foreach ($fietch as $fech) {

            $total += $fech->itemInvoice_max_kg;
        } 
        return !empty($total) ? $total : '0';
    }
    public function taotalpartiparent($item_id) {
        $total = 0;
        $asge = array(
            'itemInvoice_name_p' => $item_id
        );
        $fietch = $this->itemtypeInvoice_model->viewRecordAnyR($asge);
        foreach ($fietch as $fech) {
            $total += $fech->itemInvoice_max_kg;
        } 
        return !empty($total) ? $total : '0';
    }
    public function datewisereport($dad){
        $this->db->select('*');
        $this->db->where($dad);
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
    public function datewisereportcategory($dad){
        $this->db->select('*');
        $this->db->where($dad);
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
    public function datewisereportweekcat($cat){
        $this->db->select('*');
        $this->db->where($cat);
        $this->db->where('`invoice_dos` BETWEEN DATE_ADD(CURDATE(), INTERVAL 1-DAYOFWEEK(CURDATE()) DAY) AND DATE_ADD(CURDATE(), INTERVAL 7-DAYOFWEEK(CURDATE()) DAY)'); 
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
     public function datewisereportweek(){
        $this->db->select('*');
        $this->db->where('`invoice_dos` BETWEEN DATE_ADD(CURDATE(), INTERVAL 1-DAYOFWEEK(CURDATE()) DAY) AND DATE_ADD(CURDATE(), INTERVAL 7-DAYOFWEEK(CURDATE()) DAY)'); 
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function datewisereportmonth(){
        $this->db->select('*');
        $this->db->where('`invoice_dos` BETWEEN DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY) AND LAST_DAY(NOW())'); 
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
    public function datewisereportmonthcat($cat){
        $this->db->select('*');
        $this->db->where($cat);
        $this->db->where('`invoice_dos` BETWEEN DATE_SUB(CURDATE(),INTERVAL (DAY(CURDATE())-1) DAY) AND LAST_DAY(NOW())'); 
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
    public function datewisereportmonthlast(){
        $this->db->select('*');
        $this->db->where('MONTH(`invoice_dos`) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)'); 
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }
    public function datewisereportmonthlastc($cat){
        $this->db->select('*');
        $this->db->where($cat);
        $this->db->where('MONTH(`invoice_dos`) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)'); 
        $this->db->from('itemtypeInvoice');
        $this->db->group_by('itemInvoice_invoice');
        $this->db->join('tble_invoice', 'tble_invoice.invoice_id=itemtypeInvoice.itemInvoice_invoice');
        $query = $this->db->get();
        return $query->result();
    }

   

}

?>