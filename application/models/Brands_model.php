<?php

class Brands_model extends MY_Model {

    const tableName = 'brands';
    const tableName_pk = 'brand_id';

    public function __construct() {
	parent::__construct();
    }

    public function brandId($data) {
	$this->db->where('brand_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('brand_name');
    }

}

?>