<?php

class Event_model extends MY_Model {

    const tableName = 'tblevents';
    const tableName_pk = 'event_id';

    public function __construct() {
	parent::__construct();
    }

    public function viewRecordIdArtistId($data) {
	$this->db->where($data);
	$this->db->select('*');
	$this->db->from('tblartists');
	$this->db->join('tblevents', 'tblevents.artist_id = tblartists.artist_email');
	$query = $this->db->get();
	return $query->row();
    }

    public function viewRecordAnyRAll($arrvenue, $start_date, $end_date) {
	$start = date('Y-m-d');
	if ($start_date == null) {
	    $start_date1 = $start;
	} else if ($start_date == 'From') {
	    $start_date1 = $start;
	} else {
	    $start_date1 = $start_date;
	}

	$end = date('Y-m-d', strtotime('+2 years'));
	if ($end_date == null) {
	    $end_date1 = $end;
	} else if ($end_date == 'Until') {
	    $end_date1 = $end;
	} else {
	    $end_date1 = $end_date;
	}
	$this->db
		->select('*')
		->from('tblevents')
		->where('tblevents.venue_id', $arrvenue, true)
		->where('tblevents.event_start_date >=', $start_date1, true)
		->where('tblevents.event_start_date <= ', $end_date1, true);
	$querySql = $this->db->get();
	return $querySql->result();
    }

    public function viewRecordAnyRAllartis($arrvenue, $start_date, $end_date) {
	$start = date('Y-m-d');
	if ($start_date == null) {
	    $start_date1 = $start;
	} else if ($start_date == 'From') {
	    $start_date1 = $start;
	} else {
	    $start_date1 = $start_date;
	}

	$end = date('Y-m-d', strtotime('+2 years'));
	if ($end_date == null) {
	    $end_date1 = $end;
	} else if ($end_date == 'Until') {
	    $end_date1 = $end;
	} else {
	    $end_date1 = $end_date;
	}
	$this->db
		->select('*')
		->from('tblevents')
		->where('tblevents.artist_id', $arrvenue, true)
		->where('tblevents.event_start_date >=', $start_date1, true)
		->where('tblevents.event_start_date <= ', $end_date1, true);
	$querySql = $this->db->get();
	return $querySql->result();
    }

}

?>