<?php

class Gst_model extends MY_Model {

    const tableName = 'tbl_gst';
    const tableName_pk = 'gst_id';

    public function __construct() {
	parent::__construct();
    }

    public function cgstId($data) {
	$this->db->where('gst_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('cgst');
    }
    public function sgstId($data) {
	$this->db->where('gst_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('sgst');
    }
    public function igstId($data) {
	$this->db->where('gst_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('igst');
    }
    public function commongstId($data) {
	$this->db->where('gst_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row('commongst');
    }

}

?>