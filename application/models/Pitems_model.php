<?php

class Pitems_model extends MY_Model {

    const tableName = 'prodItem';
    const tableName_pk = 'pitem_id';

    public function __construct() {
	parent::__construct();
    }

    public function taotal($consig_id) {
	$total = 0;
	
	    $asge = array(
		'pitem_parent_particular' => $consig_id
	    );
	    $fietch = $this->pitems_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {

		$total += $fech->pitem_unit;
	    }
		return   !empty($total) ? $total : '0';
	

	 
    }
    public function taotalparti($item_id) {
	$total = 0;
	
	    $asge = array(
		'pitem_particular' => $item_id
	    );
	    $fietch = $this->pitems_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {

			$total += $fech->pitem_unit;
	    } 
	return !empty($total) ? $total : '0';
    }
    public function subtaotal($consig_id) {
	$total = 0;
	
	    $asge = array(
		'pitem_particular' => $consig_id
	    );
	    $fietch = $this->pitems_model->viewRecordAnyR($asge);
	    foreach ($fietch as $fech) {

		$total += $fech->pitem_unit;
	    }  
	return !empty($total) ? $total : '';
    }
    public function viewRecordPageRG($data, $group) {
    	$this->db->select('sum(pitem_unit * pitem_rate) as amount, purchedItem', false);
	//$this->db->select_sum('pitem_rate' * 'pitem_unit', 'amount');
	$this->db->where($data);
	$this->db->group_by($group);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$query = $this->db->get($this::tableName);
	return $query->result();
    }
    public function viewRecordPageRGs($data, $group) {
	$this->db->where($data);
	$this->db->group_by($group);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$query = $this->db->get($this::tableName);
	return $query->result();
    }


}

?>