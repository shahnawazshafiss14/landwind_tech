<?php
class Cart_model extends MY_Model{
    const tableName = 'tempcart';
    const tableName_pk = 'tcart_id';
    public function __construct() {
	parent::__construct();
    }
    public function viewRecordAllC() {
	$this->db->select('*');
	$this->db->group_by('tproduct_id');
	$querySql = $this->db->get('tempcart');
	return $querySql->result();
    }
    public function recordUpdateC($col, $data) {
	$this->db->where($col);
	$this->db->update($this::tableName, $data);
    }
}
?>