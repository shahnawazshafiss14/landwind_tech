<?php

class Backcall_model extends CI_Model{

    const tableName = 'tbl_callback';
    const tableName_pk = 'id';

    public function __construct() {
        parent::__construct();
    }

    public function recordInsert($data) {
        $this->db->insert($this::tableName, $data);
        $this->{$this::tableName_pk} = $this->db->insert_id();
    }

}

?>