<?php

class Itemtype_model extends MY_Model {

    const tableName = 'tbl_item_type';
    const tableName_pk = 'item_id';

    public function __construct() {
	parent::__construct();
    }

    public function itemId($data, $rowname) {
	$this->db->where('item_id', $data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row($rowname);
    }

}

?>