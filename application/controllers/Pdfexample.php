<?php

class Pdfexample extends CI_Controller{ 
    public function __construct()
    { 
		parent::__construct();
	}
	public function index() {
		 
		$this->load->library('Pdf');
$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('My Title');
$pdf->SetHeaderMargin(30);
$pdf->SetTopMargin(20);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');

$pdf->AddPage();

$html = '<div class="invoice"> 
   <h2 style="text-align:center; font-size:15px; font-style:italic; font-weight:600; margin-bottom:15px;">INVOICE</h2>
   
    <div class="clear"></div>
   <div style="margin-left:10px;"><b> &nbsp;Order Information :-</b></div>
   <br>
   <table class="middle-invoice-table" width="100%" style="font-family:arial; font-size:10px; line-height:20px;" cellspacing="0" cellpadding="4" border="1">
      <tbody>
         <tr>
            <th>S.No</th>
            <th>Description of Goods</th>
            <th>Quantity</th>
            <th>Rate</th>
			<th>Per</th>
           <th>Disc. %</th>
           <th>Amount</th>
         </tr>
		 <tr>
            <td>S.No</td>
            <td>Description of Goods</td>
            <td>Quantity</td>
            <td>Rate</td>
			<td>Per</td>
           <td>Disc. %</td>
           <td>Amount</td>
         </tr>'; 
		  
      $html .='</tbody>
   </table>
   <div class="clear"></div>
   <br>
   <table class="middle-invoice-table" width="100%" style="font-size:10px" cellspacing="0" cellpadding="4" border="1">
      <tbody>
         <tr>
            <td colspan="3"> Note : <br>
               ContactDelivarable:<br>
               <strong> 1. '.ucfirst($stRow->title).'</strong><br>
            </td>
         </tr>
      </tbody>
   </table>
   <div class="clear"></div>
    
   <table style="font-family:arial; text-align:left; font-size:10px; line-height:20px;" cellspacing="0" cellpadding="4">
      <tbody>
         <tr>
            <td><strong>NOTE :</strong> The cheque should be in favor of.</td>
         </tr>
      </tbody>
   </table>
     <hr>
   <table style="font-family:arial; text-align:left; font-size:10px; line-height:20px;" cellspacing="0" cellpadding="4">
      <tbody>
         <tr>
            <td>'.COMPANYNAME.'</td>
         </tr>
      </tbody>
   </table>
</div>';
ob_end_clean();
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');
ob_clean();
//$pdf->Write(5, 'Some sample text');
$pdf->Output('My-File-Name.pdf', 'D');
	}
}
    ?>