<?php

defined('BASEPATH') OR exit('No direct access');

class Services extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function findpartcat() {
	$val_id = $_POST['val_id'];
	$aarrdat = array(
	    'services_parent_id' => $val_id
	);
	$data['fetch_cat'] = $this->services_model->viewRecordAnyR($aarrdat);
	$this->load->view('admin/ajaxproductservices', $data);
    }

    public function index() {
	$condition = array('services_parent_id' => '0');
	$data['topservicess'] = $this->services_model->viewRecordAnyR($condition);
	$data['head_title'] = 'View Services';

	$data['contentView'] = 'admin/veiw_topServices';
	$this->load->view('admin/_template_model2', $data);
    }

    public function add() {
	$services_id = $this->uri->segment(3);
	$data['contentView'] = 'admin/addServices';
	$condition = array('services_parent_id' => '0');
	$data['parent_services'] = $this->services_model->viewRecordAnyR($condition);
	$data['category_menu'] = $this->category_model->viewRecordAll();
	if (isset($services_id)) {
	    $data['head_title'] = 'Edit Services';
	    $data['servicesObject'] = $this->services_model->viewRecordId($services_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Services';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function topServices() {
	//$menu_id = $this->uri->segment(4);
	$services_id = $this->input->post('serEdit');
	$servicesName = $this->input->post('servicesName');
	$selectServices = $this->input->post('selectServices');
	$services_title = $this->input->post('serTitle');
	$menu_id = $this->input->post('menu_id');
	$services_slug = str_replace(" ", "-", strtolower($services_title));
	$serDescription = $this->input->post('serDescription');
	$imagenameservic = $this->input->post('imagenameservic');
	if (!empty($services_id)) {
	    $adss = array(
		'services_name' => $servicesName,
		'services_parent_id' => '0',
		'services_title' => $services_title,
		'services_link' => $services_slug,
		'menu_id' => $menu_id,
		'serDescription' => $serDescription,
		'services_created_date' => date("Y-m-d H:i:s")
	    );
	    $this->services_model->recordUpdate($services_id, $adss);
	    echo "1";
	} else {
	    if (isset($_FILES["serimage"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/banner/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('serimage')) {
		    echo $this->upload->display_errors();
		} else {
		    $data1 = array('update_data' => $this->upload->data());
		    $ads = array(
			'serimage' => $data1['update_data']['file_name'],
			'services_name' => $servicesName,
			'services_parent_id' => '0',
			'services_title' => $services_title,
			'services_link' => $services_slug,
			'menu_id' => $menu_id,
			'serDescription' => $serDescription,
			'services_created_date' => date("Y-m-d H:i:s")
		    );
		    $this->services_model->recordInsert($ads);
		    echo "1";
		}
	    } else {
		echo "file no upload";
	    }
	}
    }

    public function addServicesc() {
	$services_id = $this->input->post('serEdit');
	if (isset($_FILES["serimage"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/banner/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('serimage')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
		$ads = array(
		    'serimage' => $data1['update_data']['file_name']
		);
		$this->services_model->recordUpdate($services_id, $ads);
		echo "1";
	    }
	} else {
	    echo "file no upload";
	}
    }

    public function deleteServices() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'services_id' => $del_id
	);
	$this->services_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>