<?php

defined("BASEPATH") OR exit('No direct access');

class Pages extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $page_id = $this->uri->segment(3);
        $data['head_title'] = 'Add Pages';
        $data['contentView'] = 'admin/page_add';
        if (isset($page_id)) {
            $data['head_title'] = 'Edit Page';
            $data['pageObject'] = $this->pages_model->viewRecordId($page_id);
            $this->load->view('admin/_template_model3', $data);
        } else {
            $data['head_title'] = 'Add Page';
            $this->load->view('admin/_template_model3', $data);
        }
    }

    public function pageview() {
        $data['head_title'] = 'List Pages';
        $data['pageObject'] = $this->pages_model->viewRecordAll();
        $data['contentView'] = 'admin/veiwpage';
        $this->load->view('admin/_template_model2', $data);
    }

    public function add() {
        $pageEdit = $this->input->post('pageEdit');
        if (!empty($pageEdit)) {
            $datas = array(
                'page_title' => $this->input->post('page_title'),
                'page_description' => $this->input->post('page_description'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '0'
            );
            $this->pages_model->recordUpdate($pageEdit, $datas);
            $this->session->set_flashdata('page_uploaded', 'Your page has been created Successfully');
            redirect('pages/pageview');
        } else {
            $datas = array(
                'page_title' => $this->input->post('page_title'),
                'page_description' => $this->input->post('page_description'),
                'created_date' => date('Y-m-d H:i:s'),
                'status' => '0'
            );
            $this->pages_model->recordInsert($datas);
            $this->session->set_flashdata('page_uploaded', 'Your page has been created Successfully');
            redirect('pages/index');
        }
    }

    public function deletePage() {
        $del_id = $_POST['del_id'];
        $data = array(
            'page_id' => $del_id
        );
        $this->pages_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Deleted!");
    }

}

?>
