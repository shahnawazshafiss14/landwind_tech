<?php

defined('BASEPATH') OR exit('No direct access');

class Gst extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function addColor() {
	$data = $this->input->post('colorName');
	$color_id = $this->input->post('edit');
	if (!empty($color_id)) {
	    $adss = array(
		'color_name' => $data,
		'status' => '1'
	    );
	    $this->gst_model->recordUpdate($color_id, $adss);
	    echo "1";
	} else {
	    $dataColor = array(
		'color_name' => $data
	    );
	    if (!empty($data)) {
		$detailColor = $this->gst_model->recordCheckAvaibility($dataColor);
		if ($detailColor > 0) {
		    echo "Color id Already Exits";
		} else {
		    $ads = array(
			'color_name' => $data,
			'status' => '1'
		    );
		    $this->gst_model->recordInsert($ads);
		    echo "Color added Successfully";
		}
	    }
	}
    }

    public function index() {
	$color_id = $this->uri->segment(3);

	$data['contentView'] = 'admin/colors';
	if (isset($color_id)) {
	    $data['head_title'] = 'Edit Color';
	    $data['colorObject'] = $this->gst_model->viewRecordId($color_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Color';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function colorList() {
	$data['head_title'] = 'View Color';
	$data['contentView'] = 'admin/veiw_colors';
	$data['colorobjects'] = $this->gst_model->viewRecordAll();
	$this->load->view('admin/_template_model2', $data);
    }

    public function deleteColor() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'color_id' => $del_id
	);
	$this->gst_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>