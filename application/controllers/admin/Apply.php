<?php

defined("BASEPATH") OR exit('No direct access');

class Apply extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$apply_id = $this->uri->segment(4);
	$data['head_title'] = 'Add Apply';
	$data['contentView'] = 'admin/apply_add';
	if (isset($apply_id)) {
	    $data['head_title'] = 'Edit Apply';
	    $data['applyObject'] = $this->apply_model->viewRecordId($apply_id);
	    $this->load->view('admin/_template_model3', $data);
	} else {
	    $data['head_title'] = 'Add Apply';
	    $this->load->view('admin/_template_model3', $data);
	}
    }

    public function view() {
	$data['head_title'] = 'List Apply';
	$data['applyObject'] = $this->apply_model->viewRecordAll();
	$data['contentView'] = 'admin/veiwapply';
	$this->load->view('admin/_template_model2', $data);
    }

    public function apply_show_list() {
	$product_id = $this->uri->segment(4);
	$data['applyObject'] = $this->apply_model->viewRecordId($product_id);
	$data['head_title'] = 'Apply Details';
	$data['contentView'] = 'admin/apply_show_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function add() {
	$applyEdit = $this->input->post('applyEdit');
	$cartitl = $this->input->post('apply_title');
	if (!empty($applyEdit)) {
	    $datas = array(
		'apply_title' => $cartitl,
		'apply_description' => $this->input->post('apply_description'),
		'apply_added' => date('Y-m-d H:i:s')
	    );
	    $this->apply_model->recordUpdate($applyEdit, $datas);
	    $this->session->set_flashdata('apply_uploaded', 'Your apply has been created Successfully');
	    redirect('admin/apply/index/' . $applyEdit);
	} else {
	    $datas = array(
		'apply_title' => $cartitl,
		'apply_type' => $this->input->post('apply_type'),
		'apply_description' => $this->input->post('apply_description'),
		'apply_added' => date('Y-m-d H:i:s'),
		'apply_status' => '0'
	    );
	    $this->apply_model->recordInsert($datas);
	    $last_id = $this->db->insert_id();
	    $datas = array(
		'apply_slug' => strtolower(strreplace($cartitl) . '-' . $last_id)
	    );
	    $this->apply_model->recordUpdate($last_id, $datas);
	    $this->session->set_flashdata('apply_uploaded', 'Your apply has been created Successfully');
	    redirect('admin/apply/index');
	}
    }

    public function deleteApply() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'apply_id' => $del_id
	);
	$this->apply_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Deleted!");
    }

}

?>
