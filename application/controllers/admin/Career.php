<?php

defined("BASEPATH") OR exit('No direct access');

class Career extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$career_id = $this->uri->segment(4);
	$data['head_title'] = 'Add Careers';
	$data['contentView'] = 'admin/career_add';
	if (isset($career_id)) {
	    $data['head_title'] = 'Edit Career';
	    $data['careerObject'] = $this->career_model->viewRecordId($career_id);
	    $this->load->view('admin/_template_model3', $data);
	} else {
	    $data['head_title'] = 'Add Career';
	    $this->load->view('admin/_template_model3', $data);
	}
    }

    public function view() {
	$data['head_title'] = 'List Careers';
	$data['careerObject'] = $this->career_model->viewRecordAll();
	$data['contentView'] = 'admin/veiwcareer';
	$this->load->view('admin/_template_model2', $data);
    }

    public function add() {
	$careerEdit = $this->input->post('careerEdit');
	$cartitl = $this->input->post('career_title');
	if (!empty($careerEdit)) {
	    $datas = array(
		'career_title' => $cartitl,
		'career_description' => $this->input->post('career_description'),
		'career_added' => date('Y-m-d H:i:s')
	    );
	    $this->career_model->recordUpdate($careerEdit, $datas);
	    $this->session->set_flashdata('career_uploaded', 'Your career has been created Successfully');
	    redirect('admin/career/index/' . $careerEdit);
	} else {
	    $datas = array(
		'career_title' => $cartitl,
		'career_type' => $this->input->post('career_type'),
		'career_description' => $this->input->post('career_description'),
		'career_added' => date('Y-m-d H:i:s'),
		'career_status' => '0'
	    );
	    $this->career_model->recordInsert($datas);
	    $last_id = $this->db->insert_id();
	    $datas = array(
		'career_slug' => strtolower(strreplace($cartitl) . '-' . $last_id)
	    );
	    $this->career_model->recordUpdate($last_id, $datas);
	    $this->session->set_flashdata('career_uploaded', 'Your career has been created Successfully');
	    redirect('admin/career/index');
	}
    }

    public function deleteCareer() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'career_id' => $del_id
	);
	$this->career_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Deleted!");
    }

}

?>
