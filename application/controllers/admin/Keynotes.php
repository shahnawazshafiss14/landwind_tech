<?php

defined('BASEPATH') OR exit('No direct access');

class Keynotes extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function statuscheck() {
        $pid = $this->input->post('pid');
        $datas = array(
            'proj_check' => $this->input->post('val_id')
        );
        $this->project_model->recordUpdate($pid, $datas);
    }

    public function proj_fillter() {
        if (!empty($_POST)) {
            $get_start_date = $this->input->post('proj_dos_from');
            $get_end_date = $this->input->post('proj_dos_to');
            $proj_no = $this->input->post('proj_number');
            $proj_vehicle_no = $this->input->post('proj_vehicle_no');
            $doc_receiver_id = $this->input->post('doc_receiver_id');
            $proj_cgst = $this->input->post('proj_cgst');
            $proj_sgst = $this->input->post('proj_sgst');
            $proj_igst = $this->input->post('proj_igst');
            if (!empty($proj_cgst)) {
                $proj_cgst_sql = " AND proj_cgst='" . $proj_cgst . "'";
            }
            if (!empty($proj_sgst)) {
                $proj_sgst_sql = " AND proj_sgst='" . $proj_sgst . "'";
            }
            if (!empty($proj_igst)) {
                $proj_igst_sql = " AND proj_igst='" . $proj_igst . "'";
            }
            if (!empty($doc_receiver_id)) {
                $proj_consig_sql = " AND proj_consig='" . $doc_receiver_id . "'";
            }
            if (!empty($proj_no)) {
                $proj_no_sql = " AND proj_number='" . $proj_no . "'";
            }
            if (!empty($proj_vehicle_no)) {
                $proj_vehicle_no_sql = " AND proj_vehicle_no='" . $proj_vehicle_no . "'";
            }
            if (!empty($get_start_date) && !empty($get_end_date)) {
                $get_date_sql = " AND (date_format(proj_dos, '%Y-%m-%d') BETWEEN '" . $get_start_date . "' AND '" . $get_end_date . "')";
            }
            if (empty($get_start_date) && empty($get_end_date) && empty($proj_no) && empty($proj_vehicle_no) && empty($doc_receiver_id) && empty($proj_cgst) && empty($proj_sgst) && empty($proj_igst)) {
                $data['notrecordfound'] = "Please select any one field";
            } else {
                $select = "account_id from tble_proj where view_status='1' AND proj_status=1" . $proj_no_sql . $get_date_sql . $proj_vehicle_no_sql . $proj_consig_sql . $proj_cgst_sql . $proj_sgst_sql . $proj_igst_sql;
                   $data['projObjects'] = $this->project_model->viewRecordGCINR($select);
            }
            $data['proj_cgst'] = $proj_cgst;
            $data['proj_sgst'] = $proj_sgst;
            $data['proj_igst'] = $proj_igst;
            $data['proj_dos_from'] = $get_start_date;
            $data['proj_dos_to'] = $get_end_date;
            $data['proj_number'] = $proj_no;
            $data['doc_receiver_id'] = $doc_receiver_id;
         
        }
        $data['head_title'] = 'Projects fillter report';
        $data['contentView'] = 'admin/proj_fillter_details';
        $this->load->view('admin/_template_model2', $data);
    }

    public function createproject() {
        $proj_id = $this->uri->segment(4);
        $data['objItem'] = $this->itemtype_model->viewRecordAll();
        $data['contentView'] = 'admin/add_project';
        if (isset($proj_id)) {
            $data['head_title'] = 'Edit Project';
            $slugfie = array(
                'proj_slug' => $proj_id
            );
            $data['projectObject'] = $this->project_model->viewRecordAny($slugfie);
            $this->load->view('admin/_template_model3', $data);
        } else {
            $data['head_title'] = 'Add Project';
            $this->load->view('admin/_template_model3', $data);
        }
    }

    public function sendProject() {
        $fetch = $this->input->post('slug_id');
        $sendemail = $this->input->post('sendemail');
        $data['subjectto'] = 'Project ';
        $data['maito'] = $sendemail;
        $data['email_messagem'] = '
			 <tr>
				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="http://crm.sonuenterprisessiwan.com/projs/' . $fetch . '" target="_blank">Print Project</a></h2></td>
			</tr>';
        $this->load->view('mail/mail_template', $data);
        echo "Project has been sent";
    }

    public function proj_show_list() {
        $proj_slug = $this->uri->segment(4);
        $fetchproj = array(
            'proj_slug' => $proj_slug
        );
        $data['projObject'] = $this->project_model->viewRecordAny($fetchproj);
        $data['head_title'] = 'Project Details';
        $data['contentView'] = 'admin/proj_show_list';
        $this->load->view('admin/_template_model2', $data);
    }
    public function generateInvoice() {
        $proj_client = $this->input->post('proj_client');
        $proj_id = $this->input->post('proj_id');
        $searchIDs = $this->input->post('searchIDs');
        $strval =  $searchIDs;
        $invoice_no = time(); 
        for($i = 0; $i < count($strval); $i++){
            $strval_update =$strval[$i]; 
            $data = array('status' => '2', 'invoice_no' => $invoice_no);
            $this->projectservice_model->recordUpdate($strval_update, $data);
        }

        echo '1';
die();

        $proj_slug = $this->uri->segment(4);
        $fetchproj = array(
            'proj_slug' => $proj_slug
        );
        $data['projObject'] = $this->project_model->viewRecordAny($fetchproj);
        $data['head_title'] = 'Project Details';
        $data['contentView'] = 'admin/proj_show_list';
        //$this->load->view('admin/_template_model2', $data);
    }

    public function projectList() {

        if ($this->session->userdata('admin_type') == '2') {
            $fetch = array(
                'view_status' => '1',
                'proj_check' => '1',
                'account_id' => $this->session->userdata('account_id')
            );
        } else {
            $fetch = array(
                'view_status' => '1',
                'account_id' => $this->session->userdata('account_id')
            );
        }
        $data["projObjects"] = $this->project_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Project ';
        $data['contentView'] = 'admin/view_projs_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function reports() {
        $fetch = array(
            'view_status' => '1'
        );
        $data["projObjects"] = $this->project_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Reports monthly Project ';
        $data['contentView'] = 'admin/view_reports_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function gstpay() {
        $fetch = array(
            'view_status' => '1'
        );
        $data["projObjects"] = $this->project_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Reports monthly Project ';
        $data['contentView'] = 'admin/view_paygst_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function addProjects() {
        $datasession = $this->session->userdata('userName_sess');
        $account_id = $this->session->userdata('account_id');
        $editproject = $this->input->post('editproject'); 
        $dor_id = $this->input->post('dor_id');  
        $proj_title = $this->input->post('proj_title');  
        $proj_price = $this->input->post('proj_price');  
        $proj_start_date = $this->input->post('proj_start_date');  
        $proj_end_date = $this->input->post('proj_end_date');  
        $proj_dos = $this->input->post('proj_dos');  
        $proj_pos = $this->input->post('proj_pos');  
        $proj_production_url = $this->input->post('proj_production_url');  
        $proj_development_url = $this->input->post('proj_development_url');  
        $proj_description = $this->input->post('proj_description'); 
        $slug = url_title($proj_title);  
        $data_i = count($particuler_id);
        $orders = array();
        if (!empty($editproj)) {
            $pdata = array(
                'account_id' => $account_id,  
                'proj_slug' => strtolower($slug),  
                'proj_client' => $dor_id,
                'proj_start_date' => $proj_start_date, 
                'proj_end_date' => $proj_end_date, 
                'proj_title' => $proj_title, 
                'proj_price' => $proj_price, 
                'proj_development_url' => $proj_development_url, 
                'proj_production_url' => $proj_production_url, 
                'proj_description' => $proj_description, 
                'proj_dos' => $proj_dos, 
                'proj_pos' => $proj_pos,  
                'updated_at' => date("Y-m-d H:i:s") 
            );
            $editproj1 = array(
                'proj_slug' => $editproject
            );
            $this->project_model->recordUpdateAny($editproj1, $pdata);
            $this->session->set_flashdata("message", "Project Updated Successfully!");
            redirect('admin/projs/proj_show_list/' . $editproj);
        } else {
            $pdata = array(
                'account_id' => $account_id, 
                'proj_slug' => strtolower($slug), 
                'proj_client' => $dor_id,
                'proj_start_date' => $proj_start_date, 
                'proj_end_date' => $proj_end_date, 
                'proj_title' => $proj_title, 
                'proj_price' => $proj_price, 
                'proj_development_url' => $proj_development_url, 
                'proj_production_url' => $proj_production_url, 
                'proj_description' => $proj_description, 
                'proj_dos' => $proj_dos, 
                'proj_pos' => $proj_pos,  
                'updated_at' => date("Y-m-d H:i:s"), 
                'created_at' => date("Y-m-d H:i:s") 
            );
            $this->project_model->recordInsert($pdata);
            $this->session->set_flashdata("message", "Project has Added! Please add items");
            redirect('admin/project/createproject/' . $slug);
        }
    }

    public function additemtyem() {
        $account_id = $this->session->userdata('account_id');
        $term_id = $this->input->post('term_id'); 
        $client_id = $this->input->post('client_id'); 
        $project_id = $this->input->post('project_id'); 
        $term_value = $this->input->post('term_value');
        $particuler_id = $this->input->post('particuler_id');
        $amount = $this->input->post('amount');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        if (!empty($particuler_id)) {
            $pdata = array(
                'account_id' => $account_id,
                'client_id' => $client_id,
                'project_id' => $project_id,
                'service_duration' => $term_id,
                'service_duration_value' => $term_value,
                'item_id' => $particuler_id,
                'item_price' => $amount,
                'service_start_date' => $start_date,
                'service_end_date' => $end_date,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')
            );
            $this->projectservice_model->recordInsert($pdata);
            echo '1';
        }
    }

    public function resizing($path, $file) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 336;
        $config['new_image'] = './assests/event/thumb/' . $file;
        $this->load->library('image_lib', $config);
        $this->image_lib->initialize();
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    public function delete_projItem() {
        $del_id = $_POST['del_id'];
        $data = array(
            'itemProject_id' => $del_id
        );
        $this->itemtypeproject_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Updated!");
    }

    public function deleteProject() {
        $del_id = $_POST['del_id'];
        $data = array(
            'proj_id' => $del_id
        );
        $this->project_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Deleted!");
    }

}

?>