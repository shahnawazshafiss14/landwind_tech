<?php

defined('BASEPATH') OR exit('No direct access');
class Sales extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
		$bil_sal = array(
		'view_status' => '1'
		);
		$data['sales'] = $this->billing_model->viewRecordAnyR($bil_sal);
		$data['head_title'] = 'View Vouchar';
		$data['contentView'] = 'admin/billList';
		$this->load->view('admin/_template_model2', $data);
    }
	public function ajaxBillprod(){
		$bill_prod = $this->input->post('bill_item'); 
		$bill_prod_arr = array(
			'p_name' => $bill_prod 
		);
		$fetch_data = $this->product_model->viewRecordAny($bill_prod_arr);
		$uits = $this->unit_model->unitbyId($fetch_data->p_unit);
		//'qty' =>  $fetch_data->p_qty,
		$fetch_d = "<input type='text' name='bill_qty[]' onkeyup='myFunction(this.value, $fetch_data->p_qty, $fetch_data->p_unit, \"$uits\")' class='form-control bill_qty number' id='bill_qty' data-qty='$fetch_data->p_qty' data-unit='$uits' />";
		$fetch_d_discount = "<input type='text' name='bill_discount[]' onkeyup='myFunctiondis(this.value, $fetch_data->p_qty, $fetch_data->p_unit, \"$uits\")' class='form-control bill_discount price' id='bill_discount' />";
		$divprice = "<input type='text' name='bill_price[]' onkeyup='myFunctionprice(this.value, $fetch_data->p_qty, $fetch_data->p_unit, \"$uits\")' class='form-control bill_price price' id='bill_price' />";
		
		echo json_encode(
				array(
					'status' => '1',
					'price' => $fetch_data->p_price,
					'units' => $uits,
					'qty' => $fetch_d,
					'discount' => $fetch_d_discount,
					'divprice' => $divprice
				));
	}
	
	
	public function ajaxprint(){
		 
		 $bill_no = $this->input->post('chkselectp');
		 $bill_no_e = explode(',',$bill_no);
		 
		if(count($bill_no_e) > 0){
			for($i = 0; $i < count($bill_no_e); $i++){
				
				$id = array(
					'bill_id' => $bill_no_e[$i]
				);
				$bill_details = $this->billing_model->viewRecordAny($id);
				$data = array(
				'bill_status' =>  '2',
				'bill_id' => $bill_no_e[$i],
				);
				$this->billing_model->recordUpdateAny($id, $data);
				$instdata = array(
					'bs_billing_id' => $bill_no_e[$i],
					'bs_adduserid' => $this->session->userdata('userName_sess'),
					'bs_congsinee' => $bill_details->bill_consignee,
					'bs_status' => '2',
					'bs_created_on' => date('Y-m-d H:i:s')
				);
				$this->billingstatus_model->recordInsert($instdata);
			}
			
			echo json_encode(array('status' => '1', 'message' =>'Item has been printed!'));
		}else{
			echo json_encode(array('status' => '0', 'message' =>'Goes somthing wrongs!'));
		} 
	}
	
	public function ajaxprintExprot(){
		$checkpi = $this->input->post('chkselectp');
		$chkselectp = implode(',', $this->input->post('chkselectp'));
		$userid = $this->input->post('selParty');
		$productid = $this->input->post('selProduct');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		if(count($checkpi) > 0 && !empty($start_date) && !empty($end_date)){ 
		$where = ""; 
		if($productid == 'All'){
			$where .= " AND bill_product_id !='".$productid."'";
		}else{
			$where .= " AND bill_product_id='".$productid."'";
		}		
		if($userid == 'All'){
			$where .= " AND bill_consignee !='".$userid."'";
		}else{
			$where .= " AND bill_consignee='".$userid."'";
		}
		
		$select = "bill_id from tbl_billing where bill_id IN(".$chkselectp.") AND bill_status=3 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		 
		$fetch_sql = $this->billing_model->viewRecordGCINR($select); 
			$this->load->library('Pdf');
			$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
			$pdf->SetTitle('My Title');
			$pdf->SetHeaderMargin(30);
			$pdf->SetTopMargin(20);
			$pdf->setFooterMargin(20);
			$pdf->SetAutoPageBreak(true);
			$pdf->SetAuthor('Author');
			$pdf->SetDisplayMode('real', 'default');

			$pdf->AddPage();

			$html = '<div class="invoice"> 
			   <h2 style="text-align:center; font-size:15px; font-style:italic; font-weight:600; margin-bottom:15px;">INVOICE</h2>
			   
				<div class="clear"></div>
			   <div style="margin-left:10px;"><b> &nbsp;Order Information :-</b></div>
			   <br>
			   <table class="middle-invoice-table" width="100%" style="font-family:arial; font-size:10px; line-height:20px;" cellspacing="0" cellpadding="4" border="1">
				  <tbody>
					 <tr>
						<th>S.No</th>
						<th>Bill No.</th>
						<th>Description of Goods</th>
						<th>Quantity/unit</th>
						<th>Rate</th> 
					   <th>Disc. %</th>
					   <th>Amount</th>
					 </tr>';
					 $i = 1;
					 $total= 0;
					 foreach($fetch_sql as $fetbp){
					$bill_prod_arr = array(
						'p_product_id' => $fetbp->bill_product_id 
					);
					///$total += $fetbp->bill_qty * $fetbp->bill_price;
					$fetch_data_p = $this->product_model->viewRecordAny($bill_prod_arr);
					
					$subtotal = ($fetbp->bill_qty * $fetch_data_p->p_qty) * $fetbp->bill_price;
					$subtotal_dis = $subtotal * $fetbp->bill_discount / 100;
					$subtotal_dis_after = $subtotal - $fetbp->bill_discount;
					$total += ($subtotal - $fetbp->bill_discount);
					
					$html .= '<tr>
						<td>'. $i++.'</td>
						<td>'.$fetbp->bill_id.'</td>
						<td>'.$fetch_data_p->p_name.'</td>
						<td>'.$fetbp->bill_qty.'/'.$fetbp->bill_qty * $fetch_data_p->p_qty .' '.$this->unit_model->unitbyId($fetch_data_p->p_unit).'</td>
						<td>'.$fetbp->bill_price.'</td> 
					   <td>'.$fetbp->bill_discount.'</td>
					   <td>'.$subtotal_dis_after.'</td>
					 </tr>';
					 }		 
					 $html .= '<tr>
						<td colspan="6">Total</td>
						<td>'.$total.'</td>
					 </tr>'; 
					  
				  $html .='</tbody>
			   </table>
			   <div class="clear"></div>
			   <br>
			   <table class="middle-invoice-table" width="100%" style="font-size:10px" cellspacing="0" cellpadding="4" border="1">
				  <tbody>
					 <tr>
						<td colspan="3"> Note : <br>
						   ContactDelivarable:<br>
						   <strong> 1. '.ucfirst($stRow->title).'</strong><br>
						</td>
					 </tr>
				  </tbody>
			   </table>
			   <div class="clear"></div>
				
			   <table style="font-family:arial; text-align:left; font-size:10px; line-height:20px;" cellspacing="0" cellpadding="4">
				  <tbody>
					 <tr>
						<td><strong>NOTE :</strong> The cheque should be in favor of.</td>
					 </tr>
				  </tbody>
			   </table>
				 <hr>
			   <table style="font-family:arial; text-align:left; font-size:10px; line-height:20px;" cellspacing="0" cellpadding="4">
				  <tbody>
					 <tr>
						<td>'.COMPANYNAME.'</td>
					 </tr>
				  </tbody>
			   </table>
			</div>';
			ob_end_clean();
			// output the HTML content
			$pdf->writeHTML($html, true, false, true, false, '');
			ob_clean();
			//$pdf->Write(5, 'Some sample text');
			$file_name = date('d-M-Y').'-'. date('H-i-A').'.pdf';
			$pdf->Output($file_name, 'D');
					 echo 1; 
		}else{
			echo "somthing is wrong";
		}
	}
	public function fillPusers(){
		$userid = $this->input->post('selParty');
		$productid = $this->input->post('selProduct');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		$where = "";
		
		if($productid == 'All'){
			$where .= " AND bill_product_id !='".$productid."'";
		}else{
			$where .= " AND bill_product_id='".$productid."'";
		}		
		if($userid == 'All'){
			$where .= " AND bill_consignee !='".$userid."'";
		}else{
			$where .= " AND bill_consignee='".$userid."'";
		}
		
		$select = "bill_id from tbl_billing where bill_status=2 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		 
		$html = '<div class="row">';
		 
		//if($admin_type == 2):
		$fetch_bill = $this->billing_model->viewRecordGCINR($select); 
		//$fetch_bill = $this->billing_model->viewRecordAnyRG($data_arry_bil, 'bill_no');
		if(count($fetch_bill) > 0){
			 
		foreach($fetch_bill as $fetc_bill):
		$asd = array(
			'bill_product_id' => $fetc_bill->bill_product_id,
			'bill_consignee' =>  $fetc_bill->bill_consignee
		);
		$product_id = array();
		$total_ctn = 0;
		$fetch_bill_f = $this->billing_model->viewRecordAnyR($asd);
		foreach($fetch_bill_f as $fd){
			$product_id[] = $fd->bill_product_id;
			$total_ctn += $fd->bill_qty;
		}
		$ima = implode(',', $product_id);
		$aes_pro = $this->product_model->viewRecordGCIN('GROUP_CONCAT(p_name SEPARATOR ",") as product_name from tblproduct where p_product_id IN('.$ima.')');
		
		$html .= '<div class="col-lg-3 col-xs-6 divcheck"> 
			<div class="checkbox">
				<label><input type="checkbox" name="chkselectp[]" class="chkselectp" value="'.  $fetc_bill->bill_no .'"></label>
			</div>
			<!-- small box -->
          <div class="small-box bg-yellow" style="cursor: pointer">
			<div class="inner">
				<h4> '. $this->user_m_f->userbyId($fetc_bill->bill_consignee) .'</h4>				
				<h4>' . $aes_pro->product_name .' </h4>
				<h4> Bill no: #'. $fetc_bill->bill_id .' </h4>
				<h5> ';
				$fet = explode(',', $aes_pro->product_name);
				 
				$html .= $total_ctn. ' CTN</h5>
				<h5>Print</h5>
			</div>
           
            
          </div>
        </div>';
		 
		endforeach;
	  echo $html .='</div>';
		}else{
			echo '1';
		}
	  
	}
	public function fillUusers(){
		
		$userid = $this->input->post('selParty');
		$productid = $this->input->post('selProduct');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		$where = ""; 
		if($productid == 'All'){
			$where .= " AND bill_product_id !='".$productid."'";
		}else{
			$where .= " AND bill_product_id='".$productid."'";
		}		
		if($userid == 'All'){
			$where .= " AND bill_consignee !='".$userid."'";
		}else{
			$where .= " AND bill_consignee='".$userid."'";
		}
		
		$select = "bill_no from tbl_billing where bill_status=1 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		 
		 	
		$html = '<div class="row">';
		 
		//if($admin_type == 2):
		$fetch_bill = $this->billing_model->viewRecordGCINR($select); 
		//$fetch_bill = $this->billing_model->viewRecordAnyRG($data_arry_bil, 'bill_no');
		if(count($fetch_bill) > 0){
			
		$html = '<div class="row">';
		 
		 
		
		foreach($fetch_bill as $fetc_bill):
		$asd = array(
			'bill_no' => $fetc_bill->bill_no,
			'bill_consignee' => $fetc_bill->bill_consignee
		);
		$product_id = array();
		$total_ctn = 0;
		$fetch_bill_f = $this->billing_model->viewRecordAnyR($asd);
		foreach($fetch_bill_f as $fd){
			$product_id[] = $fd->bill_product_id;
				$total_ctn += $fd->bill_qty;
			
		}
		$ima = implode(',', $product_id);
		$aes_pro = $this->product_model->viewRecordGCIN('GROUP_CONCAT(p_name SEPARATOR ",") as product_name from tblproduct where p_product_id IN('.$ima.')');
		
		$html .= '<div class="col-lg-3 col-xs-6 divcheck"> 
			<div class="checkbox">
				<label><input type="checkbox" name="chkselectp[]" class="chkselectp" value="'.  $fetc_bill->bill_no .'"></label>
			</div>
			<!-- small box -->
          <div class="small-box bg-red" style="cursor: pointer">
			<div class="inner">
				<h4> '. $this->user_m_f->userbyId($fetc_bill->bill_consignee) .'</h4>
				<h4>' . $aes_pro->product_name .' </h4>
				<h4>Bill no: # '. $fetc_bill->bill_id .' </h4>
				<h4> ';
				$fet = explode(',', $aes_pro->product_name);
				 
				$html .= $total_ctn . ' CTN</h4>
				<h4>Undelivered</h4>
			</div>
           
            
          </div>
        </div>';
		 
		endforeach;
		
		 
     echo $html .='</div>';
		}else{
			echo '1';
		}
	}
	public function fillDusers(){
		
		$userid = $this->input->post('selParty');
		$productid = $this->input->post('selProduct');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		$where = "";
		
		if($productid == 'All'){
			$where .= " AND bill_product_id !='".$productid."'";
		}else{
			$where .= " AND bill_product_id='".$productid."'";
		}		
		if($userid == 'All'){
			$where .= " AND bill_consignee !='".$userid."'";
		}else{
			$where .= " AND bill_consignee='".$userid."'";
		}
		
		$select = "bill_no from tbl_billing where bill_status=3 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		 
		$html = '<div class="row">';
		$admin_type = $this->session->userdata('admin_type');
		//if($admin_type == 2):
		$fetch_bill = $this->billing_model->viewRecordGCINR($select); 
		//$fetch_bill = $this->billing_model->viewRecordAnyRG($data_arry_bil, 'bill_no');
		if(count($fetch_bill) > 0){
			foreach($fetch_bill as $fetc_bill):
			$asd = array(
				'bill_product_id' => $fetc_bill->bill_product_id,
				'bill_consignee' =>  $fetc_bill->bill_consignee
			);
			$product_id = array();
			$total_ctn = 0;
			$fetch_bill_f = $this->billing_model->viewRecordAnyR($asd);
			foreach($fetch_bill_f as $fd){
				$product_id[] = $fd->bill_product_id;
				$total_ctn += $fd->bill_qty;
			}
			$asd_dev = array(
				'bs_billing_id' => $fetc_bill->bill_no,
				'bs_status' => '3'
			);
				$fetc_devey = $this->billingstatus_model->viewRecordAny($asd_dev);
			$ima = implode(',', $product_id);
			$aes_pro = $this->product_model->viewRecordGCIN('GROUP_CONCAT(p_name SEPARATOR ",") as product_name from tblproduct where p_product_id IN('.$ima.')');
			
			$html .= '<div class="col-lg-3 col-xs-6 divcheck"> 
				<div class="checkbox">
					<label><input type="checkbox" name="chkselectp[]" class="chkselectp" value="'. $fetc_bill->bill_no .'"></label>
				</div>
				<!-- small box -->
			  <div class="small-box bg-blue" style="cursor: pointer">
				<div class="inner">
					<h4> '. $this->user_m_f->userbyId($fetc_bill->bill_consignee) .'</h4>
					<h4>' . $aes_pro->product_name .' </h4>
					<h4> ';
					$fet = explode(',', $aes_pro->product_name);
					 
					$html .= $total_ctn. ' CTN</h4>
					<h4>Bill No.#'.$fetc_bill->bill_id.'</h4>
					<h4>Delivery BY: '. $this->devl_model->devlbyId($fetc_bill->bill_assigned_to) .'</h4>
					<h4>Delivered on : '. $fetc_devey->bs_created_on .'</h4>
				</div>
			   
				
			  </div>
			</div>';
			 
			endforeach;
			///endif;
			echo $html .='</div>';
		}else{
			echo '1';
		}
		
		 
      
	}
	public function fillNusers(){
		$userid = $this->input->post('selParty');
		$productid = $this->input->post('selProduct');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date'); 
		$where = ""; 
		if($productid == 'All'){
			$where .= " AND bill_product_id !='".$productid."'";
		}else{
			$where .= " AND bill_product_id='".$productid."'";
		}		
		if($userid == 'All'){
			$where .= " AND bill_consignee !='".$userid."'";
		}else{
			$where .= " AND bill_consignee='".$userid."'";
		} 
		$select = "bill_no from tbl_billing where (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		 
		$html = '<div class="row">'; 
		 
		$fetch_bill = $this->billing_model->viewRecordGCINR($select); 
		 
		if(count($fetch_bill) > 0){
		foreach($fetch_bill as $fetc_bill):
			$asd = array(
				'bill_product_id' => $fetc_bill->bill_product_id
			);
		$product_id = array();
		$total_qty = 0;
		$fetch_bill_f = $this->billing_model->viewRecordAnyR($asd);
		foreach($fetch_bill_f as $fd){
			$product_id[] = $fd->bill_product_id;
			$total_qty += $fd->bill_qty;
		}
		$ima = implode(',', $product_id);
		$aes_pro = $this->product_model->viewRecordGCIN('GROUP_CONCAT(p_name SEPARATOR ",") as product_name from tblproduct where p_product_id IN('.$ima.')');
		 
		$html .= '<div class="col-lg-2 col-xs-2 chkselectp">'; 
			
			if($fetc_bill->bill_status ==1){
				$divcol = "red";
				$status = "unDelivered";
			}else if($fetc_bill->bill_status ==2){
				$divcol = "yellow";
				$status = "Print";
				$assign ='Assign To ';
				$asd_dev = array(
			'bs_billing_id' => $fetc_bill->bill_no,
			'bs_status' => '2'
			);
			$fetc_devey = $this->billingstatus_model->viewRecordAny($asd_dev);
			$bs_created_on = 	$fetc_devey->bs_created_on;
			}else if($fetc_bill->bill_status ==3){
				$divcol = "blue";
				$status = "Delivered";
				$assign = "Del By";
				
				$asd_dev = array(
			'bs_billing_id' => $fetc_bill->bill_no,
			'bs_status' => '3'
		);
		$fetc_devey = $this->billingstatus_model->viewRecordAny($asd_dev);
		$bs_created_on = 	$fetc_devey->bs_created_on;
			
			}
			 
			
			
			
			
          $html .= '<div class="small-box bg-'.$divcol.'">
			<div class="inner">';
			$html .= '<h6><label><div class="checkbox">';
			 
			if($fetc_bill->bill_status ==1){
			 
			$html .= 'Bill no: # '.$fetc_bill->bill_id ;
			
			}else if($fetc_bill->bill_status ==2){
				
				$html .= 'Print Select Bill no: # '. $fetc_bill->bill_id.' </label>';
				
			}else{	
			
			$html .=  ucwords($assign)   .' : '.$this->devl_model->devlbyId($fetc_bill->bill_assigned_to) .' Bill No.# '. $fetc_bill->bill_id;
				
				
			}

			$html .= '</div></label></h6>
			<h6> '. $this->user_m_f->userbyId($fetc_bill->bill_consignee) .'</h6>
				<h6> '. $aes_pro->product_name .' </h6>
				<h6>';
				$fet = explode(',', $aes_pro->product_name);
				
				$html .= $total_qty .' CTN</h6>';
				
			if($fetc_bill->bill_status !=3){
			
				 $html .= '<h6>'. $status .'</h6>';
			}
			else{
			$html .=  $status ." on ".$bs_created_on;
			}
			
			 $html .= '</div>
           
            
          </div>
        </div>';
		
		endforeach;
		 
		
       echo $html .= '</div>';
		}else{
			echo '1';
		} 
      
	}
	public function ajaxdevupda(){
		 
		 $bill_no = $this->input->post('chkselectp');
		 $devlboy = $this->input->post('devlboy');
		 $bill_no_e = explode(',',$bill_no);
		 
		if(count($bill_no_e) > 0){
			for($i = 0; $i < count($bill_no_e); $i++){
				$id = array(
					'bill_id' => $bill_no_e[$i]
				);
				$bill_details = $this->billing_model->viewRecordAny($id);
				$data = array(
				'bill_status' =>  '3',
				'bill_id' => $bill_no_e[$i],
				'bill_assigned_to' => $devlboy
				);
				$this->billing_model->recordUpdateAny($id, $data);
				$instdata = array(
					'bs_billing_id' => $bill_no_e[$i],
					'bs_adduserid' => $this->session->userdata('userName_sess'),
					'bs_congsinee' => $bill_details->bill_consignee,
					'bs_status' => '3',
					'bs_created_on' => date('Y-m-d H:i:s')
				);
				$this->billingstatus_model->recordInsert($instdata);
			}
			
			echo json_encode(array('status' => '1', 'message' =>'Item has been Send!'));
		}else{
			echo json_encode(array('status' => '0', 'message' =>'Goes somthing wrongs!'));
		}
		
		
	}
	public function ajaxdelivery(){
		 $bill_no = $this->input->post('chkselectp');
		 $bill_no_e = explode(',',$bill_no);
		 $userfill = array(
				'view_status' => '1',
				'devlStatus' => '1'
			  );
			  $fetch_sql = $this->devl_model->viewRecordAnyR($userfill);
		  echo '<div class="modal-content">
	  
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Delivery Boy</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div id="example1_filter" class="dataTables_filter">
              <label>Filter Data:  
			  <select name="selDevlBoy" id="selDevlBoy" class="form-control">
			  <option value="">Select Delivery Boy</option>';
			  
			  foreach($fetch_sql as $feach_s){
			   
			  echo '<option value="'.$feach_s->d_id .'"> '.$feach_s->devlName .'</option>';
			   
			  }
			  echo '</select>
			  
			  
			 </label>
            </div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> | <button type="button" class="btn btn-primary" id="btnitemsend">Send</button> 
        </div>
      </div>'; 
	}
	public function dashboar_top(){

		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		
		 
		$html = '<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">';
			  $select_a = "bill_no from tbl_billing where (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		$fetch_bill_a = $this->billing_model->viewRecordGCINR($select_a); 
			  
            $html .= '<h3>'. count($fetch_bill_a) .'</h3>
              <p>All Vouchers</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="'. base_url() .'admin/sales/newvouchers?from='.$start_date.'&to='. $end_date.'" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
               ';
			  $select_p = "bill_no from tbl_billing where bill_status=1 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where."";
		$fetch_bill_u = $this->billing_model->viewRecordGCINR($select_p); 
			  
            $html .= '<h3>'. count($fetch_bill_u) .'</h3>  
			   

              <p>Undelivered</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="'. base_url() .'admin/sales/undelivered?from='.$start_date.'&to='. $end_date.'" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">';
                
                $select_d = "bill_no from tbl_billing where bill_status=2 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where." group by bill_no";
		$fetch_bill_p1 = $this->billing_model->viewRecordGCINR($select_d); 
			  
            $html .= '<h3>'. count($fetch_bill_p1) .'</h3>  
			   

              <p>Print</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="'. base_url() .'admin/sales/print1?from='.$start_date.'&to='. $end_date.'" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">';
                $select_w = "bill_no from tbl_billing where bill_status=3 AND (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."') ".$where." group by bill_no";
		$fetch_bill_p = $this->billing_model->viewRecordGCINR($select_w); 
			  
            echo $html .= '<h3>'. count($fetch_bill_p) .'</h3>  
              <p>Delivered</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="'. base_url(). 'admin/sales/delivered?from='.$start_date.'&to='. $end_date.'" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>';
		  
	}
	public function print1() {
		$data['head_title'] = 'View Vouchar';
		$from = $this->input->get('from');
		$to = $this->input->get('to'); 
		$userid = $this->input->get('selParty');
		$productid = $this->input->get('selProduct');
		$data['userid'] = !empty($userid) ? $userid : 'none';
		$data['productid'] = !empty($productid) ? $productid :'none';
		$data['from'] = !empty($from) ? $from : date('Y-m-d');
		$data['to'] = !empty($to) ? $to : date('Y-m-d');
		$data['contentView'] = 'admin/new-vouchers.php';
		$data['contentView'] = 'admin/print-vouchers.php';
		$this->load->view('admin/_template_model1', $data);
    }
	public function delivered() {
		$data['head_title'] = 'View Vouchar';
		$from = $this->input->get('from');
		$to = $this->input->get('to'); 
		$userid = $this->input->get('selParty');
		$productid = $this->input->get('selProduct');
		$data['userid'] = !empty($userid) ? $userid : 'none';
		$data['productid'] = !empty($productid) ? $productid :'none';
		$data['from'] = !empty($from) ? $from : date('Y-m-d');
		$data['to'] = !empty($to) ? $to : date('Y-m-d');
		$data['contentView'] = 'admin/new-vouchers.php';
		$data['contentView'] = 'admin/voucher-delivered.php';
		$this->load->view('admin/_template_model1', $data);
    }
	public function undelivered() {
		$data['head_title'] = 'View Vouchar';
		$from = $this->input->get('from');
		$to = $this->input->get('to'); 
		$userid = $this->input->get('selParty');
		$productid = $this->input->get('selProduct');
		$data['userid'] = !empty($userid) ? $userid : 'none';
		$data['productid'] = !empty($productid) ? $productid :'none';
		$data['from'] = !empty($from) ? $from : date('Y-m-d');
		$data['to'] = !empty($to) ? $to : date('Y-m-d');
		$data['contentView'] = 'admin/new-vouchers.php';
		$data['contentView'] = 'admin/voucher-undelivered.php';
		$this->load->view('admin/_template_model1', $data);
    }
	public function allvouchers() {
		$data['head_title'] = 'View Vouchar';
		$data['from'] = $this->input->get('from');
		$data['to'] = $this->input->get('to');
		$data['contentView'] = 'admin/all-vouchers.php';
		$this->load->view('admin/_template_model1', $data);
    }
	public function newvouchers() {
		$data['head_title'] = 'View Vouchar';
		$from = $this->input->get('from');
		$to = $this->input->get('to'); 
		$userid = $this->input->get('selParty');
		$productid = $this->input->get('selProduct');
		$data['userid'] = !empty($userid) ? $userid : 'none';
		$data['productid'] = !empty($productid) ? $productid :'none';
		$data['from'] = !empty($from) ? $from : date('Y-m-d');
		$data['to'] = !empty($to) ? $to : date('Y-m-d');
		$data['contentView'] = 'admin/new-vouchers.php';
		$this->load->view('admin/_template_model1', $data);
    }
	
	public function addBill() {
		$product_id = $this->uri->segment(4);
		$data['contentView'] = 'admin/add_bill';
		if (isset($product_id)) {
			$data['head_title'] = 'Edit Vouchar';
				$dap = array(
				'bill_id' => $product_id
			);
			$data['billObjects'] = $this->billing_model->viewRecordAnyR($dap);
			$this->load->view('admin/_template_model3', $data);
		} else {
			$data['head_title'] = 'Add Vouchar';
			$this->load->view('admin/_template_model3', $data);
		}
    }
	public function bill_show_list() {
		$product_id = $this->uri->segment(4);
		$dap = array(
			'bill_no' => $product_id
		);
		$data['billObjects'] = $this->billing_model->viewRecordAny($dap);
		$data['head_title'] = 'Vouchar Details';
		$data['contentView'] = 'admin/bill_show_list';
		$this->load->view('admin/_template_model2', $data);
    }
	public function ajaxBillunit(){
		$product_id = $this->input->post('product_id');
		$bill_unit = $this->input->post('bill_unit');
		$array_product = array(
			'p_product_id' => $product_id
		);
		$fetch_product = $this->product_model->viewRecordAny($array_product);
		if($bill_unit == '1'){
			echo json_encode(
				array(
					'status' => '0',
					'price' => $fetch_product->p_pc_price,
					'qty' => 1,
					'totalprice' => $fetch_product->p_pc_price
				));
		}else if($bill_unit == '2'){
			echo json_encode(
				array(
					'status' => '0',
					'price' => $fetch_product->p_do_price,
					'qty' => 1,
					'totalprice' => $fetch_product->p_do_price
				));
		}else if($bill_unit == '3'){
			echo json_encode(
				array(
					'status' => '0',
					'price' => $fetch_product->p_bn_price,
					'qty' => 1,
					'totalprice' => $fetch_product->p_bn_price
				));
		}

	}
	public function upbilling(){
		$row_id = $this->input->post('row_id');
		$up = $this->input->post('up');
		$value = $this->input->post('value');
		
		 
		if($up == 'price'){
			$ara_id = array(
				'bill_price' => $value
			);
			$this->billing_model->recordUpdate($row_id, $ara_id);
			echo 1;
		}else if($up == 'qty'){
			$ara_id = array(
			'bill_qty' => $value
			);
			$this->billing_model->recordUpdate($row_id, $ara_id);
			echo 1;
		}else if($up == 'discount'){
			$ara_id = array(
				'bill_discount' => $value
			);
			$this->billing_model->recordUpdate($row_id, $ara_id);
			echo 1;
		}
		
	}
	public function ajax_upload() {
		$salesEdit = $this->input->post('salesEdit');
		$party_name = $this->input->post('party_name');
		$bill_date = $this->input->post('bill_date');
		$bill_item = explode(",", $this->input->post('bill_item'));
		$bill_discount = explode(",", $this->input->post('bill_discount'));
		$bill_unit = explode(",", $this->input->post('bill_unit'));
		$bill_qty = explode(",", $this->input->post('bill_qty'));
		$bill_price = explode(",", $this->input->post('bill_price'));
		$data_d = count($bill_item);
		
		$fetch_d = $this->billing_model->viewRecordLast();
		$bill_last = ($fetch_d->bill_no + 1);
		 
		$orders = array();
		for ($i = 0; $i <= $data_d; $i++) {
			//////if($bill_item[$i] != 0){
				$bill_prod_arr = array(
					'p_name' => $bill_item[$i]
				);
				$fetch_data = $this->product_model->viewRecordAny($bill_prod_arr);	
				$bill_unit_arr = array(
					'u_name' => $bill_unit[$i]
				);
				$fetch_data_unit = $this->unit_model->viewRecordAny($bill_unit_arr);	
				$orders[] = array(
					'bill_product_id' => $fetch_data->p_product_id,
					'bill_no' => $bill_last,
					'bill_discount' => $bill_discount[$i],
					'bill_consignee' => $party_name,
					'bill_date' => $bill_date,
					'bill_price' => $bill_price[$i],
					'bill_qty' => $bill_qty[$i],
					'bill_unit' => $fetch_data_unit->u_id
				);
			//}
		}
		 
		if (!empty($salesEdit)) {
			$this->db->update_batch('tbl_billing', $orders, $salesEdit);
			$this->session->set_flashdata('product_uploaded', 'Your bill has been updated Successfully');
			 echo json_encode(
				array(
				'status' => '1',
				'url' => $bill_last
				));
		} else {
			//if (!empty($orders)) {
				$this->db->insert_batch('tbl_billing', $orders);
				$data_d = array(
					'bill_product_id' => 0
				);
				$this->billing_model->recordDelete($data_d);
				echo json_encode(
				array(
				'status' => '1',
				'url' => $bill_last
				));
			//} 
			//$this->session->set_flashdata('product_uploaded', 'Your bill has been uploaded Successfully');
			//redirect('admin/sales/addBill');
		}
	 
		
    }

    public function deleteBill(){
		$del_id = $_POST['del_id'];
		$data = array(
			'bill_no' => $del_id
		);
		$this->billing_model->recordDelete($data);
		$this->session->set_flashdata("message", "Record Not Updated!");
    }
	public function deleteBill_s(){
		$del_id = $_POST['del_id'];
		$data = array(
			'bill_id' => $del_id
		);
		$this->billing_model->recordDelete($data);
		$this->session->set_flashdata("message", "Record Not Updated!");
    }

     

}

?>