<?php

defined("BASEPATH") OR exit('No direct access');

class Dashboard extends Admin_Controller {

    public function __construct() {
		parent::__construct();
    }

    public function index() {
	$data['head_title'] = 'Dashboard';
	$data['contentView'] = 'admin/welcome';

	$this->load->view('admin/_template_model1', $data);
    }

    public function addMenu() {
	$this->load->view('admin/add_menu');
    }

    public function ajax_upload() {
	if (isset($_FILES["image_file"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './upload/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';

	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('image_file')) {
		echo $this->upload->display_errors();
	    } else {
		echo $this->input->post('menuName');
		$data = $this->upload->data();
		//time().$filename = $data['main_images']['update_data']['file_name'];
		$data1 = array('update_data' => $this->upload->data());
		$this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		//echo '<img width="100px" src="' . base_url() . 'upload/' . $data["file_name"] . '" />';
		//$this->load->view('admin/add_menu', $data);
	    }
	}
    }
    

    public function resizing($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = TRUE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 75;
	$config['height'] = 50;
	$config['new_image'] = './upload/thumb/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }

}

?>
