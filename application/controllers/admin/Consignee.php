<?php

defined("BASEPATH") OR exit('No direct access');

class Consignee extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function updatestatus() {
        $pid = $this->input->post('pid');
        $datas = array(
            'consignee_status' => $this->input->post('val_id')
        );
        $this->consignee_model->recordUpdate($pid, $datas);
    }
    
     public function consignee_sales_month() {
        $shop_id = $this->uri->segment(4);
        $data['shop_month'] = $this->uri->segment(5);
        $data['month_from'] = $this->input->get('to');
        $data['month_to'] = $this->input->get('from');
        $shop_idft = array(
            'consig_slug' => $shop_id
        );
        $data['shopObject'] = $this->consignee_model->viewRecordAny($shop_idft);
        $data['head_title'] = 'Consignee monthly report';
        $data['contentView'] = 'admin/consignee_sales_month_details';
        $this->load->view('admin/_template_model2', $data);
    }
    
    public function sendConsigmo() {
        $fetch = $this->input->post('slug_id');
        $sendemail = $this->input->post('sendemail');
        $month_id = $this->input->post('month_id');
        $data['subjectto'] = 'Consignee '.$month_id.' Bill Details :: ';
        $data['maito'] = $sendemail;
        $data['email_messagem'] = '
			 <tr>
				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="' . base_url() . 'consignees/' . $fetch . '/' . $month_id . '" target="_blank">Print '.$month_id.' Month Bill Details</a></h2></td>
			</tr>';
        $this->load->view('mail/mail_template', $data);
        echo "Record has been sent";
    }
    public function sendSales() {
        $fetch = $this->input->post('slug_id');
        $sendemail = $this->input->post('sendemail');
        $month_to = $this->input->post('to');
         $month_from = $this->input->post('from');
        $date_from = dateFormateYmd($month_from);
        $date_to = dateFormateYmd($month_to);
        $url_slug = 'REPORTS'.time().$_SESSION['__ci_last_regenerate'];
        if(!empty($month_to) && !empty($month_from) && !empty($fetch)){
            $pdata = array(
                'month_from' => $date_from,
                'month_to' => $date_to,
                'consignee_id' => $fetch,
                'sales_download_slug' => $url_slug
            );
            $this->salesdownload_model->recordInsert($pdata);
        }
        $data['subjectto'] = 'Sales Reports'.$month_from.' To '.$month_to.' :: ';
        $data['maito'] = $sendemail;
        $data['email_messagem'] = '
			 <tr>
                         <td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                         <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="' . base_url() . 'sales/' . $url_slug . '" target="_blank">Print Sales Reports</a></h2>
                             </td>
			</tr>';
        $this->load->view('mail/mail_template', $data);
        echo "Record has been sent";
    }
    public function sendInvoic() {
        $fetch = $this->input->post('slug_id');
        $sendemail = $this->input->post('sendemail');
        $month_to = $this->input->post('to');
         $month_from = $this->input->post('from');
        $date_from = dateFormateYmd($month_from);
        $date_to = dateFormateYmd($month_to);
        $url_slug = 'REPORTS'.time().$_SESSION['__ci_last_regenerate'];
        if(!empty($month_to) && !empty($month_from) && !empty($fetch)){
            $pdata = array(
                'month_from' => $date_from,
                'month_to' => $date_to,
                'consignee_id' => $fetch,
                'sales_download_slug' => $url_slug
            );
            $this->salesdownload_model->recordInsert($pdata);
        }
        $data['subjectto'] = 'Invoice Reports'.$month_from.' To '.$month_to.' :: ';
        $data['maito'] = $sendemail;
        $data['email_messagem'] = '
			 <tr>
                         <td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                         <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="' . base_url() . 'invoicel/' . $url_slug . '" target="_blank">Print Invoice Reports</a></h2>
                             </td>
			</tr>';
        $this->load->view('mail/mail_template', $data);
        echo "Record has been sent";
    }

    public function consigneeApprove() {
        $fetch = $this->input->post('consignee_id');
        $pwd = 'PMA' . substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $fetchid = $this->consignee_model->viewRecordId($fetch);
        $consignee_email = $fetchid->consignee_email;
        $data = array(
            'consignee_pwd' => strtoupper($pwd),
            'consignee_status' => '1'
        );
        $this->consignee_model->recordUpdate($fetch, $data);
        $data['subjectto'] = 'Consignee Approved ';
        $data['maito'] = $consignee_email;
        $data['email_messagem'] = '
			<tr>

				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);">Hi ' . $fetchid->consignee_name . '!  consignee request approved.</h2>
                <p style="padding:0 20px">
                <strong style="font-size:17px">User Name </strong>' . $consignee_email . ' <br>
                <strong style="font-size:17px">Password </strong>' . strtoupper($pwd) . '
                </p>
                <br>
                <p style="padding: 0 20px; font-size:16">In case you require any kind of assistance, please feel free to contact us. <br>
            <strong>Call us:</strong> +(626)-272-0272</p>

                </td>
			</tr>


            ';
        $this->load->view('mail/mail_template', $data);
        echo "1";
    }

    public function consignee_list() {
        $product_id = $this->uri->segment(5);
        $ararydat = array(
            'view_status' => '1'
        );
        $data["consigneeObjects"] = $this->consignee_model->viewRecordAnyR($ararydat);
        $data['head_title'] = 'List Consignee';
        $data['contentView'] = 'admin/view_consignee_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function changepass() {
        $getEamil = $this->session->userdata('userfront');
        $reloginpass = $this->input->post('reloginpass');
        $dataEmail = array(
            'userEmail' => $getEamil
        );
        if (!empty($getEamil)) {
            $detailEmail = $this->consignee_model->recordCheckAvaibility($dataEmail);
            if ($detailEmail > 0) {
                $emailfetch = $this->consignee_model->viewRecordAny($dataEmail);
                $ids = $emailfetch->u_id;
                $data = array(
                    'userPwd' => $reloginpass
                );
                $this->consignee_model->recordUpdate($ids, $data);
                echo "Password has been updated";
            } else {
                echo "Email id not matched";
            }
        }
    }

    public function statea() {
        $del_id = $this->input->post('del_id');
        $data = array(
            'location_parent' => $del_id
        );
        $datl = $this->location_model->viewRecordAnyR($data);
        $output = ' <select name="consignee_state1" id="consignee_state1" class="form-control">';
        $output .= '<option value="">Select State</option>';
        foreach ($datl as $los) {
            $output .= '<option value="' . $los->location_id . '">' . $los->location_name . '</option>';
        }
        $output .= '</select>';
        echo $output;
    }

    public function consigneeconsigneelist() {
        $data['head_title'] = 'Event List';
        $this->load->view('front/consignee_consignee_list', $data);
    }

    public function edit() {
        $artisfet = $this->uri->segment(4);
        $data['contentView'] = 'admin/add_consignee';
        if (isset($artisfet)) {
            $data['head_title'] = 'Edit Consignee';
            $dataas = array(
                'consig_slug' => $artisfet
            );
            $data['consigneeObject'] = $this->consignee_model->viewRecordAny($dataas);
            $this->load->view('admin/_template_model2', $data);
        } else {
            $data['head_title'] = 'Add Consignee';
            $this->load->view('admin/_template_model3', $data);
        }
    }

    public function updateImageConsignee() {
        $category_id = $this->input->post('consigneeid');
        if (isset($_FILES["consigneeImage"]["name"])) {
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = './assests/consignee/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('consigneeImage')) {
                echo $this->upload->display_errors();
            } else {
                $data1 = array('update_data' => $this->upload->data());
                $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
                $ads = array(
                    'consig_image' => $data1['update_data']['file_name']
                );
                $this->consignee_model->recordUpdate($category_id, $ads);
                echo "1";
            }
        } else {
            echo "file no upload";
        }
    }

    public function addEventbyartst() {
        $consignee_id = $this->input->post('editconsigneeconsignee');
        if (!empty($consignee_id)) {
            $pdata = array(
                //'consignee_id' => $this->input->post('consigneeessionid'),
                'category_parent_id' => $this->input->post('artclassconsignee'),
                'category_id' => $this->input->post('categoryartclass_id'),
                'consignee_title' => $this->input->post('consignee_title_a'),
                'consignee_slug' => $this->input->post('consignee_slug_a'),
                'consignee_price' => $this->input->post('consignee_price_a'),
                'consignee_ticket' => $this->input->post('consignee_ticket_a'),
                'consignee_desc' => $this->input->post('consignee_desc_a'),
                'consignee_start_date' => $this->input->post('consignee_start_date_a'),
                'consignee_end_date' => $this->input->post('consignee_end_date_a'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'administrative_area_level_1' => $this->input->post('administrative_area_level_1'),
                'locality' => $this->input->post('locality'),
                'route' => $this->input->post('route'),
                'street_number' => $this->input->post('street_number'),
                'latitude' => $this->input->post('latitude'),
                'longitude' => $this->input->post('longitude'),
                'place_id' => $this->input->post('place_id'),
                'consignee_added_date' => date("Y-m-d H:i:s"),
                'consignee_status' => '0'
            );
            $this->consignee_model->recordUpdate($consignee_id, $pdata);
            echo "1";
        } else {
            if (isset($_FILES["consignee_images_a"]["name"])) {
                $config['encrypt_name'] = TRUE;
                $config['upload_path'] = './assests/consignee/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('consignee_images_a')) {
                    echo $this->upload->display_errors();
                } else {
                    $data = $this->upload->data();
                    $data1 = array('update_data' => $this->upload->data());
                    $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
                    $pdata = array(
                        'consignee_id' => $this->input->post('consigneeessionid'),
                        'category_parent_id' => $this->input->post('artclassconsignee'),
                        'category_id' => $this->input->post('categoryartclass_id'),
                        'consignee_title' => $this->input->post('consignee_title_a'),
                        'consignee_slug' => $this->input->post('consignee_slug_a'),
                        'consignee_price' => $this->input->post('consignee_price_a'),
                        'consignee_ticket' => $this->input->post('consignee_ticket_a'),
                        'consignee_desc' => $this->input->post('consignee_desc_a'),
                        'consignee_start_date' => $this->input->post('consignee_start_date_a'),
                        'consignee_end_date' => $this->input->post('consignee_end_date_a'),
                        'postal_code' => $this->input->post('postal_code'),
                        'country' => $this->input->post('country'),
                        'administrative_area_level_1' => $this->input->post('administrative_area_level_1'),
                        'locality' => $this->input->post('locality'),
                        'route' => $this->input->post('route'),
                        'street_number' => $this->input->post('street_number'),
                        'latitude' => $this->input->post('latitude'),
                        'longitude' => $this->input->post('longitude'),
                        'place_id' => $this->input->post('place_id'),
                        'consignee_image' => $data1['update_data']['file_name'],
                        'consignee_added_date' => date("Y-m-d H:i:s"),
                        'consignee_status' => '0'
                    );
                    $this->session->set_flashdata('product_uploaded', 'Your product has been updated Successfully');
                    $this->consignee_model->recordInsert($pdata);
                    echo "1";
                }
            }
        }
    }

    public function addConsignee() {
        $consignee_id = $this->input->post('consigneeid');
        $consignee_name = $this->input->post('consignee_name');
        $consig_phone = $this->input->post('consignee_phone');
        $slugurl = seturl($consignee_name, $consignee_id);
        if (!empty($consignee_id)) {
            $pdata = array(
                'consig_name' => $consignee_name,
                'consig_slug' => $slugurl,
                'consig_address' => $this->input->post('consignee_address'),
                'consig_email' => $this->input->post('consignee_email'),
                'consig_type' => $this->input->post('consignee_type'),
                'consig_phone' => $this->input->post('consignee_phone'),
                'consig_gstin' => $this->input->post('consignee_gstin'),
                'consig_state' => $this->input->post('consignee_state'),
                'consig_city' => $this->input->post('consignee_city'),
                'consig_zipcode' => $this->input->post('consignee_zipcode'),
                'consig_contact_person' => $this->input->post('consignee_contact_person'),
                'consig_description' => $this->input->post('consignee_description'),
                'consig_added' => date("Y-m-d H:i:s")
            );
            $this->consignee_model->recordUpdate($consignee_id, $pdata);
            $this->session->set_flashdata("consignee_uploaded", "update successfully");
            redirect('admin/consignee/consignee_show_list/' . $slugurl);
        } else {
            $pdata = array(
                'consig_name' => $consignee_name,
                //'consig_slug' => $slugurl,
                'consig_address' => $this->input->post('consignee_address'),
                'consig_email' => $this->input->post('consignee_email'),
                'consig_type' => $this->input->post('consignee_type'),
                'consig_phone' => $this->input->post('consignee_phone'),
                'consig_gstin' => $this->input->post('consignee_gstin'),
                'consig_state' => $this->input->post('consignee_state'),
                'consig_city' => $this->input->post('consignee_city'),
                'consig_zipcode' => $this->input->post('consignee_zipcode'),
                'consig_contact_person' => $this->input->post('consignee_contact_person'),
                'consig_description' => $this->input->post('consignee_description'),
                'consig_added' => date("Y-m-d H:i:s")
            );
            $this->session->set_flashdata('product_uploaded', 'Your product has been updated Successfully');
            $this->consignee_model->recordInsert($pdata);
            $last_id = $this->db->insert_id();
            $finsulg = seturl($consignee_name, $last_id);
            $aslug = array(
                'consig_slug' => $finsulg
            );
            $this->consignee_model->recordUpdate($last_id, $aslug);
            $this->session->set_flashdata("consignee_uploaded", "Registration Successfully!");
            redirect('admin/consignee/consignee_show_list/' . $finsulg);
        }
    }

    public function consignee_show_list() {
        $consignee_id = $this->uri->segment(4);
        $consignee_idft = array(
            'consig_slug' => $consignee_id
        );
        $data['consigneeObject'] = $this->consignee_model->viewRecordAny($consignee_idft);
        $data['head_title'] = 'Consignees Details';
        $data['contentView'] = 'admin/consignee_show_list';
        $this->load->view('admin/_template_model2', $data);
    }
    public function consignee_show_list_invoice() {
        $consignee_id = $this->uri->segment(4); 
        $data['month_from'] = $this->input->get('to');
        $data['month_to'] = $this->input->get('from'); 
        $consignee_idft = array(
            'consig_slug' => $consignee_id
        );
        $data['consigneeObject'] = $this->consignee_model->viewRecordAny($consignee_idft);
        $data['head_title'] = 'Consignees Details';
        $data['contentView'] = 'admin/consignee_show_list_invoice';
        $this->load->view('admin/_template_model2', $data);
    }

    public function consignee_month() {
        $consignee_id = $this->uri->segment(4);
        $data['consignee_month'] = $this->uri->segment(5);
        $consignee_idft = array(
            'consig_slug' => $consignee_id
        );
        $data['consigneeObject'] = $this->consignee_model->viewRecordAny($consignee_idft);
        $data['head_title'] = 'Consignees invoice report';
        $data['contentView'] = 'admin/consignee_month_details';
        $this->load->view('admin/_template_model2', $data);
    }

    public function resizing($path, $file) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 336;
        $config['new_image'] = './assests/consignee/thumb/' . $file;
        $this->load->library('image_lib', $config);
        $this->image_lib->initialize();
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    public function deleteConsignee() {
        $del_id = $_POST['del_id'];
        $data = array(
            'view_status' => '0'
        );
        $this->consignee_model->recordUpdate($del_id, $data);
        $this->session->set_flashdata("message", "Record Deleted!");
    }

}

?>