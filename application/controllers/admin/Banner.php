<?php

defined("BASEPATH") OR exit('No direct access');

class Banner extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$banner_id = $this->uri->segment(3);
	$data['menuObject'] = $this->menu_model->viewRecordAll();
	$data['bannerObject'] = $this->banner_model->viewRecordId($banner_id);
	$dataa = array('category_parent_id' => '0');
	$data['categoryObject'] = $this->category_model->viewRecordAnyR($dataa);
	if (isset($banner_id)) {
	    $data['head_title'] = 'Edit Banner';

	    $data['contentView'] = 'admin/banner';
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Banner';
	    $data['contentView'] = 'admin/banner';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function bannerlist() {
	$data['head_title'] = 'List Category';
	$data['bannerList'] = $this->banner_model->viewRecordAll();
	$data['contentView'] = 'admin/veiw_banner';
	$this->load->view('admin/_template_model2', $data);
    }

    public function ajax_upload() {
	$bannerEdit = $this->input->post('bannerEdit');
	if (!empty($bannerEdit)) {
	    if (isset($_FILES["image_file"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/banner/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('image_file')) {
		    echo $this->upload->display_errors();
		} else {
		    $data = $this->upload->data();
		    $data1 = array('update_data' => $this->upload->data());
		    $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		    $datas = array(
			'banner_name' => $data1['update_data']['file_name'],
			'banner_title' => $this->input->post('bTitle'),
			'banner_description' => $this->input->post('bannerDescription'),
			'bType' => $this->input->post('bType'),
			'menu_id' => $this->input->post('bMenu'),
			'category_id' => $this->input->post('bCategory'),
			'banner_order' => $this->input->post('bOrder'),
			'banner_status' => '0'
		    );
		    $this->banner_model->recordUpdate($bannerEdit, $datas);
		    echo "1";
		}
	    }
	} else {
	    if (isset($_FILES["image_file"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/banner/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('image_file')) {
		    echo $this->upload->display_errors();
		} else {
		    $data = $this->upload->data();
		    $data1 = array('update_data' => $this->upload->data());
		    $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		    $datas = array(
			'banner_name' => $data1['update_data']['file_name'],
			'banner_title' => $this->input->post('bTitle'),
			'banner_description' => $this->input->post('bannerDescription'),
			'bType' => $this->input->post('bType'),
			'menu_id' => $this->input->post('bMenu'),
			'category_id' => $this->input->post('bCategory'),
			'banner_order' => $this->input->post('bOrder'),
			'banner_status' => '0'
		    );
		    $this->banner_model->recordInsert($datas);
		    echo "Banner uploaded successfully";
		}
	    }
	}
    }

    public function resizing($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = TRUE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 75;
	$config['height'] = 50;
	$config['new_image'] = './assests/banner/thumb/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }

    public function deleteBanner() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'banner_id' => $del_id
	);
	$this->banner_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>
