<?php

defined('BASEPATH') OR exit('No direct access');

class Role extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function add() {
	$roleName = $this->input->post('roleName');
	$category_id = $this->input->post('cateEdit');
	if (!empty($category_id)) {
	    $adss = array(
		'role_name' => $roleName,
		'role_status' => '0'
	    );
	    $this->role_model->recordUpdate($category_id, $adss);
	    echo "1";
	} else {
	    $ads = array(
		'role_name' => $roleName,
		'role_status' => '0'
	    );
	    $this->role_model->recordInsert($ads);
	    echo "Role added Successfully";
	    redirect('role/role');
	}
    }

    public function role() {
	$category_id = $this->uri->segment(4);
	$data['contentView'] = 'admin/addRole';
	$data['roleObject'] = $this->role_model->viewRecordAll();
	if (isset($category_id)) {
	    $data['head_title'] = 'Edit Role';

	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Role';

	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function viewRole() {
	$condition = array('location_parent' => '0');
	$data['toplocations'] = $this->role_model->viewRecordAnyR($condition);
	$data['head_title'] = 'View Location';
	$data['contentView'] = 'admin/veiw_location';
	$this->load->view('admin/_template_model2', $data);
    }

    public function rolea() {
	$del_id = $this->input->post('del_id');
	$data = array(
	    'role_parent' => $del_id
	);
	$datl = $this->role_model->viewRecordAnyR($data);
	$output = '<select name="role_parent" id="role_parent" class="form-control">';
	foreach ($datl as $los) {
	    $output .= '<option value="' . $los->role_id . '">' . $los->role_name . '</option>';
	}
	$output .= '</select>';
	echo $output;
    }

    public function deleteRole() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'role_id' => $del_id
	);
	$this->role_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>