<?php

defined("BASEPATH") OR exit('No direct access');

class Product extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {

    }

    public function updateqty() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'p_qty' => $this->input->post('val_id')
	);
	$this->product_model->recordUpdate($pid, $datas);
	echo "Quntity has been updated";
    }

    public function updatestatus() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'p_status' => $this->input->post('val_id')
	);
	$this->product_model->recordUpdate($pid, $datas);
    }

    public function updateoutofstock() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'outofstock' => $this->input->post('val_id')
	);
	$this->product_model->recordUpdate($pid, $datas);
    }

    public function extraproductadd() {
	$productEdit = $this->input->post('product_id');
	$p_extra_con_title = $this->input->post('p_extra_con_title');
	$p_extra_con_desc = $this->input->post('p_extra_con_desc');
	$extraid = $this->input->post('extraid');
	if (!empty($extraid)) {
	    $araray12 = array(
		'p_extra_title' => $p_extra_con_title,
		'p_extra_desc' => $p_extra_con_desc
	    );
	    $this->extraproducts_model->recordUpdate($extraid, $araray12);
	    echo "1";
	} else {
	    $araray = array(
		'p_extra_title' => $p_extra_con_title,
		'p_extra_desc' => $p_extra_con_desc,
		'p_product_id' => $productEdit,
	    );
	    $this->extraproducts_model->recordInsert($araray);
	    echo "1";
	}
    }

    public function fetchextra() {
	$udate = $_POST['extraid'];
	$adf = $this->extraproducts_model->viewRecordId($udate);
	echo json_encode($adf);
    }

    public function productList() {
	$product_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "product/productList";
	$config["total_rows"] = $this->product_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["productObjects"] = $this->product_model->viewRecordPage($config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View product';
	$data['contentView'] = 'admin/view_product_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function productLists() {
	$product_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "product/productLists";
	$config["total_rows"] = $this->product_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["productObjects"] = $this->product_model->viewRecordPage($config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View Product';
	$data['contentView'] = 'admin/view_products_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function product_show_list() {
	$product_id = $this->uri->segment(4);
	$data['productObject'] = $this->product_model->viewRecordId($product_id);
	$data['head_title'] = 'Product Details';
	$data['contentView'] = 'admin/product_show_list';
	$this->load->view('admin/_template_model2', $data);
    }

    //show product_details
    public function product_details() {
	$gepid = array(
	    'p_product_id' => $this->uri->segment(4)
	);
	$data['productObjects'] = $this->product_model->viewRecordId($gepid);
	$data['head_title'] = 'View Product';
	$data['contentView'] = 'admin/view_product_list_p';
	$this->load->view('admin/_template_model2', $data);
    }

    public function addProduct() {
	$product_id = $this->uri->segment(4);
	$data['head_title'] = 'View Product';
	$data['categories'] = $this->product_model->category_menu();
	$data['objProduct'] = $this->product_model->viewRecordAll();
	$data['contentView'] = 'admin/add_product';
	if (isset($product_id)) {
	    $data['head_title'] = 'Edit Product';
	    $data['productObject'] = $this->product_model->viewRecordId($product_id);
	    $this->load->view('admin/_template_model3', $data);
	} else {
	    $data['head_title'] = 'Add Product';
	    $this->load->view('admin/_template_model3', $data);
	}
    }

    public function ajax_upload() {
	$productEdit = $this->input->post('productEdit');
	//$related_product = implode(",",$_POST['cate']);
	$product_title = $this->input->post('product_name');
	$product_slug = str_replace(" ", "-", strtolower($product_title));
	if (!empty($productEdit)) {
	    $datas = array(
		'p_title' => $product_title,
		'p_slug' => $product_slug,
		'p_category' => $this->input->post('categoryParent'),
		'p_price' => $this->input->post('prdouct_price'),
		'p_tax_price' => $this->input->post('prdouct_tax_price'),
		'p_ship_price' => $this->input->post('prdouct_ship_price'),
		'p_qty' => $this->input->post('prdouct_qty'),
		'p_discription' => $this->input->post('product_description'),
		'p_discount' => $this->input->post('prdouct_discount'),
		'p_status' => $this->input->post('prdouct_Status'),
		'p_updated_date' => date('Y-m-d H:i:s'),
	    );
	    $this->product_model->recordUpdate($productEdit, $datas);
	    $this->session->set_flashdata('product_uploaded', 'Your product has been updated Successfully');
	    redirect('admin/product/productLists');
	} else {
	    if (isset($_FILES["product_images"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/product/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('product_images')) {
		    echo $this->upload->display_errors();
		} else {
		    $data = $this->upload->data();
		    $data1 = array('update_data' => $this->upload->data());
		    $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
            $this->resizinglarge($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		    $datas = array(
			'p_title' => $product_title,
			'p_slug' => $product_slug,
			'p_category' => $this->input->post('categoryParent'),
			'p_price' => $this->input->post('prdouct_price'),
			'p_tax_price' => $this->input->post('prdouct_tax_price'),
			'p_ship_price' => $this->input->post('prdouct_ship_price'),
			'p_qty' => $this->input->post('prdouct_qty'),
			'p_discription' => $this->input->post('product_description'),
			'p_discount' => $this->input->post('prdouct_discount'),
			'p_status' => $this->input->post('prdouct_Status'),
			'p_image' => $data1['update_data']['file_name'],
			'p_updated_date' => date('Y-m-d H:i:s'),
			'p_created_date' => date('Y-m-d H:i:s')
		    );
		    $this->product_model->recordInsert($datas);
			
		    $this->session->set_flashdata('product_uploaded', 'Your product has been uploaded Successfully');
		    redirect('admin/product/addProduct');
		}
	    }
	}
    }

    public function addProductyc() {
	$category_id = $this->input->post('prodEdit');
	if (isset($_FILES["productImage"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/product/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('productImage')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
        $this->resizinglarge($data1['update_data']['full_path'], $data1['update_data']['file_name']);    
		$this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		$ads = array(
		    'p_image' => $data1['update_data']['file_name']
		);
		$this->product_model->recordUpdate($category_id, $ads);

		echo "1";
	    }
	} else {
	    echo "file no upload";
	}
    }
    public function extraimagesupload() {
	$productEdit = $this->input->post('productEdit');
	if (!empty($productEdit)) {
	    $last_Id = $this->db->insert_id();
	    if ($_FILES["files"]["name"] != '') {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/product/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		for ($count = 0; $count < count($_FILES["files"]["name"]); $count++) {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
             $data1 = array('update_data' => $this->upload->data());   
		    }
		    $ar = $data['file_name'];
		    $araray = array(
			'fiename' => $ar,
			'pid' => $productEdit,
			'category' => 'product'
		    );
		    $this->extraimages_model->recordInsert($araray);
            $this->resizinglarge($data1['update_data']['full_path'], $data1['update_data']['file_name']);    
		    redirect('admin/product/product_show_list/' . $productEdit);
		}
	    }
	}
    }

    public function resizing($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 200;
	$config['height'] = 200;
	$config['new_image'] = './assests/product/thumb/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }
    public function resizinglarge($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 411;
	$config['height'] = 274;
	$config['new_image'] = './assests/product/large/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }

    public function deleteProduct() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'view_status' => '0'
	);
	$this->product_model->recordUpdate($del_id, $data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

    public function deleteExtractproduct() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'p_extra_id' => $del_id
	);
	$this->extraproducts_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

    public function deleteExtractimages() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'imagesid' => $del_id
	);
	$this->extraimages_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

    

}
