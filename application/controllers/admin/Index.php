<?php

defined("BASEPATH") OR exit('No direct access');

class Index extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('admin/index');
    }

    public function login() {
        $uEmail = $this->input->post('admin_email');
         
        $uPwd = $this->input->post('admin_password');
        if ((!empty($uEmail) && trim($uEmail) != '') && (!empty($uPwd) && trim($uPwd) != '')) {
            $data = array(
                'userEmail' => htmlspecialchars($uEmail),
                'userPwd' => htmlspecialchars($uPwd)
            ); 
            $checked = $this->user_m_f->recordCheckAvaibility($data);
             
            $fetchrecord = $this->user_m_f->viewRecordAny($data);
            if ($checked > 0) {
                $this->session->set_userdata('userName_sess', $uEmail);
                $this->session->set_userdata('admin_type', $fetchrecord->role_id);
                 $this->session->set_userdata('account_id', $fetchrecord->account_id);
                $output = 1;
            } else {
                $output = 'notvalid';
            }
        } else {
            $this->load->view('admin/pagenot');
        }
        echo $output;
    }

    public function logout() {
        $this->session->unset_userdata('userName_sess');
        $dashboard = 'admin/index/index';
        redirect($dashboard);
    }

}

?>
