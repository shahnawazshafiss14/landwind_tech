<?php

defined('BASEPATH') OR exit('No direct access');

class Users extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['head_title'] = 'View Users';
        $data['contentView'] = 'admin/userLst';
        $this->load->view('admin/_template_model2', $data);
    }

    public function addUsers() {
        $user_id = $this->uri->segment(4);
        if (isset($user_id)) {
            $data['head_title'] = 'Edit User';
            $user_idfind = array(
                'uUrl' => $user_id
            );
            $data['userObject'] = $this->user_m_f->viewRecordAny($user_idfind);
            $data['contentView'] = 'admin/addusers';
            $this->load->view('admin/_template_model1', $data);
        } else {
            $data['head_title'] = 'Add Users';
            $data['contentView'] = 'admin/addusers';
            $this->load->view('admin/_template_model1', $data);
        }
    }

    public function manage() {
        $data['head_title'] = 'View Users';
        $user_id = $this->uri->segment(4);
        $product_id = array(
            'uUrl' => $user_id
        );
        $data['userObjects'] = $this->user_m_f->viewRecordAnyR($product_id);
        $data['contentView'] = 'admin/userManage';
        $this->load->view('admin/_template_model2', $data);
    }

    public function addme() {
        $useid = $this->input->post('useid');
        $chageval = $this->input->post('chageval');

        $arrayfetch = array(
            'user_id' => $useid,
            'menu_id' => $chageval,
        );
        $datauser = $this->permissionmenu_model->viewRecordAny($arrayfetch);
        if (count($datauser) > 0) {
            $this->permissionmenu_model->recordDelete($arrayfetch);
            echo '1';
        } else {
            $this->permissionmenu_model->recordInsert($arrayfetch);
            echo '1';
        }
    }

    public function addmev() {
        $useid = $this->input->post('useid');
         $fethcahi = $this->input->post('chai');
        $fethdata = $this->input->post('chageval');
        $datav = $this->input->post('datav');
        if ($fethcahi == '0') {
            $chageval1 = '1';
        } else {
            $chageval1 = '0';
        }
        $arrayfetch = array(
            'user_id' => $useid,
            'menu_id' => $fethdata
        );
        $datauser = $this->permissionmenu_model->viewRecordAny($arrayfetch);
        if (count($datauser) > 0) {
            if ($datav == 'add') {
                $data = array(
                    'is_inserted' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            } else if ($datav == 'edit') {
                $data = array(
                    'is_edited' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            } else if ($datav == 'del') {
                $data = array(
                    'is_deleted' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            } else if ($datav == 'view') {
                $data = array(
                    'is_viewed' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            }else if ($datav == 'email') {
                $data = array(
                    'is_emailed' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            }else if ($datav == 'print') {
                $data = array(
                    'is_printed' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            }else if ($datav == 'checke') {
                $data = array(
                    'is_checked' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            }else if ($datav == 'otherview') {
                $data = array(
                    'is_viewed_other' => $chageval1
                );
                $this->permissionmenu_model->recordUpdateAny($arrayfetch, $data);
                echo '1';
            }
            
        }
    }

    public function details() {
        $product_id = $this->uri->segment(4);
        $data['userObjects'] = $this->customer_model->viewRecordId($product_id);
        $data['head_title'] = 'View Users';
        $data['contentView'] = 'admin/user_show_details';
        $this->load->view('admin/_template_model2', $data);
    }

    public function admindetails() {
        $product_id = $this->uri->segment(4);
        $data['userObjects'] = $this->user_m_f->viewRecordId($product_id);
        $data['head_title'] = 'View Users';
        $data['contentView'] = 'admin/admin_show_details';
        $this->load->view('admin/_template_model2', $data);
    }

    public function addUser() {
        $userEmail = $this->input->post('userEmail');
        $userpwd = $this->input->post('userPwd');
        $user_id = $this->input->post('userEdit');
        $uUrl = $this->input->post('userName') . '-' . $this->input->post('lastName');
        $pdata = array(
            'userName' => $this->input->post('userName'),
            'lastName' => $this->input->post('lastName'),
            //'userEmail' => $this->input->post('userEmail'),
            'userMobile' => $this->input->post('userMobile'),
            'userPwd' => $this->input->post('userPwd'),
            'uUrl' => seturl($uUrl, $user_id),
            // 'user_parent' => $this->input->post('user_parent'),
            //'role_id' => $this->input->post('role_id'),
            //'role_parent' => $this->input->post('hidRole'),
            'cName' => $this->input->post('companyName'),
            'cGSTIN' => $this->input->post('gstin'),
            'cAddress' => $this->input->post('userAddress'),
            'cstate' => $this->input->post('userState'),
            'cCity' => $this->input->post('userCity'),
            'cPincode' => $this->input->post('cPincode'),
            'bName' => $this->input->post('bName'),
            'bBranch' => $this->input->post('bBranch'),
            'bAccount' => $this->input->post('bAccount'),
            'bIfsc' => $this->input->post('bIfsc'),
            'uPan' => $this->input->post('uPan'),
            'userCreated' => date("Y-m-d H:i:s"),
            'userStatus' => '0'
        );
        if (!empty($user_id)) {
            $this->user_m_f->recordUpdate($user_id, $pdata);
            redirect('admin/users/admindetails/' . $user_id);
        } else {
            $dataEmail = array(
                'userEmail' => $userEmail
            );
            if (!empty($userEmail)) {
                $detailEmail = $this->user_m_f->recordCheckAvaibility($dataEmail);
                if ($detailEmail > 0) {
                    echo "Email id Already Exits";
                } else {
                    $this->user_m_f->recordInsert($pdata);
                    $userlast = $this->db->insert_id();
                    if ($this->input->post('role_id') == 2) {
                        $datadist = array(
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '148',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '153',
                                'status' => '0'
                            )
                            ,
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '123',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '124',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '149',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '152',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '115',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '117',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '127',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '137',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '138',
                                'status' => '0'
                            )
                        );
                        $this->db->insert_batch('permession_menu', $datadist);
                    }
                    if ($this->input->post('role_id') == 3) {
                        $datadist = array(
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '123',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '124',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '115',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '117',
                                'status' => '0'
                            )
                        );
                        $this->db->insert_batch('permession_menu', $datadist);
                    }
                    if ($this->input->post('role_id') == 4) {
                        $datadist = array(
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '123',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '124',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '115',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '117',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '137',
                                'status' => '0'
                            ),
                            array(
                                'user_id' => $userlast,
                                'menu_id' => '138',
                                'status' => '0'
                            )
                        );
                        $this->db->insert_batch('permession_menu', $datadist);
                    }
                    $data['subjectto'] = 'Registration ';
                    $data['maito'] = $userEmail;
                    $data['email_messagem'] = '
			<tr>
				<td style="font-family:Calibri;font-size:15px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:25px;text-align:justify;border-top: 1px solid #f1f1f1;"><span style="color:#222; text-align:center"> <strong><h1 style="color: #00559b;font-weight: 500;">Thank you for registered with us!</h1></strong><br></span></td>
			</tr>
			<tr>
				<td style="padding: 0px 180px;"> <span style="font-size:20px;font-weight:bold;color:#222;font-family:Calibri"> Username : <a style="color:#222;text-decoration:none;font-weight: normal;font-size: 16px;" href="mailto:' . $userEmail . '" target="_blank"> ' . $userEmail . '</a></span><br><span style="
    font-family: Calibri;font-weight: bold;font-size: 18px;padding-right: 21px;">Password:</span>  <span>' . $userpwd . '</span>   </td>
			</tr>';
                    $this->load->view('mail/mail_template', $data);

                    $this->session->set_flashdata("message", "Registration Successfully!");
                    redirect('admin/users/index');
                }
            }
        }
    }

    public function changepass() {
        $getEamil = $this->session->userdata('userfront');
        $reloginpass = $this->input->post('reloginpass');
        $dataEmail = array(
            'userEmail' => $getEamil
        );
        if (!empty($getEamil)) {
            $detailEmail = $this->user_m_f->recordCheckAvaibility($dataEmail);
            if ($detailEmail > 0) {
                $emailfetch = $this->user_m_f->viewRecordAny($dataEmail);
                $ids = $emailfetch->u_id;
                $data = array(
                    'userPwd' => $reloginpass
                );
                $this->user_m_f->recordUpdate($ids, $data);
                echo "Password has been updated";
            } else {
                echo "Email id not matched";
            }
        }
    }

    public function forgetpassword() {
        $userEmail = $this->input->post('userEmail');
        $dataEmail = array(
            'userEmail' => $userEmail
        );
        if (!empty($userEmail)) {
            $detailEmail = $this->user_m_f->recordCheckAvaibility($dataEmail);
            if ($detailEmail > 0) {
                $pdata = array(
                    'userEmail' => $this->input->post('userEmail')
                );

                $emailfetch = $this->user_m_f->viewRecordAny($dataEmail);
                $pasww = $emailfetch->userPwd;
                $data['subjectto'] = 'Your Password ';
                $data['maito'] = $userEmail;
                $data['email_messagem'] = '
			<tr>
				<td style="font-family: Calibri;font-size: 26px;color: #005c9f!important;text-decoration: none;line-height: 25px;padding-top: 25px;
    padding-bottom: 25px;
    text-align: justify;
    border-top: 1px solid #f1f1f1;
    text-align: center;"><span style="color:#787878"> <strong style="color:#005c9f">Thank you for Contact with us!</strong><br></span></td>
			</tr>
			<tr>
				<td style="padding: 10px 216px;"> <span style="font-size:20px;font-weight:bold;color:#000;font-family:Calibri"> Username : <a style="color:#000;text-decoration:none;font-weight: normal;font-size: 16px;" href="mailto:' . $userEmail . '" target="_blank"> ' . $userEmail . '</a></span><br><span style="
    font-family: Calibri;font-weight: bold;font-size: 18px;padding-right: 21px;">Password:</span>  <span>' . $pasww . '</span>   </td>
			</tr>';
                $this->load->view('mail/mail_template', $data);
                $this->session->set_flashdata("message", "Your password has been sent registerd Email ID!");
            } else {
                echo "Email id not matched";
            }
        }
    }

    public function deleteUser() {
        $del_id = $_POST['del_id'];
        $data = array(
            'u_id' => $del_id
        );
        $this->user_m_f->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>