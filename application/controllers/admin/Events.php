<?php

defined("BASEPATH") OR exit('No direct access');

class Events extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {

    }

    public function updatePrice() {
	$pid = $this->input->post('eventid');
	$datas = array(
	    'event_price' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
	echo "Price has been updated";
    }

    public function updateCommision() {
	$pid = $this->input->post('eventid');
	$datas = array(
	    'event_commision' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
	echo "Price has been updated";
    }

    public function updateqty() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'p_qty' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
	echo "Quntity has been updated";
    }

    public function updateSlug() {
	$pid = $this->input->post('eventid');
	$venue_sulg = $this->input->post('val_id');
	$datas = array(
	    'event_slug' => str_replace(" ", "-", strtolower($venue_sulg)),
	);
	$this->event_model->recordUpdate($pid, $datas);
	echo "slug has been updated";
    }

    public function updateDiscount1() {
	$pid = $this->input->post('eventid');
	$datas = array(
	    'event_discount1' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
	echo "Price has been updated";
    }

    public function updateDiscount2() {
	$pid = $this->input->post('eventid');
	$datas = array(
	    'event_discount2' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
	echo "Price has been updated";
    }

    public function updatestatus() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'event_status' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
    }

    public function updateoutofstock() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'outofstock' => $this->input->post('val_id')
	);
	$this->event_model->recordUpdate($pid, $datas);
    }

    public function eventApprove() {
	$fetch = $this->input->post('event_id');
	$data = array(
	    'event_status' => '1'
	);
	$this->event_model->recordUpdate($fetch, $data);
	echo "1";
    }

    public function addProductyc() {
	$category_id = $this->input->post('eventid');
	if (isset($_FILES["eventImage"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/event/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('eventImage')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
		$ads = array(
		    'event_image' => $data1['update_data']['file_name']
		);
		$this->event_model->recordUpdate($category_id, $ads);

		echo "1";
	    }
	} else {
	    echo "file no upload";
	}
    }

    public function addEventExtrac() {
	$category_id = $this->input->post('eventid');
	if (isset($_FILES["eventImage"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/event/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('eventImage')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
		$ads = array(
		    'event_image' => $data1['update_data']['file_name']
		);
		$this->event_model->recordUpdate($category_id, $ads);

		echo "1";
	    }
	} else {
	    echo "file no upload";
	}
    }

    public function extraeventadd() {
	$productEdit = $this->input->post('eventid');
	$event_con_title = $this->input->post('event_con_title');
	$eventimage = $this->input->post('eventimage');
	$event_con_desc = $this->input->post('event_con_desc');
	$extraid = $this->input->post('eextraid');
	if (!empty($extraid)) {
	    $araray = array(
		'event_con_title' => $event_con_title,
		'event_con_desc' => $event_con_desc,
		'event_con_added' => date('Y-m-d H:i:s'),
	    );
	    $this->eventextracon_model->recordUpdate($extraid, $araray);
	    echo 'events/event_show_list/' . $productEdit;
	} else {
	    if (isset($_FILES["eventimage"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/event/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('eventimage')) {
		    echo $this->upload->display_errors();
		} else {
		    $data = $this->upload->data();
		    $data1 = array('update_data' => $this->upload->data());
		    $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		    $araray = array(
			'event_con_title' => $event_con_title,
			'event_con_desc' => $event_con_desc,
			'event_id' => $productEdit,
			'event_con_image' => $data1['update_data']['file_name'],
			'event_con_added' => date('Y-m-d H:i:s'),
			'event_con_status' => '1',
		    );
		    $this->eventextracon_model->recordInsert($araray);
		    echo "1";
		}
	    }
	}
    }

    public function eventfetchextra() {
	$udate = $_POST['eextraid'];
	$adf = $this->eventextracon_model->viewRecordId($udate);
	echo json_encode($adf);
    }

    public function eventList() {
	$event_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "events/eventList";
	$config["total_rows"] = $this->event_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["eventObjects"] = $this->event_model->viewRecordPage($config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View event';
	$data['contentView'] = 'admin/view_event_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function eventLists() {
	$event_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "events/eventLists";
	$config["total_rows"] = $this->event_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["eventObjects"] = $this->event_model->viewRecordPage($config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View event';
	$data['contentView'] = 'admin/view_events_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function booked() {
	$event_id = $this->uri->segment(4);
	$config = array();
	$config["base_url"] = ADMINC . "events/booked";
	$config["total_rows"] = $this->buy_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	$data["buyObjects"] = $this->buy_model->viewRecordPageRGLs($config["per_page"], $page, 'buy_product_id');
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View event booked list';
	$data['contentView'] = 'admin/view_events_booked_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function bookedLists() {
	$event_id = $this->uri->segment(4);
	$config = array();
	$config["base_url"] = ADMINC . "events/bookedLists";
	$config["total_rows"] = $this->buy_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	$databuy = array(
	    'buy_product_id' => $event_id,
	    'buyStatus' => '1'
	);
	$data["buyObjects"] = $this->buy_model->viewRecordPageR($databuy, $config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View event booked list';
	$data['contentView'] = 'admin/view_events_book_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function event_show_list() {
	$event_id = $this->uri->segment(4);
	$data['eventObject'] = $this->event_model->viewRecordId($event_id);
	$data['head_title'] = 'Product Details';
	$data['contentView'] = 'admin/event_show_list';
	$this->load->view('admin/_template_model2', $data);
    }

    //show event_details
    public function event_details() {
	$gepid = array(
	    'p_event_id' => $this->uri->segment(4)
	);
	$data['eventObjects'] = $this->event_model->viewRecordId($gepid);
	$data['head_title'] = 'View Product';
	$data['contentView'] = 'admin/view_event_list_p';
	$this->load->view('admin/_template_model2', $data);
    }

    public function addevent() {
	$event_id = $this->uri->segment(4);
	$data['head_title'] = 'View Events';
	$data['objEvent'] = $this->event_model->viewRecordAll();
	$data['contentView'] = 'admin/add_event';
	if (isset($event_id)) {
	    $data['head_title'] = 'Edit Event';
	    $data['eventObject'] = $this->event_model->viewRecordId($event_id);
	    $this->load->view('admin/_template_model3', $data);
	} else {
	    $data['head_title'] = 'Add Event';
	    $this->load->view('admin/_template_model3', $data);
	}
    }

    public function addEventbyartst() {
	$artist_id = $this->input->post('eventEdit');
	$event_sulg = $this->input->post('event_slug_a');
	if (!empty($artist_id)) {
	    $pdata = array(
		//'artist_id' => $this->input->post('artistsessionid'),
		'category_parent_id' => $this->input->post('artclassevent'),
		'category_id' => $this->input->post('categoryartclass_id'),
		'event_title' => $this->input->post('event_title_a'),
		'event_slug' => str_replace(" ", "-", strtolower($event_sulg)),
		'event_price' => $this->input->post('event_price_a'),
		'event_ticket' => $this->input->post('event_ticket_a'),
		'event_desc' => $this->input->post('event_desc_a'),
		'event_start_date' => $this->input->post('event_start_date_a'),
		'event_end_date' => $this->input->post('event_end_date_a'),
		'event_time' => $this->input->post('event_time'),
		'venue_id' => $this->input->post('event_venue_id'),
		'postal_code' => $this->input->post('postal_code'),
		'country' => $this->input->post('country'),
		'administrative_area_level_1' => $this->input->post('administrative_area_level_1'),
		'locality' => $this->input->post('locality'),
		'route' => $this->input->post('route'),
		'street_number' => $this->input->post('street_number'),
		'latitude' => $this->input->post('latitude'),
		'longitude' => $this->input->post('longitude'),
		'place_id' => $this->input->post('place_id'),
		'event_added_date' => date("Y-m-d H:i:s"),
		'event_status' => '0'
	    );
	    $this->event_model->recordUpdate($artist_id, $pdata);
	    redirect('admin/events/eventLists');
	    $this->session->set_flashdata("message", "Record Updated!");
	} else {
	    if (isset($_FILES["event_images_a"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/event/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('event_images_a')) {
		    echo $this->upload->display_errors();
		} else {
		    $data = $this->upload->data();
		    $data1 = array('update_data' => $this->upload->data());
		    $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		    $pdata = array(
			'artist_id' => $this->input->post('artistsessionid'),
			'category_parent_id' => $this->input->post('artclassevent'),
			'category_id' => $this->input->post('categoryartclass_id'),
			'event_title' => $this->input->post('event_title_a'),
			'event_slug' => str_replace(" ", "-", strtolower($event_sulg)),
			'event_price' => $this->input->post('event_price_a'),
			'event_ticket' => $this->input->post('event_ticket_a'),
			'event_desc' => $this->input->post('event_desc_a'),
			'event_start_date' => $this->input->post('event_start_date_a'),
			'event_end_date' => $this->input->post('event_end_date_a'),
			'event_time' => $this->input->post('event_time'),
			'venue_id' => $this->input->post('event_venue_id'),
			'postal_code' => $this->input->post('postal_code'),
			'country' => $this->input->post('country'),
			'administrative_area_level_1' => $this->input->post('administrative_area_level_1'),
			'locality' => $this->input->post('locality'),
			'route' => $this->input->post('route'),
			'street_number' => $this->input->post('street_number'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
			'place_id' => $this->input->post('place_id'),
			'event_image' => $data1['update_data']['file_name'],
			'event_added_date' => date("Y-m-d H:i:s"),
			'event_status' => '0'
		    );
		    $this->session->set_flashdata('event_uploaded', 'Your Event has been updated Successfully');
		    $this->event_model->recordInsert($pdata);
		    redirect('admin/events/addevent');
		}
	    }
	}
    }

    public function deleteExtractevent() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'event_con_id' => $del_id
	);
	$this->eventextracon_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

    public function resizing($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 200;
	$config['height'] = 200;
	$config['new_image'] = './assests/event/thumb/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }

    public function deleteEvent() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'view_status' => '0'
	);
	$this->event_model->recordUpdate($del_id, $data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

    public function deleteExtractimages() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'imagesid' => $del_id
	);
	$this->extraimages_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

    public function extraimagesupload() {
	$eventEdit = $this->input->post('eventEdit');
	if (!empty($eventEdit)) {
	    $last_Id = $this->db->insert_id();
	    if ($_FILES["files"]["name"] != '') {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assests/event/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		for ($count = 0; $count < count($_FILES["files"]["name"]); $count++) {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
		    }
		    $ar = $data['file_name'];
		    $araray = array(
			'fiename' => $ar,
			'pid' => $eventEdit
		    );
		    $this->extraimages_model->recordInsert($araray);
		    redirect('events/eventList');
		}
	    }
	}
    }

}

?>
