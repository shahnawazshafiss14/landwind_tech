<?php

defined('BASEPATH') OR exit('No direct access');

class Transport extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function updatePrice() {
	$pid = $this->input->post('venueid');
	$datas = array(
	    'venue_price' => $this->input->post('val_id')
	);
	$this->venue_model->recordUpdate($pid, $datas);
	echo "Price has been updated";
    }

    public function updateSlug() {
	$pid = $this->input->post('venueid');
	$venue_sulg = $this->input->post('val_id');
	$datas = array(
	    'venue_slug' => str_replace(" ", "-", strtolower($venue_sulg)),
	);
	$this->venue_model->recordUpdate($pid, $datas);
	echo "slug has been updated";
    }

    public function updatestatus() {
	$pid = $this->input->post('pid');
	$datas = array(
	    'venue_status' => $this->input->post('val_id')
	);
	$this->venue_model->recordUpdate($pid, $datas);
    }

    public function updateCommision() {
	$pid = $this->input->post('venueid');
	$venue_sulg = $this->input->post('val_id');
	$datas = array(
	    'venue_commision' => $venue_sulg,
	);
	$this->venue_model->recordUpdate($pid, $datas);
	echo "slug has been updated";
    }

    public function add() {
	$invoice_id = $this->uri->segment(4);
	$data['objItem'] = $this->itemtype_model->viewRecordAll();
	$data['contentView'] = 'admin/add_transport_bill';
	if (isset($invoice_id)) {
	    $data['head_title'] = 'Edit Bill';
	    $slugfie = array(
		'trans_slug' => $invoice_id
	    );
	    $data['transObject'] = $this->transport_model->viewRecordAny($slugfie);
	    $this->load->view('admin/_template_model3', $data);
	} else {
	    $data['head_title'] = 'Add Bill';
	    $this->load->view('admin/_template_model3', $data);
	}
    }

    public function billList() {
	$fetch = array(
	    'view_status' => '1'
	);
	$data["transportObjects"] = $this->transport_model->viewRecordAnyR($fetch);
	$data['head_title'] = 'Bill List';
	$data['contentView'] = 'admin/view_transport_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function updateImageVenue() {
	$category_id = $this->input->post('venueid');
	if (isset($_FILES["venueImage"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/event/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('venueImage')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
		$this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
		$ads = array(
		    'venue_photo' => $data1['update_data']['file_name']
		);
		$this->venue_model->recordUpdate($category_id, $ads);
		echo "1";
	    }
	} else {
	    echo "file no upload";
	}
    }

    public function sendBill() {
	$fetch = $this->input->post('slug_id');
	$sendemail = $this->input->post('sendemail');
	$data['subjectto'] = 'Transport Bill ';
	$data['maito'] = $sendemail;
	$data['email_messagem'] = '
			 <tr>
				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="http://crm.sonuenterprisessiwan.com/transport/' . $fetch . '" target="_blank">Print Transport Bill</a></h2></td>
			</tr>';
	$this->load->view('mail/mail_template', $data);
	echo "Bill has been sent";
    }

    public function transport_show_list() {
	$invoice_slug = $this->uri->segment(4);
	$fetchinvoice = array(
	    'trans_slug' => $invoice_slug
	);
	$data['transObject'] = $this->transport_model->viewRecordAny($fetchinvoice);
	$data['head_title'] = 'Transport Bill Details';
	$data['contentView'] = 'admin/transport_show_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function addTrans() {
	$datasession = $this->session->userdata('userName_sess');
	$editinvoice = $this->input->post('edittrans');
	$challan_no = $this->input->post('challan_no');
	$trans_from = $this->input->post('trans_from');
	$trans_to = $this->input->post('trans_to');
	$doc_id = $this->input->post('doc_id');
	$dor_id = $this->input->post('dor_id');
	$truck_no = $this->input->post('truck_no');
	$owner_name = $this->input->post('owner_name');
	$owner_address = $this->input->post('owner_address');
	$driver_name = $this->input->post('driver_name');
	$driver_licence = $this->input->post('driver_licence');
	$driver_address = $this->input->post('driver_address');
	$trans_dos = $this->input->post('trans_dos');
	$trans_pos = $this->input->post('trans_pos');
	$t_amount = $this->input->post('t_amount');
	$t_advance = $this->input->post('t_advance');
	$slugedit = seturl($challan_no, $editinvoice);
	$particuler_id = $this->input->post('particuler_id');
	$trans_unit = $this->input->post('trans_unit');
	$data_i = count($particuler_id);
	$orders = array();
	if (!empty($editinvoice)) {
	    $pdata = array(
		'trans_challan_no' => $challan_no,
		'trans_date' => $trans_dos,
		'trans_from' => $trans_from,
		'trans_to' => $trans_to,
		'trans_name_consigntor' => $dor_id,
		//'trans_address' => $invoice_pos,
		'trans_name_consignee' => $doc_id,
		//'trans_consignee_address' => $invoice_number,
		'trans_truck_no' => $truck_no,
		'trans_owner_name' => $owner_name,
		'trans_owner_address' => $owner_address,
		'trans_driver_name' => $driver_name,
		'trans_driver_licence' => $driver_licence,
		'trans_driver_address' => $driver_address,
		'trans_amount' => $t_amount,
		'trans_advance' => $t_advance,
		'created_date' => date("Y-m-d H:i:s"),
		'trans_status' => '1'
	    );
	    $editinvoice1 = array(
		'trans_slug' => $editinvoice
	    );
	    $this->transport_model->recordUpdateAny($editinvoice1, $pdata);
	    $this->session->set_flashdata("message", "Transport Updated Successfully!");
	    redirect('admin/transport/transport_show_list/' . $editinvoice);
	} else {
	    $pdata = array(
		'trans_challan_no' => $challan_no,
		'trans_date' => $trans_dos,
		'trans_from' => $trans_from,
		'trans_to' => $trans_to,
		'trans_name_consigntor' => $dor_id,
		//'trans_address' => $invoice_pos,
		'trans_name_consignee' => $doc_id,
		//'trans_consignee_address' => $invoice_number,
		'trans_truck_no' => $truck_no,
		'trans_owner_name' => $owner_name,
		'trans_owner_address' => $owner_address,
		'trans_driver_name' => $driver_name,
		'trans_driver_licence' => $driver_licence,
		'trans_driver_address' => $driver_address,
		'trans_amount' => $t_amount,
		'trans_advance' => $t_advance,
		'created_date' => date("Y-m-d H:i:s"),
		'trans_status' => '1'
	    );
	    $this->transport_model->recordInsert($pdata);
	    $last_id = $this->db->insert_id();
	    $lassulinvoi = seturl($invoice_vehicle_no, $last_id);
	    $aslug = array(
		'trans_slug' => $lassulinvoi
	    );
	    $this->transport_model->recordUpdate($last_id, $aslug);
	    $data_i = count($particuler_id);
	    $orders = array();
	    for ($i = 0; $i < $data_i; $i++) {
		$orders[] = array(
		    'itemTrans_id' => null,
		    'itemTrans_trans' => $last_id,
		    'itemTrans_name' => $particuler_id[$i],
		    'itemTrans_unit' => $trans_unit[$i]
		);
	    }
	    if (!empty($data_i)) {
		$this->db->insert_batch('itemtypeTrans', $orders);
		$this->db->delete('itemtypeTrans', array('itemTrans_name' => ''));
	    }
	    $this->load->view('mail/mail_template', $data);
	    $this->session->set_flashdata("message", "Transport Bill  Created Successfully!");
	    redirect('admin/transport/transport_show_list/' . $lassulinvoi);
	}
    }

    public function additemtyem() {
	$particuler_id = $this->input->post('particuler_idup');
	$trans_unit = $this->input->post('trans_unit');
	$cateEdit = $this->input->post('cateEdit');
	if (!empty($cateEdit)) {
	    $pdata = array(
		'itemTrans_trans' => $cateEdit,
		'itemTrans_name' => $particuler_id,
		'itemTrans_unit' => $trans_unit
	    );
	    $this->itemtypetrans_model->recordInsert($pdata);
	    echo '1';
	}
    }

    public function resizing($path, $file) {

	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	//$config['create_thumb'] = TRUE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 300;
	$config['height'] = 336;
	$config['new_image'] = './assests/event/thumb/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
	$this->image_lib->clear();
    }
    public function deleteItem() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'itemTrans_id' => $del_id
	);
	$this->itemtypetrans_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
        echo '1';
    }

    public function delete_invoiceItem() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'itemInvoice_id' => $del_id
	);
	$this->itemtypeInvoice_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

    public function deleteTrans() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'view_status' => '0'
	);
	$this->transport_model->recordUpdate($del_id, $data);
	$this->session->set_flashdata("message", "Record Deleted!");
    }

}

?>