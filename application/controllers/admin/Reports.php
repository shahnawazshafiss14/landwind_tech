<?php

defined("BASEPATH") OR exit('No direct access');

class Reports extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$find_val = $this->input->post('val_id');
	$data['head_title'] = 'Reports';
	$data['contentView'] = 'admin/report_weekly';


	$ftdatast = 'oStatus=0';
	$ftdatastc = 'oStatus=3';
	$ftdatastcc = 'oStatus=1';
	$ftdatastcd = 'oStatus=2';

	if ($find_val == 1) {
	    $ftdata = "oStatus='0' AND (WEEK(oDate) = (WEEK(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (WEEK(oDate) = (WEEK(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (WEEK(oDate) = (WEEK(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (WEEK(oDate) = (WEEK(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumw($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsumw($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsumw($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsumw($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	} else if ($find_val == 2) {
	    $ftdata = "oStatus='0' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumm($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsumm($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsumm($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsumm($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	} else if ($find_val == 3) {
	    $ftdata = "oStatus='0' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumy($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsumy($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsumy($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsumy($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	} else {
	    $ftdata = "oStatus='0' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsum($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsum($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsum($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsum($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	}
	$this->load->view('admin/_template_model1', $data);
    }

    public function findvalyear() {
	$find_val = $this->input->post('val_id');
	//$data['head_title'] = 'Reports';
	//$data['contentView'] = 'admin/ajaxreports';


	$ftdatast = 'oStatus=0';
	$ftdatastc = 'oStatus=3';
	$ftdatastcc = 'oStatus=1';
	$ftdatastcd = 'oStatus=2';

	if ($find_val == 1) {
	    $ftdata = "oStatus='0' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumm($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsum($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsum($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsum($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	} else if ($find_val == 2) {
	    $ftdata = "oStatus='0' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (MONTH(oDate) = (MONTH(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumm($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsum($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsum($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsum($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	} else if ($find_val == 3) {
	    $ftdata = "oStatus='0' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumy($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsumy($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsumy($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsumy($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	} else {
	    $ftdata = "oStatus='0' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata1 = "oStatus='1' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata2 = "oStatus='2' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $ftdata3 = "oStatus='3' AND (YEAR(oDate) = (YEAR(NOW())) AND YEAR(oDate) = YEAR(NOW()))";
	    $data['orderdetails'] = $this->order_model->viewsumm($ftdatast);
	    $data['ordercount'] = $this->order_model->viewsummn($ftdata);
	    $data['orderdetailsc'] = $this->order_model->viewsum($ftdatastc);
	    $data['ordercountc'] = $this->order_model->viewsummn($ftdata1);
	    $data['orderdetailscc'] = $this->order_model->viewsum($ftdatastcc);
	    $data['ordercountcc'] = $this->order_model->viewsummn($ftdata2);
	    $data['orderdetailscd'] = $this->order_model->viewsum($ftdatastcd);
	    $data['ordercountcd'] = $this->order_model->viewsummn($ftdata3);
	}
	$this->load->view('admin/ajaxreports', $data);
    }

}

?>
