<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Expense extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Expense Details";
			$aray_fetch = array(
				'slug' => $find_url_slug
			);
			$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['expense'] = $this->expense_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		$data['contentView'] = 'admin/expense/show_details';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_expense(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Expense Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$expensed = $this->expense_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $expensed->company_name . ', '. $expensed->expense_name_ower .', '.$expensed->consignee .', '.$expensed->address;
				$arr_post['gst'] =  $expensed->gst_no;
				$arr_post['price'] =  $expensed->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'slug' => $find_url_slug
			);
			$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['expensed'] = $this->expense_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['head_title'] = "Add Expense";
		}
		$data['contentView'] = 'admin/expense/add';
		$this->load->view('admin/_template_model3', $data);
	}
	public function lists(){	 
	$find_url_slug = $this->uri->segment(3); 
		if(!empty($find_url_slug)){
			$get_expn_date = $this->input->get('expn_date');
			$get_expn_type = $this->input->get('expn_type');
			$get_expn_mode = $this->input->get('expn_mode');
			$get_start_date = $this->input->get('to');
			$get_end_date = $this->input->get('from'); 
			$get_acc_id = 'SONUO181209'; 
			$start_date = !empty($get_start_date) ? $get_start_date : '';
			$end_date = !empty($get_end_date) ? $get_end_date : '';
			if(!empty($get_acc_id)){
				$acc_id1 = " account_id='".$get_acc_id."' AND ";	
			}else{
				$acc_id1 ="";
			}
			if(!empty($get_expn_type)){
				$get_expn_type1 = " expn_type='".$get_expn_type."' AND ";	
			}else{
				$get_expn_type1 ="";
			}
			if(!empty($get_expn_mode)){
				$get_expn_mode1 = " expn_mode='".$get_expn_mode."' AND ";	
			}else{
				$get_expn_mode1 ="";
			}
			if(!empty($get_expn_date)){
				$get_expn_date1 = " expn_date='".$get_expn_date."' AND ";	
			}else{
				$get_expn_date1 ="";
			}
			if(!empty($start_date) && !empty($end_date)){
				$end_date1 = " (date_format(created_on, '%Y-%m-%d') BETWEEN '".$end_date."' AND '".$start_date."') AND ";	
			}else{
				$end_date1 = "";
			}
			if(!empty($get_expn_type) && !empty($get_expn_mode) && !empty($get_expn_date) && !empty($start_date) && !empty($end_date)){
				$data['expenseObject'] = 'recordnotfound';	
			}else{
				$select = "account_id from  tbl_expenses where ".$acc_id1. $get_expn_type1 . $get_expn_mode1 . $get_expn_date1 . $end_date1 . " view_status='1'";
				$data['expenseObject']= $this->expense_model->viewRecordGCINR($select);	
			} 
			
			$data['get_expn_date'] = $get_expn_date;
			$data['get_expn_type'] = $get_expn_type;
			$data['get_expn_mode'] = $get_expn_mode;
			$data['start_date'] = $start_date;
			$data['account_id'] = $get_acc_id;
			$data['end_date'] = $end_date;
			$data['head_title']='Expense List';		
			
			$data['contentView'] = 'admin/expense/lists';
			$this->load->view('admin/_template_model2', $data);  
		}else{
			redirect('page');
		}
  }
	public function saveexpense(){
		
		$expense_slugp = $this->input->post('expense_slug'); 
		$expn_by = $this->input->post('expn_by');
		$expn_date = $this->input->post('expn_date');
		$expn_type = $this->input->post('expn_type');
		$expn_amount = $this->input->post('expn_amount');
		$expn_desc = $this->input->post('expn_desc');
		$expn_mode = $this->input->post('expn_mode');
		$account_id = 'SONUO181209';
		$created_on = date('Y-m-d H:i:s');
		$rand = rand(); 
		$expense_slug = strtolower($expn_by.'-'.$rand);
			if(!empty($expense_slugp)){
				$aray_fetch = array(
					'expn_id' => $expense_slugp
				);
				$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
				
					$array_save = array( 
						'account_id' => $account_id,
						'expn_type' => $expn_type,
						'expn_date' => $expn_date,
						'expn_desc' => $expn_desc,
						'expn_amount' => $expn_amount,
						'expn_by' => $expn_by,
						'expn_mode' => $expn_mode,
						'created_on' => $created_on 
					);
					
					$inserted_id = $this->expense_model->recordUpdate($expense_slugp, $array_save);
					$this->session->set_flashdata('expense_save', 'success');
					redirect('admin/expense/lists/');
				 
			}else{
				if(!empty($expn_date) && !empty($expn_amount)){
				$array_save = array( 
					'account_id' => $account_id,
					'slug' => $expense_slug, 
					'expn_type' => $expn_type,
					'expn_date' => $expn_date,
					'expn_desc' => $expn_desc,
					'expn_amount' => $expn_amount,
					'expn_by' => $expn_by,
					'expn_mode' => $expn_mode,
					'created_on' => $created_on
				);
				$inserted_id = $this->expense_model->recordInsert($array_save);
				$this->session->set_flashdata('expense_save', 'success');
				redirect('admin/expense/add');
				}else {
					
				}
			}
		
		}
		/*Expense Type */
		public function type_add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'type_id' => $find_url_slug
			);
			$fetch_value = $this->expensetype_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['expenseObject'] = $this->expensetype_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['head_title'] = "Add Expense Type";
		}
		$data['contentView'] = 'admin/addExpense';
		$this->load->view('admin/_template_model3', $data);
	}
	public function topExpense(){	 
			$select = "account_id from  tbl_expense_type where view_status='1'";
			$data['topexpenses']= $this->expensetype_model->viewRecordGCINR($select);	
			$data['head_title']='Expense Type List';		
			$data['contentView'] = 'admin/veiw_expense';
			$this->load->view('admin/_template_model2', $data);  
		
  }
	public function saveexpensetype(){
		$expense_slugp = $this->input->post('editName'); 
		$type_name = $this->input->post('expenseName');
		$account_id = 'SONUO181209';
		$created_on = date('Y-m-d H:i:s');
		
			if(!empty($expense_slugp)){
				$aray_fetch = array(
					'type_id' => $expense_slugp
				);
				$fetch_value = $this->expensetype_model->viewRecordAny($aray_fetch);
				
					$array_save = array( 
						'type_name' => $type_name
					);
					$inserted_id = $this->expensetype_model->recordUpdate($expense_slugp, $array_save);
					$this->session->set_flashdata('expense_save', 'success');
					redirect('admin/expense/topExpense/');
				 
			}else{
				if(!empty($type_name)){
					$array_save = array( 
						'type_name' => $type_name
					);
				$inserted_id = $this->expensetype_model->recordInsert($array_save);
				$this->session->set_flashdata('expense_save', 'success');
				redirect('admin/expense/type_add');
				}else {
					
				}
			}
		
		}
		
}


?>