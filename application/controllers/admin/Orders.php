<?php

defined('BASEPATH') OR exit('No direct access');

class Orders extends Admin_Controller {

    public function __construct() {
	parent::__construct();
	$this->load->library("pagination");
    }

    public function orderList() {
	$product_id = $this->uri->segment(4);
	$config = array();
	$config["base_url"] = base_url() . "orders/orderList";
	$datarray = array(
	    'oStatus' => '1'
	);
	$config["total_rows"] = $this->order_model->record_countw($datarray);
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	$dataArry = array(
	    'oStatus' => '1'
	);
	$data['productObjects'] = $this->order_model->viewRecordPageR($dataArry, $config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'New Orders';
	$data['contentView'] = 'admin/view_order_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function orderDispatched() {
	$datarray = array(
	    'oStatus' => '2'
	);
	$product_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "admin/orders/orderDispatched";
	$config["total_rows"] = $this->order_model->record_countw($datarray);
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["links"] = $this->pagination->create_links();
	$dataArry = array(
	    'oStatus' => '2'
	);
	$data['productObjects'] = $this->order_model->viewRecordPageR($dataArry, $config["per_page"], $page);
	$data['head_title'] = 'Order Dispatched';
	$data['contentView'] = 'admin/view_order_list_d';
	$this->load->view('admin/_template_model2', $data);
    }

    public function orderCompleted() {
	$datarray = array(
	    'oStatus' => '2'
	);
	$product_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "admin/orders/orderCompleted";
	$config["total_rows"] = $this->order_model->record_countw($datarray);
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["links"] = $this->pagination->create_links();
	$dataArry = array(
	    'oStatus' => '2'
	);
	$data['productObjects'] = $this->order_model->viewRecordPageR($dataArry, $config["per_page"], $page);
	$data['head_title'] = 'Order Status';
	$data['contentView'] = 'admin/view_order_list_d_c';
	$this->load->view('admin/_template_model2', $data);
    }

    public function orderStatuspage() {
	$pid = $this->uri->segment(5);
	$oid = $this->uri->segment(4);
	$dataArry = array(
	    'order_id' => $oid
	);
	$data['orderObjects'] = $this->order_model->viewRecordAny($dataArry);
	$data['head_title'] = 'Orders Tracking';
	$data['contentView'] = 'admin/view_order_status_page';
	$this->load->view('admin/_template_model2', $data);
    }

    public function orderCanceled() {
	$datarray = array(
	    'oStatus' => '1'
	);
	$product_id = $this->uri->segment(5);
	$config = array();
	$config["base_url"] = base_url() . "admin/orders/orderCanceled";
	$config["total_rows"] = $this->order_model->record_countw($datarray);
	$config["per_page"] = 20;
	$config["uri_segment"] = 4;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
	$data["links"] = $this->pagination->create_links();
	$dataArry = array(
	    'oStatus' => '1'
	);
	$data['productObjects'] = $this->order_model->viewRecordPageR($dataArry, $config["per_page"], $page);
	$data['head_title'] = 'Order Canceled';
	$data['contentView'] = 'admin/view_order_list_c';
	$this->load->view('admin/_template_model2', $data);
    }

    public function orderInvoiceStatus() {
	$udate = $_POST['udate_id'];
	$udatet = $_POST['udate_stut'];
	$pid = $_POST['pid'];
	$qty = $_POST['qty'];
	$ade = array(
	    'status' => '1',
	    'dispatched' => '2',
	);
	$datas = array(
	    'oStatus' => $ade[$udatet],
	    'odispatDate' => date('Y-m-d H:i:s')
	);

	$get_i = $this->product_model->viewRecordId($pid);
	$pdata = array(
	    'p_qty' => $get_i->p_qty - $qty,
	    'p_updated_date' => date('Y-m-d H:i:s')
	);
	$jasdf = $this->order_model->viewRecordId($udate);
	$dataArray = array(
	    'order_userName' => $jasdf->order_userName,
	    'oStatus' => '0'
	);
	$detailFind = $this->order_model->viewOrderEmailUserEmail($dataArray);
	$arrad = array(
	    'p_product_id' => $detailFind->order_product_id
	);
	$findprodu = $this->product_model->viewRecordAny($arrad);
	$afind = array(
	    'order_tranction_id' => $detailFind->order_tranction_id
	);
	$diship = $this->ship_model->viewRecordAny($afind);
	$config = array(
	    'mailtype' => 'html',
	    'charset' => 'utf-8',
	    'priority' => '1'
	);
	$this->email->initialize($config);
	$this->email->from('shahnawaz.alam@dkd.co.in', COMPANYNAME);
	$this->email->to($diship->order_userName);
	$this->email->cc('shahnawazshafiss14@gmail.com');
	$this->email->subject('Order Dispatched #' . $diship->order_tranction_id);
	$message = "<html>";
	$message = '<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="#FFFFFF">
	    <table border="0" width="100%" bgcolor="#fff">
	    <tr>
		<td>
		<div align="center">
		<table border="0" width="70%" bgcolor="#f5f5f5"  style="border-top: 3px solid #2957a4;">
			<tr>
		<td>
				<table border="0" width="100%">
					<tr>
						<td width="120" style="  border-right: 1px solid #f1f1f1;"><a href="' . base_url() . '" target="_blank"><img border="0" src="' . COMPANYLOGO . '" width="100%"></a></td><td style="color: #000;
    text-align: center;
    width: 228px;
    font-size: 22px;
    float: right;
">Call Us:<br/> ' . COMPANYMOBILE . ' </td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
                    <th colspan="5" style="text-align:left;background-color:#ccc;"> Hello ' . $diship->sName . ', <br/><br/>Your order has been Dispatched.</th>
        	    </tr>

			<tr>
				<td style="font-family: Calibri;font-size: 26px;color: #005c9f!important;text-decoration: none;line-height: 25px;padding-top: 25px;
    padding-bottom: 25px;
    text-align: justify;
    border-top: 1px solid #f1f1f1;
    text-align: center;"><span style="color:#787878"> <strong style="color:#005c9f">Your order has been Dispatched.</strong><br></span></td>
			</tr>


				<tr style=" style="margin-top: 31px;display: inline-block;width: 100%;text-align: center;"">
				<td style="text-align: center;margin-top: 34px;width: 100%;display: inherit;"><span style="font-family:Calibri;font-size:14px;color:#787878;text-decoration:none;line-height:25px;text-align:left">  <strong>In case you require any kind of assistance, please feel free to contact us: -</strong><br> <strong>Call Us: ' . COMPANYMOBILE . ' </strong> </center> </span></td>
			</tr>
<p>We hope to see you again soon.</p>
<p>Happy to  serve you.</p>

			<tr>
				<td style="background-color: #7cb5de;padding-top: 0;padding-bottom: 0;margin-top: 20px;display: inline-block;width: 100%;"><center> <table width="540" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td style="font-family:Calibri;font-size:20px;color:#fff;text-decoration:none;line-height:25px;padding-top:5px;text-align:left">
					<p style="text-align:center">' . COMPANYCOPYRIGHT . ' </td> </tr> </tbody> </table> </center></td>
			</tr>
		</table>
		</div>
		</td>

	</tr>' . COMPANYNAMEFULLNAME . '</p>';

	$message .= "</table>";
	$message .= "</body></html>";
	$this->email->message($message);
	$this->email->send();
	$this->order_model->recordUpdate($udate, $datas);
	$this->product_model->recordUpdate($pid, $pdata);
	echo "invoice";
    }

    public function orderdeliveredStatus() {

	$udate = $_POST['udate_id'];
	$udatet = $_POST['udate_stut'];
	$pid = $_POST['pid'];
	$messagereg1 = $_POST['messagereg1'];
	$messageType1 = $_POST['messagetype1'];
	$messageBody = $_POST['messagebody'];

	if ($messageType1 === '3') {
	    $datas1 = array(
		'oStatus' => $messageType1,
		'ocomDate' => date('Y-m-d H:i:s')
	    );
	    $jasdf = $this->order_model->viewRecordId($udate);
	    $dataArray = array(
		'order_userName' => $jasdf->order_userName,
		'oStatus' => '0'
	    );
	    $detailFind = $this->order_model->viewOrderEmailUserEmail($dataArray);
	    $arrad = array(
		'p_product_id' => $detailFind->order_product_id
	    );
	    $findprodu = $this->product_model->viewRecordAny($arrad);
	    $afind = array(
		'order_tranction_id' => $detailFind->order_tranction_id
	    );
	    $diship = $this->ship_model->viewRecordAny($afind);
	    $config = array(
		'mailtype' => 'html',
		'charset' => 'utf-8',
		'priority' => '1'
	    );
	    $this->email->initialize($config);
	    $this->email->from('shahnawaz.alam@dkd.co.in', COMPANYNAME);
	    $this->email->to($diship->order_userName);
	    $this->email->cc('shahnawazshafiss14@gmail.com');
	    $this->email->subject('Order Received #' . $diship->order_tranction_id);
	    $message = "<html>";
	    $message = '<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="#FFFFFF">
	    <table border="0" width="100%" bgcolor="#fff">
	    <tr>
		<td>
		<div align="center">
		<table border="0" width="70%" bgcolor="#f5f5f5"  style="border-top: 3px solid #2957a4;">
			<tr>
		<td>
				<table border="0" width="100%">
					<tr>
						<td width="120" style="  border-right: 1px solid #f1f1f1;"><a href="' . base_url() . '" target="_blank"><img border="0" src="' . COMPANYLOGO . '" width="100%"></a></td><td style="color: #000;
    text-align: center;
    width: 228px;
    font-size: 22px;
    float: right;
">Call Us:<br/> ' . COMPANYMOBILE . ' </td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
                    <th colspan="5" style="text-align:left;background-color:#ccc;"> Hello ' . $diship->sName . ', <br/><br/>Your order has been received.</th>
        	    </tr>

			<tr>
				<td style="font-family: Calibri;font-size: 26px;color: #005c9f!important;text-decoration: none;line-height: 25px;padding-top: 25px;
    padding-bottom: 25px;
    text-align: justify;
    border-top: 1px solid #f1f1f1;
    text-align: center;"><span style="color:#787878"> <strong style="color:#005c9f">Your order has been received.</strong><br></span></td>
			</tr>


				<tr style=" style="margin-top: 31px;display: inline-block;width: 100%;text-align: center;"">
				<td style="text-align: center;margin-top: 34px;width: 100%;display: inherit;"><span style="font-family:Calibri;font-size:14px;color:#787878;text-decoration:none;line-height:25px;text-align:left">  <strong>In case you require any kind of assistance, please feel free to contact us: -</strong><br> <strong>Call Us: ' . COMPANYMOBILE . ' </strong> </center> </span></td>
			</tr>
<p>We hope to see you again soon.</p>
<p>Happy to  serve you.</p>

			<tr>
				<td style="background-color: #7cb5de;padding-top: 0;padding-bottom: 0;margin-top: 20px;display: inline-block;width: 100%;"><center> <table width="540" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td style="font-family:Calibri;font-size:20px;color:#fff;text-decoration:none;line-height:25px;padding-top:5px;text-align:left">
					<p style="text-align:center">' . COMPANYCOPYRIGHT . ' </td> </tr> </tbody> </table> </center></td>
			</tr>
		</table>
		</div>
		</td>

	</tr>' . COMPANYNAMEFULLNAME . '</p>';

	    $message .= "</table>";
	    $message .= "</body></html>";
	    $this->email->message($message);
	    $this->email->send();
	    $this->order_model->recordUpdate($udate, $datas1);
	} else {
	    $datas2 = array(
		'oStatus' => $messageType1,
		'ocancelDate' => date('Y-m-d H:i:s')
	    );
	    $jasdf = $this->order_model->viewRecordId($udate);
	    $dataArray = array(
		'order_userName' => $jasdf->order_userName,
		'oStatus' => '0'
	    );
	    $detailFind = $this->order_model->viewOrderEmailUserEmail($dataArray);
	    $arrad = array(
		'p_product_id' => $detailFind->order_product_id
	    );
	    $findprodu = $this->product_model->viewRecordAny($arrad);
	    $afind = array(
		'order_tranction_id' => $detailFind->order_tranction_id
	    );
	    $arr_resion = array(
		'0' => 'Done',
		'1' => 'The delivery is delayed',
		'2' => 'Bought it from somewhere else',
		'3' => 'Order placed by mistake',
		'4' => 'Expected delivery time is too long',
		'5' => 'Item Price / shipping cost is too high',
		'6' => 'My reason is not listed',
		'7' => 'Need to change shipping address'
	    );
	    $diship = $this->ship_model->viewRecordAny($afind);
	    $config = array(
		'mailtype' => 'html',
		'charset' => 'utf-8',
		'priority' => '1'
	    );
	    $this->email->initialize($config);
	    $this->email->from('shahnawaz.alam@dkd.co.in', COMPANYNAME);
	    $this->email->to($diship->order_userName);
	    $this->email->cc('shahnawazshafiss14@gmail.com');
	    $this->email->subject('Order Cancelled #' . $diship->order_tranction_id);
	    $message = "<html>";
	    $message = '<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="#FFFFFF">
	    <table border="0" width="100%" bgcolor="#fff">
	    <tr>
		<td>
		<div align="center">
		<table border="0" width="70%" bgcolor="#f5f5f5"  style="border-top: 3px solid #2957a4;">
			<tr>
		<td>
				<table border="0" width="100%">
					<tr>
						<td width="120" style="  border-right: 1px solid #f1f1f1;"><a href="' . base_url() . '" target="_blank"><img border="0" src="' . COMPANYLOGO . '" width="100%"></a></td><td style="color: #000;
    text-align: center;
    width: 228px;
    font-size: 22px;
    float: right;
">Call Us:<br/> ' . COMPANYMOBILE . ' </td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
                    <th colspan="5" style="text-align:left;background-color:#ccc;"> Hello ' . $diship->sName . ', <br/><br/>Your order has been Cancelled.</th>
        	    </tr>

			<tr>
				<td style="font-family: Calibri;font-size: 26px;color: #005c9f!important;text-decoration: none;line-height: 25px;padding-top: 25px;
    padding-bottom: 25px;
    text-align: justify;
    border-top: 1px solid #f1f1f1;
    text-align: center;"><span style="color:#787878"> <strong style="color:#005c9f">Your order has been Cancelled.</strong><br></span></td>
			</tr>
			</tr>
			<tr>
        <td style="text-align:center;" colspan="5">Due To: ' . $arr_resion[$getData] . '</td>
	</tr>


				<tr style=" style="margin-top: 31px;display: inline-block;width: 100%;text-align: center;"">
				<td style="text-align: center;margin-top: 34px;width: 100%;display: inherit;"><span style="font-family:Calibri;font-size:14px;color:#787878;text-decoration:none;line-height:25px;text-align:left">  <strong>In case you require any kind of assistance, please feel free to contact us: -</strong><br> <strong>Call Us: ' . COMPANYMOBILE . ' </strong> </center> </span></td>
			</tr>
<p>We hope to see you again soon.</p>
<p>Happy to  serve you.</p>

			<tr>
				<td style="background-color: #7cb5de;padding-top: 0;padding-bottom: 0;margin-top: 20px;display: inline-block;width: 100%;"><center> <table width="540" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td style="font-family:Calibri;font-size:20px;color:#fff;text-decoration:none;line-height:25px;padding-top:5px;text-align:left">
					<p style="text-align:center">' . COMPANYCOPYRIGHT . ' </td> </tr> </tbody> </table> </center></td>
			</tr>
		</table>
		</div>
		</td>

	</tr>' . COMPANYNAMEFULLNAME . '</p>';

	    $message .= "</table>";
	    $message .= "</body></html>";
	    $this->email->message($message);
	    $this->email->send();
	    $this->order_model->recordUpdate($udate, $datas2);
	}

	$isert = array(
	    'messageType' => $messageType1,
	    'messageResion' => $messagereg1,
	    'messageUser' => 'admin',
	    'messagePid' => $pid,
	    'messageOid' => $udate,
	    'messageBody' => $messageBody,
	    'createdDate' => date("Y-m-d H:i:s"),
	    'status' => $messageType1
	);
	$this->message_model->recordInsert($isert);
	echo "invoice";
    }

    public function orderCancelStatus() {

	$udate = $_POST['udate_id'];
	$udatet = $_POST['udate_stut'];
	$pid = $_POST['pid'];
	$messagereg = $_POST['messagereg'];
	$messageType = '1';
	$messageBody = $_POST['messageBody'];
	$ade = array(
	    'status' => '1',
	    'dispatched' => '2',
	    'cancel' => '1'
	);
	$datas = array(
	    'oStatus' => $ade[$udatet],
	    'ocancelDate' => date('Y-m-d H:i:s')
	);
	$jasdf = $this->order_model->viewRecordId($udate);
	$dataArray = array(
	    'order_userName' => $jasdf->order_userName,
	    'oStatus' => '0'
	);
	$detailFind = $this->order_model->viewOrderEmailUserEmail($dataArray);
	$arrad = array(
	    'p_product_id' => $detailFind->order_product_id
	);
	$findprodu = $this->product_model->viewRecordAny($arrad);
	$afind = array(
	    'order_tranction_id' => $detailFind->order_tranction_id
	);
	$arr_resion = array(
	    '0' => 'Done',
	    '1' => 'The delivery is delayed',
	    '2' => 'Bought it from somewhere else',
	    '3' => 'Order placed by mistake',
	    '4' => 'Expected delivery time is too long',
	    '5' => 'Item Price / shipping cost is too high',
	    '6' => 'My reason is not listed',
	    '7' => 'Need to change shipping address'
	);
	$diship = $this->ship_model->viewRecordAny($afind);
	$config = array(
	    'mailtype' => 'html',
	    'charset' => 'utf-8',
	    'priority' => '1'
	);
	$this->email->initialize($config);
	$this->email->from('shahnawaz.alam@dkd.co.in', COMPANYNAME);
	$this->email->to($diship->order_userName);
	$this->email->cc('shahnawazshafiss14@gmail.com');
	$this->email->subject('Order Cancelled #' . $diship->order_tranction_id);
	$message = "<html>";
	$message = '<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" text="#FFFFFF">
	    <table border="0" width="100%" bgcolor="#fff">
	    <tr>
		<td>
		<div align="center">
		<table border="0" width="70%" bgcolor="#f5f5f5"  style="border-top: 3px solid #2957a4;">
			<tr>
		<td>
				<table border="0" width="100%">
					<tr>
						<td width="120" style="  border-right: 1px solid #f1f1f1;"><a href="' . base_url() . '" target="_blank"><img border="0" src="' . COMPANYLOGO . '" width="100%"></a></td><td style="color: #000;
    text-align: center;
    width: 228px;
    font-size: 22px;
    float: right;
">Call Us:<br/> ' . COMPANYMOBILE . ' </td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
                    <th colspan="5" style="text-align:left;background-color:#ccc;"> Hello ' . $diship->sName . ', <br/><br/>Your order has been Cancelled.</th>
        	    </tr>

			<tr>
				<td style="font-family: Calibri;font-size: 26px;color: #005c9f!important;text-decoration: none;line-height: 25px;padding-top: 25px;
    padding-bottom: 25px;
    text-align: justify;
    border-top: 1px solid #f1f1f1;
    text-align: center;"><span style="color:#787878"> <strong style="color:#005c9f">Your order has been Cancelled.</strong><br></span></td>
			</tr>
			</tr>
			<tr>
        <td style="text-align:center;" colspan="5">Due To: ' . $arr_resion[$getData] . '</td>
	</tr>


				<tr style=" style="margin-top: 31px;display: inline-block;width: 100%;text-align: center;"">
				<td style="text-align: center;margin-top: 34px;width: 100%;display: inherit;"><span style="font-family:Calibri;font-size:14px;color:#787878;text-decoration:none;line-height:25px;text-align:left">  <strong>In case you require any kind of assistance, please feel free to contact us: -</strong><br> <strong>Call Us: ' . COMPANYMOBILE . ' </strong> </center> </span></td>
			</tr>
<p>We hope to see you again soon.</p>
<p>Happy to  serve you.</p>

			<tr>
				<td style="background-color: #7cb5de;padding-top: 0;padding-bottom: 0;margin-top: 20px;display: inline-block;width: 100%;"><center> <table width="540" border="0" cellspacing="0" cellpadding="0"> <tbody> <tr> <td style="font-family:Calibri;font-size:20px;color:#fff;text-decoration:none;line-height:25px;padding-top:5px;text-align:left">
					<p style="text-align:center">' . COMPANYCOPYRIGHT . ' </td> </tr> </tbody> </table> </center></td>
			</tr>
		</table>
		</div>
		</td>

	</tr>' . COMPANYNAMEFULLNAME . '</p>';

	$message .= "</table>";
	$message .= "</body></html>";
	$this->email->message($message);
	$this->email->send();
	$this->order_model->recordUpdate($udate, $datas);
	$isert = array(
	    'messageType' => $messageType,
	    'messageResion' => $messagereg,
	    'messageUser' => 'admin',
	    'messagePid' => $pid,
	    'messageOid' => $udate,
	    'messageBody' => $messageBody,
	    'createdDate' => date("Y-m-d H:i:s"),
	    'status' => '1'
	);
	$this->message_model->recordInsert($isert);
	echo "invoice";
    }

    public function orderInvoice() {
	$udate = $_POST['udate_id'];
	$ase = array(
	    'order_id' => $udate
	);
	$adf = $this->order_model->viewRecordIdProductId($ase);
	echo json_encode($adf);
    }

    /*
      public function orderdispatcheds() {
      $udate = $_POST['udate_id'];
      $udatet = $_POST['udate_stut'];
      $pid = $_POST['pid'];
      $ade = array(
      'status'=>'1',
      'dispatched'=>'2',
      'completed' => '3'
      );
      $datas = array(
      'oStatus'=> $ade[$udatet],
      'ocomDate'=> date('Y-m-d H:i:s')
      );
      $this->order_model->recordUpdate($udate, $datas);
      echo "invoice";
      } */

    public function orderdispatcheds() {
	$udate = $_POST['udate_id'];
	$udatet = $_POST['udate_stut'];
	$ase = array(
	    'order_id' => $udate
	);
	$adf = $this->order_model->viewRecordIdProductId($ase);
	echo json_encode($adf);
    }

}

?>