<?php

defined('BASEPATH') OR exit('No direct access');

class Location extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function addLocation() {
	$cateName = $this->input->post('cateName');
	$cateParent = $this->input->post('cateParent');
	$category_id = $this->input->post('cateEdit');
	if (!empty($category_id)) {
	    $adss = array(
		'location_name' => $cateName,
		'location_parent' => $cateParent,
		'location_status' => '0'
	    );
	    $this->location_model->recordUpdate($category_id, $adss);
	    echo "1";
	} else {
	    $ads = array(
		'location_name' => $cateName,
		'location_parent' => $cateParent,
		'location_status' => '0'
	    );
	    $this->location_model->recordInsert($ads);
	    echo "Location added Successfully";
	}
    }

    public function location() {
	$category_id = $this->uri->segment(3);
	$data['contentView'] = 'admin/addLocation';
	$condition = array('location_parent' => '0');
	$data['parent_location'] = $this->location_model->viewRecordAnyR($condition);
	if (isset($category_id)) {
	    $data['head_title'] = 'Edit Location';
	    $data['locationObject'] = $this->location_model->viewRecordId($category_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Location';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function viewLocation() {
	$condition = array('location_parent' => '0');
	$data['toplocations'] = $this->location_model->viewRecordAnyR($condition);
	$data['head_title'] = 'View Location';
	$data['contentView'] = 'admin/veiw_location';
	$this->load->view('admin/_template_model2', $data);
    }

    public function statea() {
	$del_id = $this->input->post('del_id');
	$data = array(
	    'location_parent' => $del_id
	);
	$datl = $this->location_model->viewRecordAnyR($data);
	$output = '<select name="userCity" id="userCity" class="form-control input-sm checker">';
	$output .= '<option value="">Select City</option>';
	foreach ($datl as $los) {
	    $output .= '<option value="' . $los->location_id . '">' . $los->location_name . '</option>';
	}
	$output .= '</select>';
	echo $output;
    }

    public function deleteLocation() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'location_id' => $del_id
	);
	$this->location_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>