<?php

defined('BASEPATH') OR exit('No direct access');

class Menu_top extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function addMenu() {
	$data = $this->input->post('menuName');
	$menuParent = $this->input->post('menuParent');
	$link = $this->input->post('link');
	$menu_id = $this->input->post('edit');
	if (!empty($menu_id)) {
	    $adss = array(
		'menu_name' => $data,
		'menu_parent_id' => $menuParent,
		'link' => $link,
		'menu_created_date' => date("Y-m-d H:i:s")
	    );
	    $this->menu_model->recordUpdate($menu_id, $adss);
	    echo "1";
	} else {
	    $ads = array(
		'menu_name' => $data,
		'menu_parent_id' => $menuParent,
		'link' => $link,
		'menu_created_date' => date("Y-m-d H:i:s")
	    );
	    $this->menu_model->recordInsert($ads);
	    echo "Menu added Successfully";
	}
    }

    public function permisstion_action() {
	$data['head_title'] = 'Add Permission';
	$data['contentView'] = 'admin/add_menuPermission';
	$data['menuPermission'] = $this->menu_model->viewRecordAll();
	$this->load->view('admin/_template_model1', $data);
    }

    public function topMenu() {
	$menu_id = $this->uri->segment(3);
	$data_id = array(
	    'menu_parent_id' => '0'
	);
	$data['parent_menu'] = $this->menu_model->viewRecordAnyR($data_id);
	$data['contentView'] = 'admin/add_topMenu';
	if (isset($menu_id)) {
	    $data['head_title'] = 'Edit Menu';

	    $data['menuObject'] = $this->menu_model->viewRecordId($menu_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Menu';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function viewtopMenu() {
	$data['topmenus'] = $this->menu_model->viewRecordAll();
	$data['head_title'] = 'View Menu';
	$data['contentView'] = 'admin/veiw_topMenu';
	$this->load->view('admin/_template_model2', $data);
    }

    public function deleteMenu() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'menu_id' => $del_id
	);
	$this->menu_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>