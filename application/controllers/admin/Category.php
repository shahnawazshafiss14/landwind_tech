<?php

defined('BASEPATH') OR exit('No direct access');

class Category extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function findpartcat() {
	$val_id = $_POST['val_id'];
	$aarrdat = array(
	    'category_parent_id' => $val_id
	);
	$data['fetch_cat'] = $this->category_model->viewRecordAnyR($aarrdat);
	$this->load->view('admin/ajaxproductcategory', $data);
    }

    public function addCategory() {
	//$menu_id = $this->uri->segment(4);
	$cateName = $this->input->post('cateName');
	$cateParent = $this->input->post('cateParent');
	$category_id = $this->input->post('cateEdit');
	$category_title = $this->input->post('cateTitle');
	$category_slug = str_replace(" ", "-", strtolower($category_title));
	$selectMenu = $this->input->post('selectMenu');
	if (!empty($category_id)) {
	    $adss = array(
		'category_name' => $cateName,
		//'category_parent_id' => $cateParent,
		'menu_id' => $selectMenu,
		'category_title' => $category_title,
		'category_link' => $category_slug,
		'category_created_date' => date("Y-m-d H:i:s")
	    );
	    $this->category_model->recordUpdate($category_id, $adss);
	    echo "1";
	} else {
	    $ads = array(
		'category_name' => $cateName,
		'category_parent_id' => $cateParent,
		'category_title' => $category_title,
		'category_link' => $category_slug,
		'menu_id' => $selectMenu,
		'category_created_date' => date("Y-m-d H:i:s")
	    );
	    $this->category_model->recordInsert($ads);
	    echo "1";
	}
    }

    public function details() {
	$category_id = $this->uri->segment(4);
	$data['contentView'] = 'admin/category_show_list';
	$data['categories'] = $this->category_model->category_menu();
	$condition = array('category_parent_id' => '0');
	$data['parent_category'] = $this->category_model->viewRecordAnyR($condition);
	if (isset($category_id)) {
	    $data['head_title'] = 'Edit Category';
	    $data['cateObject'] = $this->category_model->viewRecordId($category_id);
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function adddetails() {
	$cateDescription = $this->input->post('cateDescription');
	$cateVediourl = $this->input->post('cateVediourl');
	$category_id = $this->input->post('editName');
	if (!empty($category_id)) {
	    $adss = array(
		'cateVediourl' => $cateVediourl,
		'cateDescription' => $cateDescription
	    );
	    $this->category_model->recordUpdate($category_id, $adss);
	    echo "category/details/" . $category_id;
	}
    }

    public function extraimages() {
	$category_id = $this->input->post('category_ide');
	if (isset($_FILES["category_image"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/banner/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('category_image')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
		$ads = array(
		    'category' => 'category',
		    'fiename' => $data1['update_data']['file_name'],
		    'pid' => $category_id
		);
		$this->extraimages_model->recordInsert($ads);
		redirect('admin/category/details/' . $category_id);
	    }
	} else {
	    echo "file no upload";
	}
    }

    public function addCategoryc() {
	$category_id = $this->input->post('cateEdit');
	if (isset($_FILES["cateimage"]["name"])) {
	    $config['encrypt_name'] = TRUE;
	    $config['upload_path'] = './assests/banner/';
	    $config['allowed_types'] = 'jpg|jpeg|png|gif';
	    $this->load->library('upload', $config);
	    if (!$this->upload->do_upload('cateimage')) {
		echo $this->upload->display_errors();
	    } else {
		$data1 = array('update_data' => $this->upload->data());
		$ads = array(
		    'cateimage' => $data1['update_data']['file_name']
		);
		$this->category_model->recordUpdate($category_id, $ads);

		echo "1";
	    }
	} else {
	    echo "file no upload";
	}
    }

    public function topCategory() {
	$category_id = $this->uri->segment(4);
	$data['contentView'] = 'admin/addCategory';
	$data['categories'] = $this->category_model->category_menu();
	$condition = array('category_parent_id' => '0');
	$data['parent_category'] = $this->category_model->viewRecordAnyR($condition);
	if (isset($category_id)) {
	    $data['head_title'] = 'Edit Category';
	    $data['categoryObject'] = $this->category_model->viewRecordId($category_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Category';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function viewtopCategory() {
	$condition = array('category_parent_id' => '0');
	$data['topcategorys'] = $this->category_model->viewRecordAnyR($condition);
	$data['head_title'] = 'View Category';

	$data['contentView'] = 'admin/veiw_topCategory';
	$this->load->view('admin/_template_model2', $data);
    }

    public function deleteImageExtra() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'imagesid' => $del_id
	);
	$this->extraimages_model->recordDelete($data);
	echo '1';
    }

    public function deleteCategory() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'category_id' => $del_id
	);
	$this->category_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>