<?php

defined('BASEPATH') OR exit('No direct access');

class Devlboy extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$data['users'] = $this->devl_model->viewRecordAll();
	$data['head_title'] = 'View Delivery Boy';
	$data['contentView'] = 'admin/devlLst';
	$this->load->view('admin/_template_model2', $data);
    }
	public function addDevboy() {
		$product_id = $this->uri->segment(4);
		$data['contentView'] = 'admin/add_devlboy';
		if (isset($product_id)) {
			$data['head_title'] = 'Edit Delivery Boy';
			$data['userObject'] = $this->devl_model->viewRecordId($product_id);
			$this->load->view('admin/_template_model3', $data);
		} else {
			$data['head_title'] = 'Add Delivery Boy';
			$this->load->view('admin/_template_model3', $data);
		}
    }
	public function devl_show_list() {
		$product_id = $this->uri->segment(4);
		$data['userObjects'] = $this->devl_model->viewRecordId($product_id);
		$data['head_title'] = 'Delivery Details';
		$data['contentView'] = 'admin/devl_show_list';
		$this->load->view('admin/_template_model2', $data);
    }
	public function ajax_upload() {
		$productEdit = $this->input->post('devlEdit');
		$datas = array(
			'devlName' => $this->input->post('user_name'),
			'devlEmail' => $this->input->post('user_email'),
			'devlState' => $this->input->post('user_state'),
			'devlCity' => $this->input->post('user_city'),
			'devlAddress' => $this->input->post('user_address'),
			'devlMobile ' => $this->input->post('user_mobile'),
			'devlStatus' => $this->input->post('user_status'),
			'devlCreated' => date('Y-m-d H:i:s')
		);
		if (!empty($productEdit)) {
			$this->devl_model->recordUpdate($productEdit, $datas);
			$this->session->set_flashdata('product_uploaded', 'Your user has been updated Successfully');
			redirect('admin/devlboy/index');
		} else {
			$this->devl_model->recordInsert($datas);
			$this->session->set_flashdata('product_uploaded', 'Your user has been created Successfully');
			redirect('admin/devlboy/addDevboy');
		}
    }

    public function deleteDevboy() {
		$del_id = $_POST['del_id'];
		$val_id = $_POST['val_id'];
		$data = array(
			'devlStatus' => $val_id
		);
		$id = array(
			'd_id' => $del_id
		);
		$this->devl_model->recordUpdateAny($id,$data);
		$this->session->set_flashdata("message", "Record Not Updated!");
    }

    public function newsLetter() {
	$data['newsletters'] = $this->newsletter_model->viewRecordAll();
	$data['head_title'] = 'View Newsletter';
	$data['contentView'] = 'admin/newsLetter';
	$this->load->view('admin/_template_model2', $data);
    }

    public function deleteNews() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'newsLetter_Id' => $del_id
	);
	$this->newsletter_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record deleted!");
    }

}

?>