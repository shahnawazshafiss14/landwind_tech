<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Admin_Controller {

//    public function index() {
//	$data['head_title'] = 'Cart';
//	$data['items'] = $this->cart->contents();
//	$this->load->view('front/cart', $data);
//    }


    public function index() {
	//$data['items'] = $this->cart->contents();
	$product_id = $this->uri->segment(4);
	$config = array();
	$config["base_url"] = base_url() . "cart/index";
	$config["total_rows"] = $this->cart_model->record_count();
	$config["per_page"] = 20;
	$config["uri_segment"] = 3;
	$this->pagination->initialize($config);
	$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	$dataArry = array(
	    'sessionid_email' => $this->session->userdata('userName_sess')
	);
	$data["cartObjects"] = $this->cart_model->viewRecordPageR($dataArry, $config["per_page"], $page);
	$data["links"] = $this->pagination->create_links();
	$data['head_title'] = 'View Cart';
	$data['contentView'] = 'admin/view_cart_list';
	$this->load->view('admin/_template_model2', $data);
    }

    public function orderd() {
	$amount = $this->input->post('productPrice');
	$orderdescription = $this->input->post('orderdescription');
	$orderby_self = $this->input->post('orderby_self');
	$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	$getEamil = $this->session->userdata('userName_sess');
	$orduid = uniqid();
	$seldata = array(
	    'sessionid_email' => $getEamil
	);
	$this->db->where($seldata);
	$this->db->select('tcart_id, sessionid_email, tproduct_id, tproduct_qty, tproduct_price');
	$query = $this->db->get('tempcart');
	foreach ($query->result() as $row) {
	    $datas = array(
		"order_tranction_id" => $orduid . $row->tcart_id,
		"order_userName" => $getEamil,
		"order_product_id" => $row->tproduct_id,
		"order_qty" => $row->tproduct_qty,
		"order_price" => $row->tproduct_price,
		"orderdescription" => $orderdescription,
		"orderby_self" => $orderby_self,
		"oTransationId" => $txnid,
		"oUdatedDate" => date("Y-m-d H:i:s"),
		"oDate" => date("Y-m-d H:i:s"),
		"oStatus" => '0'
	    );
	    $this->db->insert('ordertable', $datas);
	}
	$datadel = array(
	    'sessionid_email' => $getEamil
	);
	$this->db->delete('tempcart', $datadel);
	redirect('cart/success');
    }

    public function re_view() {
	$data['items'] = $this->cart->contents();
	$this->load->view('front/displayCart_1', $data);
    }

    public function checkout() {
	$data['head_title'] = 'Checkout';
	$data['items'] = $this->cart->contents();
	$this->load->view('front/checkout', $data);
    }

    public function success() {
	$data['head_title'] = 'Success';
	$data['contentView'] = 'admin/success';
	$this->load->view('admin/_template_model2', $data);
    }

    public function cancel() {
	$data['head_title'] = 'Fail';
	$this->load->view('front/fail');
    }

    public function payu() {
	$this->load->view('form1');
    }

    public function re_view_count() {
	$cisession = $this->session->userdata('userName_sess');
	$datar = array(
	    'sessionid_email' => $cisession
	);
	echo $checkExits = $this->cart_model->recordCheckAvaibility($datar);
    }

    public function add() {
	$id = $_POST['product_id'];
	$quantity = $_POST['quantity'];
	$pric = $this->product_model->viewRecordId($id);
	$datar = array(
	    'sessionid_email' => $_POST['sessionid_email'],
	    'tproduct_id' => $_POST['product_id']
	);
	$qty = $this->cart_model->viewRecordAny($datar);
	$checkExits = $this->cart_model->recordCheckAvaibility($datar);
	if ($checkExits > 0) {
	    $pid = array(
		'sessionid_email' => $_POST['sessionid_email'],
		'tproduct_id' => $_POST['product_id']
	    );
	    $data1 = array(
		"tproduct_qty" => $qty->tproduct_qty + $quantity,
		"tproduct_price" => $qty->tproduct_price + $pric->p_price,
		"tcart_updated" => date("Y-m-d H:i:s"),
		"tcart_status" => '0'
	    );
	    $this->cart_model->recordUpdateC($pid, $data1);
	} else {
	    $data1 = array(
		"tproduct_id" => $_POST['product_id'],
		"tproduct_qty" => $_POST['quantity'],
		"sessionid_email" => $_POST['sessionid_email'],
		"tproduct_price" => $pric->p_price,
		"tcart_updated" => date("Y-m-d H:i:s"),
		"tcart_status" => '0'
	    );
	    $this->cart_model->recordInsert($data1);
	}
    }

    public function view() {

	$cisession = $this->session->userdata('__ci_last_regenerate');
	$datar = array(
	    'tcartsession' => $cisession
	);
	$checkExits = $this->cart_model->recordCheckAvaibility($datar);
	$carts = $this->cart_model->viewRecordAnyR($datar);
	$output = '';
	if ($checkExits > 0) {




	    $output .= '<ul class="items clearfix">';
	    foreach ($carts as $ft) {

		$ae = array(
		    'p_product_id' => $ft->tproduct_id
		);
		$dae = $this->product_model->viewRecordAny($ae);

		$output .= '<li>
                        <div class="item-thumbnail"> <a href="' . base_url() . 'cart/' . str_replace(" ", "-", strtolower($ft->tproduct_name)) . '/' . $ft->tproduct_id . ' " title=""> <img src="' . BASEURLFRONTASSET . 'product/' . $ft->tcart_image . '" alt=""> </a> </div>
                        <a href="#" class="item-name">HP LP3065</a> <span class="item-price">$147.70</span>
                        <div class="clearfix"></div>
                      </li>';
	    }

	    $output .= '</ul>
                <div class="mini-cart-total">
                      <table>
                        <tbody>
                          <tr>
                            <td class="right"><b>Sub-Total:</b></td>
                            <td class="right">$124.00</td>
                          </tr>
                          <tr>
                            <td class="right"><b>Eco Tax (-2.00):</b></td>
                            <td class="right">$2.00</td>
                          </tr>
                          <tr>
                            <td class="right"><b>VAT (17.5%):</b></td>
                            <td class="right">$21.70</td>
                          </tr>
                          <tr>
                            <td class="right"><b>Total:</b></td>
                            <td class="right">$147.70</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>';
	}
	echo $output;
    }

    public function load() {
	echo $this->view();
    }

    public function remove() {
	$row_id = $_POST["row_id"];
	$del_id = array(
	    'tcart_id' => $row_id
	);
	$this->cart_model->recordDelete($del_id);
	echo $this->view();
    }

    public function update() {
	$row_id = $_POST['rowid'];
	$qty = $_POST['qty'];
	$data = array(
	    'rowid' => $row_id,
	    'qty' => $qty
	);
	$this->cart->update($data);
	$data1 = array(
	    'tproduct_qty' => $qty
	);
	$this->cart_model->recordUpdate($row_id, $data1);
    }

}

?>