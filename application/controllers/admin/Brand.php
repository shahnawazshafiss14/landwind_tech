<?php

defined('BASEPATH') OR exit('No direct access');

class Brand extends Admin_Controller {

    public function __construct() {
		parent::__construct();
    }
	public function updatestatus() {
		$pid = $this->input->post('bid');
		$datas = array(
			'b_status' => $this->input->post('val_id')
		);
		$this->brand_model->recordUpdate($pid, $datas);
		echo '1';
    }

    public function add_brandp() {
		$b_name = $this->input->post('b_name');
		$adss = array(
			'b_name' => $b_name
		);
	 
		$this->brand_model->recordInsert($adss);
		$html = '<select name="prdouct_brand" id="prdouct_brand" class="form-control">
				<option value="">Select Brand</option>';
			$fetch_brand = $this->brand_model->viewRecordAll();
				foreach($fetch_brand as $fb){
			$html .= '<option value="'. $fb->b_id .'" '. $val .'> '. $fb->b_name.'</option>';
				}
				echo $html .= '</select>'; 
    }
	public function add_brand() {
		$b_name = $this->input->post('b_name');
		$menu_id = $this->input->post('edit');
		if (!empty($menu_id)) {
			$adss = array(
				'b_name' => $b_name
			);
			$this->brand_model->recordUpdate($menu_id, $adss);
			echo "1";
		} else {
			$adss = array(
				'b_name' => $b_name
			);
			$this->brand_model->recordInsert($adss);
			echo "1";
		}
		
    }

    public function addbrand() {
	$menu_id = $this->uri->segment(4);
	$data['contentView'] = 'admin/add_brand';
	if (isset($menu_id)) {
	    $data['head_title'] = 'Edit brand';

	    $data['brandObject'] = $this->brand_model->viewRecordId($menu_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add brand';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function listbrand() {
	$brandarr = array(
		'view_status' => '1' 
	);	
	$data['topmenus'] = $this->brand_model->viewRecordAnyR($brandarr);
	$data['head_title'] = 'View brand';
	$data['contentView'] = 'admin/veiw_brand';
	$this->load->view('admin/_template_model2', $data);
    }

    public function deletebrand() {
	$del_id = $_POST['del_id'];
	$id = array(
	'b_id' => $del_id
	);	
	$data = array(
	    'view_status' => '0'
	);
	
	$this->brand_model->recordUpdateAny($id, $data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>