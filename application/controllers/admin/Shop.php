<?php

defined("BASEPATH") OR exit('No direct access');

class Shop extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function updatestatus() {
        $pid = $this->input->post('pid');
        $datas = array(
            'shop_status' => $this->input->post('val_id')
        );
        $this->shop_model->recordUpdate($pid, $datas);
    }

    public function shopApprove() {
        $fetch = $this->input->post('shop_id');
        $pwd = 'PMA' . substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $fetchid = $this->shop_model->viewRecordId($fetch);
        $shop_email = $fetchid->shop_email;
        $data = array(
            'shop_pwd' => strtoupper($pwd),
            'shop_status' => '1'
        );
        $this->shop_model->recordUpdate($fetch, $data);
        $data['subjectto'] = 'Shop Approved ';
        $data['maito'] = $shop_email;
        $data['email_messagem'] = '
			<tr>

				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);">Hi ' . $fetchid->shop_name . '!  shop request approved.</h2>
                <p style="padding:0 20px">
                <strong style="font-size:17px">User Name </strong>' . $shop_email . ' <br>
                <strong style="font-size:17px">Password </strong>' . strtoupper($pwd) . '
                </p>
                <br>
                <p style="padding: 0 20px; font-size:16">In case you require any kind of assistance, please feel free to contact us. <br>
            <strong>Call us:</strong> +(626)-272-0272</p>

                </td>
			</tr>


            ';
        $this->load->view('mail/mail_template', $data);
        echo "1";
    }

    public function shop_list() {
        $product_id = $this->uri->segment(5);
        $ararydat = array(
            'view_status' => '1'
        );
        $data["shopObjects"] = $this->shop_model->viewRecordAnyR($ararydat);
        $data['head_title'] = 'List Shop';
        $data['contentView'] = 'admin/view_shop_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function changepass() {
        $getEamil = $this->session->userdata('userfront');
        $reloginpass = $this->input->post('reloginpass');
        $dataEmail = array(
            'userEmail' => $getEamil
        );
        if (!empty($getEamil)) {
            $detailEmail = $this->shop_model->recordCheckAvaibility($dataEmail);
            if ($detailEmail > 0) {
                $emailfetch = $this->shop_model->viewRecordAny($dataEmail);
                $ids = $emailfetch->u_id;
                $data = array(
                    'userPwd' => $reloginpass
                );
                $this->shop_model->recordUpdate($ids, $data);
                echo "Password has been updated";
            } else {
                echo "Email id not matched";
            }
        }
    }

    public function statea() {
        $del_id = $this->input->post('del_id');
        $data = array(
            'location_parent' => $del_id
        );
        $datl = $this->location_model->viewRecordAnyR($data);
        $output = ' <select name="shop_state1" id="shop_state1" class="form-control">';
        $output .= '<option value="">Select State</option>';
        foreach ($datl as $los) {
            $output .= '<option value="' . $los->location_id . '">' . $los->location_name . '</option>';
        }
        $output .= '</select>';
        echo $output;
    }

    public function shopshoplist() {
        $data['head_title'] = 'Event List';
        $this->load->view('front/shop_shop_list', $data);
    }

    public function edit() {
        $artisfet = $this->uri->segment(4);
        $data['contentView'] = 'admin/add_shop';
        if (isset($artisfet)) {
            $data['head_title'] = 'Edit Shop';
            $dataas = array(
                'shop_slug' => $artisfet
            );
            $data['shopObject'] = $this->shop_model->viewRecordAny($dataas);
            $this->load->view('admin/_template_model2', $data);
        } else {
            $data['head_title'] = 'Add Shop';
            $this->load->view('admin/_template_model3', $data);
        }
    }

    public function updateImageShop() {
        $category_id = $this->input->post('shopid');
        if (isset($_FILES["shopImage"]["name"])) {
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = './assests/shop/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('shopImage')) {
                echo $this->upload->display_errors();
            } else {
                $data1 = array('update_data' => $this->upload->data());
                $this->resizing($data1['update_data']['full_path'], $data1['update_data']['file_name']);
                $ads = array(
                    'shop_image' => $data1['update_data']['file_name']
                );
                $this->shop_model->recordUpdate($category_id, $ads);
                echo "1";
            }
        } else {
            echo "file no upload";
        }
    }

    public function addShop() {
        $shop_id = $this->input->post('shopid');
        $shop_name = $this->input->post('shop_name');
        $shop_phone = $this->input->post('shop_phone');
        $slugurl = seturl($shop_name, $shop_id);
        if (!empty($shop_id)) {
            $pdata = array(
                'shop_name' => $shop_name,
                'shop_slug' => $slugurl,
                'shop_address' => $this->input->post('shop_address'),
                'shop_email' => $this->input->post('shop_email'),
                'shop_phone' => $this->input->post('shop_phone'),
                'shop_aadhar' => $this->input->post('shop_gstin'),
                'shop_state' => $this->input->post('shop_state'),
                'shop_city' => $this->input->post('shop_city'),
                'shop_zipcode' => $this->input->post('shop_zipcode'),
                'shop_area' => $this->input->post('shop_area'),
                'shop_description' => $this->input->post('shop_description'),
                'created_date' => date("Y-m-d H:i:s")
            );
            $this->shop_model->recordUpdate($shop_id, $pdata);
            $this->session->set_flashdata("shop_uploaded", "update successfully");
            redirect('admin/shop/shop_show_list/' . $slugurl);
        } else {
            $pdata = array(
                'shop_name' => $shop_name,
                'shop_address' => $this->input->post('shop_address'),
                'shop_email' => $this->input->post('shop_email'),
                'shop_phone' => $this->input->post('shop_phone'),
                'shop_aadhar' => $this->input->post('shop_gstin'),
                'shop_state' => $this->input->post('shop_state'),
                'shop_city' => $this->input->post('shop_city'),
                'shop_zipcode' => $this->input->post('shop_zipcode'),
                'shop_area' => $this->input->post('shop_area'),
                'shop_description' => $this->input->post('shop_description'),
                'created_date' => date("Y-m-d H:i:s")
            );
            $this->shop_model->recordInsert($pdata);
            $last_id = $this->db->insert_id();
            $finsulg = seturl($shop_name, $last_id);
            $aslug = array(
                'shop_slug' => $finsulg
            );
            $this->shop_model->recordUpdate($last_id, $aslug);
            $this->session->set_flashdata("shop_uploaded", "Registration Successfully!");
            redirect('admin/shop/shop_show_list/' . $finsulg);
        }
    }

    public function shop_show_list() {
        $shop_id = $this->uri->segment(4);
        $shop_idft = array(
            'shop_slug' => $shop_id
        );
        $data['shopObject'] = $this->shop_model->viewRecordAny($shop_idft);
        $data['head_title'] = 'Shops Details';
        $data['contentView'] = 'admin/shop_show_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function shop_by_paid() {
        $shop_id = $this->uri->segment(4);
        $shop_idft = array(
            'shop_slug' => $shop_id
        );
        $data['shopObject'] = $this->shop_model->viewRecordAny($shop_idft);
        $data['head_title'] = 'Shops Details';
        $data['contentView'] = 'admin/shop_shop_by_paid';
        $this->load->view('admin/_template_model2', $data);
    }

    public function shop_by_balance() {
        $shop_id = $this->uri->segment(4);
        $shop_idft = array(
            'shop_slug' => $shop_id
        );
        $data['shopObject'] = $this->shop_model->viewRecordAny($shop_idft);
        $data['head_title'] = 'Shops Details';
        $data['contentView'] = 'admin/shop_shop_balanced';
        $this->load->view('admin/_template_model2', $data);
    }

    public function shop_paid() {
        $shop_id = $this->uri->segment(4);
        $shop_idft = array(
            'shop_slug' => $shop_id
        );
        $data['shopObject'] = $this->shop_model->viewRecordAny($shop_idft);
        $data['head_title'] = 'Shops Details';
        $data['contentView'] = 'admin/shop_shop_paid';
        $this->load->view('admin/_template_model2', $data);
    }

    public function shop_month() {
        $shop_id = $this->uri->segment(4);
        $data['shop_month'] = $this->uri->segment(5);
        $data['month_from'] = $this->input->get('to');
        $data['month_to'] = $this->input->get('from');
        $shop_idft = array(
            'shop_slug' => $shop_id
        );
        $data['shopObject'] = $this->shop_model->viewRecordAny($shop_idft);
        $data['head_title'] = 'Shops monthly report';
        $data['contentView'] = 'admin/shop_month_details';
        $this->load->view('admin/_template_model2', $data);
    }

    public function resizing($path, $file) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 336;
        $config['new_image'] = './assests/shop/thumb/' . $file;
        $this->load->library('image_lib', $config);
        $this->image_lib->initialize();
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    public function deleteShop() {
        $del_id = $_POST['del_id'];
        $data = array(
            'view_status' => '0'
        );
        $this->shop_model->recordUpdate($del_id, $data);
        $this->session->set_flashdata("message", "Record Deleted!");
    }

}

?>