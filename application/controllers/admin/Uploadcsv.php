<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadcsv extends CI_Controller {

    public function __construct() {
	parent::__construct();
	$this->load->helper('url');
	$this->load->model('Welcome_model', 'welcome');
    }

    public function index() {
	$this->data['view_data'] = $this->welcome->view_data();
	$this->load->view('admin/excelimport', $this->data, FALSE);
    }

    function ExportCSV() {
	$this->load->dbutil();
	$this->load->helper('file');
	$this->load->helper('download');
	$delimiter = ",";
	$newline = "\r\n";
	$fileName = "product_list.csv";
	$queryFetch = "SELECT * FROM tblproduct WHERE 1";
	$result = $this->db->query($queryFetch);
	$data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
	force_download($fileName, $data);
    }

    //////////////////Import subscriber emails ////////////////////////////////
    public function importbulkemail() {
	$data['head_title'] = 'import';
	$data['contentView'] = 'admin/excelimport';
	$this->load->view('admin/_template_model1', $data);

	//$this->load->view('excelimport');
    }

    public function import() {
	if (isset($_POST["import"])) {
	    $filename = $_FILES["file"]["tmp_name"];
	    if ($_FILES["file"]["size"] > 0) {
		$file = fopen($filename, "r");
		while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE) {
		    $data = array(
			'p_product_id' => $importdata[0],
			'p_item_no' => $importdata[1],
			'p_title' => $importdata[2],
			'p_image' => $importdata[3],
			'p_price' => $importdata[4],
			'p_discount' => $importdata[5],
			'p_specialOffer' => $importdata[6],
			'p_qty' => $importdata[7],
			'p_skucode' => $importdata[8],
			'p_model' => $importdata[9],
			'p_related_product' => $importdata[10],
			'p_size' => $importdata[11],
			'p_weight' => $importdata[12],
			'p_item_menu' => $importdata[13],
			'p_item_category_p' => $importdata[14],
			'p_itemType' => $importdata[15],
			'p_upc' => $importdata[16],
			'p_sellsPackage' => $importdata[17],
			'p_brand' => $importdata[18],
			'p_short_description' => $importdata[19],
			'p_subcategory2' => $importdata[20],
			'p_wholesaleprice' => $importdata[21],
			'p_gold_member_price' => $importdata[22],
			'p_pkg' => $importdata[23],
			'p_shipping' => $importdata[24],
			'p_gold_member_shipping' => $importdata[25],
			'p_discription' => $importdata[26],
			'p_thumbnail' => $importdata[27],
			'outofstock' => $importdata[28],
			'p_status' => $importdata[29],
			'p_updated_date' => $importdata[29],
			'p_created_date' => date('Y-m-d'),
		    );
		    $insert = $this->welcome->insertCSV($data);
		}
		fclose($file);
		$this->session->set_flashdata('message', 'Data are imported successfully..');
		redirect('admin/uploadcsv/importbulkemail');
	    } else {
		$this->session->set_flashdata('message', 'Something went wrong..');
		redirect('admin/uploadcsv/index');
	    }
	}
    }

}
