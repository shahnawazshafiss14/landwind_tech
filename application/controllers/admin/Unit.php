<?php

defined('BASEPATH') OR exit('No direct access');

class Unit extends Admin_Controller {

    public function __construct() {
		parent::__construct();
    }
	public function updatestatus() {
		$pid = $this->input->post('uid');
		$datas = array(
			'status' => $this->input->post('val_id')
		);
		$this->unit_model->recordUpdate($pid, $datas);
		echo '1';
    }

    public function add_unit() {
	$u_name = $this->input->post('u_name');
	$u_value = $this->input->post('u_value');
	$menu_id = $this->input->post('edit');
	if (!empty($menu_id)) {
	    $adss = array(
		'u_name' => $u_name,
		'u_value' => $u_value
	    );
	    $this->unit_model->recordUpdate($menu_id, $adss);
	    echo "1";
	} else {
	    $adss = array(
		'u_name' => $u_name,
		'u_value' => $u_value
	    );
	    $this->unit_model->recordInsert($adss);
	    echo "1";
	}
    }

    public function addunit() {
	$menu_id = $this->uri->segment(4);
	$data['contentView'] = 'admin/add_unit';
	if (isset($menu_id)) {
	    $data['head_title'] = 'Edit Unit';

	    $data['unitObject'] = $this->unit_model->viewRecordId($menu_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Unit';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function listunit() {
		$brandarr = array(
		'view_status' => '1' 
	);	
	$data['topmenus'] = $this->unit_model->viewRecordAnyR($brandarr);
	$data['head_title'] = 'View Unit';
	$data['contentView'] = 'admin/veiw_unit';
	$this->load->view('admin/_template_model2', $data);
    }

    public function deleteUnit() {
	$del_id = $_POST['del_id'];
	$id = array(
	'u_id' => $del_id
	);	
	$data = array(
	    'view_status' => '0'
	);
	
	$this->unit_model->recordUpdateAny($id, $data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>