<?php

defined('BASEPATH') OR exit('Direct no access');

class Coupon extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function index() {
	$data['topcoupons'] = $this->copan_model->viewRecordAll();
	$data['head_title'] = 'View Coupon List';
	$data['contentView'] = 'admin/veiw_couponlist';
	$this->load->view('admin/_template_model2', $data);
    }

    public function add_coupon() {
	$data = $this->input->post('couponname');
	$price = $this->input->post('couponprice');
	$ads = array(
	    'co_name' => $data,
	    'co_price' => $price,
	    'created_date' => date("Y-m-d H:i:s"),
	    'status' => '0'
	);
	$this->copan_model->recordInsert($ads);
	$this->session->set_flashdata("message", "Coupon added Successfully");
	redirect('admin/coupon/index');
    }

    public function delete_coupon() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'co_id' => $del_id
	);
	$this->copan_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record deleted!");
    }

}

?>