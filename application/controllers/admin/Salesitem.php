<?php

defined('BASEPATH') OR exit('No direct access');

class Salesitem extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function updatePrice() {
        $pid = $this->input->post('venueid');
        $datas = array(
            'venue_price' => $this->input->post('val_id')
        );
        $this->venue_model->recordUpdate($pid, $datas);
        echo "Price has been updated";
    }

    public function updateSlug() {
        $pid = $this->input->post('venueid');
        $venue_sulg = $this->input->post('val_id');
        $datas = array(
            'venue_slug' => str_replace(" ", "-", strtolower($venue_sulg)),
        );
        $this->venue_model->recordUpdate($pid, $datas);
        echo "slug has been updated";
    }

    public function totalqty() {
        $item_id = $this->input->post('particuler_idup');
        $itemss = $this->pitems_model->taotalparti($item_id);
        $invoices = $this->invoice_model->taotalparti($item_id);
        $items = $itemss - $invoices;
        echo $items;
    }

    public function updatestatus() {
        $pid = $this->input->post('pid');
        $datas = array(
            'venue_status' => $this->input->post('val_id')
        );
        $this->venue_model->recordUpdate($pid, $datas);
    }

    public function updateCommision() {
        $pid = $this->input->post('venueid');
        $venue_sulg = $this->input->post('val_id');
        $datas = array(
            'venue_commision' => $venue_sulg,
        );
        $this->venue_model->recordUpdate($pid, $datas);
        echo "slug has been updated";
    }

    public function add() {
        $items_id = $this->uri->segment(4);
        $data['objItem'] = $this->itemtype_model->viewRecordAll();
        $data['contentView'] = 'admin/add_items_sales';
        if (isset($items_id)) {
            $data['head_title'] = 'Edit Sales Items';
            $slugfie = array(
                'consig_slug' => $items_id
            );
            $data['itemsObject'] = $this->itemssales_model->viewRecordAny($slugfie);
            $this->load->view('admin/_template_model3', $data);
        } else {
            $data['head_title'] = 'Add Sales Items';
            $this->load->view('admin/_template_model3', $data);
        }
    }

    public function addItems() {
        if (isset($_GET['items_dos']) && $_GET['items_vehicle'] && isset($_GET['shop'])) {
            $itemsdos = $_GET['items_dos'];
            $items_vehicle = $_GET['items_vehicle'];
            $shopids = $_GET['shop'];
            $slugfie = array(
                'items_shop_id' => $shopids,
                'items_date' => $itemsdos,
                'items_vehicle_id' => $items_vehicle
            );
            $fetchdata = $this->itemssales_model->viewRecordAny($slugfie);
            if (count($fetchdata) > 0) {
                $data['objItem'] = $this->itemtype_model->viewRecordAll();
                $data['contentView'] = 'admin/add_item_sales';
                $data['head_title'] = 'Add Sales Items';
                $data['fetchdataf'] = $this->itemssales_model->viewRecordAny($slugfie);
                $data['shopen'] = $_GET['shop'];
                $this->load->view('admin/_template_model3', $data);
            } else {
                $arrainsert = array(
                    'items_shop_id' => $shopids,
                    'items_date' => $itemsdos,
                    'items_vehicle_id' => $items_vehicle
                );
                $this->itemssales_model->recordInsert($arrainsert);
                $data['objItem'] = $this->itemtype_model->viewRecordAll();
                $data['contentView'] = 'admin/add_item_sales';
                $data['head_title'] = 'Add Sales Item ';
                $data['shopen'] = $_GET['shop'];
                $data['fetchdataf'] = $this->itemssales_model->viewRecordAny($slugfie);
                $this->load->view('admin/_template_model3', $data);
            }
        } else {
            redirect('admin/salesitem/add');
        }
    }

    public function addItemsss() {
       $itemsdos = $_GET['items_dos'];
        $shop = $_GET['shop'];
        $vehicle = $_GET['vehicle'];
        $resutl = array(
            'dos' => $itemsdos,
            'shop' => $shop,
            'vehicle' => $vehicle
        );
        echo json_encode($resutl);
    }

    public function itemAdd() {
        $shopids = $this->encrypt->decode($_GET['shop']);
        $parst_id = $this->input->post('particuler_id');
        $items_rate = $this->input->post('items_rate');
        $items_unit = $this->input->post('items_unit');
        $sid = $this->input->post('sid');
        $slugfie = array(
            'item_id' => $parst_id
        );
        $itemsty = $this->itemtype_model->viewRecordAny($slugfie);

        $pdata = array(
            'pitem_particular' => $parst_id,
            'pitem_parent_particular' => $itemsty->parent_particular,
            'pitem_shop' => $shopids,
            'pitem_rate' => $items_rate,
            'pitem_unit' => $items_unit,
            'purchedItem' => $sid,
            'created_date' => date("Y-m-d H:i:s")
        );
        $this->pitemssales_model->recordInsert($pdata);
        echo 'Data Added Successfully';
    }

    public function itemTrans() {
        $tractitemupadd = $this->input->post('tractitemupadd');
        $t_amount = $this->input->post('t_amount');
        $t_desc = $this->input->post('t_desc');
        $sid = $this->input->post('sid');
        $pdata = array(
            'strans_type' => $tractitemupadd,
            'strans_amount' => $t_amount,
            'strans_desc' => $t_desc,
            'purchedItem' => $sid,
            'created_date' => date("Y-m-d H:i:s")
        );
        $this->consigtrans_model->recordInsert($pdata);
        echo 'Data Added Successfully';
    }

    public function updateremarks() {
        $category_id = $this->input->post('tid');
        $items_remartks = $this->input->post('items_remartks');
        $items_billno = $this->input->post('items_billno');
        $ads = array(
            'items_remarks' => $items_remartks,
            'items_bill_no' => $items_billno
        );
        $this->itemssales_model->recordUpdate($category_id, $ads);
        echo "1";
    }

    public function sendItems() {
        $fetch = $this->input->post('slug_id');
        $sendemail = $this->input->post('sendemail');
        $data['subjectto'] = 'Items ';
        $data['maito'] = $sendemail;
        $data['email_messagem'] = '
			 <tr>
				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="http://crm.sonuenterprisessiwan.com/itemss/' . $fetch . '" target="_blank">Print Items</a></h2></td>
			</tr>';
        $this->load->view('mail/mail_template', $data);
        echo "Items has been sent";
    }

    public function items_show_list() {
        $items_slug = $this->uri->segment(4);
        $fetchitems = array(
            'items_slug' => $items_slug
        );
        $data['itemsObject'] = $this->items_model->viewRecordAny($fetchitems);
        $data['head_title'] = 'Items Details';
        $data['contentView'] = 'admin/items_show_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function itemsList() {
        $fetch = array(
            'view_status' => '1'
        );
        $data["itemsObjects"] = $this->items_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Items ';
        $data['contentView'] = 'admin/view_itemss_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function reports() {
        $fetch = array(
            'view_status' => '1'
        );
        $data["itemsObjects"] = $this->items_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Particulars Wise Total Count';
        $data['contentView'] = 'admin/view_items_reports_purch';
        $this->load->view('admin/_template_model2', $data);
    }

    public function addItemss() {
        $datasession = $this->session->userdata('userName_sess');
        $edititems = $this->input->post('edititems');
        $items_number = $this->input->post('items_number');
        $items_vehicle_no = $this->input->post('items_vehicle_no');
        $dor_id = $this->input->post('dor_id');
        $doc_id = $this->input->post('doc_id');
        $items_cgst = $this->input->post('items_cgst');
        $items_sgst = $this->input->post('items_sgst');
        $items_igst = $this->input->post('items_igst');
        $items_dos = $this->input->post('items_dos');
        $items_pos = $this->input->post('items_pos');
        $slugedit = seturl($items_number, $edititems);
        $particuler_id = $this->input->post('particuler_id');
        $items_unit_max = $this->input->post('items_unit_max');
        $items_unit = $this->input->post('items_unit');
        $items_rate_max = $this->input->post('items_rate_max');
        $items_rate = $this->input->post('items_rate');
        $data_i = count($particuler_id);
        $orders = array();
        if (!empty($edititems)) {
            $pdata = array(
                'items_number' => $items_number,
                'items_vehicle_no' => $items_vehicle_no,
                'items_userid' => $datasession,
                'items_receiver' => $dor_id,
                'items_consig' => $doc_id,
                'items_cgst' => $items_cgst,
                'items_igst' => $items_igst,
                'items_sgst' => $items_sgst,
                'items_dos' => $items_dos,
                'items_pos' => $items_pos,
                'created_date' => date("Y-m-d H:i:s"),
                'items_status' => '1'
            );
            $edititems1 = array(
                'items_slug' => $edititems
            );
            $this->items_model->recordUpdateAny($edititems1, $pdata);
            $this->session->set_flashdata("message", "Itemss Updated Successfully!");
            redirect('admin/itemss/itemsList');
        } else {
            $pdata = array(
                'items_number' => $items_number,
                'items_vehicle_no' => $items_vehicle_no,
                //'items_slug' => seturl($items_number, $id),
                'items_userid' => $datasession,
                'items_receiver' => $dor_id,
                'items_consig' => $doc_id,
                'items_cgst' => $items_cgst,
                'items_sgst' => $items_sgst,
                'items_igst' => $items_igst,
                'items_dos' => $items_dos,
                'items_pos' => $items_pos,
                'created_date' => date("Y-m-d H:i:s"),
                'items_status' => '1'
            );
            $this->items_model->recordInsert($pdata);
            $last_id = $this->db->insert_id();
            $lassulinvoi = seturl($items_vehicle_no, $last_id);
            $aslug = array(
                'items_slug' => $lassulinvoi
            );
            $this->items_model->recordUpdate($last_id, $aslug);
            $data_i = count($particuler_id);
            $orders = array();
            for ($i = 0; $i < $data_i; $i++) {
                $orders[] = array(
                    'itemItems_id' => null,
                    'itemItems_items' => $last_id,
                    'itemItems_name' => $particuler_id[$i],
                    'itemItems_max_kg' => $items_unit_max[$i],
                    'itemItems_kg' => $items_unit[$i],
                    'itemItems_max_rate' => $items_rate_max[$i],
                    'itemItems_rate' => $items_rate[$i]
                );
            }
            if (!empty($data_i)) {
                $this->db->insert_batch('itemtypeItems', $orders);
                $this->db->delete('itemtypeItems', array('itemItems_name' => ''));
            }
            $this->load->view('mail/mail_template', $data);
            $this->session->set_flashdata("message", "Registration Successfully!");
            redirect('admin/itemss/items_show_list/' . $lassulinvoi);
        }
    }

    public function additemtyem() {
        $particuler_id = $this->input->post('particuler_idup');
        $items_unit_max = $this->input->post('items_unit_maxup');
        $items_unit = $this->input->post('items_unitup');
        $items_rate_max = $this->input->post('items_rate_maxup');
        $items_rate = $this->input->post('items_rateup');
        $cateEdit = $this->input->post('cateEdit');
        if (!empty($cateEdit)) {
            $pdata = array(
                'itemItems_items' => $cateEdit,
                'itemItems_name' => $particuler_id,
                'itemItems_max_kg' => $items_unit_max,
                'itemItems_kg' => $items_unit,
                'itemItems_max_rate' => $items_rate_max,
                'itemItems_rate' => $items_rate
            );
            $this->itemtypeItems_model->recordInsert($pdata);
            echo '1';
        }
    }

    public function resizing($path, $file) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 336;
        $config['new_image'] = './assests/event/thumb/' . $file;
        $this->load->library('image_lib', $config);
        $this->image_lib->initialize();
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    public function delete_itemsItem() {
        $del_id = $_POST['del_id'];
        $data = array(
            'itemItems_id' => $del_id
        );
        $this->itemtypeItems_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Updated!");
    }

    public function deleteVenue() {
        $del_id = $_POST['del_id'];
        $data = array(
            'view_status' => '0'
        );
        $this->event_model->recordUpdate($del_id, $data);
        $this->session->set_flashdata("message", "Record Deleted!");
    }

    public function deleteItem() {
        $del_id = $_POST['del_id'];
        $data = array(
            'pitem_id' => $del_id
        );
        $this->pitemssales_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Updated!");
    }

    public function deleteItemtran() {
        $del_id = $_POST['del_id'];
        $data = array(
            'strans_id' => $del_id
        );
        $this->consigtrans_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>