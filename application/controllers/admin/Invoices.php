<?php

defined('BASEPATH') OR exit('No direct access');

class Invoices extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function statuscheck() {
        $pid = $this->input->post('pid');
        $datas = array(
            'invoice_check' => $this->input->post('val_id')
        );
        $this->invoice_model->recordUpdate($pid, $datas);
    }

    public function invoice_fillter() {
        if (!empty($_POST)) {
            $get_start_date = $this->input->post('invoice_dos_from');
            $get_end_date = $this->input->post('invoice_dos_to');
            $invoice_no = $this->input->post('invoice_number');
            $invoice_vehicle_no = $this->input->post('invoice_vehicle_no');
            $doc_receiver_id = $this->input->post('doc_receiver_id');
            $invoice_cgst = $this->input->post('invoice_cgst');
            $invoice_sgst = $this->input->post('invoice_sgst');
            $invoice_igst = $this->input->post('invoice_igst');
            if (!empty($invoice_cgst)) {
                $invoice_cgst_sql = " AND invoice_cgst='" . $invoice_cgst . "'";
            }
            if (!empty($invoice_sgst)) {
                $invoice_sgst_sql = " AND invoice_sgst='" . $invoice_sgst . "'";
            }
            if (!empty($invoice_igst)) {
                $invoice_igst_sql = " AND invoice_igst='" . $invoice_igst . "'";
            }
            if (!empty($doc_receiver_id)) {
                $invoice_consig_sql = " AND invoice_consig='" . $doc_receiver_id . "'";
            }
            if (!empty($invoice_no)) {
                $invoice_no_sql = " AND invoice_number='" . $invoice_no . "'";
            }
            if (!empty($invoice_vehicle_no)) {
                $invoice_vehicle_no_sql = " AND invoice_vehicle_no='" . $invoice_vehicle_no . "'";
            }
            if (!empty($get_start_date) && !empty($get_end_date)) {
                $get_date_sql = " AND (date_format(invoice_dos, '%Y-%m-%d') BETWEEN '" . $get_start_date . "' AND '" . $get_end_date . "')";
            }
            if (empty($get_start_date) && empty($get_end_date) && empty($invoice_no) && empty($invoice_vehicle_no) && empty($doc_receiver_id) && empty($invoice_cgst) && empty($invoice_sgst) && empty($invoice_igst)) {
                $data['notrecordfound'] = "Please select any one field";
            } else {
                $select = "account_id from tble_invoice where view_status='1' AND invoice_status=1" . $invoice_no_sql . $get_date_sql . $invoice_vehicle_no_sql . $invoice_consig_sql . $invoice_cgst_sql . $invoice_sgst_sql . $invoice_igst_sql;
                   $data['invoiceObjects'] = $this->invoice_model->viewRecordGCINR($select);
            }
            $data['invoice_cgst'] = $invoice_cgst;
            $data['invoice_sgst'] = $invoice_sgst;
            $data['invoice_igst'] = $invoice_igst;
            $data['invoice_dos_from'] = $get_start_date;
            $data['invoice_dos_to'] = $get_end_date;
            $data['invoice_number'] = $invoice_no;
            $data['doc_receiver_id'] = $doc_receiver_id;
         
        }
        $data['head_title'] = 'Invoices fillter report';
        $data['contentView'] = 'admin/invoice_fillter_details';
        $this->load->view('admin/_template_model2', $data);
    }

    public function createinvoice() {
        $invoice_id = $this->uri->segment(4);
        $data['objItem'] = $this->itemtype_model->viewRecordAll();
        $data['contentView'] = 'admin/add_invoice';
        if (isset($invoice_id)) {
            $data['head_title'] = 'Edit Invoice';
            $slugfie = array(
                'invoice_slug' => $invoice_id
            );
            $data['invoiceObject'] = $this->invoice_model->viewRecordAny($slugfie);
            $this->load->view('admin/_template_model3', $data);
        } else {
            $data['head_title'] = 'Add Invoice';
            $this->load->view('admin/_template_model3', $data);
        }
    }

    public function sendInvoice() {
        $fetch = $this->input->post('slug_id');
        $sendemail = $this->input->post('sendemail');
        $data['subjectto'] = 'Invoice ';
        $data['maito'] = $sendemail;
        $data['email_messagem'] = '
			 <tr>
				<td style="font-family:Calibri;font-size:16px;color:#787878;text-decoration:none;line-height:25px;padding-top:25px;padding-bottom:0px;text-align:justify;border-top: 1px solid #f1f1f1;"><p style="padding: 0 20px;">
                <h2 style="padding: 0px 19px; color: rgb(138, 197, 63);"><a href="http://crm.sonuenterprisessiwan.com/invoices/' . $fetch . '" target="_blank">Print Invoice</a></h2></td>
			</tr>';
        $this->load->view('mail/mail_template', $data);
        echo "Invoice has been sent";
    }

    public function invoice_show_list() {
        $invoice_slug = $this->uri->segment(4);
        $fetchinvoice = array(
            'proj_slug' => $invoice_slug
        );
        $data['invoiceObject'] = $this->project_model->viewRecordAny($fetchinvoice);
        $data['head_title'] = 'Invoice Details';
        $data['contentView'] = 'admin/invoice_show_list';
        $this->load->view('admin/_template_model2', $data);
    }
    public function printInvoice() {
        $invoice_slug = $this->uri->segment(4);
        $fetchinvoice = array(
            'proj_slug' => $invoice_slug
        );
        $data['invoiceObject'] = $this->project_model->viewRecordAny($fetchinvoice);
        $data['head_title'] = 'Invoice Details'; 
        $this->load->view('admin/print_invoice', $data);
    }

    public function invoiceList() {

        if ($this->session->userdata('admin_type') == '2') {
            $fetch = array(
                'view_status' => '1',
                'invoice_check' => '1',
                'account_id' => $this->session->userdata('account_id')
            );
        } else {
            $fetch = array(
                'view_status' => '1',
                'account_id' => $this->session->userdata('account_id')
            );
        }
        $data["invoiceObjects"] = $this->project_model->viewRecordAnyROr($fetch, 'id', 'DESC');
        $data['head_title'] = 'Invoice ';
        $data['contentView'] = 'admin/view_invoices_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function reports() {
        $fetch = array(
            'view_status' => '1'
        );
        $data["invoiceObjects"] = $this->invoice_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Reports monthly Invoice ';
        $data['contentView'] = 'admin/view_reports_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function gstpay() {
        $fetch = array(
            'view_status' => '1'
        );
        $data["invoiceObjects"] = $this->invoice_model->viewRecordAnyR($fetch);
        $data['head_title'] = 'Reports monthly Invoice ';
        $data['contentView'] = 'admin/view_paygst_list';
        $this->load->view('admin/_template_model2', $data);
    }

    public function addInvoices() {
        $datasession = $this->session->userdata('userName_sess');
        $account_id = $this->session->userdata('account_id');
        $editinvoice = $this->input->post('editinvoice');
        $invoice_number = $this->input->post('invoice_number');
        $invoice_vehicle_no = $this->input->post('invoice_vehicle_no');
        $dor_id = $this->input->post('dor_id');
        $doc_id = $this->input->post('doc_id');
        $invoice_cgst = $this->input->post('invoice_cgst');
        $invoice_sgst = $this->input->post('invoice_sgst');
        $invoice_igst = $this->input->post('invoice_igst');
        $invoice_dos = $this->input->post('invoice_dos');
        $invoice_pos = $this->input->post('invoice_pos');
        $slugedit = seturl($invoice_number, $editinvoice);
        $particuler_id = $this->input->post('particuler_id');
        $invoice_unit_max = $this->input->post('invoice_unit_max');
        $invoice_unit = $this->input->post('invoice_unit');
        $invoice_rate_max = $this->input->post('invoice_rate_max');
        $invoice_rate = $this->input->post('invoice_rate');
        if ($this->session->userdata('admin_type') == '2') {
            $invoice_check = '1';
        } else {
            $invoice_check = '0';
        }

        $data_i = count($particuler_id);
        $orders = array();
        if (!empty($editinvoice)) {
            $pdata = array(
                'invoice_number' => $invoice_number,
                'invoice_vehicle_no' => $invoice_vehicle_no,
                'invoice_userid' => $datasession,
                'invoice_receiver' => $dor_id,
                'invoice_consig' => $doc_id,
                'account_id' => $account_id,
                'invoice_cgst' => $invoice_cgst,
                'invoice_igst' => $invoice_igst,
                'invoice_sgst' => $invoice_sgst,
                'invoice_dos' => $invoice_dos,
                'invoice_check' => $invoice_check,
                'invoice_pos' => $invoice_pos,
                'created_date' => date("Y-m-d H:i:s"),
                'invoice_status' => '1'
            );
            $editinvoice1 = array(
                'invoice_slug' => $editinvoice
            );
            $this->invoice_model->recordUpdateAny($editinvoice1, $pdata);
            $this->session->set_flashdata("message", "Invoices Updated Successfully!");
            redirect('admin/invoices/invoice_show_list/' . $editinvoice);
        } else {
            $pdata = array(
                'invoice_number' => $invoice_number,
                'invoice_vehicle_no' => $invoice_vehicle_no,
                'invoice_userid' => $datasession,
                'invoice_receiver' => $dor_id,
                'invoice_consig' => $doc_id,
                'account_id' => $account_id,
                'invoice_cgst' => $invoice_cgst,
                'invoice_sgst' => $invoice_sgst,
                'invoice_check' => $invoice_check,
                'invoice_igst' => $invoice_igst,
                'invoice_dos' => $invoice_dos,
                'invoice_pos' => $invoice_pos,
                'created_date' => date("Y-m-d H:i:s"),
                'invoice_status' => '1'
            );
            $this->invoice_model->recordInsert($pdata);
            $last_id = $this->db->insert_id();
            $lassulinvoi = seturl($invoice_vehicle_no, $last_id);
            $aslug = array(
                'invoice_slug' => $lassulinvoi
            );
            $this->invoice_model->recordUpdate($last_id, $aslug);
            $this->load->view('mail/mail_template', $data);
            $this->session->set_flashdata("message", "Invoices has created! Please add items");
            redirect('admin/invoices/createinvoice/' . $lassulinvoi);
        }
    }

    public function additemtyem() {
        $particuler_id = $this->input->post('particuler_idup');
        $cararry = array(
            'item_id' => $particuler_id
        );
        $category = $this->itemtype_model->viewRecordAny($cararry);

        $invoice_unit_max = $this->input->post('invoice_unit_maxup');
        $invoice_unit = $this->input->post('invoice_unitup');
        $invoice_rate_max = $this->input->post('invoice_rate_maxup');
        $invoice_rate = $this->input->post('invoice_rateup');
        $cateEdit = $this->input->post('cateEdit');
        if (!empty($cateEdit)) {
            $pdata = array(
                'itemInvoice_invoice' => $cateEdit,
                'itemInvoice_name' => $particuler_id,
                'itemInvoice_name_p' => $category->parent_particular,
                'itemInvoice_max_kg' => $invoice_unit_max,
                'itemInvoice_kg' => $invoice_unit,
                'itemInvoice_max_rate' => $invoice_rate_max,
                'itemInvoice_rate' => $invoice_rate
            );
            $this->itemtypeInvoice_model->recordInsert($pdata);
            echo '1';
        }
    }

    public function resizing($path, $file) {

        $config['image_library'] = 'gd2';
        $config['source_image'] = $path;
        //$config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 300;
        $config['height'] = 336;
        $config['new_image'] = './assests/event/thumb/' . $file;
        $this->load->library('image_lib', $config);
        $this->image_lib->initialize();
        $this->image_lib->resize();
        $this->image_lib->clear();
    }

    public function delete_invoiceItem() {
        $del_id = $_POST['del_id'];
        $data = array(
            'itemInvoice_id' => $del_id
        );
        $this->itemtypeInvoice_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Not Updated!");
    }

    public function deleteInvoice() {
        $del_id = $_POST['del_id'];
        $data = array(
            'invoice_id' => $del_id
        );
        $this->invoice_model->recordDelete($data);
        $this->session->set_flashdata("message", "Record Deleted!");
    }

}

?>