<?php

defined('BASEPATH') OR exit('No direct access');

class Particular extends Admin_Controller {

    public function __construct() {
	parent::__construct();
    }

    public function addParticular() {
	$item_particular_name = $this->input->post('item_particular_name');
	$item_hsn = $this->input->post('item_hsn');
        $item_parent = $this->input->post('item_parent');
	$item_unit = $this->input->post('item_unit');
	$particular_id = $this->input->post('edit');
	if (!empty($particular_id)) {
	    $adss = array(
		'item_particular_name' => $item_particular_name,
		'item_hsn' => $item_hsn,
                'parent_particular' => $item_parent,
                'account_id' => $this->session->userdata('account_id'),
		'item_unit' => $item_unit,
		'item_status' => '1'
	    );
	    $this->itemtype_model->recordUpdate($particular_id, $adss);
	    $this->session->set_flashdata('particular_uploaded', 'Your particulars has been updated Successfully');
	    redirect('admin/particular/index/'.$particular_id);
	} else {
	    $ads = array(
		'item_particular_name' => $item_particular_name,
		'item_hsn' => $item_hsn,
                'parent_particular' => $item_parent,
		'item_unit' => $item_unit,
                'account_id' => $this->session->userdata('account_id'),
		'item_status' => '1'
	    );
	    $this->itemtype_model->recordInsert($ads);
	    $this->session->set_flashdata('particular_uploaded', 'Your particulars has been Added Successfully');
	    redirect('admin/particular/index');
	}
    }

    public function index() {
	$particular_id = $this->uri->segment(4);
	$data['contentView'] = 'admin/particular';
	if (isset($particular_id)) {
	    $data['head_title'] = 'Edit Particular';
	    $data['particularObject'] = $this->itemtype_model->viewRecordId($particular_id);
	    $this->load->view('admin/_template_model1', $data);
	} else {
	    $data['head_title'] = 'Add Particular';
	    $this->load->view('admin/_template_model1', $data);
	}
    }

    public function particularLists() {
	$data['head_title'] = 'View Particular';
	$data['contentView'] = 'admin/veiw_particulars';
	$data['particularObject'] = $this->itemtype_model->viewRecordAll();
	$this->load->view('admin/_template_model2', $data);
    }

    public function deleteParticular() {
	$del_id = $_POST['del_id'];
	$data = array(
	    'item_id' => $del_id
	);
	$this->itemtype_model->recordDelete($data);
	$this->session->set_flashdata("message", "Record Not Updated!");
    }

}

?>