<?php

defined('BASEPATH') OR exit('No direct access');

class Index extends CI_Controller {

    public function __construct() {
        parent::__construct();
       // $this->load->model('backcall_model', 'backcall_model');
    }

    public function index() {
        $this->load->view('front/index');
    }

    public function callbackM() {
        $cotName = $this->input->post('contName');
        $cotEmail = $this->input->post('contEmail');
        $cotPhone = $this->input->post('contPhone');
        $contSubject = $this->input->post('contSubject');
        $c_type = $this->input->post('contType');
        $cotMessage = $this->input->post('contMessage');
        
        if (!empty($cotName) && !empty($cotEmail) && !empty($cotPhone) && !empty($cotMessage)) {
            $arrayData = array(
                'name' => $cotName,
                'email' => $cotEmail,
                'mobile' => $cotPhone,
                'message' => $cotMessage,
                'subject' => $contSubject,
                'enqueryid' => mt_rand(1000, 9999).strtoupper(uniqid()),
                'account_id' => 'web001',
                'added_by' => 'web',
                'type' => !empty($c_type) ? $c_type : '1',
                'created_at' => date('Y-m-d H:i:s')
            ); 
            $this->contact_model->recordInsert($arrayData);
            echo json_encode(
                    array(
                        'status' => '1',
                        'message' => 'our team contact you soon.'
            ));
        } else {
            echo json_encode(
                    array(
                        'status' => '2',
                        'message' => 'Going somthing worng'
            ));
        }
    }

    public function about() {
        $this->load->view('front/about');
    }
    
    public function services() {
        $this->load->view('front/services');
    }

         public function blog() {
        $this->load->view('front/blog');
    }          

     public function blog_detail() {
        $this->load->view('front/blog-detail');
    }  

    public function website() {
        $this->load->view('front/web');
    }

    public function search_engine_optimisation() {
        $this->load->view('front/seo');
    }

    public function landwind_tech_management() {
        $this->load->view('front/management');
    }

    public function social_media_marketing() {
        $this->load->view('front/smm');
    }

    public function customer_review_management() {
        $this->load->view('front/crm');
    }

    public function branding_and_identity() {
        $this->load->view('front/bi');
    }

    public function portfolio() {
        $this->load->view('front/portfolio');
    }

    public function training() {
        $this->load->view('front/training');
    }
    
    public function our_mission() {
        $this->load->view('front/ourmision');
    }
    
     public function webphilosophy() {
        $this->load->view('front/webphilosophy');
    }
    
     public function business() {
        $this->load->view('front/business');
    }
    
     public function mobileapp() {
        $this->load->view('front/mobileapp');
    }    

        public function payment() {
        $this->load->view('front/payment');
    }   


    public function contact() {
        $data['head_title'] = 'Contact us';
        $this->load->view('front/contact', $data);
    }

}

?>