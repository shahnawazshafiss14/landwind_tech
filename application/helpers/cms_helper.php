<?php

function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
    $theta = $longitude1 - $longitude2;
    $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
    $distance = acos($distance);
    $distance = rad2deg($distance);
    $distance = $distance * 60 * 1.1515;
    switch ($unit) {
        case 'Mi': break;
        case 'Km' : $distance = $distance * 1.609344;
    }
    return (round($distance, 2));
}

function dateFormateYmd($date) {
    $arr = explode('/', $date);
    $newDate = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    //$create = date_create($date);
    return $newDate;
} 
function dateFormatedmY($date) {
    $create = date_create($date);
    return date_format($create, "d-m-Y");
}
function dateTwoDiff($date_start, $date_end) {
    $date_start = date_create($date_start);
    $date_end = date_create($date_end);
    $diff=date_diff($date_start,$date_end);
    return $diff->format("%y Year %m Month %d Day"); 
}
function dateMonth($date) {
    $create = date_create($date);
    return date_format($create, "d-M-Y");
}
function dateMonthAP($date) {
    $create = date_create($date);
    return date_format($create, "d-M-Y H:i A");
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return (round($miles, 2));
    }
}

function nagitive_check($value, $reture1, $reture2) {
    if (isset($value)) {
        if (substr(strval($value), 0, 1) == "-") {
            return $reture1;
        } else {
            return $reture2;
        }
    }
}

function strreplace($string) {
    $str = preg_replace('/[^0-9a-zA-Z]/', "-", $string);
    $str = str_replace("--", "-", $str);
    $str = str_replace("---", "-", $str);
    $str = str_replace("----", "-", $str);
    return $str;
}

function seturl($string, $id) {
    $uniquid = uniqid();
    $str = preg_replace('/[^0-9a-zA-Z]/', "-", $string);
    $str = str_replace("--", "-", $str);
    $str = str_replace("---", "-", $str);
    $str = str_replace("----", "-", $str);
    return strtolower($str . '-' . base64_encode($id) . $uniquid);
}

function priceqty($price, $qty) {
    $output = $price * $qty;
    return $output;
}

function inrcurr($number) {
    return number_format($number, 2);
}

function checkpermission($menu_id) {
    $CI = & get_instance();
    $emailsession = $CI->session->userdata('userName_sess');
    $user_idfind = array(
        'userEmail' => $emailsession
    );
    $useidfid = $CI->user_m_f->viewRecordAny($user_idfind);
    $arrayfetch = array(
        'user_id' => $useidfid->u_id,
        'menu_id' => $menu_id,
    );
    $datauser = $CI->permissionmenu_model->viewRecordAny($arrayfetch);

    return $datauser;
}

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
}

function moneyFormatIndiaPHP($num) {
    //converting it to string 
    $numToString = (string) $num;

    //take care of decimal values
    $change = explode('.', $numToString);

    //taking care of minus sign
    $checkifminus = explode('-', $change[0]);


    //if minus then change the value as per
    $change[0] = (count($checkifminus) > 1) ? $checkifminus[1] : $checkifminus[0];

    //store the minus sign for further
    $min_sgn = '';
    $min_sgn = (count($checkifminus) > 1) ? '-' : '';



    //catch the last three
    $lastThree = substr($change[0], strlen($change[0]) - 3);



    //catch the other three
    $ExlastThree = substr($change[0], 0, strlen($change[0]) - 3);


    //check whethr empty 
    if ($ExlastThree != '')
        $lastThree = ',' . $lastThree;


    //replace through regex
    $res = preg_replace("/\B(?=(\d{2})+(?!\d))/", ",", $ExlastThree);

    //main container num
    $lst = '';

    if (isset($change[1]) == '') {
        $lst = $min_sgn . $res . $lastThree;
    } else {
        $lst = $min_sgn . $res . $lastThree . "." . $change[1];
    }

    //special case if equals to 2 then 
    if (strlen($change[0]) === 2) {
        $lst = str_replace(",", "", $lst);
    }

    return $lst;
}

function priceship($shipprice, $qty) {
    $output = $shipprice * $qty;
    return $output;
}

function totaltax($price, $qty, $tax) {
    $output = (($price * $qty)) * $tax / 100;
    return $output;
}

function percetage($price, $discount, $qty = null) {
    $output = ($price - ($price * $discount / 100)) * $qty;
    return $output;
}
function payment_mode($id){
	$array = array(
	'1' => 'Cash',
	'2' => 'Online'
	);
	return $array[$id];
	
}
function payment_mode_type($id){
	$array = array(
	'1' => 'Paid',
	'2' => 'Topay'
	);
	return $array[$id]; 
}
function itemtype() {
    $CI = & get_instance();
    $dataout = '';
    $fetarc = array(
        'parent_particular' => '0',
        'view_status' => '1'
    );
    $fetda = $CI->itemtype_model->viewRecordAnyR($fetarc);
    foreach ($fetda as $fevenue) {
        $dataout .= '<option value="' . $fevenue->item_id . '">' . $fevenue->item_particular_name . ', ' . $fevenue->item_hsn . '</option>';
    }
    return $dataout;
}
function getItemtype($id) {
    $CI = & get_instance(); 
    $fetarc = array(
        'parent_particular' => '0',
        'view_status' => '1',
        'item_id' => $id
    );
    $fetda = $CI->itemtype_model->viewRecordAny($fetarc); 
    return $fetda->item_particular_name;
}
function selectTerm() {
    $CI = & get_instance();
    $dataout = '';
    $fetarc = array( 
        'c_type' => '1',
        'view_status' => '1'
    );
    $fetda = $CI->common_model->viewRecordAnyR($fetarc);
    foreach ($fetda as $fevenue) {
        $dataout .= '<option value="' . $fevenue->id . '">' . $fevenue->name . '</option>';
    }
    return $dataout;
}
function getSelectTerm($id) {
    $CI = & get_instance();
    $dataout = '';
    $fetarc = array( 
        'c_type' => '1',
        'view_status' => '1',
        'id' => $id
    );
    $fevenue = $CI->common_model->viewRecordAny($fetarc); 
    return $fevenue->name;
}
function getCommon($id,$type) {
    $CI = & get_instance();
    $dataout = '';
    $fetarc = array( 
        'c_type' => $type,
        'view_status' => '1',
        'id' => $id
    );
    $fevenue = $CI->common_model->viewRecordAny($fetarc); 
    return $fevenue->name;
}

function dateymd($month_from) {
    return date("Y-m-d", strtotime($month_from));
}

function datedmy($month_from) {
    return date("d-m-Y", strtotime($month_from));
}

function month($month) {
    switch ($month) {
        case "01":
            return 'Jan';
            break;
        case "02":
            return 'Feb';
            break;
        case "03":
            return 'Mar';
            break;
        case "04":
            return 'Apr';
            break;
        case "05":
            return 'May';
            break;
        case "06":
            return 'June';
            break;
        case "07":
            return 'July';
            break;
        case "08":
            return 'Aug';
            break;
        case "09":
            return 'Sept';
            break;
        case "10":
            return 'Oct';
            break;
        case "11":
            return 'Nov';
            break;
        case "12":
            return 'Dec';
            break;
        default:
            echo "There is no record!";
    }
}

function datemonthname($datefull) {
    $day = date("d", strtotime($datefull));
    $mon = date("m", strtotime($datefull));
    $year = date("Y", strtotime($datefull));

    return $day . ' ' . month($mon) . ' ' . $year;
}

function monthwises() {
    $CI = & get_instance();
    $datemonth = $CI->uri->segment(5);
    switch ($datemonth) {
        case "Jan":
            $start_date = date('Y') . '-01-01';
            $end_date = date('Y') . '-01-31';
            break;
        case "Feb":
            $start_date = date('Y') . '-02-01';
            $end_date = date('Y') . '-02-29';
            break;
        case "Mar":
            $start_date = date('Y') . '-03-01';
            $end_date = date('Y') . '-03-31';
            break;
        case "Apr":
            $start_date = date('Y') . '-04-01';
            $end_date = date('Y') . '-04-30';
            break;
        case "May":
            $start_date = date('Y') . '-05-01';
            $end_date = date('Y') . '-05-31';
            break;
        case "June":
            $start_date = date('Y') . '-06-01';
            $end_date = date('Y') . '-06-30';
            break;
        case "July":
            $start_date = date('Y') . '-07-01';
            $end_date = date('Y') . '-07-31';
            break;
        case "Aug":
            $start_date = date('Y') . '-08-01';
            $end_date = date('Y') . '-08-31';
            break;
        case "Sept":
            $start_date = date('Y') . '-09-01';
            $end_date = date('Y') . '-09-30';
            break;
        case "Oct":
            $start_date = date('Y') . '-10-01';
            $end_date = date('Y') . '-10-31';
            break;
        case "Nov":
            $start_date = date('Y') . '-11-01';
            $end_date = date('Y') . '-11-30';
            break;
        case "Dec":
            $start_date = date('Y') . '-12-01';
            $end_date = date('Y') . '-12-31';
            break;
        default:
            echo "There is no record!";
    }
    return array($start_date, $end_date);
}

function monthwisesa($abc) {
    $CI = & get_instance();
    $datemonth = $CI->uri->segment($abc);
    switch ($datemonth) {
        case "Jan":
            $start_date = date('Y') . '-01-01';
            $end_date = date('Y') . '-01-31';
            break;
        case "Feb":
            $start_date = date('Y') . '-02-01';
            $end_date = date('Y') . '-02-29';
            break;
        case "Mar":
            $start_date = date('Y') . '-03-01';
            $end_date = date('Y') . '-03-31';
            break;
        case "Apr":
            $start_date = date('Y') . '-04-01';
            $end_date = date('Y') . '-04-30';
            break;
        case "May":
            $start_date = date('Y') . '-05-01';
            $end_date = date('Y') . '-05-31';
            break;
        case "June":
            $start_date = date('Y') . '-06-01';
            $end_date = date('Y') . '-06-30';
            break;
        case "July":
            $start_date = date('Y') . '-07-01';
            $end_date = date('Y') . '-07-31';
            break;
        case "Aug":
            $start_date = date('Y') . '-08-01';
            $end_date = date('Y') . '-08-31';
            break;
        case "Sept":
            $start_date = date('Y') . '-09-01';
            $end_date = date('Y') . '-09-30';
            break;
        case "Oct":
            $start_date = date('Y') . '-10-01';
            $end_date = date('Y') . '-10-31';
            break;
        case "Nov":
            $start_date = date('Y') . '-11-01';
            $end_date = date('Y') . '-11-30';
            break;
        case "Dec":
            $start_date = date('Y') . '-12-01';
            $end_date = date('Y') . '-12-31';
            break;
        default:
            echo "There is no record!";
    }
    return array($start_date, $end_date);
}

?>
