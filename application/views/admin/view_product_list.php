<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Products List<small>Manage View Product</small></h1>
        <form role="search" class="navbar-form" style="float: left; margin-left: 20px; margin-top: 0px;">
	    <div class="input-group add-on" style="width: 380px;">
		<input type="text" id="srch-term" name="srch-term" placeholder="Search" class="form-control">
		<div class="input-group-btn">
		    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
		</div>
	    </div>
	</form>
    </section>
    <section class="content">
	<div class="row">

            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">

			    <?php
			    foreach ($productObjects as $pO) :
				?>
				<?php
				if ($pO->outofstock == '2' || $pO->p_qty == '0'):
				    ?>

				    <div class="col-sm-3">
					<div class="list-block out-stock-main">
					    <div class="pro-image">
						<a href="<?= ADMINC . 'product/product_show_list/' . $pO->p_product_id; ?>">
						    <div class="no-stock"><div class="stock-no">out of stock</div></div>
						    <a href="<?= ADMINC . 'product/product_show_list/' . $pO->p_product_id; ?>"><img src="<?= ADMINPRODUCTIMG . '/thumb/' . $pO->p_image; ?>" alt=""></a>

						</a>
					    </div>
					    <div class="pro-title"><a href="<?= ADMINC . 'product/product_show_list/' . $pO->p_product_id; ?>"><?= $pO->p_title; ?></a></div>
					    <div class="pro-description"><p><?= $pO->p_discription; ?></p></div>
					    <div class="price" style="float:none"><strong><i class="fa fa-rupee" aria-hidden="true"></i><?= $pO->p_price; ?></strong></div>
					    <div class="list-details">
						<ul>
						    <li><span>Model</span><span><?= $this->models_model->modelId($pO->p_model); ?></span></li>
						    <li><span>Color</span><span><?= $this->colors_model->colorId($pO->p_color); ?></span></li>
						</ul>
					    </div>
					    <div class="stock out-stock"><span><?php
						    $aee = array(
							'1' => 'In Stock',
							'2' => 'Out Of Stock'
						    );
						    if ($pO->p_qty === '0') {
							echo "Out Of Stock";
						    } else {
							echo "Out Of Stock";
						    }
						    ?></span></div>

					    <?php
					    if ($users1->role_id !== '0'):
						?>

	    				    <div class="action">
	    					<div class="notify-me">
							<?php
							$datar = array(
							    'notify_uid' => $this->session->userdata('userName_sess'),
							    'notify_pid' => $pO->p_product_id
							);
							$checkExits = $this->notify_model->recordCheckAvaibility($datar);
							if ($checkExits > 0):
							    echo "You will be notify";
							else :
							    ?>

							    <button name="add_notifys" class="btn btn-default btn-iconed add_notifys" data-productid="<?= $pO->p_product_id; ?>" data-sessionid_email="<?= $this->session->userdata('userName_sess'); ?>" id="add_notifys"><i class="fa fa-shopping-cart"></i> Notify me </button>
							<?php
							endif;
							?>


	    					</div>
	    				    </div>
					    <?php endif; ?>
					</div>

				    </div>
				<?php else: ?>
				    <div class="col-sm-3">
					<div class="list-block">
					    <div class="pro-image">
						<a href="<?= ADMINC . 'product/product_show_list/' . $pO->p_product_id; ?>"><img src="<?= ADMINPRODUCTIMG . '/thumb/' . $pO->p_image; ?>" alt=""></a>
					    </div>
					    <div class="pro-title">
						<a href="<?= ADMINC . 'product/product_show_list/' . $pO->p_product_id; ?>"><?= $pO->p_title; ?></a>
					    </div>
					    <div class="pro-description"><p><?= $pO->p_discription; ?></p></div>
					    <div class="price" style="float:none">
						<strong><i class="fa fa-rupee" aria-hidden="true"></i><?= $pO->p_price; ?></strong>
					    </div>
					    <div class="list-details">
						<ul>
						    <li><span>Model</span><span><?= $this->models_model->modelId($pO->p_model); ?></span></li>
						    <li><span>Color</span><span><?= $this->colors_model->colorId($pO->p_color); ?></span></li>
						</ul>
					    </div>
					    <div class="stock in-stock"><span><?php
						    $aee = array(
							'1' => 'In Stock',
							'2' => 'Out Of Stock'
						    );
						    echo $aee[$pO->outofstock];
						    ?></span> / <?= $pO->p_qty; ?>
					    </div>
					    <?php
					    if ($users1->role_id !== '0'):
						?>
	    				    <div class="action">
	    					<div class="form-group ">
	    					    <div class="input-group">
	    						<div class="input-group-btn">
	    						    <button type="button" class="btn minus"><i class="fa fa-minus"></i></button>
	    						</div>
	    						<input type="text" class="form-control qty quantity" value="50" name="quantity" id="<?= $pO->p_product_id; ?>">
	    						<div class="input-group-btn">
	    						    <button type="button" class="btn add"><i class="fa fa-plus"></i></button>
	    						</div>
	    					    </div>
	    					</div>
	    					<div class="cat-btn">
	    					    <button name="add_to_cart" class="btn btn-success btn-iconed add_carts" <?php if ($pO->p_image != '') : ?> data-main_image="<?= $pO->p_image; ?>" data-productname="<?= $pO->p_title; ?>" data-price="<?= $pO->p_price; ?>" data-productid="<?= $pO->p_product_id; ?>" data-sessionid_email="<?= $this->session->userdata('userName_sess'); ?>" <?php endif; ?> id="add_to_cart"><i class="fa fa-shopping-cart"></i> Add to Cart </button>
	    					</div>
	    				    </div>
					    <?php endif; ?>

					</div>

				    </div>
				<?php endif; ?>
			    <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p><?php echo $links; ?></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
