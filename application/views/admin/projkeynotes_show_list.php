<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
 <?php 
        $checkm = checkpermission('169');
        ?>
<style>
    .input-group {
        margin-bottom: 12px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('consig_uploaded')) : ?>
            <?php echo $this->session->flashdata('consig_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="consigneeid" id="consigneeid" value="<?= !empty($consigneeObject->consig_id) ? $consigneeObject->consig_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <a href="<?= ADMINC; ?>consignee/edit/<?= !empty($consigneeObject->consig_slug) ? $consigneeObject->consig_slug : ''; ?>" class="eaddmorecontent btn btn-info btn-lg"><i class="fa fa-pencil"></i> Edit</a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div id="response">
                        <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">
                            <div class="table-responsive">          
                                <table class="table"> 
                                    <tbody>
                                        <tr style="background-color: #b3b2ff;"> 
                                            <th><b>Consignee Details</b></th> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee Name</b>: <?= $consigneeObject->consig_name; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee Address</b>: <?= $consigneeObject->consig_address; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee Type</b>: <?php
                                                $array_consig_type = array(
                                                    '0' => 'Consignee(Shipped to)',
                                                    '1' => 'Receiver(Billed to)'
                                                );
                                                echo $array_consig_type[$consigneeObject->consig_type];
                                                ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee Email</b>: <?= $consigneeObject->consig_email; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee Phone</b>: <?= $consigneeObject->consig_phone; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee GSTIN</b>: <?= $consigneeObject->consig_gstin; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee State</b>: <?= $consigneeObject->consig_state; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee City</b>: <?= $consigneeObject->consig_city; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Consignee Zipcode</b>: <?= $consigneeObject->consig_zipcode; ?></td> 
                                        </tr>

                                        <tr>
                                            <td><b>Consignee Contact Person</b>: <?= $consigneeObject->consig_contact_person; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Description</b>: <?= $consigneeObject->consig_description; ?></td> 
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                            <!--div class="col-sm-9">
                                <b>Consignee Image</b>
                                <input type="file" id="update_consig_img" name="update_consig_img" />
                            </div>
                            <div class="col-sm-3">
                                <input type="button" name="updatebtn_consig_img" id="updatebtn_consig_img" value="Upload" class="btn btn-primary" />
                            </div>
                            <img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
                                 src="<?= !empty($consigneeObject->consig_image) ? BASEURLFRONTASSET . 'consignee/' . $consigneeObject->consig_image : BASEURLFRONTASSET . 'demo.jpg'; ?>" class="img" /-->
                        </div>
                    </div>
                    <?php if(($checkm->is_viewed == '1') && ($this->session->userdata('admin_type') == '1')): ?>
                    <div class="col-sm-3" style="border-right: 2px dotted #ded2d2;">
                        <div class="table-responsive">          
                            <table class="table"> 
                                <tbody>
                                    <tr style="background-color: #ffed6d;">
                                        <th><b>Last Five Invoice Details</b></th> 
                                    </tr>
                                    <?php
                                    $array_last = array(
                                        'invoice_consig' => $consigneeObject->consig_id
                                    );
                                    $fetch_objec = $this->invoice_model->viewRecordAnyRLf($array_last);
                                    foreach ($fetch_objec as $invoic):
                                        $array_pitem_in = array(
                                            'itemInvoice_invoice' => $invoic->invoice_id
                                        );
                                        $taotal = 0;
                                        $total_unit = 0;
                                        $itemunit = '';
                                        $fetch_objec_item = $this->itemtypeInvoice_model->viewRecordAnyR($array_pitem_in);
                                        foreach ($fetch_objec_item as $invoic_item) {
                                            $total_unit += $invoic_item->itemInvoice_max_kg;
                                            $taotal += $invoic_item->itemInvoice_max_kg * $invoic_item->itemInvoice_rate;
                                            $fetchParticlearray = array(
                                                'item_id' => $invoic_item->itemInvoice_name
                                            );
                                            $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                            $itemunit = $fetchParticle->item_unit;
                                        }
                                        ?>
                                        <?php
                                        $cgest = $this->gst_model->commongstId($invoic->invoice_cgst);
                                        $sgest = $this->gst_model->commongstId($invoic->invoice_sgst);
                                        $igest = $this->gst_model->commongstId($invoic->invoice_igst);

                                        $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
                                        $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
                                        $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';




                                        $grandtoatal2 = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);


                                        $grandtoatal1 = $grandtoatal2;
                                        ?>
                                        <tr>
                                            <td><b>Entry Date</b>: <?= dateMonthAP($invoic->created_date); ?><br/><b>Inovice Date</b>: <?= dateMonth($invoic->invoice_dos); ?><br/><b>Unit</b>: <?= $total_unit . '/' . $itemunit; ?><br/><b>Amount</b>: <?= $grandtoatal1; ?></td> 
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>

                                </tbody>
                            </table>
                        </div> 

                    </div>
                    <div class="col-sm-3">
                        <div class="table-responsive">          
                            <table class="table"> 
                                <tbody>
                                    <tr style="background-color: aquamarine;"> 
                                        <th><b>Last Five Payment Details</b></th> 
                                    </tr>
                                    <?php
                                    $array_last_paymet = array(
                                        'items_shop_id' => $consigneeObject->consig_id
                                    );
                                    $fetch_objec_payment =  $this->itemssales_model->viewRecordAnyRLf($array_last_paymet);
                                    foreach ($fetch_objec_payment as $payment):
                                        $array_pitem_in = array(
                                            'purchedItem' => $payment->items_id,
                                            'strans_type' => '2'
                                        );
                                        $taotal_payment = 0; 
                                        $fetch_objec_item = $this->consigtrans_model->viewRecordAnyR($array_pitem_in);
                                        foreach ($fetch_objec_item as $invoic_item) { 
                                            $taotal_payment += $invoic_item->strans_amount;
                                          
                                        }
                                        ?>
                                       
                                        <tr>
                                            <td><b>Entry Date</b>: <?= dateMonthAP($payment->created_on); ?><br/><b>Item Date</b>: <?= dateMonth($payment->items_date); ?><br/><b>Amount</b>: <?= $taotal_payment; ?></td> 
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>

                                </tbody>
                            </table>
                        </div> 

                    </div>
                    <?php endif;?>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row" style="margin-top: 50px;margin-bottom: 50px;">
                                    <form name="frmgenratebill" id="frmgenratebill" action="<?= ADMINC; ?>consignee/consignee_show_list_invoice/<?= $consigneeObject->consig_slug; ?>" method="get">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                            <th>From</th>
                                            <th>To</th>	
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                                            <td><input type="text" id="from" name="from" class="form-control" value="<?= !empty($month_to) ? $month_to : ''; ?>"></td>
                                            <td><input type="text" id="to" name="to" class="form-control" value="<?= !empty($month_to) ? $month_from : ''; ?>"></td>
                                            <td><input type="submit" class="btn btn-success"></td>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="db-box-outer">
                            <div class="row">
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Jan">Jan</a>
                                        </div>
                                        <?php
                                        $totalqty1 = $this->consignee_model->reportvenueadmin(date('Y') . '-01-01', date('Y') . '-01-31', $consigneeObject->consig_id);
                                        $totalprice1 = $this->consignee_model->reportvenueadminprice(date('Y') . '-01-01', date('Y') . '-01-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty1) ? $totalqty1 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice1) ? $totalprice1 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Feb">Feb</a>
                                        </div>
                                        <?php
                                        $totalqty2 = $this->consignee_model->reportvenueadmin(date('Y') . '-02-01', date('Y') . '-02-29', $consigneeObject->consig_id);
                                        $totalprice2 = $this->consignee_model->reportvenueadminprice(date('Y') . '-02-01', date('Y') . '-02-29', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty2) ? $totalqty2 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice2) ? $totalprice2 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Mar">Mar</a>
                                        </div>
                                        <?php
                                        $totalqty3 = $this->consignee_model->reportvenueadmin(date('Y') . '-03-01', date('Y') . '-03-31', $consigneeObject->consig_id);
                                        $totalprice3 = $this->consignee_model->reportvenueadminprice(date('Y') . '-03-01', date('Y') . '-03-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty3) ? $totalqty3 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice3) ? $totalprice3 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Apr">Apr</a>
                                        </div>
                                        <?php
                                        $totalqty4 = $this->consignee_model->reportvenueadmin(date('Y') . '-04-01', date('Y') . '-04-30', $consigneeObject->consig_id);
                                        $totalprice4 = $this->consignee_model->reportvenueadminprice(date('Y') . '-04-01', date('Y') . '-04-30', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty4) ? $totalqty4 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice4) ? $totalprice4 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/May">May</a>
                                        </div>
                                        <?php
                                        $totalqty5 = $this->consignee_model->reportvenueadmin(date('Y') . '-05-01', date('Y') . '-05-31', $consigneeObject->consig_id);
                                        $totalprice5 = $this->consignee_model->reportvenueadminprice(date('Y') . '-05-01', date('Y') . '-05-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty5) ? $totalqty5 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice5) ? $totalprice5 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/June">June</a>
                                        </div>
                                        <?php
                                        $totalqty6 = $this->consignee_model->reportvenueadmin(date('Y') . '-06-01', date('Y') . '-06-30', $consigneeObject->consig_id);
                                        $totalprice6 = $this->consignee_model->reportvenueadminprice(date('Y') . '-06-01', date('Y') . '-06-30', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty6) ? $totalqty6 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice6) ? $totalprice6 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/July">July</a>
                                        </div>
                                        <?php
                                        $totalqty7 = $this->consignee_model->reportvenueadmin(date('Y') . '-07-01', date('Y') . '-07-31', $consigneeObject->consig_id);
                                        $totalprice7 = $this->consignee_model->reportvenueadminprice(date('Y') . '-07-01', date('Y') . '-07-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty7) ? $totalqty7 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice7) ? $totalprice7 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Aug">Aug</a>
                                        </div>
                                        <?php
                                        $totalqty8 = $this->consignee_model->reportvenueadmin(date('Y') . '-08-01', date('Y') . '-08-31', $consigneeObject->consig_id);
                                        $totalprice8 = $this->consignee_model->reportvenueadminprice(date('Y') . '-08-01', date('Y') . '-08-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty8) ? $totalqty8 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice8) ? $totalprice8 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Sept">Sept</a>
                                        </div>
                                        <?php
                                        $totalqty9 = $this->consignee_model->reportvenueadmin(date('Y') . '-09-01', date('Y') . '-09-30', $consigneeObject->consig_id);
                                        $totalprice9 = $this->consignee_model->reportvenueadminprice(date('Y') . '-09-01', date('Y') . '-09-30', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty9) ? $totalqty9 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice9) ? $totalprice9 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Oct">Oct</a>
                                        </div>
                                        <?php
                                        $totalqty10 = $this->consignee_model->reportvenueadmin(date('Y') . '-10-01', date('Y') . '-10-31', $consigneeObject->consig_id);
                                        $totalprice10 = $this->consignee_model->reportvenueadminprice(date('Y') . '-10-01', date('Y') . '-10-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty10) ? $totalqty10 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice10) ? $totalprice10 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Nov">Nov</a>
                                        </div>
                                        <?php
                                        $totalqty11 = $this->consignee_model->reportvenueadmin(date('Y') . '-11-01', date('Y') . '-11-30', $consigneeObject->consig_id);
                                        $totalprice11 = $this->consignee_model->reportvenueadminprice(date('Y') . '-11-01', date('Y') . '-11-30', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty11) ? $totalqty11 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice11) ? $totalprice11 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class=" col-md-3">
                                    <div class="db-box">
                                        <div class="title">
                                            <a href="<?= ADMINC; ?>consignee/consignee_month/<?= $consigneeObject->consig_slug; ?>/Dec">Dec</a>
                                        </div>
                                        <?php
                                        $totalqty12 = $this->consignee_model->reportvenueadmin(date('Y') . '-12-01', date('Y') . '-12-31', $consigneeObject->consig_id);
                                        $totalprice12 = $this->consignee_model->reportvenueadminprice(date('Y') . '-12-01', date('Y') . '-12-31', $consigneeObject->consig_id);
                                        ?>
                                        <table class="table" style="margin:0">
                                            <tr>
                                                <td >Total Unit</td>
                                                <td ><?= !empty($totalqty12) ? $totalqty12 : '0'; ?></td>
                                            </tr>
                                            <tr>
                                                <td >Total Revenue</td>
                                                <td ><?= !empty($totalprice12) ? $totalprice12 : '0'; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


