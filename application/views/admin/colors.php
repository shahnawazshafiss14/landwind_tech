<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($colorObject->color_id) ? 'Edit' : 'Add' ?> Color</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>colors/index">Colors</a></li>
	    <li class="active"><?= !empty($colorObject->color_id) ? 'Edit' : 'Add' ?> Color</li>
	</ol>
    </section>
    <section class="content" id="ppg">

	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($colorObject->color_id) ? 'Edit' : 'Add' ?> Color</h3>
		<div class="box-tools pull-right">
		    <form  id="colorForm" name="colorForm" action=""  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnColor" id="btnColor" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> <?= !empty($colorObject->color_id) ? 'Update' : 'Save' ?> </button>
			<?php if (!empty($colorObject->color_id)) : ?>
    			<a href="<?= base_url(); ?>color/viewList" title="Collapse" class="btn btn-info btn-sm" title="Cancel">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>


		</div>
	    </div>
	    <div class="box-body">

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="colorName">Color Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="edit" type="hidden" value="<?= !empty($colorObject->color_id) ? $colorObject->color_id : ''; ?>">
				    <input type="text" class="form-control" id="colorName" name="colorName" placeholder="Enter Color Name" value="<?= !empty($colorObject->color_name) ? $colorObject->color_name : '' ?>">


				</div>
			    </div>
			</div>
		    </div>
		</div>
		</form>
	    </div>

	    <!-- /.box-body -->
	    <div class="box-footer">

	    </div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>


</div>
<!-- /.content-wrapper -->
