<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($locationObject->location_id) ? 'Edit' : 'Add' ?> Location</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>location/viewLocation">Location</a></li>
	    <li class="active"><?= !empty($locationObject->location_id) ? 'Edit' : 'Add' ?> Location</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($locationObject->location_id) ? 'Edit' : 'Add' ?> Location</h3>
		<div class="box-tools pull-right">
		    <form  id="locationForm" name="locationForm" action=""  method="POST" enctype="multipart/form-data">
			<button type="button" name="btnLocation" id="btnLocation" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> <?= !empty($locationObject->location_id) ? 'Update' : 'Save' ?> </button>
			<?php if (!empty($locationObject->location_id)) : ?>
    			<a href="<?= base_url(); ?>location/viewLocation" title="Collapse" class="btn btn-info btn-sm" title="Cancel">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>


		</div>
	    </div>
	    <div class="box-body">

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="locationName">Location Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="editName" id="editName" type="hidden" value="<?= !empty($locationObject->location_id) ? $locationObject->location_id : ''; ?>">
				    <input type="text" class="form-control" id="locationName" name="locationName" placeholder="Enter Location Name" value="<?= !empty($locationObject->location_name) ? $locationObject->location_name : '' ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="selectLocation">Select Parent Location</label>
				<div class="col-sm-10">
				    <select name="locationParent" id="locationParent" class="form-control">

					<?php if (!empty($locationObject->location_parent)) : ?>
    					<option value="<?= $locationObject->location_parent; ?>">

						<?php
						echo $this->location_model->locationId($locationObject->location_parent);
						?>
    					</option>
					<?php endif; ?>
					<option value="0">Select Location</option>
					<?php foreach ($parent_location as $pc) : ?>
    					<option value="<?= $pc->location_id ?>"><?= $pc->location_name ?></option>
					<?php endforeach; ?>

				    </select>
				</div>
			    </div>

			</div>
		    </div>
		</div>
		</form>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
