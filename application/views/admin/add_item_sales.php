<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:;">Sales Items</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('items_uploaded')) : ?>
            <?php echo $this->session->flashdata('items_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?><?= $items; ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-primary"><i class="fa fa-user"></i> <?= ucwords($this->consignee_model->consigId($fetchdataf->items_shop_id)); ?></button> <i class="fa fa-calendar"></i> <input id="datepicker" class="btn btn-warning datechangesales" data-vehicle="<?= !empty($fetchdataf->items_vehicle_id) ? $fetchdataf->items_vehicle_id : ''; ?>" data-shop="<?= !empty($fetchdataf->items_shop_id) ? $fetchdataf->items_shop_id : ''; ?>" data-date-format='yyyy-mm-dd' value="<?= $fetchdataf->items_date; ?>">  <button title="Refresh" class="btn btn-success" id="btnitemrefere"><i class="fa fa-refresh"></i></button>
                </div> 
            </div>
            <form  id="itemsForm" name="itemsForm" action="<?= ADMINC; ?>salesitem/addItems"  method="POST" enctype="multipart/form-data">
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#General">Add Items</a>
                        </li>
                        <li><a data-toggle="tab" href="#Image">Transaction</a></li>
                    </ul>
                    <div class="form-horizontal">
                        <div class="tab-content">
                            <div id="General" class="tab-pane fade in active">

                                <div class="col-md-8">

                                    <div id="insertcol" class="removerow">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="item_table">
                                                <tr>
                                                    <th>Select Particulars</th>

                                                    <th>Rate</th>
                                                    <th>Unit</th>
                                                    <th></th>
                                                    <th>Action
                                                        <!--button type="button" name="add" class="btn btn-success add"><i class="fa fa-plus"></i></button-->
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select class="form-control particuler_id" name="particuler_id" id="particuler_id"><option value="">Select Particulars</option><?= itemtype(); ?></select>
                                                        <input type="hidden" name="parst_id" id="parst_id" value="" />
                                                    </td>
                                                    <td><input type="text" name="items_rate" id="items_rate" class="form-control items_rate"></td>
                                                    <td><input type="text" name="items_unit" id="items_unit" class="form-control items_unit"></td>
                                                    <td> </td>

                                                    <td>
                                                        <a class="btn btn-primary" id="itemsitemupaddsales" data-id="<?= $fetchdataf->items_id; ?>" title="Add Item" href="javascript:;">Add</a>		   </td>
                                                </tr>

                                            </table>

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Action</th>
                                                        <th>Particulars</th>
                                                        <th>Rate</th>
                                                        <th>Unit</th>
                                                        <th style="text-align:right">Sub Total</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $subtotal = 0;
                                                    $acc = array(
                                                        'purchedItem' => $fetchdataf->items_id
                                                    );
                                                    $fetAddItems = $this->pitemssales_model->viewRecordAnyR($acc);
                                                    foreach ($fetAddItems as $item):
                                                        ?>
                                                        <tr class="deleteRcordaddItemP">
                                                            <td>
                                                                <a title="Delete Item" class="btn btn-danger deleteRcordaddItemsales" data-id="<?= $item->pitem_id; ?>" href="javascript:;"><i class="fa fa-trash"></i></a>		   </td>
                                                            <td>
                                                                <?= $this->itemtype_model->itemId($item->pitem_particular, 'item_particular_name'); ?>
                                                            </td>
                                                            <td><i class="fa fa-inr"></i> <?= $item->pitem_rate; ?>
                                                            </td>
                                                            <td>
                                                                <?= $item->pitem_unit; ?> - <?= $this->itemtype_model->itemId($item->pitem_particular, 'item_unit'); ?>
                                                            </td>
                                                            <td align="right">
                                                                <?php $subtotal += $item->pitem_rate * $item->pitem_unit; ?>
                                                                <i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($item->pitem_rate * $item->pitem_unit); ?>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    endforeach;
                                                    ?>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right"><b>Total</b></td>
                                                        <td align="right"><b><i class="fa fa-inr"></i>  <?= moneyFormatIndiaPHP($subtotal); ?></b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="col-md-12">
                                        <div class="row">
                                            <b>Remarks</b><br/>
                                            <textarea name="items_remartks" id="items_remartks" style="margin: 0px; width: 381px; height: 94px;"><?= $fetchdataf->items_remarks; ?></textarea>
                                        </div>
                                        <div class="row">Bill No:<br/>
                                            <input type="text" class="form-control" name="items_billno" id="items_billno" value="<?= !empty($fetchdataf->items_bill_no) ? $fetchdataf->items_bill_no: ''; ?>" />
                                        </div>
                                        <div class="row">
                                            <input type="button" data-tid="<?= $fetchdataf->items_id; ?>" class="btn btn-warning" style="float: right" name="btn_items_remartks" id="btn_items_remartks" value="Submit" />
                                        </div>
                                        
                                    </div>
                                </div>

                            </div>
                            <div id="Image" class="tab-pane fade">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="item_table">
                                        <tr>
                                            <th>Select Type</th>
                                            <th>Amount</th>
                                            <th>Description</th>
                                            <th style="text-align:right">Action</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control transactiontype" name="transaction_type" id="transaction_type">
                                                    <!--option value="">Select Type</option-->
                                                    <option value="1">Dr</option>
                                                    <option value="2">Cr</option>
                                                </select>
                                                <input type="hidden" name="t_type" id="t_type" value="" />
                                            </td>
                                            <td><input type="text" name="t_amount" id="t_amount" class="form-control"></td>
                                            <td><input type="text" name="t_desc" id="t_desc" class="form-control"></td>
                                            <td>
                                                <a class="btn btn-primary" id="tractitemupaddsales" data-id="<?= $fetchdataf->items_id; ?>" title="Add Tractions" href="javascript:;">Add</a>		   </td>
                                        </tr>

                                    </table>

                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Type</th>
                                                <th>Description</th>
                                                <th style="text-align:right">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $subtotalt = 0;
                                            $acct = array(
                                                'purchedItem' => $fetchdataf->items_id
                                            );
                                            $fetAddItemst = $this->consigtrans_model->viewRecordAnyR($acct);
                                            foreach ($fetAddItemst as $itemt):
                                                ?>
                                                <tr class="deleteRcordaddItemtP">
                                                    <td>
                                                        <a title="Delete Item Transation" class="btn btn-danger deleteRcordaddItemtsales" data-id="<?= $itemt->strans_id; ?>" href="javascript:;"><i class="fa fa-trash"></i></a>		   </td>
                                                    <td>
                                                        <?php
                                                        $arrtp = array(
                                                            '1' => 'Dr',
                                                            '2' => 'Cr'
                                                        );
                                                        echo $arrtp[$itemt->strans_type];
                                                        ?>
                                                    </td>
                                                    <td align="left">
                                                        <?= $itemt->strans_desc; ?>

                                                    </td>
                                                    <td align="right">
                                                        <i class="fa fa-inr"></i> <?= $itemt->strans_amount; ?>
                                                        <?php $subtotalt += $itemt->strans_amount; ?>
                                                    </td>


                                                </tr>
                                                <?php
                                            endforeach;
                                            ?>
                                            <tr>
                                                <td colspan="3" style="text-align:right"><b>Total</b></td>
                                                <td align="right"><b><i class="fa fa-inr"></i>  <?= moneyFormatIndiaPHP($subtotalt); ?></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        </form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                    </div>
                    <!-- /.box-footer-->
                </div>

                <!-- /.box -->
                </section>
        </div>
        <!-- /.content-wrapper -->

