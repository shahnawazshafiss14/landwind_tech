<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($bannerObject->banner_id) ? 'Edit' : 'Add' ?> Banner</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>admin/banner/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Banner</a></li>
	    <li class="active"><?= !empty($bannerObject->banner_id) ? 'Edit' : 'Add' ?> Banner</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($bannerObject->banner_id) ? 'Edit' : 'Add' ?> Banner</h3>
		<div class="box-tools pull-right">
		    <form  id="bannerForm" name="bannerForm" action="" method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnBanner" id="btnBanner" class="btn btn-success btn-lg">
			    <i class="fa fa-save"></i> Save </button>
			<a href="<?= base_url(); ?>banner/bannerlist" title="Collapse" class="btn btn-info btn-lg">
			    <i class="fa fa-reply"></i> Cancel </a>
		</div>
	    </div>
	    <div class="box-body">
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">

			    <div class="form-group">
				<label class="control-label col-sm-2" for="bTitle">Banner Title<b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="bTitle" name="bTitle" placeholder="Enter Banner Name" value="<?= !empty($bannerObject->banner_name) ? $bannerObject->banner_title : '' ?>">
				</div>
			    </div>
			    <input type="hidden" name="bannerEdit" id="bannerEdit" value="<?= !empty($bannerObject->banner_id) ? $bannerObject->banner_id : ''; ?>">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="bannerDescription">Banner Description</label>
				<div class="col-sm-10">
				    <input class="form-control" placeholder="Enter Description" id="bannerDescription" name="bannerDescription" value="<?= !empty($bannerObject->banner_description) ? $bannerObject->banner_description : '' ?>" class="form-control">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="image_add">Images Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="file" class="form-control" id="image_file" name="image_file">
				    <?php if (!empty($bannerObject->banner_name)) : ?>

    				    <img src="<?= base_url(); ?>assests/banner/<?= $bannerObject->banner_name ?>" width="100" height="50" alt="">
				    <?php endif; ?>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="bType">Select Type</label>
				<div class="col-sm-10">
				<select class="form-control" id="bType" name="bType">
					<?php if (!empty($bannerObject->bType)) : ?>
    					<option value="<?= $bannerObject->bType ?>" selected>
							<?php
								$arr = array('0' => 'Not Selected', '1' => 'Top Home Slider', '2' => 'About us');
								echo $arr[$bannerObject->bType];
							?>
						</option>
					<?php endif; ?>
					<option value="0">Select Type</option>
					<option value="1">Top Home Slider</option>
					<option value="2">About us</option>
				    </select>
				</div>
			    </div>

			    <!--div class="form-group">
				<label class="control-label col-sm-2" for="bMenu">Select Menu</label>
				<div class="col-sm-10">
				    <select class="form-control" id="bMenu" name="bMenu">
					<option value="0">Select Menu</option>
			    <?php foreach ($menuObject as $value) : ?>
    					<option value="<?= $value->menu_id; ?>"><?= $value->menu_name; ?></option>
			    <?php endforeach; ?>
				    </select>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="bCategory">Select Category</label>
				<div class="col-sm-10">
				    <select class="form-control" id="bCategory" name="bCategory">
			    <?php if (!empty($bannerObject->category_id)) : ?>

    					<option value="<?= $bannerObject->category_id ?>" selected><?php
				$arr1 = array('0' => 'Not Selected', '6' => 'Necklace', '7' => 'Earring', '8' => 'Rings', '9' => 'Braclets');
				echo $arr1[$bannerObject->category_id];
				?></option>
			    <?php endif; ?>
					<option value="0">Select Category</option>
			    <?php foreach ($categoryObject as $value) : ?>
    					<option value="<?= $value->category_id; ?>"><?= $value->category_name; ?></option>
			    <?php endforeach; ?>
				    </select>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="bOrder">Banner Order</label>
				<div class="col-sm-10">

				    <input type="number" class="form-control" id="bOrder" name="bOrder" value="<?= !empty($bannerObject->banner_order) ? $bannerObject->banner_order : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="bannerDescription">Banner Description</label>
				<div class="col-sm-10">
				    <textarea id="bannerDescription" name="bannerDescription"  class="form-control">
				    </textarea>
				</div>
			    </div-->

			</div>
		    </div>
		</div>
		</form>
		<div id="uploaded_image">

		</div>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer">

	    </div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>


</div>
<!-- /.content-wrapper -->
