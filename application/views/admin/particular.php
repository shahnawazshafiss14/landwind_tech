<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= !empty($particularObject->item_id) ? 'Edit' : 'Add' ?> Particular</h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>particular/particularList">Particular</a></li>
            <li class="active"><?= !empty($particularObject->item_id) ? 'Edit' : 'Add' ?> Particular</li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('particular_uploaded')) : ?>
            <?php echo $this->session->flashdata('particular_uploaded'); ?>
            <div class="callout callout-success hidepop" id="rock">
                <p id="product_success"> </p>
            </div>
        <?php endif; ?>

        <!-- Default box -->
        <form  id="particularForm" name="particularForm" action="<?= ADMINC; ?>particular/addParticular"  method="POST" enctype="multipart/form-data">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($particularObject->item_id) ? 'Edit' : 'Add' ?> Particular</h3>
                    <div class="box-tools pull-right">

                        <?php
                        $checkm = checkpermission('162');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
                            <button type="submit" name="btnParticular" id="btnParticular" class="btn btn-info btn-sm" title="Save">
                                <i class="fa fa-save"></i> <?= !empty($particularObject->item_id) ? 'Update' : 'Save' ?> </button>
                            <?php if (!empty($particularObject->item_id)) : ?>
                                <a href="<?= ADMINC; ?>particular/particularLists" class="btn btn-info btn-sm" title="Back to List">
                                    <i class="fa fa-reply"></i> Cancel </a>
                            <?php else : ?>
                                <a href="<?= ADMINC; ?>particular/particularLists" data-toggle="tooltip" class="btn btn-info btn-sm" title="Back to List">
                                    <i class="fa fa-reply"></i> Cancel </a>
                            <?php endif; ?>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="box-body">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#General">General</a></li>
                    </ul>
                    <div class="form-horizontal">
                        <div class="tab-content">
                            <div id="General" class="tab-pane fade in active">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="particularName">Particular Name <b style="color:#f00;">*</b></label>
                                    <div class="col-sm-10">
                                        <input name="edit" type="hidden" value="<?= !empty($particularObject->item_id) ? $particularObject->item_id : ''; ?>">
                                        <input type="text" class="form-control" id="item_particular_name" name="item_particular_name" placeholder="Enter Particular Name" value="<?= !empty($particularObject->item_particular_name) ? $particularObject->item_particular_name : '' ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="particularName">Particular Hsn <b style="color:#f00;">*</b></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="item_hsn" name="item_hsn" placeholder="Enter Particular Hsn" value="<?= !empty($particularObject->item_hsn) ? $particularObject->item_hsn : '' ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="particularName">Particular Unit <b style="color:#f00;">*</b></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="item_unit" name="item_unit" placeholder="Enter Particular Unite" value="<?= !empty($particularObject->item_unit) ? $particularObject->item_unit : '' ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="particularNameParent">Select Particular Parent<b style="color:#f00;">*</b></label>
                                    <div class="col-sm-10">
                                       <select name="item_parent" id="item_parent" class="form-control">
                                           
					<?php
                                        $data = array(
                                          'parent_particular' => '0',
                                           'view_status' => '1',
                                            'item_status' => '1'
                                        );
                                        $fetch_part = $this->itemtype_model->viewRecordAnyR($data);
					?>
					<option value="0">Select Particular parent</option>
					<?php foreach ($fetch_part as $part) :
                                            
                                            if($part->item_id ==  $particularObject->parent_particular){
                                            $val = "selected";    
                                            } else {
                                                $val = "";
                                            }
                                            ?>
                                        
    					<option value="<?= $part->item_id ?>" <?= $val; ?>><?= $part->item_particular_name ?></option>
					<?php endforeach; ?>
				    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>
        </form>
        <!-- /.box -->
    </section>


</div>
<!-- /.content-wrapper -->
