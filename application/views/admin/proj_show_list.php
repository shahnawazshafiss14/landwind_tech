<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
 <?php 
        $checkm = checkpermission('169');
        ?>
<style>
    .input-group {
        margin-bottom: 12px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('consig_uploaded')) : ?>
            <?php echo $this->session->flashdata('consig_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="proj_client" id="proj_client" value="<?= !empty($projObject->proj_client) ? $projObject->proj_client : ''; ?>" />
                <input type="hidden" name="proj_id" id="proj_id" value="<?= !empty($projObject->id) ? $projObject->id : ''; ?>" />
                <div class="box-tools pull-right">
                    <a href="<?= ADMINC; ?>keynotes/index/<?= !empty($projObject->proj_slug) ? $projObject->proj_slug : ''; ?>" class="btn btn-warning btn-lg"><i class="fa fa-book"></i> Keynotes</a> | 
                    <a href="javascript:;" class="generateinvoice btn btn-info btn-lg"><i class="fa fa-envelope"></i> Genrate Invoice</a> | 
                    <a href="<?= ADMINC; ?>project/createinvoice/<?= !empty($projObject->proj_slug) ? $projObject->proj_slug : ''; ?>" class="eaddmorecontent btn btn-info btn-lg"><i class="fa fa-pencil"></i> Edit</a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div id="response">
                        <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">
                            <div class="table-responsive">          
                                <table class="table"> 
                                    <tbody>
                                        <tr style="background-color: #b3b2ff;"> 
                                            <th><b>Project Details</b></th> 
                                        </tr>
                                        <?php 
                                        $today_date = date('Y-m-d');
                                        $array_last = array(
                                            'consig_id' => $projObject->proj_client
                                        );
                                        $fetch_client_object = $this->consignee_model->viewRecordAny($array_last);
                                        
                                        ?>
                                        <tr>
                                            <td><b>Client Name</b>: <?= $fetch_client_object->consig_name; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Client Email</b>: <?= $fetch_client_object->consig_email; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Client Phone</b>: <?= $fetch_client_object->consig_phone; ?></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Client Contact Person</b>: <?= $fetch_client_object->consig_contact_person; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Project Title</b>: <?= $projObject->proj_title; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Project Development URL</b>: <a href="<?= $projObject->proj_development_url; ?>" target="_blank"><?= $projObject->proj_development_url; ?></a></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Project Product URL</b>: <a href="<?= $projObject->proj_production_url; ?>" target="_blank"><?= $projObject->proj_production_url; ?></a></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Project Price</b>: <?= $projObject->proj_price; ?></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Project Dos</b>: <?= dateMonth($projObject->proj_dos); ?></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Project Pos</b>: <?= $projObject->proj_pos; ?></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Project Start Date</b>: <?= dateMonth($projObject->proj_start_date) . '<span style="display:block;padding:5px;border-top:2px solid black;"><b>Project End Date:</b> '. dateMonth($projObject->proj_end_date); ?></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Project Duration</b>: <?= dateTwoDiff($projObject->proj_start_date, $projObject->proj_end_date) . '<span style="display:block;padding:5px;border-top:2px solid black;"><b>Project Duration Left:</b> '. dateTwoDiff($projObject->proj_end_date, $today_date); ?></td> 
                                        </tr>  
                                        <tr>
                                            <td><b>Project Description</b>: <?= $projObject->proj_description; ?></td> 
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                            <!--div class="col-sm-9">
                                <b>Consignee Image</b>
                                <input type="file" id="update_consig_img" name="update_consig_img" />
                            </div>
                            <div class="col-sm-3">
                                <input type="button" name="updatebtn_consig_img" id="updatebtn_consig_img" value="Upload" class="btn btn-primary" />
                            </div>
                            <img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
                                 src="<?= !empty($projObject->consig_image) ? BASEURLFRONTASSET . 'consignee/' . $projObject->consig_image : BASEURLFRONTASSET . 'demo.jpg'; ?>" class="img" /-->
                        </div>
                    </div>
                    <?php if(($checkm->is_viewed == '1') && ($this->session->userdata('admin_type') == '1')): ?>
                    <div class="col-sm-3" style="border-right: 2px dotted #ded2d2;">
                        <div class="table-responsive">          
                            <table class="table"> 
                                <tbody>
                                    <tr style="background-color: #ffed6d;">
                                        <th><b>Last Five Invoice Details</b></th> 
                                    </tr>
                                     

                                </tbody>
                            </table>
                        </div> 

                    </div>
                    <div class="col-sm-3">
                        <div class="table-responsive">          
                            <table class="table"> 
                                <tbody>
                                    <tr style="background-color: aquamarine;"> 
                                        <th><b>Last Five Payment Details</b></th> 
                                    </tr>
                                     

                                </tbody>
                            </table>
                        </div> 

                    </div>
                    <?php endif;?>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="" style="margin-top: 50px;margin-bottom: 50px;">
                                     
                                     
                         
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="item_table">
                                            <tr>
                                                <th>Select</th>
                                                <th>Action</th>
                                                <th>Status</th>
                                                <th style="text-align:left">Services</th>
                                                <th style="text-align:center">Term</th>
                                                <th style="text-align:center">Term Value</th>
                                                <th style="text-align:center">Amount</th>
                                                <th style="text-align:center">Start Date</th>
                                                <th style="text-align:center">End Date</th> 
                                                <th style="text-align:right">Total Amount</th>
                                            </tr>
                                            <?php 
                                            $fetch_service_array = array(
                                                'view_status' => '1',
                                                'client_id' => $projObject->proj_client,
                                                'project_id' => $projObject->id
                                            );
                                            $total_amount = 0;
                                            $fetch_services = $this->projectservice_model->viewRecordAnyR($fetch_service_array);
                                            foreach($fetch_services as $ft_ser){
                                                  
                                                if($ft_ser->status == '1'){
                                                    $defaut_cont = "Waiting Invoice";
                                                    $defaut = "white";

                                                }else if($ft_ser->status == '2'){
                                                    $defaut = "yellow";
                                                    $defaut_cont = "Payment Pending";
                                                }else if($ft_ser->status == '3'){
                                                    $defaut = "green";
                                                    $defaut_cont = "Payment Paid";
                                                }
                                                 ?>
                                            <tr style="background-color:<?= $defaut; ?>">
                                                
                                               <td>
                                               <?php
                                               if($ft_ser->status == '1'){
                                                   ?>
                                                   <input type="checkbox" name="selservices" class="selservices" value="<?= $ft_ser->id; ?>" /> 
                                               <?php
                                               }else{
                                                   echo $ft_ser->invoice_no;
                                               } 
                                               ?> 
                                               </td>
                                                
                                                <td>Action</td>
                                                <td><?= $defaut_cont; ?></td>
                                                <td style="text-align:left"><?= getItemtype($ft_ser->item_id); ?></td>
                                                <td style="text-align:center"><?= getSelectTerm($ft_ser->service_duration); ?></td>
                                                <td style="text-align:center"><?= $ft_ser->service_duration_value; ?></td>
                                                <td style="text-align:center"><?= number_format($ft_ser->item_price, 2); ?></td> 
                                                <td style="text-align:center"><?= dateMonth($ft_ser->service_start_date); ?></td>
                                                <td style="text-align:center"><?= dateMonth($ft_ser->service_end_date); ?></td>
                                                <td style="text-align:right"><?php
                                                echo  number_format(($ft_ser->item_price * $ft_ser->service_duration_value), 2);
                                                $total_amount +=  $ft_ser->item_price * $ft_ser->service_duration_value;
                                                
                                                ?>
                                                </td>
                                                
                                            </tr>
                                            <?php
                                            } 
                                            ?> 
                                            <!-- <tr>
                                                <td colspan="8" style="text-align:right">
                                                    Sub Total 
                                                </td> 
                                                <td style="text-align:right"><?= !empty($total_amount) ? number_format($total_amount, 2) : '0.00'; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" style="text-align:right">
                                                    Gst 18%
                                                </td> 
                                                <td style="text-align:right"><?php
                                                echo !empty($total_amount) ? number_format(($total_amount * 18 / 100), 2) : '0.00';
                                                $total_gst =  !empty($total_amount) ? ($total_amount * 18 / 100) : '0';
                                                
                                                ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" style="text-align:right;font-weight:bold">
                                                    Total Amount
                                                </td> 
                                                <td style="text-align:right;font-weight:bold"><?= !empty($total_amount) ? number_format(($total_amount + $total_gst), 2) : '0.00'; ?></td>
                                            </tr> -->
                                            <tr>
                                            <td></td>
                                            
                                            <td></td>
                                            <td></td>
                                            <td>
                                                    <select class="form-control" name="particuler_id" id="particuler_id">
                                                        <option value="">Select Particulars</option>
                                                        <?= itemtype(); ?>
                                                    </select>
                                                </td> 
                                                
                                                <td>
                                            <select class="form-control" name="term_id" id="term_id">
                                                        <option value="">Select Particulars</option>
                                                        <?= selectTerm(); ?>
                                                    </select>
                                            </td>
                                            <td>
                                                <input type="text" name="term_value" id="term_value" class="form-control" value="">
                                            </td>
                                            
                                                
                                                <td>
                                                    <input type="text" name="amount" id="amount" class="form-control" value="">
                                                </td> 
                                                <td>
                                                    <input type="text" name="start_date" id="start_date" class="form-control" value="<?= $today_date; ?>">
                                                </td> 
                                                <td>
                                                    <input type="text" name="end_date" id="end_date" class="form-control" value="<?= $today_date; ?>">
                                                </td> 
                                                <td><a class="btn btn-primary" id="serviceitemupadd" data-id="">Add</a></td>
                                            </tr>



                                        </table>

                                   
                                </div>
                            
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="db-box-outer">
                            <div class="row">
                                 
                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


