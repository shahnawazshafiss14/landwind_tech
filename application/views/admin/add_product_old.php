<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Products</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('product_uploaded')) : ?>
	    <?php echo $this->session->flashdata('product_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="productForm" name="productForm" action="<?= base_url(); ?>admin/product/ajax_upload"  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> <?= !empty($productObject->p_product_id) ? 'Update' : 'Save'; ?> </button>
                        <a href="<?= base_url(); ?>admin/product/productList" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
                            <i class="fa fa-reply"></i> Cancel </a>
                </div>
            </div>
            <div class="box-body">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
                    <!--li><a data-toggle="tab" href="#Data">Data</a></li-->
                     
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">
						<div class="col-md-12">
						<div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="product_name">Product Name <b style="color:#f00;">*</b></label>
                                    <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name" value="<?= !empty($productObject->p_name) ? $productObject->p_name : ''; ?>">
                            </div>
							</div>
							<div class="col-md-6">
							<div class="form-group">
                                <label class="control-label" for="prdouct_brand">Brand <b style="color:#f00;">*</b></label>
                                
								<select name="prdouct_brand" id="prdouct_brand" class="form-control">
								<option value="">Select Brand</option>
                                    <?php 
									$fetch_brand = $this->brand_model->viewRecordAll();
									foreach($fetch_brand as $fb):
									if($productObject->p_brand == $fb->b_id){
										$val = "selected";
									}else{
										$val = "";
									}
									?>
									<option value="<?= $fb->b_id; ?>" <?= $val; ?>><?= $fb->b_name; ?></option>
									<?php 
									endforeach;
									?>
									</select>
                            </div>
							</div>
							</div>
						<div class="col-md-12">
						<div class="col-md-4">
							<div class="form-group">
                                <label class="control-label" for="prdouct_unit_pices">Unit Pices <b style="color:#f00;">*</b></label>
                                <input type="hidden" name="prdouct_unit_pices" id="prdouct_unit_pices" value="<?php 
									echo $prdouct_unit_dozen = "1";
								?>" />
								<select name="prdouct_unit_pices1" id="prdouct_unit_pices1" class="form-control" disabled>
								<option value="">Select Unit</option>
                                    <?php 
									$fetch_unit = $this->unit_model->viewRecordAll();
									foreach($fetch_unit as $fb):
									if($prdouct_unit_dozen == $fb->u_id){
										$val = "selected";
									}else{
										$val = "";
									}
									?>
									<option value="<?= $fb->u_id; ?>" <?= $val; ?>><?= $fb->u_name; ?></option>
									<?php 
									endforeach;
									?>
									</select>
                            </div>
							
                            <div class="form-group">
                                <label class="control-label" for="prdouct_selling_price">Selling Pices Price <b style="color:#f00;">*</b></label>
                                
                                    <input type="text" id="prdouct_selling_pc_price" name="prdouct_selling_pc_price" class="form-control" value="<?= !empty($productObject->p_selling_pc_price) ? $productObject->p_selling_pc_price : ''; ?>">
                                
                            </div>
							<div class="form-group">
                                <label class="control-label" for="prdouct_pc_price">Pices Price <b style="color:#f00;">*</b></label>
                                    <input type="text" id="prdouct_pc_price" name="prdouct_pc_price" class="form-control" value="<?= !empty($productObject->p_pc_price) ? $productObject->p_pc_price : ''; ?>">
                            </div>
							</div>
							<div class="col-md-4">
							<div class="form-group">
                                <label class="control-label" for="prdouct_unit_dozen">Unit Dozend <b style="color:#f00;">*</b></label>
                                <input type="hidden" name="prdouct_unit_dozen" id="prdouct_unit_dozen" value="<?php 
									echo $prdouct_unit_dozen = "2";
								?>" />
								<select name="prdouct_unit_dozen1" id="prdouct_unit_dozen1" class="form-control" disabled>
								
								<option value="">Select Unit</option>
                                    <?php 
									
									$fetch_unit = $this->unit_model->viewRecordAll();
									foreach($fetch_unit as $fb):
									if($prdouct_unit_dozen == $fb->u_id){
										$val = "selected";
									}else{
										$val = "";
									}
									?>
									<option value="<?= $fb->u_id; ?>" <?= $val; ?>><?= $fb->u_name; ?></option>
									<?php 
									endforeach;
									?>
									</select>
                            </div>
							
                            <div class="form-group">
                                <label class="control-label" for="prdouct_selling_do_price">Selling Dozend Price <b style="color:#f00;">*</b></label>
                                
                                    <input type="text" id="prdouct_selling_do_price" name="prdouct_selling_do_price" class="form-control" value="<?= !empty($productObject->p_selling_do_price) ? $productObject->p_selling_do_price : ''; ?>">
                                
                            </div>
							<div class="form-group">
                                <label class="control-label" for="prdouct_do_price">Dozend Price <b style="color:#f00;">*</b></label>
                                    <input type="text" id="prdouct_do_price" name="prdouct_do_price" class="form-control" value="<?= !empty($productObject->p_do_price) ? $productObject->p_do_price : ''; ?>">
                            </div>
							</div>
							<div class="col-md-4">
							<div class="form-group">
                                <label class="control-label" for="prdouct_unit_pices">Unit Bandle <b style="color:#f00;">*</b></label>
                                <input type="hidden" name="prdouct_unit_bandle" id="prdouct_unit_bandle" value="<?php 
									echo $prdouct_unit_dozen = "3";
								?>" />
								<select name="prdouct_unit_bandle" id="prdouct_unit_bandle" class="form-control" disabled>
								<option value="">Select Bandle</option>
                                    <?php 
									$fetch_unit = $this->unit_model->viewRecordAll();
									foreach($fetch_unit as $fb):
									if($prdouct_unit_dozen == $fb->u_id){
										$val = "selected";
									}else{
										$val = "";
									}
									?>
									<option value="<?= $fb->u_id; ?>" <?= $val; ?>><?= $fb->u_name; ?></option>
									<?php 
									endforeach;
									?>
									</select>
                            </div>
							
                            <div class="form-group">
                                <label class="control-label" for="prdouct_selling_bn_price">Selling Bandle Price <b style="color:#f00;">*</b></label>
                                
                                    <input type="text" id="prdouct_selling_bn_price" name="prdouct_selling_bn_price" class="form-control" value="<?= !empty($productObject->p_selling_bn_price) ? $productObject->p_selling_bn_price : ''; ?>">
                                
                            </div>
							<div class="form-group">
                                <label class="control-label" for="prdouct_bn_price">Bandle Price <b style="color:#f00;">*</b></label>
                                    <input type="text" id="prdouct_bn_price" name="prdouct_bn_price" class="form-control" value="<?= !empty($productObject->p_bn_price) ? $productObject->p_bn_price : ''; ?>">
                            </div>
							</div>
							</div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="prdouct_discount">Discount</label>
                                <div class="col-sm-10">
                                    <select name="prdouct_discount" id="prdouct_discount" class="form-control">
										<option value="">--Select Discount--</option>
                                        <option value="5"> 5%</option>
                                        <option value="10">10%</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="productEdit" id="productEdit" value="<?= !empty($productObject->p_product_id) ? $productObject->p_product_id : ''; ?>"/>
                        </div>
                         
                          
		    </div>
		</div>

		</form>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer">
	    </div>
	    <!-- /.box-footer-->
	</div>

	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
