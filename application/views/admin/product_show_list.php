<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('product_uploaded')) : ?>
	    <?php echo $this->session->flashdata('product_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
		<input type="hidden" name="productid" id="productid" value="<?= !empty($productObject->p_product_id) ? $productObject->p_product_id : ''; ?>" />
                <div class="box-tools pull-right">

		    <!--a href="javascript:;" class="addmorecontent" name="addmorecontent" id="addmorecontent" data-extraid="" data-producid="<?= !empty($productObject->p_product_id) ? $productObject->p_product_id : ''; ?>" data-toggle="modal" data-target="#myModal">Add More Content</a-->
		</div>
	    </div>
	    <div class="box-body">


		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
			    <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			    </div>
			    <div class="modal-body">
				<div class="row">
				    <div class="form-group">
					<label class="control-label col-sm-2" for="Title">Title <b style="color:#f00;">*</b></label>
					<div class="col-sm-10">
					    <input class="form-control" id="p_extra_con_title" name="p_extra_con_title" placeholder="Title" value="<?= !empty($productObject->p_extra_con_title) ? $productObject->p_extra_con_title : ''; ?>">
					</div>
					<br/>
					<br/>
				    </div>
				    <div class="form-group">
					<label class="control-label col-sm-2" for="Description">Description <b style="color:#f00;">*</b></label>
					<div class="col-sm-10">
					    <textarea class="form-control ckeditor" id="p_extra_con_desc" name="p_extra_con_desc" placeholder="Description"> <?= !empty($productObject->p_extra_con_desc) ? $productObject->p_extra_con_desc : ''; ?>
					    </textarea>
					</div>
				    </div>
				    <div class="form-group">
					<label class="control-label col-sm-2" for="Description"></label>
					<div class="col-sm-10">
					    <input type="button" name="p_extra_con_btn" id="p_extra_con_btn" value="Save" class="btn btn-success"/>
					</div>
				    </div>

				</div>
			    </div>
			    <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    </div>
			</div>

		    </div>
		</div>

		<div class="row">
		    <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">


			<div class="col-sm-9">
			    <b>Product Images</b>
			    <input type="file" id="productImage" name="productImage" />
			</div>
			<div class="col-sm-3">
			    <input type="button" name="uploadproductImage" id="uploadproductImage" value="Upload" class="btn btn-primary" />
			</div>

			<img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
			     src="<?= base_url(); ?>assests/product/<?= $productObject->p_image; ?>" class="img" />

			<div class="row">
			    <?php
			    $ara = array(
				'pid' => $productObject->p_product_id,
				'category' => 'product',
			    );
			    $extraimgs = $this->extraimages_model->viewRecordAnyR($ara);
			    foreach ($extraimgs as $eximg):
				?>
    			    <div class="col-sm-2" style="padding: 0px;padding-left: 16px;margin-bottom:15px;">
    				<img style="width:100%;height:75px;marign-bottom:5px;" src="<?= BASEURLFRONTASSET; ?>product/<?= $eximg->fiename; ?>" />

    				<a href="javascript:;" style="float:left;" id="deleteimg" class="deleteimg" data-id="<?= $eximg->imagesid; ?>"> <i class="fa fa-trash"></i> Delete</a>


    			    </div>
			    <?php endforeach; ?>
                        </div>

                        <form class="form-horizontal" action="<?= ADMINC; ?>product/extraimagesupload" method="post" enctype="multipart/form-data">
                            <span id="addmore" style="cursor: pointer;border: 1px solid #aba0a0;padding: 5px 20px;background: #f77f14;">add more</span>
                            <div class="first">
                                <div class="form-group" style="margin-top:10px;">
                                    <input type="hidden" name="productEdit" value="<?= $productObject->p_product_id; ?>">
                                    <label class="control-label col-sm-8 imgup" for="email"><input type="file" class="imgname" name="files[]" multiple></label>
                                    <div class="col-sm-4">
                                        <input type="submit" name="upload" id="" class="btn btn-success" value="upload" >
                                    </div>
                                </div>
                            </div>
                        </form>
		    </div>
		    <div class="col-sm-6">
			<div class="product-name"><?= $productObject->p_title; ?></div>
			<b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px; display:block">Product Details</b>
			<div class="row detailsprodod">
			    <p><span class="col-sm-5"><b>Product Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $productObject->p_title; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Product Category</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $this->category_model->categorybyId($productObject->p_category); ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Product Price</b><b class="pull-right">:</b></span>  <span class="col-sm-7">$<?= $productObject->p_price; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Product Ship Price</b><b class="pull-right">:</b></span>  <span class="col-sm-7">$<?= $productObject->p_ship_price; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Product Tax Price</b><b class="pull-right">:</b></span>  <span class="col-sm-7">$<?= $productObject->p_tax_price; ?></span></p><br/>
			</div>
		    </div>
		</div>
		<div class="row">
		    <div class="col-md-12">
			<div class="input-group">
			    <b>Description: </b> <?= $productObject->p_discription; ?>
			</div>
		    </div>
		</div>
		<?php
		$daaraay = array(
		    'p_product_id' => $productObject->p_product_id
		);
		$fetchextracon = $this->extraproducts_model->viewRecordAnyR($daaraay);
		foreach ($fetchextracon as $fetchextra):
		    ?>
    		<div class="">
    		    <div class="">
    			<a href="javascript:;" class="btn btn-danger extraproddelete" data-delid="<?= !empty($fetchextra->p_extra_id) ? $fetchextra->p_extra_id : ''; ?>">Delete</a> | <a href="javascript:;" class="btn btn-primary addmorecontent" name="addmorecontent" id="addmorecontent" data-extraid="<?= !empty($fetchextra->p_extra_id) ? $fetchextra->p_extra_id : ''; ?>" data-producid="<?= !empty($productObject->p_product_id) ? $productObject->p_product_id : ''; ?>" data-toggle="modal" data-target="#myModal">Edit</a>

    		    </div>
    		    <div class="row">
    			<div class="form-group">

    			    <div class="col-sm-12 ">
    				<div class="col-sm-12 open-d">
    				    <p style="margin: 6px 0;font-size: 20px;font-weight: 600;border-bottom: 1px solid #ccc;"><?= !empty($fetchextra->p_extra_title) ? $fetchextra->p_extra_title : ''; ?> <i class="fa fa-pencil"></i></p>

    				</div>
    				<div class="content-d">
    				    <h2>Description</h2>
					<?= !empty($fetchextra->p_extra_desc) ? $fetchextra->p_extra_desc : ''; ?></div>
    			    </div>
    			</div>
    		    </div>
    		</div>
		<?php endforeach; ?>

		<div class="">
		</div><!-- box-body -->



		<div class="box-footer">

		</div><!-- box-footer -->

	    </div>
	    <!-- .box -->
    </section>



</div>
<!-- .content-wrapper -->


