<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Products List<small>Manage View Product</small>
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Tables</a></li>
	    <li class="active">Data tables</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th> 
				    <th>Images</th>
				    <th>Product Name</th>
				    <th>Brand Name</th>
				    <th>Model</th>
				    <th>SKU</th>
				    <th>Selling Price</th>				    
				    <th>Quantity</th>
				    <th>Status</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody> 
				<?php
				$productObjects->p_title;
				?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
