<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Events List</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Images</th>
                                    <th>Event Title</th>
                                    <th>Event Date</th>
				    <th>Start Time - End Time</th>
				    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				foreach ($eventObjects as $pO) :
				    ?>
    				<tr class="deleteEventtrE<?= $pO->event_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td>
    					<a href="<?= ADMINC; ?>events/event_show_list/<?= $pO->event_id ?>" target="blank">
    					    <img src="<?= BASEURLFRONTASSET; ?>event/<?= $pO->event_image; ?>" alt="" width="100" height="50">
    					</a>
    				    </td>
    				    <td><?= $pO->event_title; ?></td>
    				    <td><?= $pO->event_start_date; ?></td>
    				    <td><?= $pO->event_time . ' - ' . $pO->event_end_date; ?></td>
    				    <td><a href="javascript:;" class="estatus" data-pid="<?= $pO->event_id; ?>" data-val="<?= ($pO->event_status == 1) ? '0' : '1' ?>"><span style="font-size: 20px;" class="text-<?= ($pO->event_status == 1) ? 'green' : 'red' ?>"><?= ($pO->event_status == 1) ? 'Approved' : 'New Request' ?></span></a></td>
    				    <td><a href="javascript:;" class="deleteRcordE" data-id="<?= $pO->event_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= ADMINC; ?>events/addEvent/<?= $pO->event_id; ?>"><i class="fa fa-edit"></i></a>| <a href="<?= ADMINC; ?>events/event_show_list/<?= $pO->event_id; ?>"><i class="fa fa-book" aria-hidden="true"></i></a></td>
    				</tr>
				<?php endforeach; ?>
                                </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p><?php echo $links; ?></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
