<?php 

$bill_url_id = $this->uri->segment(4);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sales</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section> -->
    
	<?php if ($this->session->flashdata('bill_uploaded')) : ?> 
	    <?php echo $this->session->flashdata('bill_uploaded'); ?>
	<?php endif; ?>
        
            
			
			
			  <div class="row" style="padding: 3px 0px; background-color: #fff" >
	 
      <div class="col-md-12">
   
     <h3 class="text-left" style="margin-left: 18px"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>
            <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"> Back</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
          
            
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>

               
                
                      <div class="container">
						<div class="row" style="margin-top: 50px;  ">
						<div class="col-md-1"></div>
						<div class="col-md-3" style="background-color: antiquewhite; border-radius: 10px">
                            <div class="form-group">
                                <label class="control-label" for="Sales_name">Party Name <b style="color:#f00;">*</b></label>
								<select class="form-control" id="party_name" name="party_name" required>
								<option value="">Select Party</option>
								<?php 
								
								$bill_no = $this->uri->segment(4);
								$dap = array(
									'bill_no' => $bill_no
								);
								$billObjectssignle = $this->billing_model->viewRecordAny($dap); 
		 
								$fetch_ara_users = array(
									'view_status' => '1',
									'userStatus' => '1'
								);
								$consigne_fetch = $this->user_m_f->viewRecordAnyR($fetch_ara_users);
								foreach($consigne_fetch as $fet_users):
								if($billObjectssignle->bill_consignee == $fet_users->u_id){
									$val = "selected";
								}else{
									$val = "";
								}
								?>
								<option value="<?= $fet_users->u_id; ?>" <?= $val; ?>><?= $fet_users->userName; ?></option>
								<?php 
								endforeach;
								?>
								</select>
								<button type="button" title="Add new party" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i></button>
                            </div>
							</div>
							
							<div class="col-md-3" style="background-color: antiquewhite; border-radius: 10px">
							<label class="control-label" for="Sales_name">Vouchar Date <b style="color:#f00;">*</b></label>
                           <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
								<input type="text" name="bill_date" id="bill_date" class="form-control" data-date-format="yyyy-mm-dd" value="<?= !empty($billObjectssignle->bill_date) ? $billObjectssignle->bill_date : date('Y-m-d');?>">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
							</div>
							 
							
							</div>
							<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10" style="background-color: antiquewhite; border-radius: 10px">
							
          
<table align='center' id="data_table" class="table table-dark" style="">
<tr>
<th>#</th>
<th>Product</th>
<th>Qty</th>
<th>Units</th>
<!--th>Unit</th-->
<th>Rate</th>
<!--th>Units</th-->
<th>Dsc</th>
<th>Total Price</th>
<th>Action</th>
</tr> 
<?php
 
		
		if(count($billObjects) > 0): 
		$i = 1;
		$j = 1;
		$z = 1;
		
		foreach($billObjects as $fetc_p1):
		$prod_id = array('p_product_id' => $fetc_p1->bill_product_id);
		$product_model_f = $this->product_model->viewRecordAny($prod_id);
		?>
<tr id="row<?= $i++; ?>" class="deleteCategorytrC<?= $fetc_p1->bill_id; ?>">
<td scope="row"><?= $j++;?> </td>
      <td>
	  <select name="bill_item[]" class="form-control bill_item" id="bill_item" <?= !empty($billObjectssignle->bill_product_id) ? 'disabled' : ''; ?>>
		<option value="">Select item</option>
		<?php 
		
		$array_product = array(
			'view_status' => '1'
		);
		$fet_product =$this->product_model->viewRecordAnyR($array_product);
		foreach($fet_product as $fetc_p):
		 
  $udename = $this->product_model->pbyId($fetc_p1->bill_product_id);
  if($udename ==  $fetc_p->p_name){
			$val = "selected";
		}else{
			$val = "";
		}
  
?>
		<option value="<?= $fetc_p->p_name; ?>" <?= $val; ?>><?= $fetc_p->p_name; ?></option>
		<?php 
		endforeach;
		?>
	  </select>
	  <?php 
	  if(empty($billObjectssignle->bill_product_id)) {?>
	  <button type="button" title="Add new product" class="btn btn-primary" data-toggle="modal" data-target="#myProduct"><i class="fa fa-plus"></i></button>
	  <?php
	  }
	  ?>
	  </td> 
<td><input type="text" name="bill_qty[]" class="form-control bill_qty number updateajax" data-rowid="<?= $fetc_p1->bill_id; ?>" data-up="qty" id="bill_qty"  value="<?= $fetc_p1->bill_qty; ?>" /></td>


      <!--td><select name="bill_unit[]" class="form-control bill_unit" id="bill_unit">
		<option value="">Select unit</option>
		<?php 
		
		$array_unit = array(
			'view_status' => '1'
		);
		$fet_unit =$this->unit_model->viewRecordAnyR($array_unit);
		foreach($fet_unit as $fetc_u):
		  $udename_unit = $this->unit_model->unitbyId($fetc_p1->bill_unit);
		if($udename_unit == $fetc_u->u_name){
			$val = "selected";
		}else{
			$val = "";
		}
		?>
		<option value="<?= $fetc_u->u_name; ?>" <?= $val; ?>><?= $fetc_u->u_name; ?></option>
		<?php 
		endforeach;
		?>
	  </select></td-->
<td><input type="text" name="bill_units" class="form-control bill_unit" id="bill_unit" disabled value="<?= $this->unit_model->unitbyId($product_model_f->p_unit); ?>" /></td>

	  <td><input type="text" name="bill_price[]" class="form-control bill_price updateajax" data-rowid="<?= $fetc_p1->bill_id; ?>" data-up="price" id="bill_price" value="<?= $fetc_p1->bill_price; ?>" /></td>
      
      <td><input type="text" name="bill_discount[]" class="form-control bill_discount updateajax" data-rowid="<?= $fetc_p1->bill_id; ?>" data-up="discount" id="bill_discount" value="<?= $fetc_p1->bill_discount; ?>" /></td>
      <td><input type="text" name="sub_bill_price[]" class="form-control sub_bill_price" id="sub_bill_price" disabled value="<?= $fetc_p1->bill_price * $fetc_p1->bill_qty; ?>" /></td>
 
<td><button type="button" title="Delete" class="btn btn-danger deleterow" data-id="<?= $fetc_p1->bill_id; ?>"><i class="fa fa-times"></i></button></td>
</tr>
<?php 
endforeach;
else:
?>
<tr id="row1">
<td scope="row">#</td>
      <td>
	  <select name="bill_item[]" class="form-control bill_item" id="bill_item">
		<option value="">Select item</option>
		<?php 
		
		$array_product = array(
			'view_status' => '1'
		);
		$fet_product =$this->product_model->viewRecordAnyR($array_product);
		foreach($fet_product as $fetc_p):
		?>
		<option value="<?= $fetc_p->p_name; ?>"><?= $fetc_p->p_name; ?></option>
		<?php 
		endforeach;
		?>
	  </select>
	  <button type="button" title="Add new product" class="btn btn-primary" data-toggle="modal" data-target="#myProduct"><i class="fa fa-plus"></i></button>
	  </td> 
<td><div class="bill_qty1"><input type="text" name="bill_qty[]" class="form-control bill_qty number" id="bill_qty" /></div></td>
 
<td><input type="text" name="bill_unit[]" class="form-control bill_unit" id="bill_unit" disabled /></td>

      <!--td><select name="bill_unit[]" class="form-control bill_unit" id="bill_unit">
		<option value="">Select unit</option>
		<?php 
		
		$array_unit = array(
			'view_status' => '1'
		);
		$fet_unit =$this->unit_model->viewRecordAnyR($array_unit);
		foreach($fet_unit as $fetc_u):
		?>
		<option value="<?= $fetc_u->u_name; ?>"><?= $fetc_u->u_name; ?></option>
		<?php 
		endforeach;
		?>
	  </select></td-->
	  <td><div class="bill_price1"><input type="text" name="bill_price[]" class="form-control bill_price" id="bill_price" /></div></td>
      <td><div class="bill_discount1"><input type="text" name="bill_discount[]" class="form-control bill_discount" id="bill_discount" /></div></td>
      <td><input type="text" name="sub_bill_price[]" class="form-control sub_bill_price" id="sub_bill_price" disabled /></td>
 
<td><button type="button" title="Add More" class="btn btn-primary add" onclick="add_row();"><i class="fa fa-plus"></i> Add More</button></td>
</tr>
<?php 
endif;
?>

</table>

 
			 
							</div>
							<div class="col-md-1"></div>
							</div>
							 
						 
							
							</div>
						
                            
                            <input type="hidden" name="SalesEdit" id="SalesEdit" value="<?= !empty($bill_url_id) ? $bill_url_id : ''; ?>"/>
                        
                         
                          
		   

		</form>
		
	    
	    <!-- /.box-body -->
	    
	    <!-- /.box-footer-->
		
	
<div class="row" style="margin-top: 12px">
			
                
				
				
				 <?php 
	  if(empty($billObjectssignle->bill_product_id)) {?>
                <div class="col-md-3 pull-right">
                     <form  id="billForm" name="billForm" action=""  method="POST" enctype="multipart/form-data">
			<button type="button" name="btnSales" id= "btnSales" class="btn btn-primary btn-md" title="Submit">
			    <i class="fa fa-save"></i> <?= !empty($bill_url_id) ? 'Update' : 'Generate Voucher'; ?> </button>
                       
                </div>
				<?php 
	  }
	  ?>
            
			</div>
	<!-- /.box -->
	<div id="myProduct" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Product</h4>
      </div>
      <div class="modal-body">
       
						<div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="product_name">Product Name <b style="color:#f00;">*</b></label>
                                    <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter Product Name" value="">
                            </div>
							</div>
							<div class="col-md-6">
							<div class="form-group">
                                <label class="control-label" for="prdouct_brand">Brand <b style="color:#f00;">*</b></label>
                                
								<select name="prdouct_brand" id="prdouct_brand" class="form-control">
								<option value="">Select Brand</option>
                                    <?php 
									$fetch_brand = $this->brand_model->viewRecordAll();
									foreach($fetch_brand as $fb):
									if($productObject->p_brand == $fb->b_id){
										$val = "selected";
									}else{
										$val = "";
									}
									?>
									<option value="<?= $fb->b_id; ?>" <?= $val; ?>><?= $fb->b_name; ?></option>
									<?php 
									endforeach;
									?>
									</select>
                            </div>
							</div>
							 
						 
						<div class="col-md-3">
							<div class="form-group">
                                <label class="control-label" for="prdouct_unit_pices">Unit Pices <b style="color:#f00;">*</b></label>
                                <input type="hidden" name="prdouct_unit_pices" id="prdouct_unit_pices" value="<?php 
									echo $prdouct_unit_dozen = "1";
								?>" />
								<select name="prdouct_unit_pices1" id="prdouct_unit_pices1" class="form-control" disabled>
									<option value="" selected>CTN</option>
								</select>
                            </div> 
							</div>
							<div class="col-md-6">
							<div class="form-group">
                                <label class="control-label" for="prdouct_qty">Qty<b style="color:#f00;">*</b></label>
                                    <input type="text" id="prdouct_qty" name="prdouct_qty" class="form-control number" autocomplete="off" value="">
                            </div> 
							</div>
							<div class="col-md-3">
							 <div class="form-group">
                                <label class="control-label" for="prdouct_unit_dozen">Unit Pices<b style="color:#f00;">*</b></label>
								<select name="prdouct_unit" id="prdouct_unit" class="form-control">
									<option value="">Select Unit</option>
                                    <?php 
									$fetch_unit = $this->unit_model->viewRecordAll();
									foreach($fetch_unit as $fb):
									/*
									if($prdouct_unit_dozen == $fb->u_id){
										$val = "selected";
									}else{
										$val = "";
									}
									*/
									?>
									<option value="<?= $fb->u_id; ?>"><?= $fb->u_name; ?></option>
									<?php 
									endforeach;
									?>
									</select>
                            </div>
							</div>
							 
							 
							<div class="form-group">
								<label><input type="checkbox" name="check_prod_price" id="check_prod_price"> Price Yes</label><br/>
								<div class="col-md-3" id="divprice" style="display:none">
									<input type="text" name="prdouct_price" class="form-control" id="prdouct_price" value="" />
								</div>
							</div>
						 
			
			 
				<div class="form-group">
					<label><input type="checkbox" name="check_prod_discount" id="check_prod_discount"> Discount Yes</label><br/>
								<div class="col-md-3" id="divdiscount" style="display:none">
									<input type="text" name="prdouct_discount" class="form-control" id="prdouct_discount" value="" />
								</div>
				</div>
			 
      </div>
      <div class="modal-footer">
	  <button type="button" title="Add new Product" class="btn btn-primary" name="btnAddproduct" id="btnAddproduct">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Party</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="user_name">Name <b style="color:#f00;">*</b></label>
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter User Name" value="">
                            </div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
                                <label class="control-label" for="user_status">User City</label>
                                <input type="text" class="form-control" id="user_city" name="user_city" placeholder="Enter User City" value="">    
                            </div>
							</div>
      </div>
      <div class="modal-footer">
		<button type="button" title="Add new party" class="btn btn-primary" name="btnAddparty" id="btnAddparty">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</div>
<!-- Modal -->

<!-- /.content-wrapper -->
<script>
 /*
function edit_row(no)
{
 document.getElementById("edit_button"+no).style.display="none";
 document.getElementById("save_button"+no).style.display="block";
	
 var name=document.getElementById("name_row"+no);
 var country=document.getElementById("country_row"+no);
 var age=document.getElementById("age_row"+no);
	
 var name_data=name.innerHTML;
 var country_data=country.innerHTML;
 var age_data=age.innerHTML;
	
 name.innerHTML="<input type='text' id='name_text"+no+"' value='"+name_data+"'>";
 country.innerHTML="<input type='text' id='country_text"+no+"' value='"+country_data+"'>";
 age.innerHTML="<input type='text' id='age_text"+no+"' value='"+age_data+"'>";
}
<button type='button' id='edit_button"+table_len+"' title='Edit' class='btn btn-warning edit' onclick='edit_row("+table_len+")'><i class='fa fa-pencil'></i></button>
*/
function save_row(no)
{
 var name_val=document.getElementById("name_text"+no).value;
 var country_val=document.getElementById("country_text"+no).value;
 var age_val=document.getElementById("age_text"+no).value;

 document.getElementById("name_row"+no).innerHTML=name_val;
 document.getElementById("country_row"+no).innerHTML=country_val;
 document.getElementById("age_row"+no).innerHTML=age_val;

 document.getElementById("edit_button"+no).style.display="block";
 document.getElementById("save_button"+no).style.display="none";
}

function delete_row(no)
{
 document.getElementById("row"+no+"").outerHTML="";
}

function add_row()
{
 var bill_item = document.getElementById("bill_item").value;
 var bill_qty = document.getElementById("bill_qty").value;
 var bill_unit = document.getElementById("bill_unit").value;
 var bill_price = document.getElementById("bill_price").value;
 var bill_discount = document.getElementById("bill_discount").value;
 var sub_bill_price = document.getElementById("sub_bill_price").value;
	
 var table=document.getElementById("data_table");
 var table_len=(table.rows.length)-1;
 var row = table.insertRow(table_len).outerHTML=
 "<tr id='row"+table_len+"'><td id='tdle'>"+table_len+"</td><td id='bill_item"+table_len+"'>"+bill_item+"<input type='hidden' name='bill_item[]' value='"+bill_item+"' /></td><td id='bill_qty"+table_len+"'>"+bill_qty+"<input type='hidden' name='bill_qty[]' value='"+bill_qty+"'/></td><td id='bill_unit"+table_len+"'>"+bill_unit+"<input type='hidden' name='bill_unit[]' value='"+bill_unit+"' /></td><td id='bill_price"+table_len+"'>"+bill_price+"<input  type ='hidden' name='bill_price[]' value='"+bill_price+"'/></td><td id='bill_discount"+table_len+"'>"+bill_discount+"<input  type ='hidden' name='bill_discount[]' value='"+bill_discount+"'/></td><td id='sub_bill_price"+table_len+"'>"+sub_bill_price+"</td><td> <button title='Save' type='button' style='display:none' id='save_button"+table_len+"' class='btn btn-primary save' onclick='save_row("+table_len+")'><i class='fa fa-save'></i></button> <button title='Remove' type='button' class='btn btn-danger delete' onclick='delete_row("+table_len+")'><i class='fa fa-times'></i></button></td></tr>";

 document.getElementById("bill_item").value="";
 document.getElementById("bill_qty").value="";
 document.getElementById("bill_unit").value="";
 document.getElementById("bill_price").value="";
 document.getElementById("bill_discount").value="";
 document.getElementById("sub_bill_price").value="";
}
</script>