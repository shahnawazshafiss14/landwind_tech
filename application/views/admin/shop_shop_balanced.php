<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<style>
    .input-group {
        margin-bottom: 12px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('shop_uploaded')) : ?>
            <?php echo $this->session->flashdata('shop_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="shopid" id="shopid" value="<?= !empty($shopObject->shop_id) ? $shopObject->shop_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <a href="<?= ADMINC; ?>shop/edit/<?= !empty($shopObject->shop_slug) ? $shopObject->shop_slug : ''; ?>" class="eaddmorecontent btn btn-info btn-lg">Edit <i class="fa fa-pencil"></i></a> <button title="Refresh" class="btn btn-success" id="btnitemrefere"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div id="response">
                        <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">
                            <div class="col-sm-6">
                                <?php
                                $slugfie = array(
                                    'items_shop_id' => $shopObject->shop_id
                                );
                                $totalt = 0;
                                $fetchdata = $this->items_model->viewRecordAnyR($slugfie);
                                foreach ($fetchdata as $ft):

                                    $slugfiet = array(
                                        'purchedItem' => $ft->items_id,
                                        'strans_type' => 1
                                    );
                                    $fetchdatat = $this->shoptrans_model->viewRecordAnyR($slugfiet);
                                    foreach ($fetchdatat as $ftt):
                                        ?>
                                        <?php $totalt += $ftt->strans_amount; ?>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                                <?php
                                $totaltbitem = 0;
                                foreach ($fetchdata as $ft):
                                    $slugfiet = array(
                                        'purchedItem' => $ft->items_id
                                    );
                                    $fetchdatap = $this->pitems_model->viewRecordAnyR($slugfiet);
                                    foreach ($fetchdatap as $ftp):
                                        ?>
                                        <?php $totaltbitem += $ftp->pitem_unit * $ftp->pitem_rate; ?>
                                    <?php endforeach; ?>
                                    <?php
                                    $fetchParticlearraydr = array(
                                        'purchedItem' => $ft->items_id,
                                        'strans_type' => 2
                                    );
                                    // $fetchdatat = $this->shoptrans_model->viewRecordPageRG($fetchParticlearray1, 'purchedItem');
                                    $fetchdatat = $this->shoptrans_model->viewRecordAnyR($fetchParticlearraydr);
                                    foreach ($fetchdatat as $ftt):
                                        $toalspayment += $ftt->strans_amount;
                                        ?>

                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <h3><i class="fa fa-inr"></i> <?= !empty($totaltbitem + $toalspayment) ? $totaltbitem + $toalspayment : '0.00'; ?> </h3>
                                        <p>Total Amount</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-bag"></i>
                                    </div>
                                    <a href="<?= ADMINC; ?>shop/shop_by_paid/<?= $shopObject->shop_slug; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="small-box bg-red">
                                    <div class="inner">
                                        <h3><i class="fa fa-inr"></i> <?= !empty($totaltbitem + $toalspayment) && ($totaltbitem + $toalspayment) > $totalt ? (($totaltbitem + $toalspayment) - $totalt) : '0.00'; ?></h3>
                                        <p>Total Payable Amount</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                    </div>
                                    <a href="<?= ADMINC; ?>shop/shop_by_balance/<?= $shopObject->shop_slug; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h3><i class="fa fa-inr"></i> <?= !empty($totalt) && $totalt > ($totaltbitem + $toalspayment) ? ($totalt - ($totaltbitem + $toalspayment)) : '0.00'; ?></h3>
                                        <p>Total Receiver Amount</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                    </div>
                                    <a href="<?= ADMINC; ?>shop/shop_by_balance/<?= $shopObject->shop_slug; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3><i class="fa fa-inr"></i> <?= !empty($totalt) ? $totalt : '0.00'; ?> </h3> 
                                        <p>Total Paid Amount</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                    </div>
                                    <a href="<?= ADMINC; ?>shop/shop_paid/<?= $shopObject->shop_slug; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;display:block;float:  left;width: 100%;"><h3 style="float: left;margin:  0; margin-bottom: 9px; margin-top:  5px;
                                                                                                                                                                           ">Shop Details</h3>   
                        </div>
                        <div class="row detailsprodod">
                            <p><span class="col-sm-5"><b>Shop Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $shopObject->shop_name; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop Address</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $shopObject->shop_address; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop Email</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= !empty($shopObject->shop_email) ? $shopObject->shop_email : 'null'; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop Phone</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= !empty($shopObject->shop_phone) ? $shopObject->shop_phone : 'null'; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop GSTIN</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= !empty($shopObject->shop_gstin) ? $shopObject->shop_gstin : 'null'; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop State</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= !empty($shopObject->shop_state) ? $shopObject->shop_state : 'null'; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop City</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= !empty($shopObject->shop_city) ? $shopObject->shop_city : 'null'; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>Shop Zipcode</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= !empty($shopObject->shop_zipcode) ? $shopObject->shop_zipcode : 'null'; ?></span></p><br/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <b>Description: </b> <?= !empty($shopObject->shop_description) ? $shopObject->shop_description : 'null'; ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example1" class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Sr.</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Amount</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $slugfie = array(
                                            'items_shop_id' => $shopObject->shop_id
                                        );
                                        $totalt = 0;
                                        $fetchdata = $this->items_model->viewRecordAnyR($slugfie);
                                        ?>
                                        <?php $totalt += $ftt->strans_amount; ?>

                                        <?php
                                        $i = 1;
                                        $totaltbitem = 0;
                                        foreach ($fetchdata as $ft):
                                            $slugfiet = array(
                                                'purchedItem' => $ft->items_id
                                            );
                                            $fetchdatap = $this->pitems_model->viewRecordPageRG($slugfiet, 'purchedItem');
                                            //$fetchdatap = $this->pitems_model->viewRecordAnyR($slugfiet);
                                            foreach ($fetchdatap as $ftp):
                                                ?>
                                                <?php $totaltbitem += $ftp->pitem_unit * $ftp->pitem_rate; ?>

                                                <tr class="deleteInvoicetrV" style="background-color:#da2626;color: #ffffff">
                                                    <td><?= $i++; ?></td>
                                                    <td><?= $ft->items_date; ?></td>
                                                    <td>Cr</td>
                                                    <td><b><i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($ftp->amount); ?></b></td>
                                                    <td>Items</td>
                                                    <td></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <?php
                                        $slugfietcr = array(
                                            'purchedItem' => $ft->items_id,
                                            'strans_type' => 2
                                        );
                                        $fetchdatat = $this->shoptrans_model->viewRecordAnyR($slugfietcr);
                                        foreach ($fetchdatat as $ftt):
                                            ?>
                                            <tr class="deleteInvoicetrV<?= $pO->invoice_id; ?>" style="background-color:#da2626;color: #ffffff">
                                                <td><?= $i++; ?></td>
                                                <td><?= $ft->items_date; ?></td>
                                                <td>Cr</td>
                                                <td><b><i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($ftt->strans_amount); ?></b></td>
                                                <td><?= $ftt->strans_desc; ?></td>
                                                <td></td>

                                            </tr>
                                        <?php endforeach; ?>
                                        
                                        <?php
                                        $slugfietdr = array(
                                            'purchedItem' => $ft->items_id,
                                            'strans_type' => 1
                                        );
                                        $fetchdatat = $this->shoptrans_model->viewRecordAnyR($slugfietdr);
                                        foreach ($fetchdatat as $ftt):
                                            ?>
                                            <tr class="deleteInvoicetrV<?= $pO->invoice_id; ?>" style="background-color:#00a65a;color: #ffffff">
                                                <td><?= $i++; ?></td>
                                                <td><?= $ft->items_date; ?></td>
                                                <td>Dr</td>
                                                <td><b><i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($ftt->strans_amount); ?></b></td>
                                                <td><?= $ftt->strans_desc; ?></td>
                                                <td></td>

                                            </tr>
                                        <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


