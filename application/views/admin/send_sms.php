<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>expense/lists">View List</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('expense_uploaded')) : ?>
            <?php echo $this->session->flashdata('expense_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-body">
                <div class="form-horizontal"> 
                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
											<?php 
											$flash_data =  $this->session->flashdata('expense_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Expense has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('expense_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                           <form action="<?= ADMINC . 'sms/addsendsms'?>" id="frmExpense" name="frmExpense" method="POST" />
											 
											 
												<div class="row">
											 
                                                <div class="col-md-3"> 
												
                                                            <label>Client Name</label>
                                                            <select id="client_name" name="client_name" class="form-control" required>
															<option value="">Select Type</option>
																<?php 
																$array_types = array(
																	'view_status' => '1'
																);
																$typequery = $this->consignee_model->viewRecordAnyR($array_types);
																if(count($typequery) > 0):
																foreach($typequery as $type): 
																?>
																<option value="<?= $type->consig_phone; ?>"><?= $type->consig_name; ?></option>
																<?php endforeach;?>
																<?php endif;?>
															</select>
                                                
                                                </div> 
												 <div class="col-md-3">
                                                        
                                                            <label>Mobile</label>
                                                            <input type="text" id="sms_mobile" name="sms_mobile" value="" class="form-control" placeholder="" required>  
                                                </div>  
												<div class="col-md-12">
                                                            <label>Message</label> 
                                                            <textarea id="sms_message" name="sms_message" class="form-control" rows="2"></textarea>
														 
                                                </div>
												</div>
												<div class="row">
												<div class="col-md-3 col-md-offset-5">  
												 <?php
                        $checkm = checkpermission('189');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
							 <button type="submit" id="btnAddExpense" name="btnAddExpense" class="btn btn-primary btn-lg"><?= !empty($expensed->slug) ? 'Update' : 'Submit'; ?></button> 
                            <a href="<?= ADMINC; ?>sms/sentsms" title="Back to details" class="btn btn-info btn-lg" ><i class="fa fa-reply"></i> Cancel </a>
                        <?php endif; ?>
						</div>
												 
                                                </div>
												
												
                                             
											</form> 
														
												 
                                                </div>
												
												
												
                                            </div>
											 
                    
                    </div>   
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->

