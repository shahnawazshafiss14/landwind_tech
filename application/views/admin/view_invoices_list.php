<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Invoices List</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <!--th>Date of Invoice</th-->
                                    <th>Project Name</th>
                                    <th>Contact Number</th>
                                    <th>Consignee</th>
                                    <th>Date of Supply</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($invoiceObjects as $po) :
                                    ?>
                                    <tr class="deleteInvoicetrV<?= $po->id; ?>">
                                        <td><?= $i++; ?></td> 
                                        <td>
                                        <?php 
                                            echo $po->proj_title;
                                            ?>
                                        </td> 
                                        <td>
                                        <?php 
                                        $fetad = array(
                                            'consig_id' => $po->proj_client
                                        );
                                        $fetchany = $this->consignee_model->viewRecordAny($fetad);
                                        echo $fetchany->consig_contact_person;
                                        ?>
                                        </td>
                                        <td><?php 
                                            echo $fetchany->consig_name . ', ' . $fetchany->consig_address;
                                            ?></td> 
                                        <td><?= dateMonth($po->proj_dos); ?></td>
                                        <td>
                                            <?php
                                            $checkm = checkpermission('172');
                                            ?> 
                                            <?php if ($checkm->is_edited == '1'): ?>
                                                <a title="Edit Invoice Details" class="btn btn-warning btn-xs" href="<?= ADMINC; ?>project/proj_show_list/<?= $po->proj_slug; ?>"><i class="fa fa-edit"></i></a> |
                                            <?php endif; ?>
                                            <?php if ($checkm->is_viewed == '1'): ?>
                                                <a title="View Invoice Details" class="btn btn-primary btn-xs" href="<?= ADMINC; ?>invoices/invoice_show_list/<?= $po->proj_slug; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <p></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
