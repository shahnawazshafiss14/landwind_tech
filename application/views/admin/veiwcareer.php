<!-- Content Wrapper. Contains career content -->
<div class="content-wrapper">
    <!-- Content Header (Career header) -->
    <section class="content-header">
	<h1>All Careers List<small>Manage View Career</small></h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Career</a></li>
	    <li class="active">Career List</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>Number</th>
				    <th>Career Type</th>
				    <th>Career Title</th>
				    <th>Status</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($careerObject as $pO) :
				    ?>
    				<tr class="deletetrcareer<?= $pO->career_id; ?>">
    				    <td><?= $i++; ?></td>
					<?php
					$arrcareetp = array(
					    '1' => 'Accounting',
					    '2' => 'Event Assistant',
					    '3' => 'Dev Ops Engineer',
					    '4' => 'Engineering Manager',
					    '5' => 'Marketing Manager',
					    '6' => 'Product Specialists'
					);
					?>
    				    <td><?= $arrcareetp[$pO->career_type]; ?></td>
    				    <td><?= $pO->career_title; ?></td>
    				    <td><?= $pO->status; ?></td>
    				    <td>
    					<a href="javascript:;" class="deleteRcordCareer" data-id="<?= $pO->career_id; ?>">
    					    <i class="fa fa-trash"></i></a> | <a href="<?= ADMINC; ?>career/index/<?= $pO->career_id; ?>" id=""><i class="fa fa-edit"></i></a></td>
    				</tr>
				<?php endforeach; ?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
