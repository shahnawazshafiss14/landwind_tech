<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($categoryObject->category_id) ? 'Edit' : 'Add' ?> Category</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= ADMINC; ?>category/topCategory">Category</a></li>
	    <li class="active"><?= !empty($categoryObject->category_id) ? 'Edit' : 'Add' ?> Category</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($categoryObject->category_id) ? 'Edit' : 'Add' ?> Category</h3>
		<div class="box-tools pull-right">
		    <form  id="categoryForm" name="categoryForm" action=""  method="POST" enctype="multipart/form-data">
			<button type="button" name="btnCategory" id="btnCategory" class="btn btn-lg btn-success">
			    <i class="fa fa-save"></i> <?= !empty($categoryObject->category_id) ? 'Update' : 'Save' ?> </button>
			<?php if (!empty($categoryObject->category_id)) : ?>
    			<a href="<?= ADMINC; ?>category/topCategory" title="Collapse" class="btn btn-info btn-lg">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-lg" >
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>
		</div>
	    </div>
	    <div class="box-body">
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <!--div class="form-group">
				<label class="control-label col-sm-2" for="selectMenu">Select Menu</label>
				<div class="col-sm-10">
				    <select name="selectMenu" id="selectMenu" class="form-control">
					<option value="0">Select Menu</option>
			    <?php
			    $mdata = array(
				'menu_parent_id' => '0'
			    );
			    $menus = $this->menu_model->viewRecordAnyR($mdata);
			    foreach ($menus as $me) :
				?>
    					<option value="<?= $me->menu_id ?>"><?= $me->menu_name ?></option>
			    <?php endforeach; ?>
				    </select>
				</div>
			    </div-->
			    <div class="form-group">
				<label class="control-label col-sm-2" for="categoryName">Category Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="editName" id="editName" type="hidden" value="<?= !empty($categoryObject->category_id) ? $categoryObject->category_id : ''; ?>">
				    <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Enter Category Name" value="<?= !empty($categoryObject->category_name) ? $categoryObject->category_name : '' ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="cateTitle">Category Title <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">

				    <input type="text" class="form-control strnum" id="cateTitle" name="cateTitle" placeholder="Enter Title Name" value="<?= !empty($categoryObject->category_title) ? $categoryObject->category_title : '' ?>">
				</div>
			    </div>
			    <?php
			    $get_idu = $this->uri->segment(4);
			    if (!empty($get_idu)):
				?>
    			    <div class="form-group">
    				<label class="control-label col-sm-2" for="UpdateImage">Update Image</label>
    				<div class="col-sm-10">
    				    <div class="col-sm-5">
    					<img src="<?= BASEURLFRONTASSET; ?><?= !empty($categoryObject->cateimage) ? 'banner/' . $categoryObject->cateimage : 'demo.jpg' ?>" alt="" width="300" height="250">
    				    </div>
    				    <div class="col-sm-5">
    					<input class="form-control" type="file" id="cateimagechange" name="cateimagechange">
    					<input type="button" class="btn btn-primary" name="uploadimagechange" id="uploadimagechange" value="Upload">

    				    </div>

    				</div>
    			    </div>
				<?php
			    endif;
			    ?>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="selectCategory">Select Parent Category</label>
				<div style="height: 300px; overflow: auto;">
				    <?php
				    echo $categories;
				    ?>
				</div>
			    </div>

			</div>
		    </div>
		</div>

		</form>
	    </div>

	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
