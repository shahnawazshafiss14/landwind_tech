<style>
    .thcla{
        text-align: center
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section> 

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <!-- small box -->

            </div>
            <!-- ./col -->

            <!-- ./col -->
        </div>
        <div class="row">
            <?php
            if ($this->session->userdata('admin_type') != '2'):
                ?>
                
                <div class="col-md-12">
                    <div class="table-responsive">
                        <label class="btn btn-primary">Invoice Report</label>
                        <table class="table table-striped table-bordered"> 
                            <tbody>
                                <?php
                                $cararry = array(
                                    'parent_particular' => '0'
                                );
                                $category = $this->itemtype_model->viewRecordAnyR($cararry);
                                foreach ($category as $cat) :
                                    $abcd = $this->pitems_model->taotal($cat->item_id);
                                    $invoices = $this->invoice_model->taotalpartiparent($cat->item_id);
                                    $finalqty = ($abcd - $invoices);
                                    $today = date('Y-m-d');
                                    $yesterday = date('Y-m-d', strtotime("-1 days"));
                                    $date3 = date('Y-m-d', strtotime("-2 days"));
                                    $date4 = date('Y-m-d', strtotime("-3 days"));
                                    $date5 = date('Y-m-d', strtotime("-4 days"));
                                    $date6 = date('Y-m-d', strtotime("-5 days"));
                                    $date7 = date('Y-m-d', strtotime("-6 days"));
                                    ?>
                                    <tr>
                                        <th><?= $cat->item_particular_name; ?></th>
                                        <td align="center"><?php
                                            $todaycountc = array(
                                                'invoice_dos' => $today,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetchc = $this->invoice_model->datewisereportcategory($todaycountc);
                                            echo count($todaycount_fetchc);
                                            ?></td>
                                        <td align="center"><?php
                                            $todaycountyc = array(
                                                'invoice_dos' => $yesterday,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetchyc = $this->invoice_model->datewisereportcategory($todaycountyc);
                                            echo count($todaycount_fetchyc);
                                            ?></td>
                                        <td align="center"><?php
                                            $todaycount3c = array(
                                                'invoice_dos' => $date3,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetch3c = $this->invoice_model->datewisereportcategory($todaycount3c);

                                            echo count($todaycount_fetch3c);
                                            ?></td>
                                        <td align="center"><?php
                                            $todaycount4c = array(
                                                'invoice_dos' => $date4,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetch4c = $this->invoice_model->datewisereportcategory($todaycount4c);
                                            echo count($todaycount_fetch4c);
                                            ?></td>
                                        <td align="center"><?php
                                            $todaycount5c = array(
                                                'invoice_dos' => $date5,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetch5c = $this->invoice_model->datewisereportcategory($todaycount5c);
                                            echo count($todaycount_fetch5c);
                                            ?></td>
                                        <td align="center"><?php
                                            $todaycount6c = array(
                                                'invoice_dos' => $date6,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetch6c = $this->invoice_model->datewisereportcategory($todaycount6c);
                                            echo count($todaycount_fetch6c);
                                            ?></td>
                                        <td align="center"><?php
                                            $todaycount7c = array(
                                                'invoice_dos' => $date7,
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetch7c = $this->invoice_model->datewisereportcategory($todaycount7c);
                                            echo count($todaycount_fetch7c);
                                            ?></td>
                                        <td align="center"><?php
                                            $weekcategory = array(
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetchweekc = $this->invoice_model->datewisereportweekcat($weekcategory);
                                            echo count($todaycount_fetchweekc);
                                            ?></td>
                                        <td align="center"><?php
                                            $monthcategory = array(
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetchmonthc = $this->invoice_model->datewisereportmonthcat($monthcategory);
                                            echo count($todaycount_fetchmonthc);
                                            ?></td>
                                        <td style="text-align: right"> <?php
                                            $monthcategory = array(
                                                'itemInvoice_name_p' => $cat->item_id
                                            );
                                            $todaycount_fetchmonthc = $this->invoice_model->datewisereportmonthlastc($monthcategory);
                                            echo count($todaycount_fetchmonthc);
                                            ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr class="danger">
                                    <th>Total</th>
                                    <td align="center"><?php
                                        $todaycount = array(
                                            'invoice_dos' => $today
                                        );
                                        $todaycount_fetch = $this->invoice_model->datewisereport($todaycount);
                                        echo count($todaycount_fetch);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycounty = array(
                                            'invoice_dos' => $yesterday
                                        );
                                        $todaycount_fetchy = $this->invoice_model->datewisereport($todaycounty);
                                        echo count($todaycount_fetchy);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount3 = array(
                                            'invoice_dos' => $date3
                                        );
                                        $todaycount_fetch3 = $this->invoice_model->datewisereport($todaycount3);
                                        echo count($todaycount_fetch3);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount4 = array(
                                            'invoice_dos' => $date4
                                        );
                                        $todaycount_fetch4 = $this->invoice_model->datewisereport($todaycount4);
                                        echo count($todaycount_fetch4);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount5 = array(
                                            'invoice_dos' => $date5
                                        );
                                        $todaycount_fetch5 = $this->invoice_model->datewisereport($todaycount5);
                                        echo count($todaycount_fetch5);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount6 = array(
                                            'invoice_dos' => $date6
                                        );
                                        $todaycount_fetch6 = $this->invoice_model->datewisereport($todaycount6);
                                        echo count($todaycount_fetch6);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount7 = array(
                                            'invoice_dos' => $date7
                                        );
                                        $todaycount_fetch7 = $this->invoice_model->datewisereport($todaycount7);
                                        echo count($todaycount_fetch7);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount_fetchweek = $this->invoice_model->datewisereportweek();
                                        echo count($todaycount_fetchweek);
                                        ?></td>
                                    <td align="center"><?php
                                        $todaycount_fetchmonth = $this->invoice_model->datewisereportmonth();
                                        echo count($todaycount_fetchmonth);
                                        ?></td>
                                    <td style="text-align: right"><?php
                                        $todaycount_fetchmonthl = $this->invoice_model->datewisereportmonthlast();
                                        echo count($todaycount_fetchmonthl);
                                        ?></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th class="thcla">Today</th>
                                    <th class="thcla">Yesterday</th>
                                    <th class="thcla"><?= dateMonth($date3); ?></th>
                                    <th class="thcla"><?= dateMonth($date4); ?></th>
                                    <th class="thcla"><?= dateMonth($date5); ?></th>
                                    <th class="thcla"><?= dateMonth($date6); ?></th>
                                    <th class="thcla"><?= dateMonth($date7); ?></th>
                                    <th class="thcla">Week</th>
                                    <th class="thcla">Month</th> 
                                    <th style="text-align: right">Last Month</th> 
                                </tr>
                            </tfoot>
                        </table>
                    </div> 

                </div>
                <?php
            endif;
            ?>
        </div>
</div>
<!-- /.row -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
