<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Particulars Wise Total Count</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php
                $url = $this->uri->segment(4);
                $uel = $this->encrypt->decode($_GET['geval']);
                $cararry = array(
                    'pitem_parent_particular' => $uel
                );
                $category = $this->pitems_model->viewRecordPageRGs($cararry, 'pitem_particular');
                ?>
                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">

                            </div>

                        </div>
                        <?php if (count($category) > 0) : ?>
                        
                        
                            <div class="row" style="width: 1200px; height: 400px;">
                                <div class="col-md-12">
                                    <div id="barchart_values" ></div>
                                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                                    <script type="text/javascript">
                                        google.charts.load("current", {packages: ["corechart"]});
                                        google.charts.setOnLoadCallback(drawChart);
                                        function drawChart() {
                                            var data = google.visualization.arrayToDataTable([
                                                ["Element", "Total Unit", {role: "style"}],
    <?php
    foreach ($category as $cat) :
        $abcd = $this->pitems_model->subtaotal($cat->pitem_particular);
     $invoices = $this->invoice_model->subtaotal($cat->pitem_particular);
     $finalqty = $abcd  - $invoices;
        ?>
                                                    ["<?= $this->itemtype_model->itemId($cat->pitem_particular, 'item_particular_name'); ?>", <?= $finalqty; ?>, "#1a76c5"],
    <?php endforeach; ?>

                                            ]);

                                            var view = new google.visualization.DataView(data);
                                            view.setColumns([0, 1,
                                                {calc: "stringify",
                                                    sourceColumn: 1,
                                                    type: "string",
                                                    role: "annotation"},
                                                2]);

                                            var options = {
                                                title: "Total Unit in , <?= $this->itemtype_model->itemId($cat->pitem_particular, 'item_unit'); ?>",
                                                width: 900,
                                                height: 400,
                                                bar: {groupWidth: "95%"},
                                                legend: {position: "none"},
                                            };
                                            var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
                                            chart.draw(view, options);
                                        }
                                    </script>

                                </div>
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>There is no Records</p>
                                </div>

                            </div>
                        <?php endif; ?>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <p></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
