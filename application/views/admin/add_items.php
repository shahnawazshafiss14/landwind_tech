<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>items/itemsList">Items</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('items_uploaded')) : ?>
	    <?php echo $this->session->flashdata('items_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
		    <form  id="itemsForm" name="itemsForm" action="<?= ADMINC; ?>items/addItems/"  method="get">
			
                </div>
            </div>
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="col-md-4">
				
			    </div>
			    <div class="col-md-8">
			    <div class="form-group">
				<input type="hidden" name="shop" id="dor_id" value="<?= !empty($itemsObject->items_receiver) ? $itemsObject->items_receiver : ''; ?>" />
				<div class="col-md-12">
				    Select Shop <b style="color:#f00;">*</b><br/>
				    <select class="selectpicker1" data-live-search="true" id="dor_receiver_id" style="width:100%;">
					<?php
					$fetarcsi = array(
					    'shop_id' => $itemsObject->items_shop_id,
					    'view_status' => '1',
					);
					$fetdasingle = $this->shop_model->viewRecordAny($fetarcsi);
					if (!empty($itemsObject->items_shop_id)):
					    ?>
    					<option value="<?= $itemsObject->items_shop_id; ?>" selected><?= $fetdasingle->shop_name . ', ' . $fetdasingle->shop_phone; ?></option>
					<?php endif; ?>
					<option value="">Select Shop Name</option>
					<option value="0">NO</option>
					<?php
					$fetarc = array(
					    'view_status' => '1'
					);
					$fetda = $this->shop_model->viewRecordAnyR($fetarc);
					foreach ($fetda as $fevenue):
					    ?>
    					<option data-tokens="<?= $fevenue->shop_name; ?>, <?= $fevenue->shop_phone; ?>" value="<?= $fevenue->shop_id; ?>"><?= $fevenue->shop_name; ?>, <?= $fevenue->shop_phone; ?></option>
					<?php endforeach; ?>
				    </select>
				    <a class="btn btn-primary" href="<?= ADMINC; ?>shop/edit" title="Add Shop"><i class="fa fa-plus"></i></a>
				    <br/>
				    <br/>
				</div>
				<div class="col-md-4">
				    Date of Supply <b style="color:#f00;">*</b><br/>
				    <input type="text" class="form-control" name="items_dos" id="items_dos" value="<?= !empty($itemsObject->items_dos) ? $itemsObject->items_dos : date('Y-m-d'); ?>">
				</div>
                                <div class="col-md-4">
				    Vehicle Number <b style="color:#f00;">*</b><br/>
				    <input type="text" class="form-control" name="items_vehicle" id="items_vehicle" value="<?= !empty($itemsObject->items_vehicle_id) ? $itemsObject->items_vehicle_id : ''; ?>">
				</div>
			    </div>
			    <button type="submit" class="btn btn-success btn-lg" id="<?= !empty($itemsObject->items_id) ? 'btnItems' : 'btnItems'; ?>">
			    <?= !empty($itemsObject->items_id) ? 'Continue' : 'Continue'; ?> <i class="fa fa-angle-double-right"></i></button>
			    </div>
			</div>
		    </div>
		    </form>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
		</div>
		<!-- /.box-footer-->
	    </div>

	    <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->

