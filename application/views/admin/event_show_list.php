<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
$getval = $this->uri->segment(4);
if (!empty($getval)) {
    $adf = $this->eventextracon_model->viewRecordId($getval);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);

    $artifetch = array(
	'artist_email' => $eventObject->artist_id
    );
    $arti = $this->artists_model->viewRecordAny($artifetch);
    $venues = $this->venue_model->viewRecordId($eventObject->venue_id);
    ?>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('event_uploaded')) : ?>
	    <?php echo $this->session->flashdata('event_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
		<input type="hidden" name="eventid" id="eventid" value="<?= !empty($eventObject->event_id) ? $eventObject->event_id : ''; ?>" />
		<input type="hidden" name="eextraid" id="eextraid" value="<?= !empty($getval) ? $getval : ''; ?>" />
                <div class="box-tools pull-right">

		    <a href="<?= ADMINC; ?>events/addEvent/<?= !empty($eventObject->event_id) ? $eventObject->event_id : ''; ?>" class="eaddmorecontent btn btn-info btn-lg"><i class="fa fa-pencil"></i> Edit</a>
		</div>
	    </div>
	    <div class="box-body">

		<div class="row">
		    <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">


			<div class="col-sm-9">
			    <b>Event Image</b>
			    <input type="file" id="eventImage" name="eventImage" />
			</div>
			<div class="col-sm-3">
			    <input type="button" name="uploadeventImage" id="uploadeventImage" value="Upload" class="btn btn-primary" />
			</div>

			<img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
			     src="<?= BASEURLFRONTASSET; ?>event/<?= $eventObject->event_image; ?>" class="img" />

		    </div>
		    <div class="col-sm-6">
			<b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px; display:block">Event Details
			    <?php
			    if (!empty($eventObject->event_status) == 1):
				?>
    			    <a class="btn btn-success">Approved</a>
			    <?php else: ?>
    			    <a href="javascript:;" id="eventapproved" class="btn btn-danger">Request</a>
			    <?php endif; ?>

			</b>
			<div class="row detailsprodod">
			    <p><span class="col-sm-5"><b>Event Price</b><b class="pull-right">:</b></span>  <span class="col-sm-7">
				    <span style="display:none">
					<input type="number" class="form-control txtprice price" name="txtprice" id="txtprice" value="<?= !empty($eventObject->event_price) ? $eventObject->event_price : ''; ?>" data-pid="<?= $eventObject->event_id; ?>" />
					<button class="btnupdateeventPrice" type="button" data-eventid="<?= $eventObject->event_id; ?>">Update</button></span>

				    <a href="javascript:;" class="updatepriceevent"><span style="font-size: 30px;"><?= !empty($eventObject->event_price) ? '$' . $eventObject->event_price : '$00.00'; ?></span></a>




				</span></p><br/>
			    <p><span class="col-sm-5"><b>Event Commission Price</b><b class="pull-right">:</b></span>  <span class="col-sm-7">
				    <span style="display:none">
					<input type="number" class="form-control txteventcomm price" name="txteventcomm" id="txteventcomm" value="<?= !empty($eventObject->event_commision) ? $eventObject->event_commision : ''; ?>" data-pid="<?= $eventObject->event_id; ?>" />
					<button class="btnupdateeventcommPrice" type="button" data-eventid="<?= $eventObject->event_id; ?>">Update</button></span>

				    <a href="javascript:;" class="updatepriceeventcomm"><span style="font-size: 30px;"><?= !empty($eventObject->event_commision) ? $eventObject->event_commision . '%' : '0%'; ?></span></a>

				</span></p><br/>
			    <p><span class="col-sm-5"><b>Set Event Discount Price 1</b><b class="pull-right">:</b></span>  <span class="col-sm-7">
				    <span style="display:none">
					<input type="number" class="form-control disprice1 price" name="disprice1" id="disprice1" value="<?= !empty($eventObject->event_discount1) ? $eventObject->event_discount1 : ''; ?>" data-pid="<?= $eventObject->event_id; ?>" />
					<button class="btnupdateeventdisprice1" type="button" data-eventid="<?= $eventObject->event_id; ?>">Update</button></span>
				    <a href="javascript:;" class="updatedispriceevent"><span style="font-size: 30px;"><?= !empty($eventObject->event_discount1) ? $eventObject->event_discount1 . '%' : '0%'; ?></span></a>
				</span></p><br/>
			    <p><span class="col-sm-5"><b>Set Event Discount Price More</b><b class="pull-right">:</b></span>  <span class="col-sm-7">
				    <span style="display:none">
					<input type="number" class="form-control disprice2 price" name="disprice2" id="disprice2" value="<?= !empty($eventObject->event_discount2) ? $eventObject->event_discount2 : ''; ?>" data-pid="<?= $eventObject->event_id; ?>" />
					<button class="btnupdateeventdisprice2" type="button" data-eventid="<?= $eventObject->event_id; ?>">Update</button></span>
				    <a href="javascript:;" class="updatedispriceevent2"><span style="font-size: 30px;"><?= !empty($eventObject->event_discount2) ? $eventObject->event_discount2 . '%' : '0%'; ?></span></a>
				</span></p><br/>
			    <p><span class="col-sm-5"><b>Event Slug</b><b class="pull-right">:</b></span>  <span class="col-sm-7">
				    <span style="display:none" >
					<input type="text" class="form-control txtprice" name="txtslug" id="txtslug" value="<?= !empty($eventObject->event_slug) ? $eventObject->event_slug : ''; ?>" data-pid="<?= $eventObject->event_id; ?>" />
					<button class="btnupdateeventSlug" type="button" data-eventid="<?= $eventObject->event_id; ?>">Update</button></span>

				    <a href="javascript:;" class="updateslugevent"><span style="font-size: 30px;"><?= !empty($eventObject->event_slug) ? $eventObject->event_slug : ''; ?></span></a>




				</span></p> <span id="notifcationslug"></span><br/>
			    <p><span class="col-sm-5"><b>Event Title</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $eventObject->event_title; ?></span></p><br/>


			    <p><span class="col-sm-5"><b>Event Parent Category</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $this->category_model->categorybyId($eventObject->category_parent_id); ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Event Category</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $this->category_model->categorybyId($eventObject->category_id); ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Event Price</b><b class="pull-right">:</b></span>  <span class="col-sm-7">$<?= $eventObject->event_price; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Event Tickets</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $eventObject->event_ticket; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Event Date</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $eventObject->event_start_date; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Event Start Time</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $eventObject->event_time; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Event End Time</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $eventObject->event_end_date; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Artist Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?php
				    echo $arti->artist_name . ' ' . $arti->artist_lname;
				    ?> <a class="btn btn-info" href="<?= ADMINC; ?>artists/artist_show_list/<?= $arti->artist_id; ?>">Artists Details</a></span></p><br/>
			    <p><span class="col-sm-5"><b>Venue Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?php
				    echo $venues->venue_name;
				    ?> <a class="btn btn-info" href="<?= ADMINC; ?>venues/venue_show_list/<?= $venues->venue_id; ?>">Venue Details</a></span></p><br/>

			    <p><span class="col-sm-5"><b>Event Added Date</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $eventObject->event_added_date; ?></span></p><br/>
			</div>
		    </div>
		</div>
		<div class="row">
		    <div class="col-md-12">
			<div class="input-group">
			    <b>Description: </b> <?= $eventObject->event_desc; ?>
			</div>
		    </div>
		</div>





	    </div>
	    <div class=""></div>
	    <!-- box-body -->


	</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


