<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
$checkm = checkpermission('169');
?>
<style>
    .input-group {
        margin-bottom: 12px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('shop_uploaded')) : ?>
            <?php echo $this->session->flashdata('shop_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="shopid" id="shopid" value="<?= !empty($shopObject->shop_id) ? $shopObject->shop_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <a href="<?= ADMINC; ?>shop/edit/<?= !empty($shopObject->shop_slug) ? $shopObject->shop_slug : ''; ?>" class="eaddmorecontent btn btn-info btn-lg">Edit <i class="fa fa-pencil"></i></a> <button title="Refresh" class="btn btn-success" id="btnitemrefere"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div id="response">
                        <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">
                            <div class="table-responsive">          
                                <table class="table"> 
                                    <tbody>
                                        <tr style="background-color: #b3b2ff;"> 
                                            <th><b>Shop Details</b></th> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop Name</b>: <?= $shopObject->shop_name; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop Address</b>: <?= $shopObject->shop_address; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop Email</b>: <?= $shopObject->shop_email; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop Aadhaar</b>: <?= $shopObject->shop_aadhar; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop Phone</b>: <?= $shopObject->shop_phone; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop State</b>: <?= $shopObject->shop_state; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop City</b>: <?= $shopObject->shop_city; ?></td> 
                                        </tr>
                                        <tr>
                                            <td><b>Shop Zipcode</b>: <?= !empty($shopObject->shop_zipcode) ? $shopObject->shop_zipcode : ''; ?></td> 
                                        </tr> 
                                        <tr>
                                            <td><b>Description</b>: <?= $shopObject->shop_description; ?></td> 
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                            <!--div class="col-sm-9">
                                <b>Consignee Image</b>
                                <input type="file" id="update_consig_img" name="update_consig_img" />
                            </div>
                            <div class="col-sm-3">
                                <input type="button" name="updatebtn_consig_img" id="updatebtn_consig_img" value="Upload" class="btn btn-primary" />
                            </div>
                            <img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
                                 src="<?= !empty($shopObject->consig_image) ? BASEURLFRONTASSET . 'consignee/' . $shopObject->consig_image : BASEURLFRONTASSET . 'demo.jpg'; ?>" class="img" /-->
                        </div>
                    </div>
                    <?php if (($checkm->is_viewed == '1') && ($this->session->userdata('admin_type') == '1')): ?>
                        <div class="col-sm-3" style="border-right: 2px dotted #ded2d2;">
                            <div class="table-responsive">          
                                <table class="table"> 
                                    <tbody>
                                        <tr style="background-color: #ffed6d;">
                                            <th><b>Last Five Billing Details</b></th> 
                                        </tr>
                                        <?php
                                        $array_last = array(
                                            'items_shop_id' => $shopObject->shop_id
                                        );
                                        $fetch_objec = $this->items_model->viewRecordAnyRLf($array_last);
                                        foreach ($fetch_objec as $invoic):
                                            $array_pitem_in = array(
                                                'purchedItem' => $invoic->items_id
                                            );
                                            $taotal = 0;
                                            $total_unit = 0;
                                            $itemunit = '';
                                            $fetch_objec_item = $this->pitems_model->viewRecordAnyR($array_pitem_in);
                                            foreach ($fetch_objec_item as $invoic_item) {
                                                $total_unit += $invoic_item->pitem_unit;
                                                $taotal += $invoic_item->pitem_unit * $invoic_item->pitem_rate;
                                                $fetchParticlearray = array(
                                                    'item_id' => $invoic_item->pitem_particular
                                                );
                                                $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                                $itemunit = $fetchParticle->item_unit;
                                            }
                                            ?>
                                            <?php
                                          

                                            $aftegcalcgst = !empty($taotal) ? $taotal : '0.00';
                                            $grandtoatal1 = $aftegcalcgst;
                                            ?>
                                            <tr>
                                                <td><b>Entry Date</b>: <?= dateMonthAP($invoic->created_on); ?><br/><b>Billing Date</b>: <?= dateMonth($invoic->items_date); ?><br/><b>Unit</b>: <?= $total_unit . '/' . $itemunit; ?><br/><b>Amount</b>: <?= $grandtoatal1; ?></td> 
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>

                                    </tbody>
                                </table>
                            </div> 

                        </div>
                        <div class="col-sm-3">
                            <div class="table-responsive">          
                                <table class="table"> 
                                    <tbody>
                                        <tr style="background-color: aquamarine;"> 
                                            <th><b>Last Five Payment Details</b></th> 
                                        </tr>
                                        <?php
                                        $array_last_paymet = array(
                                            'items_shop_id' => $shopObject->shop_id
                                        );
                                        $fetch_objec_payment = $this->items_model->viewRecordAnyRLf($array_last_paymet);
                                        foreach ($fetch_objec_payment as $payment):
                                            $array_pitem_in = array(
                                                'purchedItem' => $payment->items_id,
                                                'strans_type' => '2'
                                            );
                                            $taotal_payment = 0;
                                            $fetch_objec_item = $this->shoptrans_model->viewRecordAnyR($array_pitem_in);
                                            foreach ($fetch_objec_item as $invoic_item) {
                                                $taotal_payment += $invoic_item->strans_amount;
                                            }
                                            ?>

                                            <tr>
                                                <td><b>Entry Date</b>: <?= dateMonthAP($payment->created_on); ?><br/><b>Item Date</b>: <?= dateMonth($payment->items_date); ?><br/><b>Amount</b>: <?= $taotal_payment; ?></td> 
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>

                                    </tbody>
                                </table>
                            </div> 

                        </div>
                    <?php endif; ?>
                </div>


                <div class="row">
                    <div class="col-md-12">

                        <div style="margin-top: 50px;margin-bottom: 400px;z-index: 9999999">
                            <form name="frmgenratebill" id="frmgenratebill" action="<?= ADMINC; ?>shop/shop_month/<?= $shopObject->shop_slug; ?>" method="get">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                    <th>From</th>
                                    <th>To</th>	
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    <td><input type="text" id="from" name="from" class="form-control" value="<?= !empty($month_to) ? $month_to : ''; ?>"></td>
                                    <td><input type="text" id="to" name="to" class="form-control" value="<?= !empty($month_to) ? $month_from : ''; ?>"></td>
                                    <td><input type="submit" class="btn btn-success"></td>
                                    </tbody>
                                </table>
                            </form>
                        </div>



                    </div>
                </div>
            </div>

        </div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>


</div>
<!-- .content-wrapper -->


