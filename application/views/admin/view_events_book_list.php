<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Events Booked</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
			<div class="row">
			    <div class="col-md-12">
				<div class="event-dp">
				    <?php
				    $gepid = array(
					'event_id' => $this->uri->segment(4)
				    );
				    $eventObjects = $this->event_model->viewRecordAny($gepid);
				    ?>
				    <a href="<?= base_url(); ?>events/<?= $eventObjects->event_slug; ?>" target="_blank"><img src="<?= BASEURLFRONTASSET; ?>event/<?= $eventObjects->event_image; ?>" alt="" width="250" height="200"></a>
				</div>
				<div class="event-mini-detail">
				    <div class="name-event">Total Tickets:
					<?php
					$argbo = array(
					    'buy_product_id' => $eventObjects->event_id,
					    'buyStatus' => '1'
					);
					$datie = $this->buy_model->viewRecordRsum($argbo);
					echo $datie->qty;
					?>
				    </div>
				    <div class="title-event"> Title: <?= $eventObjects->event_title; ?></div>
				    <div class="time-event"> Event Date: <?= $eventObjects->event_start_date; ?></div>
				    <div class="time-event"> Event Start Time: <?= $eventObjects->event_time; ?></div>
				    <div class="venue-event"> Event End Time: <?= $eventObjects->event_end_date; ?></div>
				    <div class="price">
					Total Price: $<?= $this->buy_model->viewRecordRsumqtypri($argbo); ?><br/>
				    </div>
				</div>
			    </div>
			    <div class="col-md-4">

				<p></p>
			    </div>
			</div>
		    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Transaction id</th>
                                    <th>Buyer Name</th>
                                    <th>Booked Date</th>
				    <th>Booked Tickets</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				$uno = 1;
				;

				foreach ($buyObjects as $pO) :
				    ?>
    				<tr class="deleteEventtrE<?= $pO->buy_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td>
					    <?= $pO->buyTransationId; ?>
    				    </td>
    				    <td>

					    <?php
					    $fetcustomer = array(
						'customerEmail' => $pO->buy_userName
					    );
					    $fetextcustomer = $this->customer_model->viewRecordAny($fetcustomer);
					    ?>
    					<table class="table" style="margin-bottom:0">
    					    <tr>
    						<td><?= $uno++; ?></td>
    						<td><?= $fetextcustomer->customerName . ' ' . $fetextcustomer->customerlastName ?></td>
    						<td><?= $fetextcustomer->customerEmail ?></td>
    					    </tr>
    					    <table class="table">
						    <?php
						    $fetcustom = array(
							'buy_id' => $pO->buy_id
						    );
						    $fetextcon = $this->buyextra_model->viewRecordAnyR($fetcustom);
						    foreach ($fetextcon as $feop):
							?>
							<tr>
							    <td><?= $uno++ ?></td>
							    <td><?= $feop->buyEcon_fname . ' ' . $feop->buyEcon_lname ?></td>
							    <td><?= $feop->buyEcon_email ?></td>
							</tr>

						    <?php endforeach; ?>
    					    </table>
    				    </td>
    				    <td><?= $pO->buyDate; ?></td>
    				    <td><?= $pO->buy_qty; ?></td>
    				</tr>
				<?php endforeach; ?>
				</tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
