<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Carts List</h1>
	<a href="<?= ADMINC ?>product/productList">
	    <div class="cart-view">
		<i class="fa fa-product-hunt" aria-hidden="true"></i> product <span></span>
	    </div>
	</a>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
			<?php
			if (!empty($cartObjects)):
			    ?>
    			<table class="table table-hover table-bordered table-striped">
    			    <thead>
    				<tr>
    				    <th>S.No.</th>
    				    <th>Images</th>
    				    <th>Product Name</th>
    				    <th>Model</th>
    				    <th>Color</th>
    				    <th>Price</th>
    				    <th>Quantity</th>
    				    <th>Sub Total</th>
    				    <th>Action</th>
    				</tr>
    			    </thead>
    			    <tbody>
    			    <form action="<?= base_url(); ?>cart/orderd" method="post" name="confirmCheckout" id="confirmCheckout" class="form-horizontal" role="form">
				    <?php
				    $i = 1;
				    $total = 0;
				    foreach ($cartObjects as $pO) :

					$arrayfet = array(
					    'p_product_id' => $pO->tproduct_id
					);
					$dataproduct = $this->product_model->viewRecordAny($arrayfet);
					$total = $total + ($pO->tproduct_price * $pO->tproduct_qty);
					?>
					<tr class="deleteTemptrP<?= $pO->tcart_id; ?>">
					    <td><?= $i++; ?></td>
					    <td>
						<a href="<?= base_url(); ?>product/product_show_list/<?= $dataproduct->p_product_id ?>" target="blank">
						    <img src="<?= base_url(); ?>assests/product/<?= $dataproduct->p_image; ?>" alt="" width="100" height="50">
						</a>
					    </td>
					    <td><?= htmlspecialchars_decode($dataproduct->p_title); ?></td>
					    <td><?= $this->models_model->modelId($dataproduct->p_model); ?></td>
					    <td><?= $this->colors_model->colorId($dataproduct->p_color); ?></td>
					    <td><?= $pO->tproduct_price; ?></td>
					    <td>
						<span style="display:none" >
						    <input type="number" class="form-control txtqtycat quantity" name="txtqtycat" value="<?= $pO->tproduct_qty; ?>"/>
						    <button class="btnupdatecart" type="button" id="<?= $pO->tcart_id; ?>">Update</button>
						</span>
						<a href="javascript:;" class="updateqtyactive"><span style="font-size: 30px;" class="text-yellow"><?= $pO->tproduct_qty; ?> <i class="fa fa-edit"></i></span>
						</a>
					    </td>
					    <td><?= money_format('%i', $pO->tproduct_price * $pO->tproduct_qty) ?></td>
					    <td>
						<a href="javascript:;" class="deleteRcordTemp" data-id="<?= $pO->tcart_id; ?>"><i class="fa fa-trash"></i></a>
					    </td>
					</tr>
					<input type="hidden" name="productId" value="<?= $pO->tproduct_id; ?>">
					<input type="hidden" name="productQty" value="<?= $pO->tproduct_qty; ?>">
					<input type="hidden" name="productPrice" value="<?= $pO->tproduct_price; ?>">
					<?php
				    endforeach;
				    ?>
    				<tr>
    				    <td colspan="7" align="right"><strong>Total</strong></td>
    				    <td colspan="2" align="left"><?= money_format('%i', $total); ?></td>
    				</tr>
    				<tr>
    				    <td align="right"><strong>Description</strong></td>
    				    <td colspan="8" align="left"><textarea rows="4" cols="70" name="orderdescription" id="orderdescription"></textarea>
    				    </td>
    				</tr>
    				<tr>
    				    <td align="right">
    					<input name="orderby_self" type="<?= $users1->role_id == 4 ? '' : 'hidden' ?>" id="orderby_self" class="orderby_self" value="<?= $users1->role_id == 4 ? '' : $getEamil ?>">

    				    </td>

    				    <td colspan="8" align="right">
    					<button class="btn btn-primary" name="btnordernow" id="btnordernow">Order Now</button>
    				    </td>
    				</tr>

    				</tfoot>
    			</table>
			    <?php
			else:
			    ?>
    			<p>Cart is empty</p>
			<?php
			endif;
			?>
                    </div>

		    </form>

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p><?php echo $links; ?></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
