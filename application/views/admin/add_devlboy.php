<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <div class="row" style="padding: 3px 0px; background-color: #fff" >
	 
      <div class="col-md-12">
   
      <h3 class="text-left" style="margin-left: 18px"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>
            <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"> Back</a></li>
          
           
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>

   <div class="col-sm-4"> 
	<?php if ($this->session->flashdata('product_uploaded')) : ?> 
	    <?php echo $this->session->flashdata('product_uploaded'); ?>
	<?php endif; ?>
      </div>
            
               
                
                    <form  id="productForm" name="productForm" action="<?= base_url(); ?>admin/devlboy/ajax_upload"  method="POST" enctype="multipart/form-data">
			
				 
                        <div class="row" style="margin-top: 50px;">
						<div class="col-sm-1"></div>
						<div class="col-sm-10" style="background-color: antiquewhite; border-radius: 10px">
						<div class="container" style="margin-top: 30px">
						<div class="row">
						
						
						<div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label" for="user_name">Name <b style="color:#f00;">*</b></label>
                                    <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter User Name" value="<?= !empty($userObject->devlName) ? $userObject->devlName : ''; ?>">
                            </div>
							</div>
							
							<div class="col-md-3">
							<div class="form-group">
                                <label class="control-label" for="user_status">Mobile</label>
                                <input type="text" maxlength="10" class="form-control" id="user_mobile" name="user_mobile" placeholder="Enter User Mobile" value="<?= !empty($userObject->devlMobile) ? $userObject->devlMobile : ''; ?>">    
                            </div>
							</div>
							<input name="user_status" id="user_status" type="hidden" value="0"/>
							<div class="col-md-3">
							<!---div class="form-group">
                                <label class="control-label" for="user_status">Status</label>
                                    <select name="user_status" id="user_status" class="form-control">
										<option value="">--Select Status--</option>
                                        <option value="0">inActive</option>
                                        <option value="1">Active</option>
                                    </select>
									</div-->
									</div>
									
                            </div>
							</div>
							</div>
							</div>
							
						
                            
                            <input type="hidden" name="devlEdit" id="devlEdit" value="<?= !empty($userObject->d_id) ? $userObject->d_id : ''; ?>"/>
                       
                         
                          
		   <div class="row" style="margin-top: 12px">
			
                
                <div class="col-md-3 pull-right">
                   
			<button type="submit" name="btnDevl" id="btnDevl" class="btn btn-primary btn-md" title="Submit">
			    
				 <?= !empty($userObject->d_id) ? 'Update' : 'Add Delivery Boy'; ?> </button>
                        
                </div>
            
			</div>

		</form>
	   
	    
	

	<!-- /.box -->
   
</div>
<!-- /.content-wrapper -->
