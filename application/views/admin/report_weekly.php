<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reports
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Reports</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-4 col-sm-offset-4">
		    <p style="background: #005c9f;
		       padding: 5px;
		       border-radius: 3px;margin-bottom: 50px;"><select class="form-control" name="selectdate" id="selectdate">
			    <option value="">Select Date</option>
			    <option value="1">Week</option>
			    <option value="2">Month</option>
			    <option value="3">Year</option>
			</select></p>
		</div>
	    </div>
        </div>
        <div id="yearview">
            <div class="row">
		<div class="col-lg-3 col-xs-6">
		    <!-- small box -->
		    <div class="small-box bg-aqua">
			<div class="inner">

			    <h3><?= $ordercount; ?>

			    </h3>

			    <p>New Orders</p>
			</div>
			<div>
			    <p style="font-size: 35px !important">$<?= money_format('%i', $orderdetails->amount_paid) . "\n" ?></p>
			</div>
			<a href="<?= base_url(); ?>admin/orders/orderList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		    </div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
		    <!-- small box -->
		    <div class="small-box bg-green">
			<div class="inner">
			    <h3><?= $ordercountcd; ?></h3>
			    <p>Completed Orders</p>
			</div>
			<div>
			    <p style="font-size: 35px !important">$<?= money_format('%i', $orderdetailsc->amount_paid) . "\n" ?></p>
			</div>
			<a href="<?= base_url(); ?>admin/orders/orderCompleted" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		    </div>
		</div>
		<!-- ./col -->

		<!-- ./col -->
		<!--div class="col-lg-3 col-xs-6">

		    <div class="small-box bg-red">
			<div class="inner">
			    <h3><?= $ordercountc; ?></h3>

			    <p>Canceled Orders</p>
			</div>
			<div>
			    <p style="font-size: 35px !important">$<?php
		$number = $orderdetailscc->amount_paid;
		echo money_format('%i', $number) . "\n";
		?></p>
			</div>
			<a href="<?= base_url(); ?>admin/orders/orderCanceled" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		    </div>
		</div>
		<div class="col-lg-3 col-xs-6">

		    <div class="small-box bg-yellow">
			<div class="inner">
			    <h3><?= $ordercountcc; ?></h3>
			    <p>Dispatched Orders</p>
			</div>
			<div>
			    <p style="font-size: 35px !important">$<?= money_format('%i', $orderdetailscd->amount_paid) . "\n" ?></p>
			</div>
			<a href="<?= base_url(); ?>admin/orders/orderDispatched" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		    </div>
		</div>
		<!-- ./col -->
	    </div>
        </div>

        <!-- /.row -->
        <div class="row">

        </div>

    </section>

    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
