<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Shop List</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Shop Name</th>
				    <th>Shop Area</th>
				    <th>Country > State</th>
                                    <th>Mobile</th>
				    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				
				foreach ($shopObjects as $pO) :
				    ?>
    				<tr class="deleteShoptrP<?= $pO->shop_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $pO->shop_name; ?></td>
				    <td><?= $pO->shop_area; ?></td>
    				    
    				    <td><?= $pO->shop_state . ' > ' . $pO->shop_city; ?></td>
				    <td><?= $pO->shop_phone; ?></td>
    				    <td>
                                         <?php
                                            $checkm = checkpermission('183');
                                            ?>
                                            <?php if ($checkm->is_deleted == '1'): ?>
    					<a href="javascript:;" title="Delete Shop" class="btn btn-danger deleteRcordShop" data-id="<?= $pO->shop_id; ?>"><i class="fa fa-trash"></i></a>  |
                                        <?php endif; ?>
                                            <?php if ($checkm->is_edited == '1'): ?>
                                        <a title="Edit Shop Details" class="btn btn-warning" href="<?= ADMINC; ?>shop/edit/<?= $pO->shop_slug; ?>"><i class="fa fa-edit"></i></a> |
                                        <?php endif; ?>
                                            <?php if ($checkm->is_viewed == '1'): ?>
    					<a title="View Shop Details" class="btn btn-info" href="<?= ADMINC; ?>shop/shop_show_list/<?= $pO->shop_slug; ?>">
    					    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                        <?php endif; ?>
    				    </td>
    				</tr>
				<?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
