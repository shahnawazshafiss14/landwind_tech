<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
   <div class="row" style="padding: 3px 0px; background-color: #fff" >
	 
      <div class="col-md-12">
   
      <h3 class="text-left" style="margin-left: 18px"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>

	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->


 <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"> Back</a></li>
          <li><a href="<?= base_url(); ?>admin/unit/listunit"> List Unit</a></li>
           
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>

	   
		<form  id="unitForm" name="unitForm" action=""  method="POST" enctype="multipart/form-data">

		
	    
	     <div class="row" style="margin-top: 50px;">
						<div class="col-sm-1"></div>
						<div class="col-sm-10" style="background-color: antiquewhite; border-radius: 10px">
						<div class="container" style="margin-top: 30px">
						<div class="row">
						
			<div class="col-sm-3">
			    <div class="form-group">
				<label class="control-label " for="UnitName">Unit Name <b style="color:#f00;">*</b></label
				<input name="edit" type="hidden" value="<?= !empty($unitObject->u_id) ? $unitObject->u_id : ''; ?>">
				    <input type="text" class="form-control" id="u_name" name="u_name" placeholder="Enter Unit Name" value="<?= !empty($unitObject->u_name) ? $unitObject->u_name : '' ?>">
				</div>
				
				    
				
			    </div>
				<div class="col-sm-3">
				<div class="form-group">
				<label class="control-label " for="BrandName">Unit Value <b style="color:#f00;">*</b></label>
				
				    <input type="text" class="form-control" id="u_value" name="u_value" placeholder="Enter Unit Value" value="<?= !empty($unitObject->u_value) ? $unitObject->u_value : '' ?>">
				
			
		    </div>
							</div>
							
							
							</div>
							</div>
							</div>
							</div>
		
			   
	   
	     <div class="row" style="margin-top: 12px">
			
                
                <div class="col-md-3 pull-right">
                   
				<button type="submit" name="btnUnit" id="btnUnit" class="btn btn-primary btn-md" title="Collapse">
			     <?= !empty($unitObject->u_id) ? 'Update' : 'Add'?> </button>
                        
                </div>
            
			</div>
			
			<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($unitObject->u_id) ? 'Edit' : 'Add'?> Unit</h3>
		 
		    
		
			<?php if (!empty($unitObject->u_id)) : ?> 
    			<a href="<?= base_url(); ?>admin/unit/listunit" title="Collapse" class="btn btn-info btn-sm" title="Cancel">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-sm" title="Cancel">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?> 
			

</form>		
</div>
<!-- /.content-wrapper -->
