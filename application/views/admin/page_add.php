<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Products</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('page_uploaded')) : ?>
	    <?php echo $this->session->flashdata('page_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="pageForm" name="pageForm" action="<?= base_url(); ?>pages/add"  method="POST" enctype="multipart/form-data">

                        <a href="<?= base_url(); ?>admin/pages/pageview" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
                            <i class="fa fa-reply"></i> Cancel </a>
                </div>
            </div>
            <div class="box-body">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="page_name">Page Title <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="page_title" name="page_title" placeholder="Enter Product Name" value="<?= !empty($pageObject->page_title) ? $pageObject->page_title : ''; ?>">
                                </div>
                            </div>
                            <input type="hidden" name="pageEdit" id="pageEdit" value="<?= !empty($pageObject->page_id) ? $pageObject->page_id : ''; ?>"/>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Description">Description <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control ckeditor" id="page_description" name="page_description" required> <?= !empty($pageObject->page_description) ? $pageObject->page_description : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="btnSubmitpage" id="btnSubmitpage" class="btn btn-info btn-sm" title="Collapse">
                        <i class="fa fa-save"></i> <?= !empty($pageObject->page_id) ? 'Update' : 'Save'; ?> </button>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
