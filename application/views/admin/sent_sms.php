<?php

$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<style>
    .input-group {
	margin-bottom: 12px;
    }
    .form-group div {
        margin-bottom:10px !important;
        margin-top:10px !important;
    }
</style>
<?php 
        $checkm = checkpermission('190');
        ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('consig_uploaded')) : ?>
	    <?php echo $this->session->flashdata('consig_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
		 
                <!--div class="box-tools pull-right">
                    <div id="sentm" style="display:none">
                        <input type="text" name="sendEmail" id="sendEmail" style="width: 350px; height: 35px;" /> |
                        <input type="button" name="btnsendEmail" id="btnsendConsigmo" data-month="<?= $expense_month ?>" data-slug="<?= !empty($expenseObject->consig_slug) ? $expenseObject->consig_slug : ''; ?>" value="Send" class="btn btn-success" /> |
                        <a href="javascript:;" id="emailcancel" class="btn btn-danger">Cancel</a>
                    </div>
                        <div id="actonw" style="float:right">
                            <a href="javascript:;"  class="btn btn-primary" id="sendemailaction"><i class="fa fa-envelope"></i> Send Email</a> |
                            <a target="_blank" href="<?= base_url(); ?>expenses/<?= !empty($expenseObject->consig_slug) ? $expenseObject->consig_slug : ''; ?>/<?= $expense_month ?>" class="btn btn-waning"><i class="fa fa-print"></i> Print Bill</a> | <label class="btn btn-success"><?= $expense_month ?></label>
                        </div>
		</div-->
	    </div>
	    <div class="box-body">
		<div class="row">
		    <div class="tab-content">
                         <form id="invoiceForm" name="invoiceForm" action="<?= ADMINC; ?>expense/lists"  method="GET">
                            <div class="form-group">
                                 
                                <div class="col-md-4">
                                    <span style="<?= !empty($expn_type) ? 'background:#d3f274' : ''; ?>">Payment Type</span> <br/>
                                    <select class="selectpicker1" data-live-search="true" name="expn_type" id="expn_type" style="width:100%;<?= !empty($expn_type) ? 'background:#d3f274' : ''; ?>">
                                        <?php
                                        $fetarcsi1 = array(
											'type_id' => $get_expn_type,
                                            'status' => '1',
                                            'view_status' => '1' 
                                        );
                                        $fetdasingle1 = $this->expensetype_model->viewRecordAny($fetarcsi1);
                                        if (!empty($fetdasingle1->type_id)):
                                            ?>
                                            <option value="<?= $fetdasingle1->type_id; ?>" selected><?= $fetdasingle1->type_name; ?></option>
                                        <?php endif; ?>
                                        <option value="">List of Expense Type</option>
                                        <option value="0">NO</option>
                                        <?php
													$array_types = array(
														'status' => '1',
														'view_status' => '1'
													);
																$typequery = $this->expensetype_model->viewRecordAnyR($array_types);
																if(count($typequery) > 0):
																 
                                        foreach ($typequery as $fevenue):
                                            ?>
                               <option data-tokens="<?= $fevenue->type_name; ?>" value="<?= $fevenue->type_id; ?>"><?= $fevenue->type_name; ?></option>
											
                                        <?php 
										endforeach;
										endif;

										?>
                                    </select>
                                     
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2">
                                    Payment Mode <br/>
                                    <select id="expn_mode" name="expn_mode" class="form-control">
																<option value="">Select Mode</option>
																<option value="1" <?= $get_expn_mode == 1 ? 'selected' : ''; ?>>Cash</option>
																<option value="2" <?= $get_expn_mode == 2 ? 'selected' : ''; ?>>Online</option>
															</select>
                                </div>
                                    <div class="col-md-2"> 
													<label>Expense Date</label> 
													<input type="text" id="expn_date" name="expn_date" value="<?= !empty($get_expn_date) ? $get_expn_date : ''; ?>" class="form-control datepicker123" data-date-format="yyyy-mm-dd" placeholder="" autocomplete="off">
											</div>
                                
                                 
                                <div class="col-sm-4 col-md-2">
                                    Date of expnse From<br/>
                                    <input type="text" class="form-control datepicker123" name="from" id="from" value="<?= !empty($start_date) ? $start_date : ''; ?>" style="<?= !empty($start_date) ? 'background-color:#d3f274' : ''; ?>" autocomplete="off">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Date of expnse To<br/>
									<input type="text" class="form-control datepicker123" name="to" id="to" value="<?= !empty($end_date) ? $end_date : ''; ?>" style="<?= !empty($end_date) ? 'background-color:#d3f274' : ''; ?>" autocomplete="off">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-6 col-md-offset-5">
                                    <button type="submit" class="btn btn-primary btn-lg" name="btnexpensefillter" id="btnexpensefillter">Search</button> 
                                    <button type="button" class="btn btn-primary btn-lg" name="reset" id="btnresetexp">Reset All</button> 
                                </div>
                            </div> 
                         </form>
                    </div>
		</div>
		<div class="row">
		    <div class="col-md-12">
                        <?php 
                          if(!empty($expenseObject) && $expenseObject != 'recordnotfound'){
                        ?>
			<table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Sr.no.</th>
									<th>Client Name</th>
									<th>Mobile No</th>
									<th>Message</th>
									<th style="text-align:right">Date</th>
								</tr>
											</thead>
											<tbody>
				
				<?php
				$s=1;	
														if(count($expenseObject)>0):			
														$toatoal = 0;
															foreach($expenseObject as $expense): ?>
																<tr>
																	<td><?=$s++;?></td> 
																	<td><?= !empty($expense->client_id) ? $this->consignee_model->find_by_name($expense->client_id) : ''; ?></td>
																	<td><?= !empty($expense->mobile_no) ? $expense->mobile_no : ''; ?></td>
																	<td><?= !empty($expense->message) ? $expense->message : ''; ?></td>
																	<td style="text-align:right"><?= !empty($expense->created_at) ? $expense->created_at : ''; ?></td> 
																</tr>
															<?php
															endforeach; 
														endif;
													?>	
													 
				    
				 
			    </tbody>
                        </table>
                        <?php 
                          }
                        ?>
		    </div>
		</div>


	    </div>

	</div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


