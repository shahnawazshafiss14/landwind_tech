<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>project/projectList">Projects</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('proj_uploaded')) : ?>
            <?php echo $this->session->flashdata('proj_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="projectForm" name="projectForm" action="<?= ADMINC; ?>project/addProjects"  method="POST" enctype="multipart/form-data">
                        <?php
                        $checkm = checkpermission('171');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
                            <button type="submit" title="Add" name="btnproject" class="btn btn-success btn-lg" id="<?= !empty($projectObject->id) ? 'btnproject' : 'btnproject'; ?>">
                                <i class="fa fa-save"></i> <?= !empty($projectObject->id) ? 'Update' : 'Save'; ?> </button> 
                      | 
                            <a title="Refresh" class="btn btn-success" href="<?= ADMINC; ?>project/createproject/<?= !empty($projectObject->proj_slug) ? $projectObject->proj_slug : ''; ?>"><i class="fa fa-refresh"></i></a>
                        <?php endif; ?> 
                </div>
            </div>
            <div class="box-body">
                <!--ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#General">General</a>
                    </li>
                </ul-->
                <div class="form-horizontal">
                    <div class="tab-content">
                           
                            <input name="editproject" id="editproject" type="hidden" class="form-control" value="<?= !empty($projectObject->proj_slug) ? $projectObject->proj_slug : ''; ?>">
                            <div class="form-group">
                                
                                <input type="hidden" name="dor_id" id="dor_id" value="<?= !empty($projectObject->proj_client) ? $projectObject->proj_client : ''; ?>" />
                                <div class="col-sm-12 col-md-4">
                                    Details of Receiver (Billed to) <b style="color:#f00;">*</b><br/>
                                    <select class="selectpicker1" data-live-search="true" id="dor_receiver_id" style="width:100%;">
                                        <?php
                                        $fetarcsi = array(
                                            'consig_id' => $projectObject->proj_client,
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetdasingle = $this->consignee_model->viewRecordAny($fetarcsi);
                                        if (!empty($projectObject->proj_client)):
                                            ?>
                                            <option value="<?= $projectObject->proj_client; ?>" selected><?= $fetdasingle->consig_name . ', ' . $fetdasingle->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Receiver</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <a class="btn btn-primary" href="<?= ADMINC; ?>consignee/edit" title="Add Details of Receiver (Billed to)"><i class="fa fa-plus"></i></a>
                                </div>
                                <div class="col-sm-3 col-md-6">
                                    Project Title <b style="color:#f00;">*</b><br/>
                                    <input name="proj_title" id="proj_title" type="text" class="form-control" value="<?= !empty($projectObject->proj_title) ? $projectObject->proj_title : ''; ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Project Price <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="proj_price" id="proj_price" value="<?= !empty($projectObject->proj_price) ? $projectObject->proj_price : ''; ?>">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="col-sm-4 col-md-2">
                                    Project Start Date <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="proj_start_date" id="proj_start_date" value="<?= !empty($projectObject->proj_start_date) ? $projectObject->proj_start_date : date('Y-m-d'); ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Project End Date <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="proj_end_date" id="proj_end_date" value="<?= !empty($projectObject->proj_end_date) ? $projectObject->proj_end_date : date('Y-m-d'); ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Date of Supply <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="proj_dos" id="invoice_dos" value="<?= !empty($projectObject->proj_dos) ? $projectObject->proj_dos : date('Y-m-d'); ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Place of Supply <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="proj_pos" id="proj_pos" value="<?= !empty($projectObject->proj_pos) ? $projectObject->proj_pos : 'New Delhi'; ?>">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="col-sm-12 col-md-6">
                                    Project Production Url<br/>
                                    <input name="proj_production_url" id="proj_production_url" type="text" class="form-control" value="<?= !empty($projectObject->proj_production_url) ? $projectObject->proj_production_url : ''; ?>">
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    Project Development Url <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="proj_development_url" id="proj_development_url" value="<?= !empty($projectObject->proj_development_url) ? $projectObject->proj_development_url : ''; ?>">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="col-sm-12 col-md-12">
                                    Project Description<br/>
                                    <textarea class="form-control" name="proj_description" id="proj_description"><?= !empty($projectObject->proj_description) ? $projectObject->proj_description : ''; ?></textarea>
                                </div>
                                 
                            </div>
                              

                       
                    </div>

                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->

