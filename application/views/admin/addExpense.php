<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($categoryObject->category_id) ? 'Edit' : 'Add' ?> Expense</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= ADMINC; ?>expense/topExpense">Expense</a></li>
	    <li class="active"><?= !empty($expenseObject->type_id) ? 'Edit' : 'Add' ?> Expense</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($expenseObject->type_id) ? 'Edit' : 'Add' ?> Expense</h3>
		<div class="box-tools pull-right">
		    <form  id="expenseForm" name="expenseForm" action="<?= base_url(); ?>admin/expense/saveexpensetype"  method="POST">
			<button type="submit" name="btnExpense" id="btnExpense" class="btn btn-lg btn-success">
			    <i class="fa fa-save"></i> <?= !empty($expenseObject->type_id) ? 'Update' : 'Save' ?></button>
			<?php if (!empty($expenseObject->type_id)) : ?>
    			<a href="<?= ADMINC; ?>expense/topExpense" title="Collapse" class="btn btn-info btn-lg">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-lg" >
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>
		</div>
	    </div>
	    <div class="box-body">
		<!--ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul-->
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="expenseName">Expense Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="editName" id="editName" type="hidden" value="<?= !empty($expenseObject->type_id) ? $expenseObject->type_id : ''; ?>">
				    <input type="text" class="form-control" id="expenseName" name="expenseName" placeholder="Enter Expense Name" value="<?= !empty($expenseObject->type_name) ? $expenseObject->type_name : '' ?>" required>
				</div>
			    </div> 
			</div>
		    </div>
		</div>

		</form>
	    </div>

	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
