<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assests/images/favicon.ico" type="image/x-icon">
         
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/AdminLTE.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/plugins/iCheck/square/blue.css">
        <style>
            .errors{
                border: 2px solid #f00 !important;
            }
            input:focus{
                border: 2px solid #f00;
            }
        </style>
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="login_bg">
        <div class="backgroundadmin">
            <div class="item">
                <div class="bg_img" style="background:url(<?= base_url(); ?>assests/images/2200x954_accounting_background.jpg) no-repeat;background-size: cover; -webkit-background-size: cover;">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-sm-4">
                <!-- <div class="col-sm-4 col-sm-offset-4"> -->
                <!-- /.login-logo -->
                <div class="login-box-body">
                    <p class="login-box-msg">
                        <img src="<?= COMPANYLOGO; ?>" alt="" style="max-width:200px;"></p>
                    <?php
                    if (!empty($error)) {
                        echo $error;
                    }
                    ?>
                    <form action="" method="post" name="loginForm" id="loginForm">
                        <p id="email_error">Username</p>
                        <div class="form-group has-feedback">

                            <input type="email" class="form-control" id="admin_email" name="admin_email" value="" placeholder="Email">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <p id="pass_error">Password</p>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" id="admin_password" name="admin_password" placeholder="Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <button type="button" id="adminloginbtn" class="btn btn-primary btn-block btn-flat">Login</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-8 hidden-xs">
                <div class="login-box-body" style="min-height: 310px; max-width:620px;float:right;background: rgba(0,0,0,.4); color:#fff;">
                    <h3 style="color: inherit;">LANDWIND TECH PROVIED FOR BUSINESS SOLUTIONS</h3>
                    <h4>Track Sales, Purchased and Invoicing Solutions.</h4>
                    <p style="color: inherit;">Welcome to the Landwind Tech Customer Success Platform. Our new Lightning Platform gives you the fastest, most complete way to put your customers at the center of everything you do.</p>
                </div>  
            </div>
        </div>
        <!-- jQuery 3 -->
        <script src="<?= base_url(); ?>assests/components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url(); ?>assests/components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?= base_url(); ?>assests/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
        <script>
            var validationErro = 'animated shake errors';
            $(document).on('click', '#adminloginbtn', function () {
                var uName = $('#admin_email').val();
                var uPassword = $('#admin_password').val();
                if ($.trim(uName) == '') {
                    $('#admin_email').focus();
                    $("#admin_email").notify("Email id required!");
                    $('#admin_email').addClass(validationErro);
                    setTimeout(function () {
                        $('#admin_email').removeClass(validationErro);
                    }, 2000);
                    return false;
                }
                var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                if (!filter.test(uName)) {
                     $('#admin_email').focus();
                    $("#admin_email").notify("Email id invalid!");
                    $('#admin_email').addClass(validationErro);
                    setTimeout(function () {
                        $('#admin_email').removeClass(validationErro);
                    }, 2000);
                    return false;
                } else if ($.trim(uPassword) == '') {
                    $('#admin_password').focus();
                    $("#admin_password").notify("Please enter password!");
                    $('#admin_password').addClass(validationErro);
                    setTimeout(function () {
                        $('#admin_password').removeClass(validationErro);
                    }, 2000);
                    return false;
                } else {
                    $.post('<?= ADMINC; ?>index/login', {admin_email: uName, admin_password: uPassword}, function (data) {
                         if (data != 'notvalid') {
                            window.open('<?= ADMINC; ?>dashboard/index', '_self');
                        } else {
                            swal({
                                type: 'error',
                                title: 'Sorry...',
                                text: 'Email and password is invalid! Please try again...'
                            });
                        }
                    });
                }
            });

        </script>
        <script src="<?= base_url(); ?>assests/dist/js/notify.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/top-js/top_custom.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/js/sweetalert2.js" type="text/javascript"></script>
        <div class="copyright"><p class="pull-left"><?= COMPANYCOPYRIGHT ?></p><p class="pull-right"><?= LWDEVLOPED ?></p></div>
    </body>
</html>
