<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArray = array(
    'order_userName' => $getEamil,
    'oStatus' => '0'
);
$detailFind = $this->order_model->viewOrderEmailUserEmail($dataArray);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Products List<small>Manage View Product</small></h1>
        <form role="search" class="navbar-form" style="float: left; margin-left: 20px; margin-top: 0px;">
	    <div class="input-group add-on" style="width: 380px;">
		<input type="text" id="srch-term" name="srch-term" placeholder="Search" class="form-control">
		<div class="input-group-btn">
		    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
		</div>
	    </div>
	</form>
	<a href="<?= ADMINC ?>cart/index">
	    <div class="cart-view" id="cartadd">
		<i class="fa fa-shopping-cart" aria-hidden="true"></i> cart <span class="cart_count"></span>

	    </div>
	</a>

    </section>
    <section class="content">
	<div class="row">

            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
			<div class="row">
			    <div class="col-md-12" style="text-align:center">
				<h3 class="text-center" style="color: green;font-size: 38px;margin-top: 90px;}">Thank You !</h3>
				<p style="color:#424242; font-size:18px; letter-spacing: 1px;">Your Order No. for this order is <?= $detailFind->oTransationId; ?></p>
				<p style="color:#424242; font-size:18px; letter-spacing: 1px;">Your Order is received and well contact you soon</p>
			    </div>
			</div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
