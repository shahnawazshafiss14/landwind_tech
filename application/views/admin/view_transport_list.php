<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Transport Bill List</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Date of Bill</th>
                                    <th>Challan Number</th>
                                    <th>Consignee</th>
				    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				foreach ($transportObjects as $pO) :
				    ?>
    				<tr class="deleteRcordtransT<?= $pO->trans_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td>
    					<?= $pO->created_date; ?>
    				    </td>
    				    <td><?= $pO->trans_challan_no; ?></td>
    				    <td><?php 
				    $fetad = array(
					'consig_id' => $pO->trans_name_consignee
				    );
				    $fetchany = $this->consignee_model->viewRecordAny($fetad);
echo $fetchany->consig_name . ', '. $fetchany->consig_address;				
				    ?></td>
    				    <td><?= $pO->trans_date; ?></td>
                                    <td>
                                        <?php
                                            $checkm = checkpermission('180');
                                            ?>
                                            <?php if ($checkm->is_deleted == '1'): ?>
                                        <a href="javascript:;" title="Delete Transport Bill" class="btn btn-danger deleteRcordtrans" data-id="<?= $pO->trans_id; ?>"><i class="fa fa-trash"></i></a> |
                                        <?php endif; ?>
                                            <?php if ($checkm->is_edited == '1'): ?>
                                        <a title="Edit Transport Bill" class="btn btn-warning" href="<?= ADMINC; ?>transport/add/<?= $pO->trans_slug; ?>"><i class="fa fa-edit"></i></a> |
                                         <?php endif; ?>
                                            <?php if ($checkm->is_viewed == '1'): ?>
                                        <a title="View Transport Bill" class="btn btn-info" href="<?= ADMINC; ?>transport/transport_show_list/<?= $pO->trans_slug; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                         <?php endif; ?>
                                    </td>
    				</tr>
				<?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
