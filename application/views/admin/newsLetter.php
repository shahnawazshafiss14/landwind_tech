<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Location List</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>admin/users/newsLetter">newsLetter</a></li>
	    <li class="active">List NewsLetter</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>NewsLetter</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 0;
				foreach ($newsletters as $news):
				    ?>
    				<tr class="deletenewsLettertrL<?= $news->newsLetter_Id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $news->newsLetter_email; ?></td>
    				    <td><a href="javascript:;" class="deletenewsLettertr" data-id="<?= $news->newsLetter_Id; ?>"><i class="fa fa-trash"></i></a></td>
    				</tr>
				<?php endforeach; ?>
			    </tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
