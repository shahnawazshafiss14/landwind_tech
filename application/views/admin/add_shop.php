<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>shop/shop_list">Shop</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('shop_uploaded')) : ?>
            <?php echo $this->session->flashdata('shop_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="shopForm" name="shopForm" action="<?= ADMINC; ?>shop/addShop"  method="POST" enctype="multipart/form-data">
                        <?php
                        $checkm = checkpermission('182');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
                            <button type="submit" title="Save" name="btnSubmitshop" id="<?= !empty($shopObject->shop_id) ? 'ubtnShop' : 'ubtnShop'; ?>" class="btn btn-success btn-lg" >
                                <i class="fa fa-save"></i> <?= !empty($shopObject->shop_id) ? 'Update' : 'Save'; ?> </button>
                            <a href="<?= ADMINC; ?>shop/shop_list" title="Back to Shop List" class="btn btn-info btn-lg" >
                                <i class="fa fa-reply"></i> Cancel </a>
                        <?php endif; ?>
                </div>
            </div>
            <div class="box-body">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#General">General</a>
                    </li>
                    <!--li><a data-toggle="tab" href="#Image">Image</a></li-->
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_name">Shop Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="shopid" id="shopid" value="<?= !empty($shopObject->shop_id) ? $shopObject->shop_id : ''; ?>"/>
                                    <input type="text" class="form-control" id="shop_name" name="shop_name" placeholder="Enter Shop Name" value="<?= !empty($shopObject->shop_name) ? $shopObject->shop_name : '';
                        ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_add">Shop Address <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="shop_address" name="shop_address" placeholder="Enter Shop Address" value="<?= !empty($shopObject->shop_address) ? $shopObject->shop_address : '';
                        ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_email">Shop Email </label>
                                <div class="col-sm-10">
                                    <input type="text" id="shop_email" name="shop_email" class="form-control" value="<?= !empty($shopObject->shop_email) ? $shopObject->shop_email : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_phone">Shop Phone </label>
                                <div class="col-sm-10">
                                    <input type="text" id="shop_phone" name="shop_phone" class="form-control number" maxlength="10" value="<?= !empty($shopObject->shop_phone) ? $shopObject->shop_phone : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_phone">Aadhar Number <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="text" id="shop_gstin" name="shop_gstin" class="form-control" value="<?= !empty($shopObject->shop_gstin) ? $shopObject->shop_gstin : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_state">State Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">

                                    <input name="shop_state" id="shop_state" class="form-control" value="<?= !empty($shopObject->shop_state) ? $shopObject->shop_state : 'Bihar' ?>"/>

                                </div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-sm-2" for="shop_city">City <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="text" id="shop_city" name="shop_city" class="form-control" value="<?= !empty($shopObject->shop_city) ? $shopObject->shop_city : 'Siwan'; ?>">
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-sm-2" for="shop_area">Area <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="text" id="shop_area" name="shop_area" class="form-control" value="<?= !empty($shopObject->shop_area) ? $shopObject->shop_area : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_zipcode">Zip Code </label>
                                <div class="col-sm-10">
                                    <input type="text" id="shop_zipcode" name="shop_zipcode" class="form-control number" maxlength="6" value="<?= !empty($shopObject->shop_zipcode) ? $shopObject->shop_zipcode : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="Description">Description </label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="shop_description" name="shop_description" placeholder="Description"> <?= !empty($shopObject->shop_description) ? $shopObject->shop_description : ''; ?></textarea>
                                    </div>
                                </div>

                            </div>
                            <!--div class="form-group">
                                <label class="control-label col-sm-2" for="shop_Status">Status</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="shop_status" id="shop_status">
                                        <option value="">--select--</option>
                                        <option value="1">Approved</option>
                                        <option value="0">Request</option>

                                    </select>
                                </div>
                            </div-->
                        </div>
                        <!--div id="Image" class="tab-pane fade">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="shop_images">Image <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                        <?php
                        if (!empty($shopObject->shop_image)):
                            ?>
                                        <img src="<?= BASEURLFRONTASSET; ?>shop/<?= $shopObject->shop_image; ?>" alt="" width="200px" height="200px">
                                        <input type="hidden" id="shop_images" name="shop_images" value="<?= $shopObject->shop_image; ?>">
                        <?php else: ?>
                                        <input type="file" id="shop_images" name="shop_images">
                        <?php endif; ?>

                                </div>
                            </div>
                        </div-->
                    </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
