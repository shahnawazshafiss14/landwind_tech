<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
$userParentId = $users1->u_id;
$userRole = $users1->role_id;
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('user_uploaded')) : ?>
	    <?php echo $this->session->flashdata('user_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="userForm" name="userForm" action="<?= ADMINC; ?>users/addUser"  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnSubmitUsers" id="btnSubmitUsers" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> <?= !empty($userObject->u_id) ? 'Update' : 'Save'; ?> </button>
                        <a href="<?= ADMINC; ?>users/admindetails/<?= $userObject->u_id; ?>" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
                            <i class="fa fa-reply"></i> Cancel </a>
                </div>
            </div>
            <div class="box-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		    <li><a data-toggle="tab" href="#company">Company Details</a></li>
		    <li><a data-toggle="tab" href="#bann">Bank Details</a></li>
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">First Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="text" name="userName" id="userName" class="form-control" placeholder="First Name" tabindex="1" value="<?= !empty($userObject->userName) ? $userObject->userName : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">Last Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="text" name="lastName" id="lastName" class="form-control" placeholder="Last Name" tabindex="2" value="<?= !empty($userObject->lastName) ? $userObject->lastName : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">Email <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="email" name="userEmail" id="userEmail" class="form-control" placeholder="Email Address" tabindex="3" value="<?= !empty($userObject->userEmail) ? $userObject->userEmail : ''; ?>" <?= !empty($userObject->userEmail) ? 'disabled' : ''; ?>>

				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">Role <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <?php
				    if ($userRole == '2'):
					?>
    				    <select name="role_id" id="role_id" class="form-control">
					    <?php if (!empty($userObject->role_id)) : ?>
						<option value="<?= $userObject->role_id; ?>">

						    <?php
						    echo $this->role_model->roleId($userObject->role_id);
						    ?>
						</option>
					    <?php endif; ?>
    					<option value="">Select Parent Role</option>
					    <?php
					    $dataArrayRole = array(
						'role_parent' => '2'
					    );
					    $roledata = $this->role_model->viewRecordAnyR($dataArrayRole);
					    foreach ($roledata as $rd):
						?>
						<option value="<?= $rd->role_id; ?>"><?= $rd->role_name; ?></option>
						<?php
					    endforeach;
					    ?>
    				    </select>
				    <?php else: ?>
    				    <select name="role_id" id="role_id" class="form-control">
					    <?php if (!empty($userObject->role_id)) : ?>
						<option value="<?= $userObject->role_id; ?>">

						    <?php
						    echo $this->role_model->roleId($userObject->role_id);
						    ?>
						</option>
					    <?php endif; ?>
    					<option value="">Select Parent Role</option>
					    <?php
					    $dataArrayRole = array(
						'role_parent' => '1'
					    );
					    $roledata = $this->role_model->viewRecordAnyR($dataArrayRole);
					    foreach ($roledata as $rd):
						?>
						<option value="<?= $rd->role_id; ?>"><?= $rd->role_name; ?></option>
						<?php
					    endforeach;
					    ?>
    				    </select>
				    <?php
				    endif;
				    ?>
				</div>
			    </div><div class="form-group">
				<input type="hidden" name="hidRole" id="hidRole" value="<?= !empty($userObject->userRole) ? $userObject->userRole : ''; ?>"/>
				<input type="hidden" name="user_parent" id="user_parent" value="<?= $userParentId; ?>"/>
                                <label class="control-label col-sm-2" for="user_name">Password <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="password" name="userPwd" id="userPwd" class="form-control" placeholder="Password" tabindex="4" value="<?= !empty($userObject->userPwd) ? $userObject->userPwd : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">Mobile <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="text" name="userMobile" id="userMobile" class="form-control" placeholder="Phone Number" tabindex="5" maxlength="10" value="<?= !empty($userObject->userMobile) ? $userObject->userMobile : ''; ?>">
				</div>
			    </div>
			</div>
			<div id="company" class="tab-pane fade">
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="compay_name">Company Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="text" name="companyName" id="companyName" class="form-control" placeholder="Company Name" tabindex="6" value="<?= !empty($userObject->cName) ? $userObject->cName : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="gstin">GSTIN<b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="text" name="gstin" id="gstin" class="form-control" placeholder="GSTIN" tabindex="6" value="<?= !empty($userObject->cGSTIN) ? $userObject->cGSTIN : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">Address <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input type="text" name="userAddress" id="userAddress" class="form-control" placeholder="Address" tabindex="6" value="<?= !empty($userObject->cAddress) ? $userObject->cAddress : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">State <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <input name="userState" id="userState" class="form-control" value="<?= !empty($userObject->cstate) ? $userObject->cstate : ''; ?>">

				    <!--select name="userState" id="userState" class="form-control" onchange="onCity(getElementById('userState').value);">
				    <?php
				    $datacity = array('location_parent' => '0');
				    $fetchCo = $this->location_model->viewRecordAnyR($datacity);
				    ?>
									<select name="userState" id="userState" class="form-control" onchange="onCity(getElementById('userState').value);">
				    <?php if (!empty($userObject->userState)) : ?>
    									    <option value="<?= $userObject->userState; ?>">
					<?= $this->location_model->locationId($userObject->userState); ?>
    									    </option>
				    <?php endif; ?>
									    <option value="">Select State</option>
				    <?php foreach ($fetchCo as $co): ?>
    									    <option value="<?= $co->location_id; ?>">
					<?= $co->location_name; ?>
    									    </option>
				    <?php endforeach; ?>
									</select-->
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="user_name">City <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="userCity" id="userCity" value="<?= !empty($userObject->cCity) ? $userObject->cCity : ''; ?>"/>
					<!--select name="userCity" id="userCity" class="form-control">
					<?php if (!empty($userObject->userCity)) : ?>
    					    <option value="<?= $userObject->userCity; ?>">
					    <?= $this->location_model->locationId($userObject->userCity); ?>
    					    </option>
					<?php endif; ?>
					    <option value="">Select City</option>
					</select-->

				    </div>
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="pincode">Pin Code <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="cPincode" id="cPincode" value="<?= !empty($userObject->cPincode) ? $userObject->cPincode : ''; ?>"/>
				    </div>
				</div>
			    </div>
			</div>
			<div id="bann" class="tab-pane fade">
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="Bank Name">Bank Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="bName" id="bName" value="<?= !empty($userObject->bName) ? $userObject->bName : ''; ?>"/>
				    </div>
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="Bank Name">Branch <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="bBranch" id="bBranch" value="<?= !empty($userObject->bBranch) ? $userObject->bBranch : ''; ?>"/>
				    </div>
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="Bank A/C">Bank A/C No.<b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="bAccount" id="bAccount" value="<?= !empty($userObject->bAccount) ? $userObject->bAccount : ''; ?>"/>
				    </div>
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="Bank Name">Bank IFSC Code <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="bIfsc" id="bIfsc" value="<?= !empty($userObject->bIfsc) ? $userObject->bIfsc : ''; ?>"/>
				    </div>
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="Bank Name">uPan <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
				    <div id="disi" style="display: block">
					<input type="text" class="form-control" name="uPan" id="uPan" value="<?= !empty($userObject->uPan) ? $userObject->uPan : ''; ?>"/>
				    </div>
				</div>
			    </div>
			</div>

			<input type="hidden" name="userEdit" id="userEdit" value="<?= !empty($userObject->u_id) ? $userObject->u_id : ''; ?>"/>

		    </div>


		</div>
	    </div>

	    </form>
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
	</div>
	<!-- /.box-footer-->
</div>

<!-- /.box -->
</section>
</div>
<!-- /.content-wrapper -->
