
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($menuObject->menu_id) ? 'Edit' : 'Add' ?> Menu</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>menu_top/viewtopMenu">Menu</a></li>
	    <li class="active"><?= !empty($menuObject->menu_id) ? 'Edit' : 'Add' ?> Menu</li>
	</ol>
    </section>
    <section class="content" id="ppg">

	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($menuObject->menu_id) ? 'Edit' : 'Add' ?> Menu</h3>
		<div class="box-tools pull-right">
		    <form  id="menuForm" name="menuForm" action=""  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnMenu" id="btnMenu" class="btn btn-success btn-lg" >
			    <i class="fa fa-save"></i> <?= !empty($menuObject->menu_id) ? 'Update' : 'Save' ?> </button>
			<?php if (!empty($menuObject->menu_id)) : ?>
    			<a href="<?= base_url(); ?>menu_top/viewtopMenu" title="Collapse" class="btn btn-info btn-lg" >
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-lg" >
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>


		</div>
	    </div>
	    <div class="box-body">

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="menuName">Menu Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="edit" type="hidden" value="<?= !empty($menuObject->menu_id) ? $menuObject->menu_id : ''; ?>">
				    <input type="text" class="form-control" id="menuName" name="menuName" placeholder="Enter Menu Name" value="<?= !empty($menuObject->menu_name) ? $menuObject->menu_name : '' ?>">


				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="selectCategory">Select Parent Category</label>
				<div class="col-sm-10">
				    <select name="menuParent" id="menuParent" class="form-control">
					<?php if (!empty($menuObject->menu_parent_id)) : ?>
    					<option value="<?= $menuObject->menu_parent_id; ?>">

						<?php
						echo $this->menu_model->menubyId($menuObject->menu_parent_id);
						?>
    					</option>
					<?php endif; ?>

					<option value="0">Select Menu</option>
					<?php foreach ($parent_menu as $pc) : ?>
    					<option value="<?= $pc->menu_id ?>"><?= $pc->menu_name ?></option>
					<?php endforeach; ?>
				    </select>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="menuName">Link <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="link" name="link" placeholder="Enter Link " value="<?= !empty($menuObject->link) ? $menuObject->link : '' ?>">


				</div>
			    </div>
			</div>
		    </div>
		</div>
		</form>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer">

	    </div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>


</div>
<!-- /.content-wrapper -->
