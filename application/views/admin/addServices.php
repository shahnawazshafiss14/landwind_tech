<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($servicesObject->services_id) ? 'Edit' : 'Add' ?> Services</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>services/index">Services</a></li>
	    <li class="active"><?= !empty($servicesObject->services_id) ? 'Edit' : 'Add' ?> Services</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($servicesObject->services_id) ? 'Edit' : 'Add' ?> Services</h3>
		<div class="box-tools pull-right">
		    <form  id="servicesForm" name="servicesForm" action=""  method="POST" enctype="multipart/form-data">
			<button type="button" name="btnServices" id="<?= !empty($servicesObject->services_id) ? 'btnServicesupdate' : 'btnServices' ?>" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> <?= !empty($servicesObject->services_id) ? 'Update' : 'Save' ?> </button>
			<?php if (!empty($servicesObject->services_id)) : ?>
    			<a href="<?= base_url(); ?>services/index" title="Collapse" class="btn btn-info btn-sm" title="Cancel">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>
		</div>
	    </div>
	    <div class="box-body">
		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="servicesName">Services Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="editName" id="editName" type="hidden" value="<?= !empty($servicesObject->services_id) ? $servicesObject->services_id : ''; ?>">
				    <input type="text" class="form-control" id="servicesName" name="servicesName" placeholder="Enter Services Name" value="<?= !empty($servicesObject->services_name) ? $servicesObject->services_name : '' ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="selectServices">Select Menu</label>
				<div class="col-sm-10">
				    <select name="menu_id" id="menu_id" class="form-control">
					<?php
					if (!empty($servicesObject->menu_id)):
					    ?>
    					<option value="<?= $servicesObject->menu_id ?>"><?= $this->services_model->servicesbyId($servicesObject->menu_id) ?></option>
					<?php endif; ?>
					<option value="0">Select Services</option>
					<?php foreach ($category_menu as $cm) : ?>
    					<option value="<?= $cm->category_id ?>"><?= $cm->category_name ?></option>
					<?php endforeach; ?>
				    </select>
				</div>
			    </div>
			    <!--div class="form-group">
				<label class="control-label col-sm-2" for="selectServices">Select Parent Services</label>
				<div class="col-sm-10">
				    <select name="selectServices" id="selectServices" class="form-control">
			    <?php
			    if (!empty($servicesObject->services_id)):
				$parent_id = $this->services_model->viewRecordId($servicesObject->services_id);
				?>
    					<option value="<?= $parent_id->services_parent_id ?>"><?= $this->services_model->servicesbyId($servicesObject->services_id) ?></option>
			    <?php endif; ?>
					<option value="0">Select Services</option>
			    <?php foreach ($parent_services as $pc) : ?>
    					<option value="<?= $pc->services_id ?>"><?= $pc->services_name ?></option>
			    <?php endforeach; ?>
				    </select>
				</div>
			    </div-->
			    <div class="form-group">
				<label class="control-label col-sm-2" for="serTitle">Services Title <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="serTitle" name="serTitle" placeholder="Enter Title Name" value="<?= !empty($servicesObject->services_title) ? $servicesObject->services_title : '' ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="images">Image<b style="color:#f00;">*</b></label>
				<?php
				if (!empty($servicesObject->serimage)):
				    ?>
    				<div class="col-md-12">
    				    <div class="col-md-6">
    					<img src="<?= BASEURLFRONTASSET . '/banner/' . $servicesObject->serimage; ?>" width="300" height="300" />
    					<input type="hidden" value="<?= $servicesObject->serimage; ?>" name="imagenameservic" id="imagenameservic" />
    				    </div>
    				    <div class="col-md-6">
    					<input class="form-control" type="file" id="serimagechange" name="serimagechange">
    					<input type="button" class="btn btn-primary" name="seruploadimagechange" id="seruploadimagechange" value="Upload">
    				    </div>
    				</div>
				<?php else: ?>
    				<div class="col-sm-10">
    				    <input class="form-control" type="file" id="serimage" name="serimage">
    				</div>
				<?php endif; ?>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="serDescription">Services Description <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <textarea class="form-control ckeditor" id="serDescription" name="serDescription">
					<?= !empty($servicesObject->serDescription) ? $servicesObject->serDescription : '' ?>
				    </textarea>
				</div>
			    </div>

			</div>
		    </div>
		</div>

		</form>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
