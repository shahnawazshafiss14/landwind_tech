<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Orders Status
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Orders</a></li>
	    <li class="active">Orders Tracking</li>
	</ol>
    </section>
    <section class="content">
	     <div class="row">
          <div class="col-sm-12" id="panel-showinvoice">
              <div class="panel panel-info">
                    <div class="panel-body" id="invoice-body">
                  
                        <ol class="progtrckr" data-progtrckr-steps="4">
                            <li class="progtrckr-<?= $orderObjects->oDate == ""? 'todo' : 'done' ?>">Order Processing</li>
                            <li class="progtrckr-<?= $orderObjects->odispatDate == ""? 'todo' : 'done' ?>">Shipped</li>
                            <li class="progtrckr-<?= $orderObjects->ocomDate == ""? 'todo' : 'done' ?>">Delivered </li>
                            <li class="progtrckr-<?= $orderObjects->ocancelDate == ""? 'todo' : 'done' ?>">Canceled </li>
                        </ol>
                      
                	<div class="row">
                	    <?php if($orderObjects->oDate != "") : ?>
                        <div class="col-sm-3">
                         <div class="track_order">
                            <p id="orderDataProgracee7"><?= $orderObjects->oDate; ?></p>
                            </div>
                	    </div>
                	    <?php endif; ?>
                	    <?php if($orderObjects->odispatDate != "") : ?>
                        <div class="col-sm-3"> 
                         <div class="track_order">
                            <p id="orderDataProgracee7"><?= $orderObjects->odispatDate; ?></p>
                            </div>
                	    </div>
                	    <?php endif; ?>
                	    <?php if($orderObjects->ocomDate != "") : ?>
                        <div class="col-sm-3">
                            <div class="track_order">
                            <p id="orderDataProgracee7"><?= $orderObjects->ocomDate; ?></p>
                	        </div>
                	    </div> 
                	    <?php endif; ?>
                	     <?php if($orderObjects->ocancelDate != "") : ?>
                        <div class="col-sm-3 col-sm-offset-3" style="padding-right: 65px;">
                         <div class="track_order"> 
                            <p id="orderDataProgracee7"><?= $orderObjects->ocancelDate; ?></p>
                            </div>
                	    </div>
                	    <?php endif; ?>
                	    
                	</div> 
                 <?php 
                 $dar_r = array(
                     'messageOid' =>  $orderObjects->order_id
                     );
                 $fetchmessage = $this->message_model->viewRecordAnyR($dar_r);
                 if(count($fetchmessage) > 0) :
                 foreach($fetchmessage as $fm): ?>
	            <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="alert alert-<?= $fm->messageType == '1'? 'danger' : 'success'?>">
                            <strong>Message!</strong> <?= $fm->messageBody; ?>
                        </div>
                    </div>
                </div>
                
                <?php endforeach; ?>
                <?php else: ?>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="alert alert-info">
                            <strong>Warning!</strong> There is no histry.
                        </div>
                    </div>
                </div>
                <?php endif?>
                           
                    </div>
               
               </div>
           
          </div>
      </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
