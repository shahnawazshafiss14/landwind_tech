<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Coupon List<small>Manage View Coupon</small>
	</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>admin/coupon/viewtopcoupon">coupon</a></li>
	    <li class="active">List Coupon</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> Add coupon</h3>
		<div class="box-tools pull-right">
		    <form  id="couponForm" name="couponForm" action="<?= base_url(); ?>admin/coupon/add_coupon"  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btncoupon" id="btncoupon" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> Save </button>
		</div>
	    </div>
	    <div class="box-body">

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="couponName">Coupon Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="couponname" name="couponname" placeholder="Enter Coupon Name" value="">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="couponprice">Coupon Discount in % <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="couponprice" name="couponprice" placeholder="Enter Coupon Price" value="" maxlength="2"> 
				</div>
			    </div>
			</div>
		    </div>
		</div>
		</form>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>coupon Name</th>
				    <th>coupon Price</th>
				    <!--th>Status</th-->
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($topcoupons as $tcoupon):
				    ?>
    				<tr class="deletecoupontrCp<?= $tcoupon->co_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $tcoupon->co_name; ?></td>
    				    <td><?= $tcoupon->co_price; ?></td>
    				    <!--td><?= $tcoupon->status; ?></td-->
    				    <td> <a href="javascript:;" class="deleteRcordconp" data-id="<?= $tcoupon->co_id; ?>"><i class="fa fa-trash"></i></a></td>
    				</tr>
				<?php endforeach; ?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
