<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>invoices/invoiceList">Invoices</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('invoice_uploaded')) : ?>
            <?php echo $this->session->flashdata('invoice_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="invoiceForm" name="invoiceForm" action="<?= ADMINC; ?>invoices/addInvoices"  method="POST" enctype="multipart/form-data">
                        <?php
                        $checkm = checkpermission('171');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
                            <button type="submit" title="Add" name="btnInvoice" class="btn btn-success btn-lg" id="<?= !empty($invoiceObject->invoice_id) ? 'btnInvoice' : 'btnInvoice'; ?>">
                                <i class="fa fa-save"></i> <?= !empty($invoiceObject->invoice_id) ? 'Update' : 'Save'; ?> </button>
								
					<button type="submit" title="Generate Invoice" name="btnCreate" id="btnCreate" class="btn btn-success btn-lg">
									Create </button>
                            <a href="<?= ADMINC; ?>invoices/invoice_show_list/<?= $invoiceObject->invoice_slug; ?>" title="Back to List" class="btn btn-info btn-lg" >
                                <i class="fa fa-reply"></i> Cancel </a> | 
                            <a title="Refresh" class="btn btn-success" href="<?= ADMINC; ?>invoices/createinvoice/<?= $invoiceObject->invoice_slug; ?>"><i class="fa fa-refresh"></i></a>
                        <?php endif; ?> 
                </div>
            </div>
            <div class="box-body">
                <!--ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#General">General</a>
                    </li>
                </ul-->
                <div class="form-horizontal">
                    <div class="tab-content">
                        <?php
                        $ass = $this->invoice_model->lastrecordId();
                        $invoadd = $ass->invoice_number + 1;
                        ?>
                         
                            <input name="editinvoice" id="editinvoice" type="hidden" class="form-control" value="<?= !empty($invoiceObject->invoice_slug) ? $invoiceObject->invoice_slug : ''; ?>">
                            <div class="form-group">
                                <div class="col-sm-3 col-md-2">
                                    Invoice Number <b style="color:#f00;">*</b><br/>
                                    <input name="invoice_number" id="invoice_number" type="text" class="form-control" value="<?= !empty($invoiceObject->invoice_number) ? $invoiceObject->invoice_number : $invoadd; ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Vehicle Number <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="invoice_vehicle_no" id="invoice_vehicle_no" value="<?= !empty($invoiceObject->invoice_vehicle_no) ? $invoiceObject->invoice_vehicle_no : ''; ?>">
                                </div>
                                <input type="hidden" name="dor_id" id="dor_id" value="<?= !empty($invoiceObject->invoice_receiver) ? $invoiceObject->invoice_receiver : ''; ?>" />
                                <div class="col-sm-12 col-md-4">
                                    Details of Receiver (Billed to) <b style="color:#f00;">*</b><br/>
                                    <select class="selectpicker1" data-live-search="true" id="dor_receiver_id" style="width:100%;">
                                        <?php
                                        $fetarcsi = array(
                                            'consig_id' => $invoiceObject->invoice_receiver,
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetdasingle = $this->consignee_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoiceObject->invoice_receiver)):
                                            ?>
                                            <option value="<?= $invoiceObject->invoice_receiver; ?>" selected><?= $fetdasingle->consig_name . ', ' . $fetdasingle->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Receiver</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <a class="btn btn-primary" href="<?= ADMINC; ?>consignee/edit" title="Add Details of Receiver (Billed to)"><i class="fa fa-plus"></i></a>
                                </div>
                                <input type="hidden" name="doc_id" id="doc_id" value="<?= !empty($invoiceObject->invoice_consig) ? $invoiceObject->invoice_consig : ''; ?>" />
                                <div class="col-md-4">
                                    Details of Consignee (Shipped to) <b style="color:#f00;">*</b><br/>
                                    <select class="selectpicker1" data-live-search="true" id="doc_receiver_id" style="width:100%;">
                                        <?php
                                        $fetarcsi1 = array(
                                            'consig_id' => $invoiceObject->invoice_consig,
                                            'view_status' => '1',
                                            'consig_type' => '0'
                                        );
                                        $fetdasingle1 = $this->consignee_model->viewRecordAny($fetarcsi1);
                                        if (!empty($invoiceObject->invoice_consig)):
                                            ?>
                                            <option value="<?= $invoiceObject->invoice_consig; ?>" selected><?= $fetdasingle1->consig_name . ', ' . $fetdasingle1->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Consignee</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '0'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <a class="btn btn-primary" href="<?= ADMINC; ?>consignee/edit" title="Add Details of Consignee (Shipped to)"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2">
                                    Select CGST <b style="color:#f00;">*</b><br/>
                                    <select name="invoice_cgst" class="form-control" id="invoice_cgst" style="width:100%;">
                                        <?php
                                        $fetarcsi = array(
                                            'gst_id' => $invoiceObject->invoice_cgst,
                                        );
                                        $fetdasingle = $this->gst_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoiceObject->invoice_cgst)):
                                            ?>
                                            <option value="<?= $invoiceObject->invoice_cgst; ?>" selected><?= $fetdasingle->commongst; ?></option>
                                        <?php endif; ?>
                                        <option value="">Select CGst</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                        );
                                        $fetda = $this->gst_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option value="<?= $fevenue->gst_id; ?>"><?= $fevenue->commongst; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Select SGST <b style="color:#f00;">*</b><br/>
                                    <select name="invoice_sgst" class="form-control" id="invoice_sgst" style="width:100%;">
                                        <?php
                                        $fetarcsi = array(
                                            'gst_id' => $invoiceObject->invoice_sgst,
                                        );
                                        $fetdasingle = $this->gst_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoiceObject->invoice_sgst)):
                                            ?>
                                            <option value="<?= $invoiceObject->invoice_sgst; ?>" selected><?= $fetdasingle->commongst; ?></option>
                                        <?php endif; ?>
                                        <option value="">Select SGst</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                        );
                                        $fetda = $this->gst_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option value="<?= $fevenue->gst_id; ?>"><?= $fevenue->commongst; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Select IGST <b style="color:#f00;">*</b><br/>
                                    <select name="invoice_igst" class="form-control" id="invoice_igst" style="width:100%;">
                                        <?php
                                        $fetarcsi = array(
                                            'gst_id' => $invoiceObject->invoice_igst,
                                        );
                                        $fetdasingle = $this->gst_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoiceObject->invoice_igst)):
                                            ?>
                                            <option value="<?= $invoiceObject->invoice_igst; ?>" selected><?= $fetdasingle->commongst; ?></option>
                                        <?php endif; ?>
                                        <option value="">Select IGst</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                        );
                                        $fetda = $this->gst_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option value="<?= $fevenue->gst_id; ?>"><?= $fevenue->commongst; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Date of Supply <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="invoice_dos" id="invoice_dos" value="<?= !empty($invoiceObject->invoice_dos) ? $invoiceObject->invoice_dos : date('Y-m-d'); ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Place of Supply <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="invoice_pos" id="invoice_pos" value="<?= !empty($invoiceObject->invoice_pos) ? $invoiceObject->invoice_pos : 'Siwan'; ?>">
                                </div>
                            </div>
                            <?php
                            if (!empty($invoiceObject->invoice_slug)):
                                ?>
                                <div id="insertcol" class="removerow">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="item_table">
                                            <tr>
                                                <th>Select Particulars</th>
                                                <th>Available Unit</th>
                                                <th>Max Unit</th>
                                                <th>Unit</th>
                                                <th>Rate</th>
                                                <th>Action
                                                    <!--button type="button" name="add" class="btn btn-success add"><i class="fa fa-plus"></i></button-->
                                                </th>
                                            </tr>
                                            <?php
                                            $toatalqty = 0;
                                            $fetchParticlearray1 = array(
                                                'itemInvoice_invoice' => $invoiceObject->invoice_id
                                            );
                                            $fetchParticle1 = $this->itemtypeInvoice_model->viewRecordAnyR($fetchParticlearray1);
                                            foreach ($fetchParticle1 as $tr):
                                                $fetchParticlearray = array(
                                                    'item_id' => $tr->itemInvoice_name
                                                );
                                                $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                                ?>
                                                <tr class="deleteinvoiceitemtr<?= $tr->itemInvoice_id; ?>">
                                                    <td>
                                                        <select class="form-control particuler_id" name="particuler_id[]" disabled>
                                                            <?php
                                                            if (!empty($tr->itemInvoice_name)):
                                                                ?>
                                                                <option value="<?= $tr->itemInvoice_name; ?>" selected><?= $this->itemtype_model->itemId($tr->itemInvoice_name, 'item_particular_name') . ', ' . $this->itemtype_model->itemId($tr->itemInvoice_name, 'item_hsn'); ?></option>
                                                            <?php endif; ?>

                                                            <option value="">Select Particulars</option>
                                                            <?= itemtype(); ?>
                                                        </select>
                                                    </td>
                                                    <td></td>
                                                    <td><input type="text" name="invoice_unit_max[]" class="form-control invoice_unit_max" value="<?= $tr->itemInvoice_max_kg; ?>" disabled></td>
                                                    <?php
                                                    $toatalqty += $tr->itemInvoice_kg;
                                                    ?>
                                                    <td><input type="text" name="invoice_unit[]" class="form-control invoice_unit" value="<?= $tr->itemInvoice_kg; ?>" disabled></td>
                                                    <td><input type="text" class="form-control invoice_rate" value="<?= $tr->itemInvoice_rate; ?>" disabled></td>

                                                    <td>
                                                        <a href="javascript:;" class="btn btn-danger deleteinvoiceitem" data-id="<?= $tr->itemInvoice_id; ?>"><i class="fa fa-remove"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td colspan="3" style="text-align:right">
                                                    Total Unit
                                                </td>
                                                <td style="background-color: #000033; color: #ffffff"><?= !empty($toatalqty) ? $toatalqty : 'Non'; ?></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="particuler_id" id="particuler_idup">
                                                        <option value="">Select Particulars</option>
                                                        <?= itemtype(); ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div id="response"></div>
                                                    <input type="text" name="invoice_rate_max" id="invoice_rate_maxup" class="form-control invoice_rate_maxup" value="" disabled>
                                                    <input type="hidden" name="availabelqty" id="availabelqty" class="form-control invoice_rate_maxup" value="">
                                                </td>

                                                <td><input type="text" name="invoice_unit_max" id="invoice_unit_maxup" class="form-control invoice_unit_max" value=""></td>
                                                <td><input type="text" name="invoice_unit" id="invoice_unitup" class="form-control invoice_unit" value=""></td>
                                                <td><input type="text" name="invoice_rate" id="invoice_rateup" class="form-control invoice_rate" value=""></td>
                                                <td><a class="btn btn-primary" id="invoiceitemupadd" data-id="<?= $invoiceObject->invoice_id; ?>">Add</a></td>
                                            </tr>



                                        </table>

                                    </div>
                                </div>
                            <?php endif; ?>

                       
                    </div>

                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->

