 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div><div class="col-sm-10">
							<!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                                    <div class="card">
                                       
                                        <div class="card-body">
										
                                            
                                            <div class="row">
											 
											<div class="col-xl-4 col-md-6 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User Name</label>
                                                          : <?= $this->login_model->find_by_user($expense->expn_by);?> 
                                                        </div> 
                                                </div>
											
                                                
                                                <div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Expense Type</label>
                                                            : <?= !empty($expense->expn_type) ? $this->expensetype_model->find_by_name($expense->expn_type) : ''; ?> 
                                                        </div> 
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Expense Amount</label>
                                                            : <?= !empty($expense->expn_amount) ? $expense->expn_amount : ''; ?> 
                                                        </div> 
                                                </div>
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Expense Date</label>
                                                            : <?= !empty($expense->expn_date) ? $expense->expn_date : ''; ?> 
                                                        </div> 
                                                </div> 
												<div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Expense Mode</label>
                                                            : <?= !empty($expense->expn_mode) ? payment_mode_type($expense->expn_mode) : ''; ?> 
                                                        </div> 
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Created on</label>
                                                            : <?= !empty($expense->created_on) ? $expense->created_on : ''; ?> 
                                                        </div> 
                                                </div>												
												
												 
												
												
                                            </div>
											 
														
												 
                                                </div>
												
												
												
                                            </div>
											
											
											
                                                </div><div class="col-sm-1"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>