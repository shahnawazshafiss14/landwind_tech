<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>expense/lists">View List</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('expense_uploaded')) : ?>
            <?php echo $this->session->flashdata('expense_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-body">
                <div class="form-horizontal"> 
                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
											<?php 
											$flash_data =  $this->session->flashdata('expense_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Expense has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('expense_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                           <form action="<?= ADMINC . 'expense/saveexpense'?>" id="frmExpense" name="frmExpense" method="POST" />
											 
											<input id="expn_by" name="expn_by" class="form-control" type="hidden" value="<?= $this->acc_id; ?>">
												<div class="row">
											<div class="col-md-3"> 
													<label>Expense Date</label> 
													<input type="text" id="expn_date" name="expn_date" value="<?= !empty($expensed->expn_date) ? $expensed->expn_date : date('Y-m-d'); ?>" class="form-control" data-date-format="yyyy-mm-dd" placeholder="" autocomplete="off">
											</div>
                                                <div class="col-md-3"> 
												
                                                            <label>Expense Type</label>
                                                            <select id="expn_type" name="expn_type" class="form-control" required>
															<option value="">Select Type</option>
																<?php 
																$array_types = array(
																	'view_status' => '1'
																);
																$typequery = $this->expensetype_model->viewRecordAnyR($array_types);
																if(count($typequery) > 0):
																foreach($typequery as $type):
																if($type->type_id == $expensed->expn_type){
																	$val = "selected";
																}else{
																	$val = "";
																}
																?>
																<option value="<?= $type->type_id; ?>" <?= $val; ?>><?= $type->type_name; ?></option>
																<?php endforeach;?>
																<?php endif;?>
															</select>
                                                
                                                </div> 
												 <div class="col-md-3"> 
                                                        
                                                            <label>Expense Mode</label>
                                                            <select id="expn_mode" name="expn_mode" class="form-control" required>
															<?php 
															if(!empty($expensed->expn_mode)):
															?>
																<option value="<?= $expensed->expn_mode; ?>"><?= payment_mode($expensed->expn_mode); ?></option>
																<?php 
																endif;
															?>
																<option value="">Select Mode</option>
																<option value="1">Cash</option>
																<option value="2">Online</option>
															</select>
                                                        
                                                </div>
												 <div class="col-md-3">
                                                        
                                                            <label>Amount</label>
                                                            <input type="text" id="expn_amount" name="expn_amount" value="<?= !empty($expensed->expn_amount) ? $expensed->expn_amount : ''; ?>" class="form-control" placeholder="" required>
                                                        
														<input type="hidden" id="expense_slug" name="expense_slug" value="<?= !empty($expensed->expn_id) ? $expensed->expn_id : ''; ?>" class="form-control" placeholder="">
                                                </div> 
												
												<div class="col-md-12">
                                                            <label>Description</label> 
                                                            <textarea id="expn_desc" name="expn_desc" class="form-control" rows="2"><?= !empty($expensed->expn_desc) ? $expensed->expn_desc : ''; ?></textarea>
														 
                                                </div>
												</div>
												<div class="row">
												<div class="col-md-3 col-md-offset-5">  
												 <?php
                        $checkm = checkpermission('189');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
							 <button type="submit" id="btnAddExpense" name="btnAddExpense" class="btn btn-primary btn-lg"><?= !empty($expensed->slug) ? 'Update' : 'Submit'; ?></button> 
                            <a href="<?= ADMINC; ?>expense/lists" title="Back to details" class="btn btn-info btn-lg" ><i class="fa fa-reply"></i> Cancel </a>
                        <?php endif; ?>
						</div>
												 
                                                </div>
												
												
                                             
											</form> 
														
												 
                                                </div>
												
												
												
                                            </div>
											 
                    
                    </div>   
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->

