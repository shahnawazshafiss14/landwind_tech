<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('apply_uploaded')) : ?>
	    <?php echo $this->session->flashdata('apply_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
		<input type="hidden" name="applyid" id="applyid" value="<?= !empty($applyObject->apply_id) ? $applyObject->apply_id : ''; ?>" />
	    </div>
	    <div class="box-body">
		<div class="row">
		    <div class="col-sm-12">
			<div style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;display:block;float: left;width: 100%;"><h3 style="float: left;margin: 0; margin-bottom: 9px; margin-top: 5px; ">Apply Details</h3>

			</div>
			<div class="row detailsprodod">
			    <p><span class="col-sm-5"><b>First Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyfirstName; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Last Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applylastName; ?></span></p><br/>
			    <?php
			    $arrcareetp = array(
				'1' => 'Accounting',
				'2' => 'Event Assistant',
				'3' => 'Dev Ops Engineer',
				'4' => 'Engineering Manager',
				'5' => 'Marketing Manager',
				'6' => 'Product Specialists'
			    );
			    ?>
			    <p><span class="col-sm-5"><b>Apply For</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $arrcareetp[$applyObject->categoryApply]; ?></span></p><br/>
			    <?php if (!empty($applyObject->applyEmail)): ?>
    			    <p><span class="col-sm-5"><b>Email</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyEmail; ?></span></p><br/>
			    <?php endif; ?>
			    <?php if (!empty($applyObject->applyAddress)): ?>
    			    <p><span class="col-sm-5"><b>Address</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyAddress; ?></span></p><br/>
			    <?php endif; ?>
			    <?php if (!empty($applyObject->applyCity)): ?>
    			    <p><span class="col-sm-5"><b>City</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyCity; ?></span></p><br/>
			    <?php endif; ?>
			    <?php if (!empty($applyObject->applyState)): ?>
    			    <p><span class="col-sm-5"><b>State</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyState; ?></span></p><br/>
			    <?php endif; ?>
			    <?php if (!empty($applyObject->applyZip)): ?>
    			    <p><span class="col-sm-5"><b>Zip code</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyZip; ?></span></p><br/>
			    <?php endif; ?>
			    <?php if (!empty($applyObject->applyPhone)): ?>
    			    <p><span class="col-sm-5"><b>Phone</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $applyObject->applyPhone; ?></span></p><br/>
			    <?php endif; ?>

			    <?php if (!empty($applyObject->applyFile)): ?>
    			    <p><span class="col-sm-5"><b>Download File</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><a href="<?= $file = BASEURLFRONTASSET . 'resume/' . $applyObject->applyFile ?>"><?= $applyObject->applyFile ?></a></span></p><br/>
				    <?php endif; ?>
			</div>
		    </div>
		</div>
	    </div>
	    <div class=""></div>
	    <!-- box-body -->


	</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


