<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
   
   
			   <div class="row">
      <div class="col-xs-12">
    <div class="row" style="padding: 3px 0px; background-color: #fff">
        
		<h3 class="text-center"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>
      
  </div>
            <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/sales/addBill"> Back</a></li>
			
          
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>
   
    
	<div class="row">
	    <div class="col-xs-12">
		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Bill No</th>
				    <th>Bill Customer</th>
				    <th>City Name</th>
				    <th>Bill Date</th>
					<th>Status</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($sales as $bills):
					$asd_dev = array(
						'u_id' => $bills->bill_consignee
					);
					$fetchUSer = $this->user_m_f->viewRecordAny($asd_dev);
				?>
    				<tr class="deleteUsertrU<?= $bills->bill_no; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $bills->bill_no; ?></td>
				    <td><?= $this->user_m_f->userbyId($bills->bill_consignee); ?></td>
				    <td><?= $fetchUSer->userCity; ?></td>
				    <td><?= $bills->bill_date; ?></td>
					<td><?php
					$ar = array(
						'1' => 'Warehouse',
						'2' => 'Print',
						'3' => 'Delivered',
						'4' => 'unDelivered'
					);
					
					echo $ar[$bills->bill_status]; ?></td>
    				    <td> <a class="btn btn-danger deleteRcordddd" href="javascript:;" data-id="<?= $bills->bill_no; ?>"><i class="fa fa-trash"></i></a> | <a class="btn btn-warning" href="<?= base_url(); ?>admin/sales/addBill/<?= $bills->bill_no; ?>"><i class="fa fa-edit"></i></a> | <a class="btn btn-primary" href="<?= base_url(); ?>admin/sales/bill_show_list/<?= $bills->bill_no; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
    				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
