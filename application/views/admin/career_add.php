<!-- Content Wrapper. Contains career content -->
<div class="content-wrapper">
    <!-- Content Header (Career header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Career</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('career_uploaded')) : ?>
	    <?php echo $this->session->flashdata('career_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="careerForm" name="careerForm" action="<?= ADMINC; ?>career/add"  method="POST" enctype="multipart/form-data">

                        <a href="<?= base_url(); ?>admin/career/view" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
                            <i class="fa fa-reply"></i> Cancel </a>
                </div>
            </div>
            <div class="box-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="career_name">Career Type <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <select type="text" class="form-control" id="career_type" name="career_type">
					<?php
					$arrcareetp = array(
					    '1' => 'Accounting',
					    '2' => 'Event Assistant',
					    '3' => 'Dev Ops Engineer',
					    '4' => 'Engineering Manager',
					    '5' => 'Marketing Manager',
					    '6' => 'Product Specialists'
					);
					?>
					<?php
					if (!empty($careerObject->career_type)):
					    ?>
    					<option value="<?= $careerObject->career_type; ?>" selected><?= $arrcareetp[$careerObject->career_type]; ?></option>
					<?php endif; ?>
					<option value="">Select Type</option>

					<option value="0">Accounting</option>
					<option value="1">Event Assistant</option>
					<option value="2">Dev Ops Engineer</option>
					<option value="3">Engineering Manager</option>
					<option value="4">Marketing Manager</option>
					<option value="5">Product Specialists</option>
				    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="career_name">Career Title <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="career_title" name="career_title" placeholder="Enter Product Name" value="<?= !empty($careerObject->career_title) ? $careerObject->career_title : ''; ?>">
                                </div>
                            </div>
                            <input type="hidden" name="careerEdit" id="careerEdit" value="<?= !empty($careerObject->career_id) ? $careerObject->career_id : ''; ?>"/>

                            <div class="form-group">
                                <label class="control-label col-sm-2" for="Description">Description <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control ckeditor" id="career_description" name="career_description" required> <?= !empty($careerObject->career_description) ? $careerObject->career_description : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="btnSubmitcareer" id="btnSubmitcareer" class="btn btn-info btn-sm" title="Collapse">
                        <i class="fa fa-save"></i> <?= !empty($careerObject->career_id) ? 'Update' : 'Save'; ?> </button>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
