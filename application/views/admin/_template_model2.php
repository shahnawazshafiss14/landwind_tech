<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $head_title ?> - <?= COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assests/images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/select2/dist/css/select2.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/Ionicons/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- Theme style -->

        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/AdminLTE.css">

        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/skins/_all-skins.min.css">

 
        <link href="<?= base_url(); ?>assests/dist/css/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url(); ?>assests/dist/css/alertify.default.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url(); ?>assests/dist/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php
            $this->load->view('admin/include/topmenu');
            $this->load->view('admin/include/sidemenu');
            ?>

            <?php $this->load->view($contentView); ?>

            <?php $this->load->view('admin/include/footer'); ?>

            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <script src="<?= base_url(); ?>assests/components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url(); ?>assests/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="<?= base_url(); ?>assests/components/select2/dist/js/select2.full.min.js"></script>
        <!-- DataTables -->
        <script src="<?= base_url(); ?>assests/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="<?= base_url(); ?>assests/components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url(); ?>assests/components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?= base_url(); ?>assests/components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

        <!-- FastClick -->
        <script src="<?= base_url(); ?>assests/components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url(); ?>assests/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url(); ?>assests/dist/js/demo.js"></script>
        <!-- page script -->
        <script src="<?= base_url(); ?>assests/dist/js/custom.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/script.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/alertify.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/notify.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/jquery-ui.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/js/sweetalert2.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('#example1').DataTable()
                $('#example2').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': false,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                });
            });
        </script>

        <script>
            $(function () {
                $('.selectpicker1').select2();
                 $('.datepicker123').datepicker({
                         dateFormat: 'yy-mm-dd'});
                var dateFormat = "dd/mm/yy",
                        from = $("#from")
                        .datepicker({
                            defaultDate: "-1w",
                            changeMonth: true,
                            numberOfMonths: 3
                        })
                        .on("change", function () {
                            to.datepicker("option", "minDate", getDate(this));
                        }),
                        to = $("#to").datepicker({
                    defaultDate: "-1w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                        .on("change", function () {
                            from.datepicker("option", "maxDate", getDate(this));
                        });

                function getDate(element) {
                    var date;
                    try {
                        date = $.datepicker.parseDate(dateFormat, element.value);
                    } catch (error) {
                        date = null;
                    }

                    return date;
                }
            });
        </script>
        <script>
            $(function () {
                $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
                
            });
           
        </script>
    </body>
</html>

