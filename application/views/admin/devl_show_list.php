<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>admin/devlboy/index">Delivery</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>

    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('user_uploaded')) : ?>
	    <?php echo $this->session->flashdata('user_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">

                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;">User Details</b>
                        <div class="row detailsprodod">
                            <p><span class="col-sm-5"><b>User Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= htmlspecialchars_decode($userObjects->devlName); ?></span></p><br/>
							<p><span class="col-sm-5"><b>User Mobile</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->devlMobile; ?></span></p><br/>
							<p><span class="col-sm-5"><b>User State</b><b class="pull-right">:</b></span> <span class="col-sm-7">
							<?php 
						$location_name1 = $this->location_model->locationId($userObjects->devlState);
						echo $location_name1;
						?></span></p><br/>
                            <p><span class="col-sm-5"><b>User City</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->devlCity; ?></span></p><br/>
                            <p><span class="col-sm-5"><b>User Address</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->devlAddress; ?></span></p><br/>
                        </div>
                    </div>
                </div>
                <div class="">
                </div><!-- box-body -->

                <div class="box-footer">

                </div><!-- box-footer -->

            </div>
            <!-- .box -->
    </section>


</div>
<!-- .content-wrapper -->
