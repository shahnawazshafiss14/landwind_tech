<?php
$dataUser1 = $userObjects->customerEmail;
$dataArraym1 = array(
    'buy_userName' => $dataUser1,
    'buyStatus' => '1'
);
$eventbuys = $this->buy_model->viewRecordAnyR($dataArraym1);
$dataorder = array(
    'order_userName' => $dataUser1,
    'oStatus' => '1'
);
$eventorders = $this->order_model->viewRecordAnyR($dataorder);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <!--ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>users/index">Users</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol-->
    </section>

    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('user_uploaded')) : ?>
	    <?php echo $this->session->flashdata('user_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">

                </div>
            </div>
            <div class="box-body">
                <div class="row">
		    <?php
		    if (!empty($userObjects->customer_id)):
			?>
    		    <div class="col-sm-12">
    			<b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;">User Details</b>
    			<div class="row detailsprodod">
    			    <p><span class="col-sm-5"><b>User Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= htmlspecialchars_decode($userObjects->customerName); ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Last Name</b><b class="pull-right">:</b></span> <span class="col-sm-7"><?= $userObjects->customerlastName; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Email</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->customerEmail; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Mobile</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->customerMobile; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Password</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->customerPwd; ?></span></p><br/>
    			</div>
    		    </div>
			<?php
		    else :
			?>
    		    <div class="col-sm-12">
    			<h3>User Details Not Found</h3>
    		    </div>
		    <?php endif; ?>
                </div>
                <div class=""></div>
		<div class="box box-info">
		    <div class="box-header">
			<h3 class="box-title"><i class="fa fa-shopping-cart"></i> History Details</h3>
			<div class="box-tools pull-right">

			</div>
		    </div>
		    <div class="box-body">
			<div class="row">
			    <div class="col-sm-12">

				<div class="panel panel-default">
				    <div class="panel-heading">
					Users
				    </div>
				    <!-- /.panel-heading -->
				    <div class="panel-body">
					<div class="table-responsive">
					    <table id="example1" class="table table-hover table-bordered table-striped">
						<thead>
						    <tr>
							<th>#</th>
							<th>Image</th>
							<th>Title</th>
							<th>Type</th>
							<th>Order Date</th>
							<th>Action</th>
						    </tr>
						</thead>
						<tbody>
						    <?php
						    $i = 1;
						    foreach ($eventbuys as $eventbuy):

							$fevent = $this->event_model->viewRecordId($eventbuy->buy_product_id);
							?>
    						    <tr class="success">
    							<td><?= $i++; ?></td>
    							<td><img src="<?= BASEURLFRONTASSET . 'event/' . $fevent->event_image; ?>" width="100" height="50"/></td>
    							<td><?= $fevent->event_title; ?></td>
    							<td>Event</td>
    							<td><?= $eventbuy->buyDate; ?></td>
    							<td><a href="<?= ADMINC . 'events/bookedLists/' . $fevent->event_id; ?>"><i class="fa fa-book"></i></a></td>
    						    </tr>
						    <?php endforeach; ?>
						    <?php
						    // $i = 1;
						    foreach ($eventorders as $eventorder):

							$forder = $this->product_model->viewRecordId($eventorder->order_product_id);
							?>
    						    <tr class="success">
    							<td><?= $i++; ?></td>
    							<td><img src="<?= BASEURLFRONTASSET . 'product/' . $forder->p_image; ?>" width="100" height="50"/></td>
    							<td><?= $forder->p_title; ?></td>
    							<td>Painting</td>
    							<td><?= $eventorder->oDate; ?></td>
    							<td><a href="<?= ADMINC . 'orders/orderCompleted'; ?>"><i class="fa fa-book"></i></a></td>
    						    </tr>
						    <?php endforeach; ?>
						</tbody>
					    </table>
					</div>
					<!-- /.table-responsive -->
				    </div>
				    <!-- /.panel-body -->
				</div>
				<!-- /.panel -->



			    </div>
			</div>
		    </div>
		    <div class="">
		    </div><!-- box-body -->

		    <div class="box-footer">

		    </div><!-- box-footer -->

		</div>
		<!-- .box -->
	    </div>
	    <!-- box-body -->

	    <div class="box-footer">

	    </div><!-- box-footer -->

	</div>
	<!-- .box -->
    </section>


</div>
<!-- .content-wrapper -->
