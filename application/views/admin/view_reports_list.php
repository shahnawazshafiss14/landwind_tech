<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Invoices Monthly reports</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
			<?php
			$totalqty1 = $this->consignee_model->reportvenueadminwithout(date('Y').'-01-01', date('Y').'-01-31');
			$totalprice1 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-01-01', date('Y').'-01-31');
			$totalqty2 = $this->consignee_model->reportvenueadminwithout(date('Y').'-02-01', date('Y').'-02-29');
			$totalprice2 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-02-01', date('Y').'-02-29');
			$totalqty3 = $this->consignee_model->reportvenueadminwithout(date('Y').'-03-01', date('Y').'-03-31');
			$totalprice3 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-03-01', date('Y').'-03-31');
			$totalqty4 = $this->consignee_model->reportvenueadminwithout(date('Y').'-04-01', date('Y').'-04-30');
			$totalprice4 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-04-01', date('Y').'-04-30');
			$totalqty5 = $this->consignee_model->reportvenueadminwithout(date('Y').'-05-01', date('Y').'-05-31');
			$totalprice5 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-05-01', date('Y').'-05-31');
			$totalqty6 = $this->consignee_model->reportvenueadminwithout(date('Y').'-06-01', date('Y').'-06-30');
			$totalprice6 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-06-01', date('Y').'-06-30');
			$totalqty7 = $this->consignee_model->reportvenueadminwithout(date('Y').'-07-01', date('Y').'-07-31');
			$totalprice7 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-07-01', date('Y').'-07-31');
			$totalqty8 = $this->consignee_model->reportvenueadminwithout(date('Y').'-08-01', date('Y').'-08-31');
			$totalprice8 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-08-01', date('Y').'-08-31');
			$totalqty9 = $this->consignee_model->reportvenueadminwithout(date('Y').'-09-01', date('Y').'-09-30');
			$totalprice9 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-09-01', date('Y').'-09-30');
			$totalqty10 = $this->consignee_model->reportvenueadminwithout(date('Y').'-10-01', date('Y').'-10-31');
			$totalprice10 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-10-01', date('Y').'-10-31');
			$totalqty11 = $this->consignee_model->reportvenueadminwithout(date('Y').'-11-01', date('Y').'-11-30');
			$totalprice11 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-11-01', date('Y').'-11-30');
			$totalqty12 = $this->consignee_model->reportvenueadminwithout(date('Y').'-12-01', date('Y').'-12-31');
			$totalprice12 = $this->consignee_model->reportvenueadminpricewithout(date('Y').'-12-01', date('Y').'-12-31');


			$dataPoints = array(
			    array("y" => $totalprice1, "label" => "Jan"),
			    array("y" => $totalprice2, "label" => "Feb"),
			    array("y" => $totalprice3, "label" => "Mar"),
			    array("y" => $totalprice4, "label" => "Apr"),
			    array("y" => $totalprice5, "label" => "May"),
			    array("y" => $totalprice6, "label" => "June"),
			    array("y" => $totalprice7, "label" => "July"),
			    array("y" => $totalprice8, "label" => "Aug"),
			    array("y" => $totalprice9, "label" => "Sept"),
			    array("y" => $totalprice10, "label" => "Oct"),
			    array("y" => $totalprice11, "label" => "Nov"),
			    array("y" => $totalprice12, "label" => "Dec")
			);
			?>
			<script>
			    window.onload = function() {

				var chart = new CanvasJS.Chart("chartContainer", {
				    animationEnabled: true,
				    theme: "light2",
				    data: [{
					    type: "column",
					    yValueFormatString: "#,##0.## Rs",
					    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
					}]
				});
				chart.render();

			    }
			</script>
			<div id="chartContainer" style="height: 370px; width: 100%;"></div>
			<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
