<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<style>
    .input-group {
        margin-bottom: 12px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
        <?php
        ?>
        <?php if ($this->session->flashdata('shop_uploaded')) : ?>
            <?php echo $this->session->flashdata('shop_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="shopid" id="shopid" value="<?= !empty($consigneeObject->consig_id) ? $consigneeObject->consig_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <div id="sentm" style="display:none">
                        <input type="text" name="sendEmail" id="sendEmail" style="width: 350px; height: 35px;" /> |
                        <input type="button" name="btnsendEmailinlist" id="btnsendEmailinlist" data-slug="<?= !empty($consigneeObject->consig_id) ? $consigneeObject->consig_id : ''; ?>" value="Send" class="btn btn-success" /> |
                        <a href="javascript:;" id="emailcancel" class="btn btn-danger">Cancel</a>
                    </div>
                    <div id="actonw" style="float:right">
                        <a href="javascript:;" class="btn btn-primary" id="sendemailaction"><i class="fa fa-envelope"></i> Send Email</a> |
                        <a href="javascript:;" class="btn btn-success" onclick="printDiv('print_area1')"><i class="fa fa-print"></i> Print Invoice</a> |
                        <button title="Refresh" class="btn btn-success" id="btnitemrefere"><i class="fa fa-refresh"></i></button>
                    </div>

                </div>
            </div>
            <div class="box-body">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row" style="margin-top: 50px;margin-bottom: 50px;">
                                <form name="frmgenratebill" id="frmgenratebill" action="<?= ADMINC; ?>consignee/consignee_show_list_invoice/<?= $consigneeObject->consig_slug; ?>" method="get">
                                    <table class="table table-hover table-bordered table-striped">
                                        <thead>
                                        <th>From</th>
                                        <th>To</th>	
                                        <th>Action</th>
                                        </thead>
                                        <tbody>
                                        <td><input type="text" id="from" name="from" class="form-control" value="<?= !empty($month_to) ? $month_to : ''; ?>"></td>
                                        <td><input type="text" id="to" name="to" class="form-control" value="<?= !empty($month_to) ? $month_from : ''; ?>"></td>
                                        <td><input type="submit" class="btn btn-success"></td>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-12" id="print_area1">
                    <style>
                        fieldset.scheduler-border {
                            border: 3px solid #ddd !important;
                            padding: 0px 0px 0px 0px !important;
                            margin: 0 0 1.5em 0 !important;

                        }

                        legend.scheduler-border {
                            font-size: 1.2em !important;
                            font-weight: bold !important;
                            text-align: center !important;
                            width:auto;
                            padding:0px 0px 0px 0px;
                            border-bottom:none;
                        }
                    </style>
                    <?php
                    $ara = array(
                        '0' => dateFormateYmd($month_to),
                        '1' => dateFormateYmd($month_from),
                    );
                    
                    ?>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border" align="center">
                            <?= $consigneeObject->consig_name; ?><br/>
                            <?= $consigneeObject->consig_address; ?><br/>
                            <b>Ledger Monthly Reports</b><br/>
                            <span style="font-size:15px;"><?= datemonthname($ara[0]) . ' - ' . datemonthname($ara[1]); ?></span>

                        </legend>
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>View Invoice</th>				    
                                    <th>Invoice Number</th>
                                    <th>Vehicle Number</th>
                                    <th>Date of Supply</th>
                                    <th>GST %</th>
                                    <th>GST Amount</th>
                                    <th>Sub Total</th>
                                    <th>Total Amount</th>
                                    <th>Qty/weight</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $taotalsubt = 0;
                                 
                                $fetch = array(
                                    'invoice_consig' => $consigneeObject->consig_id
                                );
                                $invoiceObjects = $this->consignee_model->viewRecordAnyRAll($fetch, $ara[0], $ara[1]);
                                $i = 1;
                                $taotalkg1 = 0;
                                $grandtoatal1 = 0;
                                foreach ($invoiceObjects as $pO) :
                                    ?>
                                    <tr class="deleteInvoicetrV<?= $pO->invoice_id; ?>">
                                        <td><?= $i++; ?></td>
                                        <td><a href="<?= ADMINC; ?>invoices/invoice_show_list/<?= $pO->invoice_slug; ?>" target="_blank"><i class="fa fa-book" aria-hidden="true"></i></a></td>
                                        <td><?= $pO->invoice_number; ?></td>
                                        <td><?= $pO->invoice_vehicle_no; ?></td>
                                        <td><?= dateMonth($pO->invoice_dos); ?></td>


                                        <?php
                                        $fetchParticlearray1 = array(
                                            'itemInvoice_invoice' => $pO->invoice_id
                                        );
                                        $fetchParticle1 = $this->itemtypeInvoice_model->viewRecordAnyR($fetchParticlearray1);
                                        $taotal = 0;
                                        $taotalrate = 0;

                                        $taotalkg = '';
                                        $itemunit = '';
                                        foreach ($fetchParticle1 as $tr):
                                            $fetchParticlearray = array(
                                                'item_id' => $tr->itemInvoice_name
                                            );
                                            $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                            ?>
                                            <?php
                                            $taotal1 = $tr->itemInvoice_kg * $tr->itemInvoice_rate;
                                            //echo inrcurr($taotal1);
                                            $taotal += $tr->itemInvoice_kg * $tr->itemInvoice_rate;
                                            $taotalsubt += $tr->itemInvoice_kg * $tr->itemInvoice_rate;
                                            $taotalrate += $tr->itemInvoice_rate;
                                            $taotalkg = $tr->itemInvoice_kg;
                                            $itemunit = $fetchParticle->item_unit;
                                            ?>
                                        <?php endforeach; ?>
                                        <?php
                                        $cgest = $this->gst_model->commongstId($pO->invoice_cgst);
                                        $sgest = $this->gst_model->commongstId($pO->invoice_sgst);
                                        $igest = $this->gst_model->commongstId($pO->invoice_igst);
                                        ?>
                                        <td align="right">
                                            <?php
                                            echo!empty($cgest) ? $cgest . " %" : '';
                                            echo!empty($sgest) ? $sgest . " %" : '';
                                            echo!empty($igest) ? $igest . " %" : '';

                                            $aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
                                            $aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';
                                            $aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
                                            ?>
                                        </td>
                                        <td align="right">
                                            <i class="fa fa-inr"></i>  <?php
                                        echo!empty($cgest) ? $taotal * $cgest / 100 : '';
                                        echo!empty($sgest) ? $taotal * $sgest / 100 : '';
                                        echo!empty($igest) ? $taotal * $igest / 100 : '';

                                        $subtotal1 = !empty($cgest) ? $taotal * $cgest / 100 : '';
                                        $subtotal2 = !empty($sgest) ? $taotal * $sgest / 100 : '';
                                        $subtotal3 = !empty($igest) ? $taotal * $igest / 100 : '';

                                        $grandtoatalsub += ($subtotal3 + $subtotal2 + $subtotal1);
                                            ?></td>

                                        <td align="right"><i class="fa fa-inr"></i>  
    <?= inrcurr($taotal); ?>

                                        </td>
                                        <td align="right">
                                            <i class="fa fa-inr"></i> 
    <?php
    $grandtoatal = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
    $grandtoatal2 = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
    echo inrcurr($grandtoatal);
    ?>
                                        </td>
                                        <td align="right">
                                            <?php
                                            echo $taotalkg . ' /' . $itemunit;
                                            $taotalkg1 += $taotalkg;
                                            $grandtoatal1 += $grandtoatal2;
                                            ?>
                                        </td>
                                    </tr>
                                        <?php endforeach; ?>
                                <tr>
                                    <td colspan="6" align="right"><b>Total</b></td>
                                    <td align="right"><b><i class="fa fa-inr"></i> <?php echo inrcurr($grandtoatalsub); ?></b></td>
                                    <td align="right"><b><i class="fa fa-inr"></i> <?php echo inrcurr($taotalsubt); ?></b></td>
                                    <td align="right"><b><i class="fa fa-inr"></i> <?php echo inrcurr($grandtoatal1); ?></b></td>
                                    <td align="right"><b><?= $taotalkg1 . ' /' . $itemunit; ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                    <!-- /.panel -->
                </div>


            </div>

        </div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


