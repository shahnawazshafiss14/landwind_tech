<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Sales Man Orders
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Orders</a></li>
	    <li class="active">Orders</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<div class="table-responsive">
			    <table id="example1" class="table table-hover table-bordered table-striped">
				<thead>
				    <tr>
					<th>S.No.</th>
					<th>Order Id</th>
					<th>Sales  Email</th>
					<th>State</th>
					<th>City</th>
					<th>Order Date</th>
				    </tr>
				</thead>
				<tbody>
				    <?php
				    $i = 1;
				    foreach ($productObjects as $pOm):


					$daarr = array(
					    'p_product_id' => $pOm->order_product_id
					);
					$pO = $this->product_model->viewRecordAny($daarr);
					$daarruser = array(
					    'userEmail' => $pOm->order_userName
					);
					$userDe = $this->user_m_f->viewRecordAny($daarruser);
					?>
    				    <tr class="deleteOrdertrO">
    					<td><?= $i++; ?></td>
    					<td>
    					    <a href="<?= ADMINC ?>orders/orderView/<?= $pOm->oTransationId; ?>" target="blank"><?= $pOm->oTransationId; ?></a>
    					</td>
    					<td><?= $userDe->userEmail; ?></td>
    					<td><?= $this->location_model->locationId($userDe->userState); ?></td>
    					<td><?= $this->location_model->locationId($userDe->userCity); ?></td>
    					<td><?= $pOm->oUdatedDate; ?></td>
    				    </tr>
				    <?php endforeach; ?>
				    </tfoot>
			    </table>
			</div>
		    </div>
		    <p><?php echo $links; ?></p>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>

	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
