<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>admin/user/index">Party</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>

    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('user_uploaded')) : ?>
	    <?php echo $this->session->flashdata('user_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">

                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;">Party Details</b>
                        <div class="row detailsprodod">
                            <p><span class="col-sm-5"><b>Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= htmlspecialchars_decode($userObjects->userName); ?></span></p><br/>
							<!--p><span class="col-sm-5"><b>Mobile</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->userMobile; ?></span></p><br/-->
							<!--p><span class="col-sm-5"><b>State</b><b class="pull-right">:</b></span> <span class="col-sm-7">
							<?php 
						$location_name1 = $this->location_model->locationId($userObjects->userState);
						echo $location_name1;
						?></span></p><br/-->
                            <p><span class="col-sm-5"><b>City</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->userCity; ?></span></p><br/>
                            <!--p><span class="col-sm-5"><b>Address</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->userAddress; ?></span></p><br/-->
                        </div>
                    </div>
                </div>
                <div class="">
                </div><!-- box-body -->

                <div class="box-footer">

                </div><!-- box-footer -->

            </div>
            <!-- .box -->
    </section>


</div>
<!-- .content-wrapper -->
