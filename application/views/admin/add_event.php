<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>events/eventLists">Events</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('event_uploaded')) : ?>
	    <?php echo $this->session->flashdata('event_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="eventForm" name="eventForm" action="<?= ADMINC; ?>events/addEventbyartst"  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnEvent" id="<?= !empty($eventObject->event_id) ? 'ubtnEvent' : 'btnEvent'; ?>" class="btn btn-success btn-lg" >
			    <i class="fa fa-save"></i> <?= !empty($eventObject->event_id) ? 'Update' : 'Save'; ?> </button>
                        <a href="<?= ADMINC; ?>events/eventLists"  class="btn btn-info btn-lg" title="Collapse">
                            <i class="fa fa-reply"></i> Cancel </a>
                </div>
            </div>
            <div class="box-body">

                <ul class="nav nav-tabs">
                    <li class="active">
			<a data-toggle="tab" href="#General">General</a>
		    </li>
                    <li><a data-toggle="tab" href="#Image">Image</a></li>
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">

			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Select Art Class <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <select class="form-control" name="artclassevent" id="artclassevent">
					<?php if (!empty($eventObject->category_parent_id)): ?>
    					<option value="<?= $eventObject->category_parent_id; ?>" selected><?= $this->category_model->categorybyId($eventObject->category_parent_id); ?></option>
					<?php endif; ?>
					<option value="">Select Art Class</option>
					<?php
					$arp = array(
					    'category_parent_id' => '0'
					);
					$objMenu = $this->category_model->viewRecordAnyR($arp);
					foreach ($objMenu as $val1) :
					    ?>
    					<option value="<?= $val1->category_id; ?>"><?= $val1->category_name; ?></option>
					<?php endforeach; ?>
				    </select>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Select Art Class <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <div id="artclasseventp">
					<select name="categoryevent" id="categoryevent" class="form-control">
					    <?php if (!empty($eventObject->category_id)): ?>
    					    <option value="<?= $eventObject->category_id; ?>" selected><?= $this->category_model->categorybyId($eventObject->category_id); ?></option>
					    <?php endif; ?>
					    <option>Select Main Category</option>
					</select>
				    </div>
				    <input type="hidden" name="categoryartclass_id" id="categoryartclass_id" value="<?= !empty($eventObject->category_id) ? $eventObject->category_id : ''; ?>"/>
				</div>
			    </div>

			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Venue/Location <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <div id="artclasseventp">
					<select class="selectpicker1 form-control" data-live-search="true" name="event_venue_id" id="event_venue_id">
					    <?php
					    $fetarcsi = array(
						'venue_id' => $eventObject->venue_id,
						'view_status' => '1'
					    );
					    $fetdasingle = $this->venue_model->viewRecordAny($fetarcsi);

					    if (!empty($eventObject->venue_id)):
						?>
    					    <option value="<?= $eventObject->venue_id; ?>" selected><?= $fetdasingle->venue_name . ', ' . $fetdasingle->venue_address; ?></option>
					    <?php endif; ?>
					    <option value="">Select Address</option>
					    <?php
					    $fetarc = array(
						'view_status' => '1'
					    );
					    $fetda = $this->venue_model->viewRecordAnyR($fetarc);
					    foreach ($fetda as $fevenue):
						?>
    					    <option data-tokens="<?= $fevenue->venue_name; ?>, <?= $fevenue->venue_address; ?>" value="<?= $fevenue->venue_id; ?>"><?= $fevenue->venue_name; ?>, <?= $fevenue->venue_address; ?></option>
					    <?php endforeach; ?>
					</select>
				    </div>

				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Artist <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <div id="artclasseventp">
					<select class="selectpicker1 form-control" data-live-search="true" name="artistsessionid" id="artistsessionid">
					    <?php
					    $fetarcsi = array(
						'artist_email' => $eventObject->artist_id,
						'view_status' => '1'
					    );
					    $fetdasingle = $this->artists_model->viewRecordAny($fetarcsi);

					    if (!empty($eventObject->artist_id)):
						?>
    					    <option value="<?= $eventObject->artist_id; ?>" selected><?= $fetdasingle->artist_name . ' ' . $fetdasingle->artist_lname . ', ' . $fetdasingle->artist_city; ?></option>
					    <?php endif; ?>
					    <option value="">Select Address</option>
					    <?php
					    $fetarc = array(
						'view_status' => '1'
					    );
					    $fetda = $this->artists_model->viewRecordAnyR($fetarc);
					    foreach ($fetda as $fevenue):
						?>
    					    <option data-tokens="<?= $fevenue->artist_name; ?>, <?= $fevenue->artist_city; ?>" value="<?= $fevenue->artist_email; ?>"><?= $fevenue->artist_name . ' ' . $fevenue->artist_lname . ', ' . $fevenue->artist_city; ?></option>
					    <?php endforeach; ?>
					</select>
				    </div>

				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Event Title <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="event_title_a" name="event_title_a" placeholder="Enter Event Name" value="<?= !empty($eventObject->event_title) ? $eventObject->event_title : '';
					    ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Event Slug <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="event_slug_a" name="event_slug_a" placeholder="Enter Event Slug" value="<?= !empty($eventObject->event_slug) ? $eventObject->event_slug : '';
					    ?>" required>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Ticket Price <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control price" id="event_price_a" name="event_price_a" placeholder="Event Price" value="<?= !empty($eventObject->event_price) ? $eventObject->event_price : '' ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_title">Event number of tickets  <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control number" id="event_ticket_a" name="event_ticket_a" placeholder="Event Ticket" value="<?= !empty($eventObject->event_ticket) ? $eventObject->event_ticket : '' ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_desc">Event Description <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <textarea class="form-control ckeditor" id="event_desc_a" name="event_desc_a"> <?= !empty($eventObject->event_desc) ? $eventObject->event_desc : ''; ?></textarea>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_start_date">Event Date <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <div class="input-group date" data-provide="datepicker">
					<input type="text" class="form-control" name="event_start_date_a" id="event_start_date_a" value="<?= !empty($eventObject->event_start_date) ? $eventObject->event_start_date : '';
					    ?>">
					<div class="input-group-addon">
					    <span class="glyphicon glyphicon-th"></span>
					</div>
				    </div>

				</div>
			    </div>
			    <input type="hidden" name="eventEdit" id="eventEdit" value="<?= !empty($eventObject->event_id) ? $eventObject->event_id : ''; ?>" />
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_end_date">Event Start Time<b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <div class="input-group bootstrap-timepicker">
					<input class="input-small timepicker1" id="event_time" name="event_time" value="<?= !empty($eventObject->event_time) ? $eventObject->event_time : ''; ?>">
					<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
				    </div>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_end_date">Event End Time<b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <div class="input-group bootstrap-timepicker">
					<input class="input-small timepicker1" id="event_end_date_a" name="event_end_date_a" value="<?= !empty($eventObject->event_end_date) ? $eventObject->event_end_date : ''; ?>">
					<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
				    </div>
				</div>
			    </div>

			</div>
			<div id="Image" class="tab-pane fade">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="event_image">Image <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <?php
				    if (!empty($eventObject->event_image)):
					?>
    				    <img src="<?= base_url(); ?>assests/event/<?= $eventObject->event_image ?>" alt="" width="200px" height="200px">
    				    <input type="hidden" id="event_images_a" name="event_images_a" value="<?= $eventObject->event_image; ?>">
				    <?php else: ?>
    				    <input type="file" id="event_images_a" name="event_images_a">
				    <?php endif; ?>

				</div>
			    </div>
			</div>
		    </div>

		    </form>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
		</div>
		<!-- /.box-footer-->
	    </div>

	    <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
