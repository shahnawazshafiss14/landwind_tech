<header class="main-header">
    <!-- Logo -->
    <a href="<?= ADMINC; ?>dashboard/index" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>A</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="<?= COMPANYLOGO; ?>" alt=""></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="notifications-menu">
                    <a href="<?= ADMINC; ?>index/logout"><i class="fa fa-sign-out"></i> Logout</a>
                </li>
            </ul>
        </div>
    </nav>
</header>


<!----Show Order Details--->
<div class="showinvicebg1" id="showinvicebg1">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" id="panel-showinvoice">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span style="font-size:18px;letter-spacing:1px">All Orders Dispatched List</span> <i class="pull-right closeshowinvicebg" onclick="hide_obj('.showinvicebg1');">&times;</i>
                    </div>
                    <div class="panel-body" id="invoice-body">
                        <ol class="progtrckr" data-progtrckr-steps="3">
                            <li class="progtrckr-done">Order Processing</li>
                            <li class="progtrckr-done">Shipped</li>
                            <li class="progtrckr-todo">Delivered</li>
                        </ol>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="track_order">
                                    <p id="orderDataProgracee2"></p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="track_order">
                                    <p id="orderDataProgracee3"></p>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <form action="" method="POST" name="messageForm1" id="messageForm1">
                                    <div class="form-group">
                                        <label>Provider Information For This Order</label>
                                        <select name="messageType1" id="messageType1" class="form-control">
                                            <option value="0">Select Action</option>
                                            <option value="3">Delivered</option>
                                            <option value="1">Cancel</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="messagereg1" id="messagereg1" class="form-control">
                                            <option value="0">Select reason</option>
                                            <option value="1">The delivery is delayed</option>
                                            <option value="2">Bought it from somewhere else</option>
                                            <option value="3">Order placed by mistake</option>
                                            <option value="4">Expected delivery time is too long</option>
                                            <option value="5">Item Price/shipping cost is too high</option>
                                            <option value="6">My reason is not listed</option>
                                            <option value="7">Need to change shipping address</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label>Why did You Decide to Cancel</label>
                                        <input id="messageBody1" class="form-control" name="messageBody1" value="">
                                    </div>
                                </form>
                            </div>



                        </div>

                    </div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-default" onclick="hide_obj('.showinvicebg1');">Close</button>
                        <button type="button" class="btn btn-success pull-right delivered" onclick="hide_obj('.showinvicebg1');">Update</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>



<!----Cancel Details--->
<div class="showinvicebg2" id="showinvicebg2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" id="panel-showinvoice">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span style="font-size:18px;letter-spacing:1px">All Order Details List</span> <i class="pull-right closeshowinvicebg" onclick="hide_obj('.showinvicebg2');">&times;</i>
                    </div>
                    <div class="panel-body" id="invoice-body">
                        <ol class="progtrckr" data-progtrckr-steps="3">
                            <li class="progtrckr-done" id="check-right1">Order Processing</li>
                            <li class="progtrckr-todo" id="check-right2">Shipped</li>
                            <li class="progtrckr-todo" id="check-right3">Delivered</li>

                        </ol>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="track_order">
                                    <p id="orderDataProgracee1"></p>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <form action="" method="POST" name="messageForm" id="messageForm">
                                    <div class="form-group">
                                        <label>Provider Cancellation Information For This Order</label>
                                        <select name="messagereg" id="messagereg" class="form-control">
                                            <option value="0">Select reason</option>
                                            <option value="1">The delivery is delayed</option>
                                            <option value="2">Bought it from somewhere else</option>
                                            <option value="3">Order placed by mistake</option>
                                            <option value="4">Expected delivery time is too long</option>
                                            <option value="5">Item Price/shipping cost is too high</option>
                                            <option value="6">My reason is not listed</option>
                                            <option value="7">Need to change shipping address</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Why did You Decide to Cancel</label>
                                        <input id="messageBody" class="form-control" name="messageBody" value="">
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-default"  onclick="hide_obj('.showinvicebg2');">Close</button>
                        <button type="button" class="btn btn-success pull-right cancelorderli">Update</button>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>



<!---ORDER COMPLETE--->

<div class="showinvicebg3" id="showinvicebg3">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" id="panel-showinvoice">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span style="font-size:18px;letter-spacing:1px">All Order Complete Status List</span> <i class="pull-right closeshowinvicebg" onclick="hide_obj('.showinvicebg3');">&times;</i>
                    </div>
                    <div class="panel-body" id="invoice-body">
                        <ol class="progtrckr" data-progtrckr-steps="3">
                            <li class="progtrckr-done">Order Processing</li>
                            <li class="progtrckr-done">Shipped</li>
                            <li class="progtrckr-done">Delivered</li>
                        </ol>

                        <div class="row">
                            <div class="col-sm-3">
                                <div class="track_order">
                                    <p id="orderDataProgracee7"></p>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="track_order">
                                    <p id="orderDataProgracee8"></p>
                                </div>
                            </div>
                            <div class="col-sm-3 track_order">
                                <div class="track_order">
                                    <p id="orderDataProgracee9"></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <div class="alert alert-success">
                                    <strong>Success!</strong> Indicates a successful or positive action.
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-default" onclick="hide_obj('.showinvicebg3');">Close</button>
                        <!--button type="button" class="btn btn-success pull-right delivered" onclick="closeshowe()">Update</button-->
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!--SHOW CANCEL FORM-->

<div class="show_cancel_order_form" id="show_cancel_order_form">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" id="panel-showinvoice">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span style="font-size:18px;letter-spacing:1px">Order Cancel status </span> <i class="pull-right closeshowinvicebg" onclick="hide_obj('.show_cancel_order_form');">&times;</i>
                    </div>
                    <div class="panel-body" id="invoice-body">
                        <ol class="progtrckr" data-progtrckr-steps="3">
                            <li class="progtrckr-done">Order Processing</li>
                            <li class="progtrckr-done">Shipped</li>
                            <li class="progtrckr-done">Delivered</li>
                        </ol>

                        <div class="row">
                            <div class="col-sm-3 track_order">
                                <p class="label label-success" id="orderDataProgracee10"></p>
                            </div>
                            <div class="col-sm-3 track_order">
                                <p class="label label-success" id="orderDataProgracee11"></p>
                            </div>
                            <div class="col-sm-3 track_order">
                                <p class="label label-success" id="orderDataProgracee12"></p>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                                <div class="alert alert-danger">
                                    <strong>Success!</strong> Indicates a successful or positive action.
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-default"  onclick="hide_obj('.show_cancel_order_form');">Close</button>
                        <!--button type="button" class="btn btn-success pull-right delivered"  onclick="hide_obj('.show_cancel_order_form');">Update</button-->
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>



<!----SHOW INVOICE--->
<div class="showinvicebg" id="showinvicebg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12" id="panel-showinvoice">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span style="font-size:18px;letter-spacing:1px">INVOICE</span> <i class="pull-right closeshowinvicebg" onclick="closeshowinvoice()">&times;</i>

                    </div>
                    <div class="panel-body" id="invoice-body">
                        <div class="table-responsive" id="print_area">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="2" style="text-align:left;background-color:#ccc;"> Invoice Number: <span id="productuniteivoiceinv"></span</th>
                                        <th colspan="4" style="text-align:right;background-color:#ccc;"> Order Number: PMYART100<span id="order_id"></span><br/>Date: <?= date('Y-m-d H:i:s'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2"><b><span>
                                                    From:<br/>
                                                    Pickup Address: <span><?= COMPANYNAME; ?></span><br/>
                                                    Pin: <span>110036</span><br/>
                                                    Mobile: <span><?= COMPANYMOBILE; ?></span><br/>
                                                    Email: <span><?= COMPANYINFO; ?></span>
                                                    </td>

                                                    <td colspan="4" style="border-left:1px solid #ccc;text-align:left ;">
                                                        To:<br/>
                                                        <b>Name: </b><span id="sName"></span><br/>
                                                        <b>Address: </b><span id="sAddress"></span></b><br/>
                                                        <b>City: </b><span id="sCity"></span><br/>
                                                        <b>State: </b>
							<span id="sState"></span>
							<?php
							//$html = file_get_html('<span id="sStatev"></span>');
							//echo $this->location_model->locationId($html);
							?>
							<br/>
                                                        <b>Zipcode: </b><span id="sZipcode"></span><br/>
                                                        <b>Mobile: </b><span id="sMobile"></span><br/>
                                                        <b>Email: </b><span id="sEmail"></span>
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" style="text-align:left;">Product Name</th>
                                                        <!--th style="text-align:left;">SKU Code</th-->
                                                        <th style="text-align:left;">Quantity</th>
                                                        <th style="text-align:left;">Unit Price</th>
                                                        <th style="text-align:left;">Total</th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="text-align:left;"><b><span id="productnameivoice"></span></b></td>
                                                        <!--td style="text-align:right;"><span id="productmodelivoice"></span></td-->
                                                        <td style="text-align:right;"><span id="productqtyivoice"></span></td>
                                                        <td style="text-align:left;"> <span id="productuniteivoice"></span><br/></td>
                                                        <td style="text-align:left;"> <span class="productsubtotalivoice"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"  style="text-align:right;"><b>Sub-Total</b></td>
                                                        <td colspan="1">$ <span class="productsubtotalivoice"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align:right;"><b>Flat Shipping Rate<b></td>
                                                                    <td colspan="1" ><b></b>$ <span id="productshipedivoice"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" style="text-align:right;"><b>Tax <b></td>
                                                                                    <td colspan="1" ><b></b>$ <span id="productsubtaxivoice"></span></td>
                                                                                    </tr>

										    <tr>
											<td colspan="4" style="text-align:right;"><b>Total</b></td>
											<td colspan="1">$ <span id="productgrandivoice"></span></td>
										    </tr>
										    <tr>
											<td colspan="5" style="text-align:center;"><b>Happy to serve you <?= COMPANYNAME; ?></b></td>
										    </tr>
										    </tbody>
										    </table>
										    </div>
										    </div>
										    <div class="panel-footer">
											<button type="button" class="btn btn-warning" onclick="closeshowinvoice()">Close</button>

											<button type="button" class="btn btn-success pull-right printinvoice" onclick="printDiv('print_area')"><i class="fa fa-print"></i> Print</button>

										    </div>

										    </div>

										    </div>
										    </div>
										    </div>
										    </div>