<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
	<!-- Sidebar user panel -->
	<div class="user-panel">
	    <div class="pull-left image">
		<img src="<?= base_url(); ?>assests/dist/img/user2-160x160.png" class="img-circle">
	    </div>
	    <div class="pull-left info">
		<p>
		    <?php
		    $getEamil = $this->session->userdata('userName_sess');
		    $dataArraym = array('userEmail' => $getEamil);
		    $users1 = $this->user_m_f->viewRecordAny($dataArraym);
		    echo ucfirst($users1->userName) . ' ' . ucfirst($users1->lastName);
                    $get_url_active = $this->uri->segment(2);
		    ?>
		</p>
		<a href="#"><i class="fa fa-circle text-success"></i> Online </a>
	    </div>
	</div>
	<ul class="sidebar-menu" data-widget="tree">
	    <li>
		<a href="<?= ADMINC; ?>dashboard/index">
		    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	    </li>
	    <?php
	    $getEamil = $this->session->userdata('userName_sess');
	    $dataArraym = array('userEmail' => $getEamil);
	    $dataUser = $this->user_m_f->viewRecordAny($dataArraym);
//	    $dataArray = array(
//		'menu_parent_id' => '0',
//		'status' => '0'
//	    );
	    // $dataMenu = $this->menu_model->viewRecordAnyR($dataArray);

	    $dataArray = array(
		'menu_parent_id' => '0',
		'user_id' => $dataUser->u_id
	    );

	    $dataMenu = $this->permissionmenu_model->viewRecordIdUserId($dataArray);
	    foreach ($dataMenu as $dm):
		?>
            <li class="treeview <?= strtolower($dm->menu_name) == strtolower($get_url_active) ? 'menu-open' :'';?>">
    		<a href="javascript:;">
    		    <i class="fa fa fa-bars"></i>
    		    <span><?= $dm->menu_name; ?></span>
    		    <span class="pull-right-container">
    			<i class="fa fa-angle-left pull-right"></i>
    		    </span>
    		</a>

                <ul class="treeview-menu" style="display:<?= strtolower($dm->menu_name) == strtolower($get_url_active) ? 'block' :'none';?>">
			<?php
			$dataArray1 = array(
			    'menu_parent_id' => $dm->menu_id,
			    'user_id' => $dataUser->u_id,
				//'status' => '0'
			);
			$dataMenu1 = $this->permissionmenu_model->viewRecordIdUserId($dataArray1);
			//$dataMenu1 = $this->menu_model->viewRecordAnyR($dataArray1);
			foreach ($dataMenu1 as $dm1):
			    ?>
			    <li><a href="<?= ADMINC . $dm1->link; ?>"><i class="fa fa-plus-circle"></i> <?= $dm1->menu_name; ?></a></li>
				<?php
			    endforeach;
			    ?>
    		</ul>

    	    </li>
	    <?php endforeach; ?>
	    <li>
		<a href="<?= ADMINC; ?>users/admindetails/<?= $dataUser->u_id; ?>">Profile</a>
	    </li>
	</ul>
    </section>
    <!-- /.sidebar -->
</aside>