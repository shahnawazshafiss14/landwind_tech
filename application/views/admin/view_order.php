<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Orders New
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Orders</a></li>
	    <li class="active">Orders New</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<div class="table-responsive">
			    <table id="example1" class="table table-hover table-bordered table-striped">
				<thead>
				    <tr>
					<th>S.No.</th>
					<th>Images</th>
					<th>Product Name</th>
					<th>Model</th>
					<th>Color</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Order Date</th>
					<th>Status</th>
					<th>Action</th>
				    </tr>
				</thead>
				<tbody>
				    <?php
				    $i = 1;
				    foreach ($productObjects as $pOm) :
					$daarr = array(
					    'p_product_id' => $pOm->order_product_id
					);
					$pO = $this->product_model->viewRecordAny($daarr);
					?>
    				    <tr class="deleteOrdertrO">
    					<td><?= $i++; ?></td>
    					<td>
    					    <a href="<?= ADMINC ?>product/product_show_list/<?= $pO->p_product_id; ?>" target="blank">
    						<img src="<?= ADMINPRODUCTIMG . $pO->p_image; ?>" alt="100%50" width="100" height="50"></a>
    					</td>
    					<td><?= htmlspecialchars_decode($pO->p_title); ?></td>
    					<td><?= $this->models_model->modelId($pO->p_model); ?></td>
    					<td><?= $this->colors_model->colorId($pO->p_color); ?></td>
    					<td><?= $pOm->order_price; ?></td>
    					<td><?= $pOm->order_qty; ?></td>
    					<td><?= $pOm->oUdatedDate; ?></td>
    					<td>

						<?php
						$starray = array('0' => 'Processing', '1' => 'Cancel', '2' => 'Dispatched', '3' => 'Completed');
						echo $starray[$pOm->oStatus];
						?>

    					</td>
    					<td>
						<?php
						if ($pOm->oStatus == 3):

						    echo $pOm->ocomDate
						    ?>

						<?php else: ?>
						    <a href="javascript:;" class="orderPro" data-usersid="<?= $getEamil; ?>" data-stut="completed" data-opro="<?= $pOm->order_id; ?>" data-pid="<?= $pO->p_product_id; ?>" data-qty="<?= $pOm->order_qty; ?>">
							<i class="fa fa-edit"></i>
							Received
						    </a>

						<?php endif; ?>
    					</td>
    				    </tr>
				    <?php endforeach; ?>
				    </tfoot>
			    </table>
			</div>
		    </div>
		    <p><?php echo $links; ?></p>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>

	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
