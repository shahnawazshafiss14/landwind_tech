<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Colors List<small>Manage View Color</small>
	</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>colors/colorList">Color</a></li>
	    <li class="active">List Color</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Color Name</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($colorobjects as $color):
				    ?>
    				<tr class="deleteColorsstrU<?= $color->color_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $color->color_name; ?></td>
    				    <td>
    					<a href="<?= base_url(); ?>colors/index/<?= $color->color_id; ?>" ><i class="fa fa-edit"></i></a> | <a href="javascript:;" class="deleteColortruuuu" data-id="<?= $color->color_id; ?>"><i class="fa fa-trash"></i></a></td>
    				</tr>
				<?php endforeach; ?>
			    </tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
