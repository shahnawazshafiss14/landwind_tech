<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Pages List<small>Manage View Page</small></h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Page</a></li>
	    <li class="active">Page List</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Page Title</th>
				    <th>Status</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($pageObject as $pO) :
				    ?>
    				<tr class="deleteLocationtrpage<?= $pO->page_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $pO->page_title; ?></td>
    				    <td><?= $pO->status; ?></td>
    				    <td>
						<a href="javascript:;" class="deleteRcordPage" data-id="<?= $pO->page_id; ?>">
						<i class="fa fa-trash"></i></a> | <a href="<?= base_url(); ?>pages/index/<?= $pO->page_id; ?>" id=""><i class="fa fa-edit"></i></a></td>
    				</tr>
				<?php endforeach; ?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
