<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>transport/billList">View Bill</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('transport_uploaded')) : ?>
            <?php echo $this->session->flashdata('transport_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="transForm" name="transForm" action="<?= ADMINC; ?>transport/addTrans"  method="POST" enctype="multipart/form-data">
                        <?php
                        $checkm = checkpermission('179');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
                            <button type="submit" name="btnTrans" class="btn btn-success btn-lg" id="<?= !empty($transObject->trans_id) ? 'btnTrans' : 'btnTrans'; ?>" title="Save bill">
                                <i class="fa fa-save"></i> <?= !empty($transObject->trans_id) ? 'Update' : 'Save'; ?> </button>
                            <a href="<?= ADMINC; ?>transport/transport_show_list/<?= !empty($transObject->trans_slug) ? $transObject->trans_slug : ''; ?>" title="Back to details" class="btn btn-info btn-lg" ><i class="fa fa-reply"></i> Cancel </a>
                        <?php endif; ?>
                </div>
            </div>
            <div class="box-body">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#General">General</a>
                    </li>
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <?php
                        $ass = $this->transport_model->lastrecordId();
                        $invoadd = $ass->trans_challan_no + 1;
                        ?>
                        <div id="General" class="tab-pane fade in active">
                            <input name="edittrans" id="edittrans" type="hidden" class="form-control" value="<?= !empty($transObject->trans_slug) ? $transObject->trans_slug : ''; ?>">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    Challan Number <b style="color:#f00;">*</b><br/>
                                    <input name="challan_no" id="challan_no" type="text" class="form-control" value="<?= !empty($transObject->trans_challan_no) ? $transObject->trans_challan_no : $invoadd; ?>">
                                </div>
                                <div class="col-sm-3">
                                    From <b style="color:#f00;">*</b><br/>
                                    <input name="trans_from" id="trans_from" type="text" class="form-control" value="<?= !empty($transObject->trans_from) ? $transObject->trans_from : 'SIWAN'; ?>">
                                </div>
                                <div class="col-sm-3">
                                    To <b style="color:#f00;">*</b><br/>
                                    <input name="trans_to" id="trans_to" type="text" class="form-control" value="<?= !empty($transObject->trans_to) ? $transObject->trans_to : 'BAREILY '; ?>">
                                </div> 
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="doc_id" id="doc_id" value="<?= !empty($transObject->trans_name_consignee) ? $transObject->trans_name_consignee : ''; ?>" />
                                <div class="col-md-4">
                                    Details of Consignee (Shipped to) <b style="color:#f00;">*</b><br/>
                                    <select class="selectpicker1" data-live-search="true" id="doc_receiver_id" style="width:100%;">
                                        <?php
                                        $fetarcsi1 = array(
                                            'consig_id' => $transObject->trans_name_consignee,
                                            'view_status' => '1',
                                            'consig_type' => '0'
                                        );
                                        $fetdasingle1 = $this->consignee_model->viewRecordAny($fetarcsi1);
                                        if (!empty($transObject->trans_name_consignee)):
                                            ?>
                                            <option value="<?= $transObject->trans_name_consignee; ?>" selected><?= $fetdasingle1->consig_name . ', ' . $fetdasingle1->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Consignee</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '0'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <a class="btn btn-primary" href="<?= ADMINC; ?>consignee/edit" title="Add Details of Consignee (Shipped to)"><i class="fa fa-plus"></i></a>
                                </div>
                                <input type="hidden" name="dor_id" id="dor_id" value="<?= !empty($transObject->trans_name_consigntor) ? $transObject->trans_name_consigntor : ''; ?>" />
                                <div class="col-md-4">
                                    Details of Consigner (Bill to) <b style="color:#f00;">*</b><br/>
                                    <select class="selectpicker1" data-live-search="true" id="dor_receiver_id" style="width:100%;">
                                        <?php
                                        $fetarcsi1 = array(
                                            'consig_id' => $transObject->trans_name_consigntor,
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetdasingle1 = $this->consignee_model->viewRecordAny($fetarcsi1);
                                        if (!empty($transObject->trans_name_consigntor)):
                                            ?>
                                            <option value="<?= $transObject->trans_name_consigntor; ?>" selected><?= $fetdasingle1->consig_name . ', ' . $fetdasingle1->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Consigner</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <a class="btn btn-primary" href="<?= ADMINC; ?>consignee/edit" title="Add Details of Consignee (Shipped to)"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    Truck Number <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="truck_no" id="truck_no" value="<?= !empty($transObject->trans_truck_no) ? $transObject->trans_truck_no : ''; ?>">
                                </div>
                                <div class="col-sm-3">
                                    Truck Owner Name <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="owner_name" id="owner_name" value="<?= !empty($transObject->trans_owner_name) ? $transObject->trans_owner_name : ''; ?>">
                                </div>
                                <div class="col-sm-6">
                                    Truck Owner Address <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="owner_address" id="owner_address" value="<?= !empty($transObject->trans_owner_address) ? $transObject->trans_owner_address : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    Driver's Name <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="driver_name" id="driver_name" value="<?= !empty($transObject->trans_driver_name) ? $transObject->trans_driver_name : ''; ?>">
                                </div>
                                <div class="col-sm-3">
                                    Driver's Licence Number <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="driver_licence" id="driver_licence" value="<?= !empty($transObject->trans_driver_licence) ? $transObject->trans_driver_licence : ''; ?>">
                                </div>
                                <div class="col-sm-6">
                                    Driver's Address <b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="driver_address" id="driver_address" value="<?= !empty($transObject->trans_driver_address) ? $transObject->trans_driver_address : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    Date<b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="trans_dos" id="trans_dos" value="<?= !empty($transObject->trans_dos) ? $transObject->trans_dos : date('Y-m-d'); ?>">
                                </div>
                                <div class="col-sm-2">
                                    Place<b style="color:#f00;">*</b><br/>
                                    <input type="text" class="form-control" name="trans_pos" id="trans_pos" value="<?= !empty($transObject->trans_pos) ? $transObject->trans_pos : 'Siwan'; ?>">
                                </div>
                                <div class="col-sm-2">
                                    Fare<br/> 
                                    <input type="text" class="form-control" name="t_amount" id="t_amount" value="<?= !empty($transObject->trans_amount) ? $transObject->trans_amount : '0.00'; ?>">
                                </div>
                                <div class="col-sm-2">
                                    Advance Amount<br/>
                                    <input type="text" class="form-control" name="t_advance" id="t_advance" value="<?= !empty($transObject->trans_advance) ? $transObject->trans_advance : '0.00'; ?>">
                                </div>
                            </div>
                            <div id="insertcol" class="removerow">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="item_table">
                                        <tr>
                                            <th>Select Particulars</th>
                                            <th>Kg / Qty</th>
                                            <th>Action
                                                <!--button type="button" name="add" class="btn btn-success add"><i class="fa fa-plus"></i></button-->
                                            </th>
                                        </tr>
                                        <?php
                                        if (!empty($transObject->trans_slug)):
                                            $fetchParticlearray1 = array(
                                                'itemTrans_trans' => $transObject->trans_id
                                            );
                                            $fetchParticle1 = $this->itemtypetrans_model->viewRecordAnyR($fetchParticlearray1);
                                            foreach ($fetchParticle1 as $tr):
                                                $fetchParticlearray = array(
                                                    'item_id' => $tr->itemTrans_name
                                                );
                                                $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                                ?>
                                                <tr class="deletetransitemtr<?= $tr->itemTrans_id; ?>">
                                                    <td>
                                                        <select class="form-control particuler_id" name="particuler_id[]">
                                                            <?php
                                                            if (!empty($tr->itemTrans_name)):
                                                                ?>
                                                                <option value="<?= $tr->itemTrans_name; ?>" selected><?= $this->itemtype_model->itemId($tr->itemTrans_name, 'item_particular_name') . ', ' . $this->itemtype_model->itemId($tr->itemTrans_name, 'item_hsn'); ?></option>
                                                            <?php endif; ?>

                                                            <option value="">Select Particulars</option>
                                                            <?= itemtype(); ?>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" name="trans_unit[]" class="form-control trans_unit" value="<?= $tr->itemTrans_unit; ?>"></td>
                                                    <td>
                                                        <a href="javascript:;" class="btn btn-danger deletetransitem" data-id="<?= $tr->itemTrans_id; ?>"><i class="fa fa-remove"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="particuler_id" id="particuler_idup">
                                                        <option value="">Select Particulars</option>
                                                        <?= itemtype(); ?>
                                                    </select>
                                                </td>
                                                <td><input type="text" name="trans_unit" id="trans_unit" class="form-control trans_unit" value=""></td>
                                                <td><a class="btn btn-primary" id="transitemupadd" data-id="<?= $transObject->trans_id; ?>">Add</a></td>
                                            </tr>
                                        <?php else: ?>
                                            <tr>
                                                <td>
                                                    <select class="form-control particuler_id" name="particuler_id[]"><option value="">Select Particulars</option><?= itemtype(); ?></select></td>
                                                <td><input type="text" name="trans_unit[]" class="form-control trans_unit"></td>
                                                <td>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control particuler_id" name="particuler_id[]"><option value="">Select Particulars</option><?= itemtype(); ?></select></td>
                                                <td><input type="text" name="trans_unit[]" class="form-control trans_unit"></td>
                                                <td>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control particuler_id" name="particuler_id[]"><option value="">Select Particulars</option><?= itemtype(); ?></select></td>
                                                <td><input type="text" name="trans_unit[]" class="form-control trans_unit"></td>
                                                <td>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control particuler_id" name="particuler_id[]"><option value="">Select Particulars</option><?= itemtype(); ?></select></td>
                                                <td><input type="text" name="trans_unit[]" class="form-control trans_unit"></td>
                                                <td>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control particuler_id" name="particuler_id[]"><option value="">Select Particulars</option><?= itemtype(); ?></select></td>
                                                <td><input type="text" name="trans_unit[]" class="form-control trans_unit"></td>
                                                <td>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <select class="form-control particuler_id" name="particuler_id[]"><option value="">Select Particulars</option><?= itemtype(); ?></select></td>
                                                <td><input type="text" name="trans_unit[]" class="form-control trans_unit"></td>
                                                <td>

                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>

                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>

            <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->

