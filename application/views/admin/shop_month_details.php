<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<style>
    .input-group {
        margin-bottom: 12px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
        <?php
        ?>
        <?php if ($this->session->flashdata('shop_uploaded')) : ?>
            <?php echo $this->session->flashdata('shop_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="shopid" id="shopid" value="<?= !empty($shopObject->shop_id) ? $shopObject->shop_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <a href="javascript:;" class="btn btn-primary" onclick="printDiv('print_area1')"><i class="fa fa-print"></i> Print Invoice</a>
                    <button title="Refresh" class="btn btn-success" id="btnitemrefere"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
            <div class="box-body">

                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row" style="margin-top: 50px;margin-bottom: 50px;">
                                <form name="frmgenratebill" id="frmgenratebill" action="<?= ADMINC; ?>shop/shop_month/<?= $shopObject->shop_slug; ?>" method="get">
                                    <table class="table table-hover table-bordered table-striped">
                                        <thead>
                                        <th>From</th>
                                        <th>To</th>	
                                        <th>Action</th>
                                        </thead>
                                        <tbody>
                                        <td><input type="text" id="from" name="from" class="form-control" value="<?= !empty($month_to) ? $month_to : ''; ?>"></td>
                                        <td><input type="text" id="to" name="to" class="form-control" value="<?= !empty($month_to) ? $month_from : ''; ?>"></td>
                                        <td><input type="submit" class="btn btn-success"></td>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-12" id="print_area1">
                    <style>
                        fieldset.scheduler-border {
                            border: 3px solid #ddd !important;
                            padding: 0px 0px 0px 0px !important;
                            margin: 0 0 1.5em 0 !important;

                        }

                        legend.scheduler-border {
                            font-size: 1.2em !important;
                            font-weight: bold !important;
                            text-align: center !important;
                            width:auto;
                            padding:0px 0px 0px 0px;
                            border-bottom:none;
                        }
                    </style>
                    <?php
                    $ara = array(
                        '0' => dateFormateYmd($month_to),
                        '1' => dateFormateYmd($month_from),
                    );

                    $fetch = array(
                        'items_shop_id' => $shopObject->shop_id
                    );
                    $totaldatepitmeprice = $this->shop_model->find_date_all_sum($fetch);

                    $itemsObjects = $this->shop_model->viewRecordAnyRAll($fetch, $ara[0], $ara[1]);
                    $i = 1;
                    $grandtoatal1 = 0;
                    $toalpurchased = 0;
                    $toalspayment = 0;
                    $toalspaymentdr = 0;
                    ?>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border" align="center">
                            <?= $shopObject->shop_name; ?><br/>
                            <?= $shopObject->shop_address; ?>, <?= $shopObject->shop_state; ?>, <?= $shopObject->shop_city; ?><br/>
                            <b>Ledger Monthly Reports</b><br/>
                            <span style="font-size:15px;"><?= datemonthname($ara[0]) . ' - ' . datemonthname($ara[1]); ?></span>

                        </legend>
                        <table class="table table-hover table-bordered table-striped">
                            <tr>
                                <th>Date<br/><span style="border-top: 1px solid black">Vehicle Number</span></th>
                                <th>Particular Name</th>
                                <th style="text-align:right">Bill no.</th>
                                <th style="text-align:right">Debit</th>
                                <th style="text-align:right">Credit</th>
                            </tr>

                            <tbody>
                                <?php
                                foreach ($itemsObjects as $pO) :
                                    $fetchParticlearray1 = array(
                                        'purchedItem' => $pO->items_id
                                    );
                                    $fetchParticlegr = $this->pitems_model->viewRecordPageRG($fetchParticlearray1, 'purchedItem');
                                    $fetchParticle1 = $this->pitems_model->viewRecordAnyR($fetchParticlearray1);
                                    if (count($fetchParticle1) > 0):
                                        ?>

                                        <?php
                                        $taotal = 0;
                                        $taotalrate = 0;
                                        $taotalkg = '';
                                        $itemunit = '';
                                        foreach ($fetchParticlegr as $tr):
                                            ?>
                                            <tr>
                                                <td>
                                                    <?= dateMonth($pO->items_date); ?><br/><span style="border-top: 2px solid black"><b><?= $pO->items_vehicle_id; ?></b></span>
                                                </td>
                                                <td><b>Purchased Items</b><br/>
                                                    <table>
                                                        <?php
                                                        foreach ($fetchParticle1 as $tr):
                                                            $fetchParticlearray = array(
                                                                'item_id' => $tr->pitem_particular
                                                            );
                                                            $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                                            $taotal += $tr->pitem_unit * $tr->pitem_rate;
                                                            $toalpurchased += $tr->pitem_unit * $tr->pitem_rate;
                                                            ?>
                                                            <tr style="border-bottom: 1px solid #111010;">
                                                                <td>
                                                                    <?= $fetchParticle->item_particular_name; ?> - <?= $tr->pitem_unit; ?> / <?= $fetchParticle->item_unit; ?> X <i class="fa fa-inr"></i> <?= $tr->pitem_rate; ?> =&nbsp;&nbsp;<i class="fa fa-inr"></i> <?= $tr->pitem_unit * $tr->pitem_rate; ?>
                                                                </td> 
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    </table>
                                                    <?php
                                                    if (!empty($pO->items_remarks)):
                                                        ?>
                                                        <b>Remarks</b><br/>
                                                        <?= $pO->items_remarks; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td style="text-align:right"><?= $pO->items_bill_no; ?></td>
                                                <td></td> 
                                                <td style="text-align:right"><i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($taotal); ?></td>
                                            </tr>
                                        <?php endforeach; ?>

                                    <?php endif; ?>
                                    <?php
                                    $fetchParticlearraycr = array(
                                        'purchedItem' => $pO->items_id,
                                        'strans_type' => 1
                                    );
                                    // $fetchdatat = $this->shoptrans_model->viewRecordPageRG($fetchParticlearray1, 'purchedItem');
                                    $fetchdatat = $this->shoptrans_model->viewRecordAnyR($fetchParticlearraycr);
                                    foreach ($fetchdatat as $ftt):
                                        $toalspayment += $ftt->strans_amount;
                                        ?>
                                        <tr>
                                            <td> <?= dateMonth($pO->items_date); ?><br/><span style="border-top: 2px solid black"><b><?= $pO->items_vehicle_id; ?></b></span></td>
                                            <td><b>Payment</b><br/>Description: 
                                                <?= $ftt->strans_desc; ?></td>
                                            <td style="text-align:right"><?= $pO->items_bill_no; ?></td>
                                            <td style="text-align:right"><b><i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($ftt->strans_amount); ?></b></td>
                                            <td></td>

                                        </tr>
                                    <?php endforeach; ?>
                                    <?php
                                    $fetchParticlearraydr = array(
                                        'purchedItem' => $pO->items_id,
                                        'strans_type' => 2
                                    );
                                    // $fetchdatat = $this->shoptrans_model->viewRecordPageRG($fetchParticlearray1, 'purchedItem');
                                    $fetchdatat = $this->shoptrans_model->viewRecordAnyR($fetchParticlearraydr);
                                    foreach ($fetchdatat as $ftt):
                                        $toalpurchased += $ftt->strans_amount;
                                        //$totaldatepitmeprice += $ftt->strans_amount;
                                        ?>
                                        <tr>
                                            <td> <?= dateMonth($pO->items_date); ?><br/><span style="border-top: 2px solid black"><b><?= $pO->items_vehicle_id; ?></b></span></td>
                                            <td><b>Payment</b><br/>Description: 
                                                <?= $ftt->strans_desc; ?></td>
                                            <td style="text-align:right"><?= $pO->items_bill_no; ?></td>
                                            <td></td>
                                            <td style="text-align:right"><b><i class="fa fa-inr"></i> <?= moneyFormatIndiaPHP($ftt->strans_amount); ?></b></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endforeach; ?> 
                                <tr style="background-color: #c09e9e;color: #fff;">
                                    <td colspan="3" style="text-align:right">Sub Total Amount</td>
                                    <td style="text-align:right"><b><i class="fa fa-inr"></i> <?= !empty($toalspayment) ? moneyFormatIndiaPHP($toalspayment) : '0.00'; ?></b></td>
                                    <td style="text-align:right"><b><i class="fa fa-inr"></i> <?= !empty($toalpurchased) ? moneyFormatIndiaPHP($toalpurchased) : '0.00'; ?></b></td>

                                </tr>
                                <tr style="background-color: #ee7979;color: #fff;"> 
                                    <td colspan="3" style="text-align:right">Total Amount</td>
                                    <td colspan="2" style="text-align:center;font-size: 15px;"><b><i class="fa fa-inr"></i> 
                                            <?= moneyFormatIndiaPHP($toalspayment - $toalpurchased); ?> 
                                        </b></td>

                                </tr>
                                <tr style="background-color: #ff5454;color: #fff;">
                                    <td colspan="3" style="text-align:right">Closing Balance</td>
                                    <td colspan="2" style="text-align:center;font-size: 15px;"><b><i class="fa fa-inr"></i> 
                                            <?php
                                            $current_blance = $toalspayment - $toalpurchased;
//$total = $totaldatepitmeprice + $current_blance;                                                
                                            echo moneyFormatIndiaPHP($totaldatepitmeprice - $current_blance);
//moneyFormatIndiaPHP($toalspayment - $toalpurchased) . ' / ' . nagitive_check($toalspayment - $toalpurchased, ' Pay BY: ' . COMPANYNAME, ' Pay BY: ' . $shopObject->shop_name); 
                                            ?> 
                                        </b></td>

                                </tr>
                                <tr style="background-color: #e32c2c;color: #fff;">
                                    <td colspan="3" style="text-align:right">Balanced Amount</td>
                                    <td colspan="2" style="text-align:center;font-size: 15px;"><b><i class="fa fa-inr"></i> 
                                                <?= moneyFormatIndiaPHP($totaldatepitmeprice) . ' / ' . nagitive_check($totaldatepitmeprice, ' Pay BY: ' . COMPANYNAME, ' Pay BY: ' . $shopObject->shop_name); ?> 
                                        </b></td>

                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                    <!-- /.panel -->
                </div>


            </div>

        </div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


