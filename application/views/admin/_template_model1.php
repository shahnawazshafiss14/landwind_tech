<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $head_title ?> - <?= COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assests/images/favicon.ico" type="image/x-icon">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/Ionicons/css/ionicons.min.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/plugins/iCheck/all.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- Select2 -->

        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/skins/_all-skins.min.css">
        <!-- fileuploader -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/multifile.css" type="text/css">
        <link href="<?= base_url(); ?>assests/dist/css/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url(); ?>assests/dist/css/alertify.default.css" rel="stylesheet" type="text/css"/>
         
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/rohitsingh_file_uplodaer.css" type="text/css">
        <link href="<?= base_url(); ?>assests/dist/css/custom.css" rel="stylesheet" type="text/css"/>

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php
            $this->load->view('admin/include/topmenu');
            $this->load->view('admin/include/sidemenu');
            ?>

            <?php $this->load->view($contentView); ?>

            <?php $this->load->view('admin/include/footer'); ?>

            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?= base_url(); ?>assests/components/jquery/dist/jquery.min.js"></script>
        <!--Products Slider-->
        <script>
            jQuery(document).ready(function () {
                // Products Slider
                // Can also be used with $(document).ready()
                $('.listing-slider').owlCarousel({
                    loop: true,
                    margin: 0,
                    items: 7,
                    nav: true,
                    loop: false,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2,
                        },
                        1024: {
                            items: 3,
                        },
                        1170: {
                            items: 3,
                        },
                        2500: {
                            items: 7,
                        }

                    }
                });
            });
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url(); ?>assests/components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?= base_url(); ?>assests/components/select2/dist/js/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="<?= base_url(); ?>assests/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?= base_url(); ?>assests/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?= base_url(); ?>assests/plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- date-range-picker -->
        <script src="<?= base_url(); ?>assests/components/moment/min/moment.min.js"></script>
        <script src="<?= base_url(); ?>assests/components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?= base_url(); ?>assests/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?= base_url(); ?>assests/components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?= base_url(); ?>assests/ckeditor/ckeditor.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="<?= base_url(); ?>assests/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?= base_url(); ?>assests/components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?= base_url(); ?>assests/plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="<?= base_url(); ?>assests/components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url(); ?>assests/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url(); ?>assests/dist/js/demo.js"></script>
        <!-- CK Editor -->

        <script src="<?= base_url(); ?>assests/dist/js/script.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/custom.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/alertify.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/notify.js" type="text/javascript"></script>

        <script src="<?= base_url(); ?>assests/dist/js/owl.carousel.js" type="text/javascript"></script>

        <script src="<?= base_url(); ?>assests/dist/js/jquery-ui.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/js/sweetalert2.js" type="text/javascript"></script>
        <script>
            
            
        </script>
    </body>
</html>

