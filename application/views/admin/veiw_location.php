<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Location List</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>location/viewLocation">Location</a></li>
	    <li class="active">List Location</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Location Name</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 0;
				foreach ($toplocations as $tcategory):
				    ?>
    				<tr class="deleteLocationtrL<?= $tcategory->location_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $tcategory->location_name; ?></td>
    				    <td> <a href="javascript:;" class="deleteRcordL" data-id="<?= $tcategory->location_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= base_url(); ?>location/location/<?= $tcategory->location_id; ?>" id=""><i class="fa fa-edit"></i></a> </td>
    				</tr>
				    <?php
				    $condition = array('location_parent' => $tcategory->location_id);
				    $datadd = $this->location_model->viewRecordAnyR($condition);
				    foreach ($datadd as $tcategory1):
					?>
					<tr class="deleteLocationtrL<?= $tcategory1->location_id; ?>">
					    <td><?= $i++; ?></td>
					    <td><?= $tcategory->location_name; ?> > <?= $tcategory1->location_name; ?></td>
					    <td> <a href="javascript:;" class="deleteRcordL" data-id="<?= $tcategory1->location_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= base_url(); ?>location/location/<?= $tcategory1->location_id; ?>" id=""><i class="fa fa-edit"></i></a> </td>
					</tr>
				    <?php endforeach; ?>
				<?php endforeach; ?>
			    </tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
