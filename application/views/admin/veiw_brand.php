<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
     <div class="row" style="padding: 3px 0px; background-color: #fff" >
	 
      <div class="col-md-12">
   
     <h3 class="text-left" style="margin-left: 18px"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>
            <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/brand/addbrand"> Back</a></li>
          
           
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Brand Name</th>
				    <th>Status</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($topmenus as $tmenu):
				    ?>
    				<tr class="deleteMenutr<?= $tmenu->b_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $tmenu->b_name; ?></td>
						 <td><a href="javascript:;" class="bstatus" data-bid="<?= $tmenu->b_id; ?>" data-val="<?= ($tmenu->b_status == 1) ? '0' : '1' ?>" title="<?= ($tmenu->b_status == 1) ? 'inActive' : 'Active' ?>"><span style="font-size: 20px;" class="text-<?= ($tmenu->b_status == 1) ? 'green' : 'red' ?>"><?= ($tmenu->b_status == 1) ? 'Active' : 'inActive' ?></span></a></td>
    				    <td> <!--a href="javascript:;" class="btn btn-danger deleteRcord" data-id="<?= $tmenu->b_id; ?>"><i class="fa fa-trash"></i></a> |--> <a class="btn btn-primary" href="<?= base_url(); ?>admin/brand/addbrand/<?= $tmenu->b_id; ?>" id=""><i class="fa fa-edit"></i></a> </td>
    				</tr>
				<?php endforeach; ?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
   
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
