<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Particulars List<small>Manage View Particular</small>
	</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= ADMINC; ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= ADMINC; ?>particular/modelList">Particular</a></li>
	    <li class="active">List Particular</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Particular Name</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
                                $checkm = checkpermission('163');
				$i = 1;
                                $fetch_parent_items = array(
                                    'view_status' => '1',
                                    'account_id' => $this->session->userdata('account_id'),
                                    'parent_particular' => '0'
                                );
                                $particular_parent = $this->itemtype_model->viewRecordAnyR($fetch_parent_items);
				foreach ($particular_parent as $particular):
                                   
				    ?>
    				<tr class="deleteParticularsstrU<?= $particular->item_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $particular->item_particular_name; ?></td>
    				    <td>
                                        <?php  
                                        if($checkm->is_edited == '1'):
                                        ?>
                                        <a title="Edit Particulars" class="btn btn-info" href="<?= ADMINC; ?>particular/index/<?= $particular->item_id; ?>" ><i class="fa fa-edit"></i></a> 
                                            <?php endif; if($checkm->is_deleted == '1'): ?> <a href="javascript:;" title="Edit Particulars" class="btn btn-danger deleteParticulartruuuu" data-id="<?= $particular->item_id; ?>"><i class="fa fa-trash"></i></a><?php endif; ?></td>
    				</tr>
                                <?php 
                                 $fetch_items = array(
                                    'view_status' => '1',
                                    'account_id' => $this->session->userdata('account_id'),
                                    'parent_particular' => $particular->item_id
                                );
                                $particular_item = $this->itemtype_model->viewRecordAnyR($fetch_items);
				foreach ($particular_item as $part_item):
                                ?>
                                <tr class="deleteParticularsstrU<?= $part_item->item_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $particular->item_particular_name; ?> >> <?= $part_item->item_particular_name; ?></td>
    				    <td>
                                        <?php  
                                        if($checkm->is_edited == '1'):
                                        ?>
                                        <a title="Edit Particulars" class="btn btn-info" href="<?= ADMINC; ?>particular/index/<?= $part_item->item_id; ?>" ><i class="fa fa-edit"></i></a> 
                                            <?php endif; if($checkm->is_deleted == '1'): ?> <a href="javascript:;" title="Edit Particulars" class="btn btn-danger deleteParticulartruuuu" data-id="<?= $part_item->item_id; ?>"><i class="fa fa-trash"></i></a><?php endif; ?></td>
    				</tr>
                                <?php endforeach; ?>
				<?php endforeach; ?>
			    </tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
