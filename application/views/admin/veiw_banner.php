<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Banners List<small>Manage View Banner</small>
	</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>banner/bannerlist">Banner</a></li>
	    <li class="active">List Banner</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>Number</th>
				    <th>Banner</th>
				    <th>Banner Type</th>
				    <!--th>Category Name</th-->
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 0;
				foreach ($bannerList as $bl):
				    ?>
    				<tr class="deleteBannertrB<?= $bl->banner_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><img src="<?= base_url(); ?>assests/banner/<?= $bl->banner_name; ?>" alt="" width="150" height="60"/></td>
    				    <th><?php
					    $arr = array('0' => 'Not Selected', '1' => 'Top Home Slider', '2' => 'About Us');
					    echo $arr[$bl->bType];
					    ?></th>
    				    <!--th><?php
					$arr1 = array('0' => 'Not Selected', '6' => 'Necklace', '7' => 'Earring', '8' => 'Rings', '9' => 'Braclets');
					echo $arr1[$bl->category_id];
					?></th-->
    				    <td> <a href="javascript:;" class="deleteRcordB" data-id="<?= $bl->banner_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= base_url(); ?>banner/index/<?= $bl->banner_id; ?>" id=""><i class="fa fa-edit"></i></a> </td>
    				</tr>
				<?php endforeach; ?>
			    </tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
