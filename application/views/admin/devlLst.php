<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <div class="row" style="padding: 3px 0px; background-color: #fff" >
	 
      <div class="col-md-12">
   
     <h3 class="text-left" style="margin-left: 18px"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>
            <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/devlboy/addDevboy"> Back</a></li> 
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>

	<div class="row">
	    <div class="col-xs-12">
		<div class="box">
		    
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Name</th> 
				    <th>Mobile</th>
				    <th>Status</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i =1;
				foreach ($users as $tcategory):  ?>
    				<tr class="deleteUsertrU<?= $tcategory->d_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $tcategory->devlName; ?></td> 
				    <td><?= $tcategory->devlMobile; ?></td>
					<td><?= $tcategory->devlStatus == '0' ? '<label class="label label-danger">inActive</label>' : '<label class="label label-warning">Active</label>'; ?></td>
    				    <td> <button class="btn btn-<?= $tcategory->devlStatus == '0' ? 'danger' : 'success'; ?> deleteRcordDev" type="button" data-id="<?= $tcategory->d_id; ?>" data-vid="<?= $tcategory->devlStatus == '0' ? '1' : '0'; ?>"><i class="fa fa-<?= $tcategory->devlStatus == '0' ? 'times' : 'check'; ?>"></i></button> <a class="btn btn-warning" href="<?= base_url(); ?>admin/devlboy/addDevboy/<?= $tcategory->d_id; ?>"><i class="fa fa-edit"></i></a>| <a class="btn btn-primary" href="<?= base_url(); ?>admin/devlboy/devl_show_list/<?= $tcategory->d_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
    				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
