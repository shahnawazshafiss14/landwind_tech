<?php
$users = $this->user_m_f->viewRecordAll();
$user_id = $this->uri->segment(4);
$user_idfind = array(
    'uUrl' => $user_id);
$useidfid = $this->user_m_f->viewRecordAny($user_idfind);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Users Manage</h1>
        <!--ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>users/index">users</a></li>
            <li class="active">List Users</li>
        </ol-->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-5">
                <div class="box">
                    <div class="box-header">
                        <div><p id="msg"></p></div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Full Name </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $user = 0;
                                foreach ($users as $tcategory):
                                    ?>
                                    <tr class="deleteUsertrU<?= $tcategory->u_id; ?>">
                                        <td><?= $i++; ?></td>
                                        <td><a href="<?= ADMINC ?>users/manage/<?= $tcategory->uUrl; ?>"><?= $tcategory->userName . ' ' . $tcategory->lastName; ?><br/>
                                                <?= $tcategory->userMobile; ?><br/>
                                                <?= $tcategory->userEmail; ?><br/>
                                                <?= $tcategory->cCity; ?></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-xs-7">
                <div class="box">
                    <div class="box-header">
                        <div><p id="msg"></p></div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover table-bordered table-striped">
                            <!--thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Menu Name</th>
                                    <th>Page Permission</th>
                                    <th>Add</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                    <th>View</th>
                                </tr>
                            </thead-->
                            <tbody>
                            <form name="menuform" id="menuform" method="POST" action="<?= ADMINC; ?>users/addme">
                                <input type="hidden" name="useris" id="useris" value="<?= $useidfid->u_id; ?>" />
                                <!--tr>
                                    <td colspan="3" align="center"><input type="submit" name="btnsubmit" class="btn btn-success" value="Update" /></td>
                                </tr-->


                                <?php
                                $datamenu = array(
                                    'menu_parent_id' => '0',
                                    'view_status' => '1',
                                    'status' => '1'
                                );
                                $menus = $this->menu_model->viewRecordAnyR($datamenu);
                                $i = 1;
                                foreach ($menus as $menu):
                                    $arrayfetch = array(
                                        'user_id' => $useidfid->u_id,
                                        'menu_id' => $menu->menu_id,
                                    );
                                    $datauser = $this->permissionmenu_model->viewRecordAny($arrayfetch);
                                    
                                    ?>
                                    <tr class="deleteUsertrU<?= $menu->menu_id; ?>">
                                        <td><?= $i++; ?></td>
                                        <td><?= $menu->menu_name; ?></td>
                                        <td><input type="checkbox" name="menumiss" value="<?= $menu->menu_id; ?>" class="menumiss" <?= count($datauser) > 0 ? 'checked' : ''; ?> /></td>
                                        <td><span class="label label-primary" title="Add Permission"><i class="fa fa-plus"></i></span></td>
                                        <td><span class="label label-warning" title="Edit Permission"><i class="fa fa-edit"></i></span></td>
                                        <td><span class="label label-danger" title="Delete Permission"><i class="fa fa-trash"></i></span></td>
                                        <td><span class="label label-primary" title="View Permission"><i class="fa fa-eye"></i></span></td>
                                        
                                        <td><span class="label label-primary" title="View Send Email"><i class="fa fa-envelope"></i></span></td>
                                        <td><span class="label label-primary" title="Print"><i class="fa fa-print"></i></span></td>
                                        <td><span class="label label-primary" title="Checked"><i class="fa fa-check"></i></span></td>
                                        <td><span class="label label-primary" title="Other view"><i class="fa fa-eye"></i></span></td>
                                    </tr>
                                    <?php
                                    $datamenu = array(
                                        'menu_parent_id' => $menu->menu_id,
                                        'view_status' => '1',
                                        'status' => '1'
                                    );
                                    $menus1 = $this->menu_model->viewRecordAnyR($datamenu);
                                    foreach ($menus1 as $menu1):
                                        $arrayfetch1 = array(
                                        'user_id' => $useidfid->u_id,
                                        'menu_id' => $menu1->menu_id,
                                    );
                                        $datauser1 = $this->permissionmenu_model->viewRecordAny($arrayfetch1);
                                        
                                        
                                        ?>
                                        <tr class="deleteUsertrU<?= $menu1->menu_id; ?>">
                                            <td></td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $menu1->menu_name; ?></td>
                                             <td><input type="checkbox" name="menumiss" value="<?= $menu1->menu_id; ?>" class="menumiss" <?= count($datauser1) > 0 ? 'checked' : ''; ?> /></td> 
                                             <td><input type="checkbox" title="Add Permission" data-val="add" data-i="<?= $menu1->menu_id ? $menu1->menu_id :''; ?>" name="menumis" value="<?=  !empty($datauser1->is_inserted) ? $datauser1->is_inserted : '0'; ?>" class="menumis" <?= !empty($datauser1->is_inserted )  ? 'checked' : ''; ?> /></td>
                                            <td><input type="checkbox" title="Edit Permission" data-val="edit" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_edited) ? $datauser1->is_edited : '0'; ?>" class="menumis" <?= !empty($datauser1->is_edited)   ? 'checked' : ''; ?> /></td>
                                            <td><input type="checkbox" title="Delete Permission" data-val="del" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_deleted) ? $datauser1->is_deleted : '0'; ?>" class="menumis" <?= !empty($datauser1->is_deleted) ? 'checked' : ''; ?> /></td>
                                            <td><input type="checkbox" title="View Permission" data-val="view" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_viewed) ? $datauser1->is_viewed : '0'; ?>" class="menumis" <?= !empty($datauser1->is_viewed) ? 'checked' : ''; ?> /></td>
                                            <td><input type="checkbox" title="Send Email" data-val="email" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_emailed) ? $datauser1->is_emailed : '0'; ?>" class="menumis" <?= !empty($datauser1->is_emailed) ? 'checked' : ''; ?> /></td>
                                            <td><input type="checkbox" title="Print" data-val="print" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_printed) ? $datauser1->is_printed : '0'; ?>" class="menumis" <?= !empty($datauser1->is_printed) ? 'checked' : ''; ?> /></td>
                                            <td><input type="checkbox" title="Checked" data-val="checke" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_checked) ? $datauser1->is_checked : '0'; ?>" class="menumis" <?= !empty($datauser1->is_checked) ? 'checked' : ''; ?> /></td>
                                             <td><input type="checkbox" title="Others View" data-val="otherview" data-i="<?= $menu1->menu_id; ?>" name="menumis" value="<?= !empty($datauser1->is_viewed_other) ? $datauser1->is_viewed_other : '0'; ?>" class="menumis" <?= !empty($datauser1->is_viewed_other) ? 'checked' : ''; ?> /></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            </form>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
