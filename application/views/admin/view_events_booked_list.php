<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Events Booked List</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Images</th>
                                    <th>Event Title</th>
                                    <th>Event Date</th>
                                    <!--th>Parent Category</th-->
				    <th>Start Time - End Time</th>
				    <th>Booked Tickets</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				foreach ($buyObjects as $pO) :

				    $gepid = array(
					'event_id' => $pO->buy_product_id
				    );
				    $eventObjects = $this->event_model->viewRecordAny($gepid);
				    ?>
    				<tr class="deleteEventtrE<?= $pO->buy_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td>
    					<a href="<?= ADMINC; ?>events/bookedList/<?= $pO->buy_id ?>" target="blank">
    					    <img src="<?= BASEURLFRONTASSET; ?>event/<?= $eventObjects->event_image; ?>" alt="" width="100" height="50">
    					</a>
    				    </td>
    				    <td><?= $eventObjects->event_title; ?></td>
    				    <td><?= $eventObjects->event_start_date; ?></td>
    				    <td><?= $eventObjects->event_time . ' - ' . $eventObjects->event_end_date; ?></td>
    				    <td>
					    <?php
					    $argbo = array(
						'buy_product_id' => $eventObjects->event_id,
						'buyStatus' => '1'
					    );
					    $datie = $this->buy_model->viewRecordRsum($argbo);
					    echo $datie->qty;
					    ?>
    				    </td>
    				    <td><a href="<?= ADMINC; ?>events/bookedLists/<?= $eventObjects->event_id; ?>"><i class="fa fa-book" aria-hidden="true"></i></a></td>
    				</tr>
				<?php endforeach; ?>
                                </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p><?php echo $links; ?></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
