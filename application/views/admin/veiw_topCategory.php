<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Products List<small>Manage View Product</small>
	</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>category/viewtopCategory">Category</a></li>
	    <li class="active">List Category</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>S.No.</th>
				    <th>Category Name</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 0;
				foreach ($topcategorys as $tcategory):
				    ?>
    				<tr class="deleteCategorytrC<?= $tcategory->category_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $tcategory->category_name; ?></td>
    				    <td> <a href="javascript:;" class="deleteRcordC" data-id="<?= $tcategory->category_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= base_url(); ?>category/topCategory/<?= $tcategory->category_id; ?>" id=""><i class="fa fa-edit"></i></a> </td>
    				</tr>
				    <?php
				    $condition = array('category_parent_id' => $tcategory->category_id);
				    $datadd = $this->category_model->viewRecordAnyR($condition);
				    foreach ($datadd as $tcategory1):
					?>
					<tr class="deleteCategorytrC<?= $tcategory1->category_id; ?>">
					    <td><?= $i++; ?></td>
					    <td><?= $tcategory->category_name; ?> > <?= $tcategory1->category_name; ?></td>
					    <td> <a href="javascript:;" class="deleteRcordC" data-id="<?= $tcategory1->category_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= base_url(); ?>category/topCategory/<?= $tcategory1->category_id; ?>" id=""><i class="fa fa-edit"></i></a> </td>
					</tr>
				    <?php endforeach; ?>
				<?php endforeach; ?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
