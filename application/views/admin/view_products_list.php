<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Products List</h1>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Images</th>
                                    <th>Product Title</th>
                                    <th>Category</th>
                                    <!--th>Parent Category</th-->
				    <th>Price</th>
                                    <th>Discount</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				foreach ($productObjects as $pO) :
				    ?>
    				<tr class="deleteProducttrP<?= $pO->p_product_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td>
    					<a href="<?= ADMINC; ?>product/product_show_list/<?= $pO->p_product_id ?>" target="blank">
    					    <img src="<?= base_url(); ?>assests/product/<?= $pO->p_image; ?>" alt="" width="100" height="50">
    					</a>
    				    </td>
    				    <td><?= $pO->p_title; ?></td>
    				    <td><?= $this->category_model->categorybyId($pO->p_category); ?></td>
    				    <td><?= $pO->p_price; ?></td>
    				    <td><?= $pO->p_discount; ?></td>
    				    <td><a href="javascript:;" class="pstatus" data-pid="<?= $pO->p_product_id; ?>" data-val="<?= ($pO->p_status == 1) ? '0' : '1' ?>"><span style="font-size: 20px;" class="text-<?= ($pO->p_status == 1) ? 'green' : 'red' ?>"><?= ($pO->p_status == 1) ? 'Show' : 'Hide' ?></span></a></td>
    				    <td><a href="javascript:;" class="deleteRcordP" data-id="<?= $pO->p_product_id; ?>"><i class="fa fa-trash"></i></a> | <a href="<?= ADMINC; ?>product/addProduct/<?= $pO->p_product_id; ?>"><i class="fa fa-edit"></i></a>| <a href="<?= ADMINC; ?>product/product_show_list/<?= $pO->p_product_id; ?>"><i class="fa fa-book" aria-hidden="true"></i></a></td>
    				</tr>
				<?php endforeach; ?>
                                </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	<p><?php echo $links; ?></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
