<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Orders Canceled
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Orders</a></li>
	    <li class="active">Orders Canceled</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<div class="table-responsive">
			    <table class="table table-hover table-bordered table-striped">
				<thead>
				    <tr>
					<th>S.No.</th>
					<th>Images</th>
					<th>Product Name</th>
					<th>Model</th>
					<th>Color</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Order Date</th>
					<th>Status</th>
					<th>Action</th>
				    </tr>
				</thead>
				<tbody>
				    <?php
				    $i = 1;
				    foreach ($productObjects as $pOm) :
					$daarr = array(
					    'p_product_id' => $pOm->order_product_id
					);
					$pO = $this->product_model->viewRecordAny($daarr);
					?>
    				    <tr class="deleteOrdertrO">
    					<td><?= $i++; ?></td>
    					<td><a href="<?= base_url(); ?>product/<?= str_replace(" ", "-", strtolower($pO->p_title)) ?>/<?= $pO->p_product_id ?>" target="blank">
    						<img src="<?= $pO->p_image; ?>" alt="100%50" width="100" height="50">
    					    </a></td>
    					<td><?= htmlspecialchars_decode($pO->p_title); ?></td>
    					<td><?= $this->models_model->modelId($pO->p_model); ?></td>
    					<td><?= $this->colors_model->colorId($pO->p_color); ?></td>
    					<td><?= $pOm->order_price; ?></td>
    					<td><?= $pOm->order_qty; ?></td>
    					<td><?= $pOm->oUdatedDate; ?></td>
    					<td>

						<?php
						$starray = array('0' => 'Processing', '1' => 'Cancel', '2' => 'Dispatched', '3' => 'Completed');
						echo $starray[$pOm->oStatus];
						?>
    					</td>
    					<td><a href="<?= base_url(); ?>admin/orders/orderStatuspage/<?= $pOm->order_id; ?>/<?= $pO->p_product_id; ?>" data-opro="<?= $pOm->order_id; ?>" data-pid="<?= $pO->p_product_id; ?>" id="show_cancel_order"><i class="fa fa-edit"></i></a></td>
    				    </tr>
				    <?php endforeach; ?>
				    </tfoot>
			    </table>
			</div>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<p><?php echo $links; ?></p>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
