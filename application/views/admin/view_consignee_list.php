<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>All Consignee List</h1>
        <?php 
        $checkm = checkpermission('169');
        ?>
    </section>
    <section class="content">
	<div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Consignee Name</th>
				    <th>Consignee Type</th>
                                    <th>Country > State</th>
                                    <th>Mobile</th>
				     <?php if(($checkm->is_viewed == '1') && ($this->session->userdata('admin_type') == '1')): ?>
                                    <th>Sales Items</th>
                                    <?php endif; ?>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$i = 1;
				$array_consig_type= array(
				  '0' => 'Consignee(Shipped to)',
				   '1' => 'Receiver(Billed to)' 
				);
				foreach ($consigneeObjects as $pO) :
				    ?>
    				<tr class="deleteConsigneetrP<?= $pO->consig_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $pO->consig_name; ?></td>
				    <td><?= $array_consig_type[$pO->consig_type]; ?></td>
    				    
    				    <td><?= $pO->consig_state . ' > ' . $pO->consig_city; ?></td>
				    <td><?= $pO->consig_phone; ?></td>
    				   <?php if(($checkm->is_viewed == '1') && ($this->session->userdata('admin_type') == '1')): ?>
                                    <td>
                                        <a title="Consignee Sales Reports" class="btn btn-warning" href="<?= ADMINC; ?>consignee/consignee_sales_month/<?= $pO->consig_slug; ?>">
                                        <i class="fa fa-folder-open"></i>
    					</a>
                                    </td>
                                    <?php endif; ?> 
    				    <td>
    					 <?php   if($checkm->is_deleted == '1'): ?>
                                        <a href="javascript:;" title="Delete Consignee" class="btn btn-danger deleteRcordConsignee" data-id="<?= $pO->consig_id; ?>"><i class="fa fa-trash"></i></a> |
                                        <?php endif; ?>
                                        <?php   if($checkm->is_edited == '1'): ?>
                                        <a title="Edit Consignee Details" class="btn btn-warning" href="<?= ADMINC; ?>consignee/edit/<?= $pO->consig_slug; ?>"><i class="fa fa-edit"></i></a> 
    					<?php endif; ?>
                                        <?php if(($checkm->is_viewed == '1') && ($this->session->userdata('admin_type') == '1')): ?> 
                                        | <a title="View Consignee Details" class="btn btn-info" href="<?= ADMINC; ?>consignee/consignee_show_list/<?= $pO->consig_slug; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <?php endif; ?>
                                        <?php if(($checkm->is_viewed_other == '1') && ($this->session->userdata('admin_type') == '1')): ?> 
                                        | <a title="Consignee Details Invoice" class="btn btn-primary" href="<?= ADMINC; ?>consignee/consignee_show_list_invoice/<?= $pO->consig_slug; ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                                        <?php endif; ?>
    				    </td>
    				</tr>
				<?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
	
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
