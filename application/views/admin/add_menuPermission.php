
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>Add/Remove Permission</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>menu_top/permisstion_action">Menu</a></li>
	    <li class="active">Add/Remove Permission</li>
	</ol>
    </section>
    <section class="content" id="ppg">

	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> Add/Remove Menu Permission</h3>
		<div class="box-tools pull-right">


		</div>
	    </div>
	    <div class="box-body">

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="col-md-3">

			    </div>
			    <div class="col-md-9">
				<div class="form-group">
				    <?php
				    foreach ($menuPermission as $mp):
					?>
    				    <label class = "control-label" for = "menuName">
					    <?= $mp->menu_name; ?>
    					<b style="color:#f00;">*</b></label>
    				    <input type="checkbox" name="link"  value="<?= $mp->menu_id; ?>"><br/>

				    <?php endforeach; ?>
				</div>
			    </div>

			</div>
		    </div>
		</div>

	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer">

	    </div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>


</div>
<!-- /.content-wrapper -->
