<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">

                <h3><?= $ordercount; ?>

                </h3>

                <p>New Orders</p>
            </div>
            <div>
		<p style="font-size: 35px !important">$<?= money_format('%i', $orderdetails->amount_paid) . "\n" ?></p>
            </div>
            <a href="<?= base_url(); ?>admin/orders/orderList" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $ordercountcd; ?></h3>
                <p>Completed Orders</p>
            </div>
            <div>
		<p style="font-size: 35px !important">$<?= money_format('%i', $orderdetailsc->amount_paid) . "\n" ?></p>
            </div>
            <a href="<?= base_url(); ?>admin/orders/orderCompleted" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!--div class="col-lg-3 col-xs-6">

        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= $ordercountc; ?></h3>

                <p>Canceled Orders</p>
            </div>
            <div>
                <p style="font-size: 35px !important">$<?php
    $number = $orderdetailscc->amount_paid;
    echo money_format('%i', $number) . "\n";
    ?></p>
            </div>
            <a href="<?= base_url(); ?>admin/orders/orderCanceled" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">

        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= $ordercountcc; ?></h3>
                <p>Dispatched Orders</p>
            </div>
            <div>
                <p style="font-size: 35px !important">$<?= money_format('%i', $orderdetailscd->amount_paid) . "\n" ?></p>
            </div>
            <a href="<?= base_url(); ?>admin/orders/orderDispatched" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div-->
    <!-- ./col -->
</div>