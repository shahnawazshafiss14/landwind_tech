<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<style>
    .input-group {
	margin-bottom: 12px;
    }
    .form-group div {
        margin-bottom:10px !important;
        margin-top:10px !important;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('consig_uploaded')) : ?>
	    <?php echo $this->session->flashdata('consig_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
		<input type="hidden" name="consigneeid" id="consigneeid" value="<?= !empty($consigneeObject->consig_id) ? $consigneeObject->consig_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <div id="sentm" style="display:none">
                        <input type="text" name="sendEmail" id="sendEmail" style="width: 350px; height: 35px;" /> |
                        <input type="button" name="btnsendEmail" id="btnsendConsigmo" data-month="<?= $consignee_month ?>" data-slug="<?= !empty($consigneeObject->consig_slug) ? $consigneeObject->consig_slug : ''; ?>" value="Send" class="btn btn-success" /> |
                        <a href="javascript:;" id="emailcancel" class="btn btn-danger">Cancel</a>
                    </div>
                        <div id="actonw" style="float:right">
                            <a href="javascript:;"  class="btn btn-primary" id="sendemailaction"><i class="fa fa-envelope"></i> Send Email</a> |
                            <a target="_blank" href="<?= base_url(); ?>consignees/<?= !empty($consigneeObject->consig_slug) ? $consigneeObject->consig_slug : ''; ?>/<?= $consignee_month ?>" class="btn btn-waning"><i class="fa fa-print"></i> Print Bill</a> | <label class="btn btn-success"><?= $consignee_month ?></label>
                        </div>
		</div>
	    </div>
	    <div class="box-body">
		<div class="row">
		    <div class="tab-content">
                        <?php
                        $ass = $this->invoice_model->lastrecordId();
                        $invoadd = $ass->invoice_number + 1;
                        ?>
                         <form  id="invoiceForm" name="invoiceForm" action="<?= ADMINC; ?>invoices/invoice_fillter"  method="POST" enctype="multipart/form-data">
                         
                           
                            <div class="form-group">
                                <div class="col-sm-3 col-md-2">
                                    Invoice Number <br/>
                                    <input name="invoice_number" id="invoice_number" type="text" class="form-control" value="<?= !empty($invoice_number) ? $invoice_number : ''; ?>" style="<?= !empty($invoice_number) ? 'background-color:#d3f274' : ''; ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Vehicle Number <br/>
                                    <input type="text" class="form-control" name="invoice_vehicle_no" id="invoice_vehicle_no" value="<?= !empty($invoice_vehicle_no) ? $invoice_vehicle_no : ''; ?>" style="<?= !empty($invoice_vehicle_no) ? 'background-color:#d3f274' : ''; ?>">
                                </div>
                                <!--input type="hidden" name="dor_id" id="dor_id" value="<?= !empty($invoiceObject->invoice_receiver) ? $invoiceObject->invoice_receiver : ''; ?>" />
                                <!--div class="col-sm-12 col-md-4">
                                    Details of Receiver (Billed to) <b style="color:#f00;">*</b><br/>
                                    <select class="selectpicker1" data-live-search="true" id="dor_receiver_id" style="width:100%;">
                                        <?php
                                        $fetarcsi = array(
                                            'consig_id' => $invoiceObject->invoice_receiver,
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetdasingle = $this->consignee_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoiceObject->invoice_receiver)):
                                            ?>
                                            <option value="<?= $invoiceObject->invoice_receiver; ?>" selected><?= $fetdasingle->consig_name . ', ' . $fetdasingle->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Receiver</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '1'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                </div--->
                                <input type="hidden" name="doc_id" id="doc_id" value="<?= !empty($invoiceObject->invoice_consig) ? $invoiceObject->invoice_consig : ''; ?>" />
                                <div class="col-md-4">
                                    <span style="<?= !empty($doc_receiver_id) ? 'background:#d3f274' : ''; ?>">Details of Consignee (Shipped to)</span> <br/>
                                    <select class="selectpicker1" data-live-search="true" name="doc_receiver_id" id="doc_receiver_id" style="width:100%;<?= !empty($doc_receiver_id) ? 'background:#d3f274' : ''; ?>">
                                        <?php
                                        $fetarcsi1 = array(
                                            'consig_id' => $doc_receiver_id,
                                            'consig_type' => '0',
                                            'view_status' => '1' 
                                        );
                                        $fetdasingle1 = $this->consignee_model->viewRecordAny($fetarcsi1);
                                        if (!empty($doc_receiver_id)):
                                            ?>
                                            <option value="<?= $doc_receiver_id; ?>" selected><?= $fetdasingle1->consig_name . ', ' . $fetdasingle1->consig_phone; ?></option>
                                        <?php endif; ?>
                                        <option value="">Details of Consignee</option>
                                        <option value="0">NO</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                            'consig_type' => '0'
                                        );
                                        $fetda = $this->consignee_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option data-tokens="<?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?>" value="<?= $fevenue->consig_id; ?>"><?= $fevenue->consig_name; ?>, <?= $fevenue->consig_phone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                     
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-md-2">
                                    Select CGST <br/>
                                    <select name="invoice_cgst" class="form-control" id="invoice_cgst" style="width:100%;<?= !empty($invoice_cgst) ? 'background-color:#d3f274' : ''; ?>">
                                        <?php
                                        $fetarcsi = array(
                                            'gst_id' => $invoice_cgst,
                                        );
                                        $fetdasingle = $this->gst_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoice_cgst)):
                                            ?>
                                            <option value="<?= $invoice_cgst; ?>" selected><?= $fetdasingle->commongst; ?></option>
                                        <?php endif; ?>
                                        <option value="">Select CGst</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                        );
                                        $fetda = $this->gst_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option value="<?= $fevenue->gst_id; ?>"><?= $fevenue->commongst; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Select SGST <br/>
                                    <select name="invoice_sgst" class="form-control" id="invoice_sgst" style="width:100%;<?= !empty($invoice_sgst) ? 'background-color:#d3f274' : ''; ?>">
                                        <?php
                                        $fetarcsi = array(
                                            'gst_id' => $invoice_sgst,
                                        );
                                        $fetdasingle = $this->gst_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoice_sgst)):
                                            ?>
                                            <option value="<?= $invoice_sgst; ?>" selected><?= $fetdasingle->commongst; ?></option>
                                        <?php endif; ?>
                                        <option value="">Select SGst</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                        );
                                        $fetda = $this->gst_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option value="<?= $fevenue->gst_id; ?>"><?= $fevenue->commongst; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Select IGST <br/>
                                    <select name="invoice_igst" class="form-control" id="invoice_igst" style="width:100%;<?= !empty($invoice_igst) ? 'background-color:#d3f274' : ''; ?>">
                                        <?php
                                        $fetarcsi = array(
                                            'gst_id' => $invoice_igst,
                                        );
                                        $fetdasingle = $this->gst_model->viewRecordAny($fetarcsi);
                                        if (!empty($invoice_igst)):
                                            ?>
                                            <option value="<?= $invoice_igst; ?>" selected><?= $fetdasingle->commongst; ?></option>
                                        <?php endif; ?>
                                        <option value="">Select IGst</option>
                                        <?php
                                        $fetarc = array(
                                            'view_status' => '1',
                                        );
                                        $fetda = $this->gst_model->viewRecordAnyR($fetarc);
                                        foreach ($fetda as $fevenue):
                                            ?>
                                            <option value="<?= $fevenue->gst_id; ?>"><?= $fevenue->commongst; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Date of Supply From<br/>
                                    <input type="text" class="form-control datepicker123" name="invoice_dos_from" id="invoice_dos_from" value="<?= !empty($invoice_dos_from) ? $invoice_dos_from : ''; ?>" style="<?= !empty($invoice_dos_from) ? 'background-color:#d3f274' : ''; ?>">
                                </div>
                                <div class="col-sm-4 col-md-2">
                                    Date of Supply To<br/>
                                    <input type="text" class="form-control datepicker123" name="invoice_dos_to" id="invoice_dos_to" value="<?= !empty($invoice_dos_to) ? $invoice_dos_to : ''; ?>" style="<?= !empty($invoice_dos_to) ? 'background-color:#d3f274' : ''; ?>">
                                </div>
                                <!--div class="col-sm-4 col-md-2">
                                    Place of Supply <br/>
                                    <input type="text" class="form-control" name="invoice_pos" id="invoice_pos" value="<?= !empty($invoiceObject->invoice_pos) ? $invoiceObject->invoice_pos : 'Siwan'; ?>" style="background-color:#d3f274">
                                </div-->
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-6 col-md-offset-5">
                                    <button type="submit" class="btn btn-primary btn-lg" name="btnInvoicefillter" id="btnInvoicefillter">Search</button> 
                                    <button type="button" class="btn btn-primary btn-lg" name="reset" id="btnreset">Reset All</button> 
                                </div>
                            </div> 
                         </form>
                    </div>
		</div>
		<div class="row">
		    <div class="col-md-12">
                        <?php 
                          if(!empty($invoiceObjects)){
                        ?>
			<table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>View Invoice</th>				    
                                    <th>Invoice Number</th>
                                    <th>Vehicle Number</th>
				    <th>Date of Supply</th>
				    <th>GST %</th>
				    <th>GST Amount</th>
				    <th>Sub Total</th>
				    <th>Total Amount</th>
				    <th>Qty/weight</th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				$taotalsubt = 0;
				$ara = monthwises();
				/*
                                $fetch = array(
				    'invoice_consig' => $consigneeObject->consig_id
				);
                                 * 
                                 */
				//$invoiceObjects = $this->consignee_model->viewRecordAnyRAll($fetch, $ara[0], $ara[1]);
				$i = 1;
				$taotalkg1 = 0;
				$grandtoatal1 = 0;
                              
				foreach ($invoiceObjects as $pO) :
				    ?>
    				<tr class="deleteInvoicetrV<?= $pO->invoice_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><a href="<?= ADMINC; ?>invoices/invoice_show_list/<?= $pO->invoice_slug; ?>"><i class="fa fa-book" aria-hidden="true"></i></a></td>
				    <td><?= $pO->invoice_number; ?></td>
    				    <td><?= $pO->invoice_vehicle_no; ?></td>
				    <td><?= dateMonth($pO->invoice_dos); ?></td>
    				    
				    
				    <?php
				$fetchParticlearray1 = array(
				    'itemInvoice_invoice' => $pO->invoice_id
				);
				$fetchParticle1 = $this->itemtypeInvoice_model->viewRecordAnyR($fetchParticlearray1);
				$taotal = 0;
				$taotalrate = 0;
				
				$taotalkg = '';
				$itemunit = '';
				foreach ($fetchParticle1 as $tr):
				    $fetchParticlearray = array(
					'item_id' => $tr->itemInvoice_name
				    );
				    $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
				    ?>
    				    <?php
				$taotal1 = $tr->itemInvoice_kg * $tr->itemInvoice_rate;
				//echo inrcurr($taotal1);
				$taotal += $tr->itemInvoice_kg * $tr->itemInvoice_rate;
				$taotalsubt += $tr->itemInvoice_kg * $tr->itemInvoice_rate;
				$taotalrate += $tr->itemInvoice_rate;
				$taotalkg = $tr->itemInvoice_kg;
				$itemunit = $fetchParticle->item_unit;
				    ?>
					<?php endforeach; ?>
				    <?php 
				
				    $cgest = $this->gst_model->commongstId($pO->invoice_cgst);
				    $sgest = $this->gst_model->commongstId($pO->invoice_sgst);
				    $igest = $this->gst_model->commongstId($pO->invoice_igst);
				    ?>
				    <td align="right">
				   <?php 
				    echo !empty($cgest) ? $cgest. " %" : '';
					echo !empty($sgest) ? ' | '.$sgest. " %" : '';	
				echo !empty($igest) ? $igest. " %" : '';
				
				$aftegcalcgst = !empty($cgest) ? $taotal * $cgest / 100 : '0.00';
				$aftegcalsgst = !empty($sgest) ? $taotal * $sgest / 100 : '0.00';	
				$aftegcaligst = !empty($igest) ? $taotal * $igest / 100 : '0.00';
				
				    ?>
				    </td>
				    <td align="right">
				    <i class="fa fa-inr"></i>  <?php
				     echo !empty($cgest) ? $taotal * $cgest / 100 : '';
				echo !empty($sgest) ? $taotal * $sgest / 100 : '';	
				echo !empty($igest) ? $taotal * $igest / 100 : '';
				
				 $subtotal1 = !empty($cgest) ? $taotal * $cgest / 100 : '';
				$subtotal2 = !empty($sgest) ? $taotal * $sgest / 100 : '';	
				$subtotal3 = !empty($igest) ? $taotal * $igest / 100 : '';
				
				 $grandtoatalsub += ($subtotal3 + $subtotal2 + $subtotal1);
				      ?></td>
				    
    				    <td align="right"><i class="fa fa-inr"></i>  
    				    <?= inrcurr($taotal); ?>
    				    
    				    </td>
    				    <td align="right">
    				    <i class="fa fa-inr"></i> 
				    <?php
					
				$grandtoatal = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
				$grandtoatal2 = ($aftegcalcgst + $aftegcalsgst + $aftegcaligst + $taotal);
					echo inrcurr($grandtoatal);
				?>
				    </td>
				    <td align="right">
					<?php 
				echo $taotalkg . ' /'. $itemunit;	
				  $taotalkg1 += $taotalkg;
				  $grandtoatal1 += $grandtoatal2;
					?>
				    </td>
    				</tr>
				<?php 
                                endforeach; 
                               
                                ?>
				<tr>
				    <td colspan="6" align="right"><b>Total</b></td>
				    <td align="right"><b><i class="fa fa-inr"></i> <?php echo inrcurr($grandtoatalsub);?></b></td>
				    <td align="right"><b><i class="fa fa-inr"></i> <?php echo inrcurr($taotalsubt);?></b></td>
				    <td align="right"><b><i class="fa fa-inr"></i> <?php echo inrcurr($grandtoatal1);?></b></td>
				    <td align="right"><b><?= $taotalkg1 . ' /'.$itemunit;?></b></td>
				</tr>
			    </tbody>
                        </table>
                        <?php 
                          }
                        ?>
		    </div>
		</div>


	    </div>

	</div>
</div>
</div>
<div class=""></div>
<!-- box-body -->


</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


