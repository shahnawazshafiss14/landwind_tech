<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assests/images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/AdminLTE.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/plugins/iCheck/square/blue.css">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="login_bg">
        <div class="backgroundadmin">
            <div class="item">
                <div class="bg_img" style="background:url(<?= base_url(); ?>/assests/images/2200x954_accounting_background.jpg) no-repeat;background-size: cover; -webkit-background-size: cover;">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-sm-4">
                <!-- <div class="col-sm-4 col-sm-offset-4"> -->
                <!-- /.login-logo -->
                <div class="login-box-body">
                    <div class="error-box">
                        <div class="error-body text-center">
                            <h1 class="error-title text-danger">404</h1>
                            <h3 class="text-uppercase error-subtitle">PAGE NOT FOUND !</h3>
                            <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
                            <a href="<?= ADMINC ?>" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">Back to home</a> </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- jQuery 3 -->
        <script src="<?= base_url(); ?>assests/components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url(); ?>assests/components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?= base_url(); ?>assests/plugins/iCheck/icheck.min.js"></script>

        <script src="<?= base_url(); ?>assests/dist/js/alertify.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/top-js/top_custom.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/js/sweetalert2.js" type="text/javascript"></script>
        <div class="copyright"><p class="pull-left"><?= COMPANYCOPYRIGHT ?></p><p class="pull-right"><?= LWDEVLOPED ?></p></div>
    </body>
</html>
