<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th, td {
        padding: 5px;
    }
    h2,h3,p,th {
        text-align: center;
    }
</style>
<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    ?>
    <section class="content" id="ppg">
        <?php if ($this->session->flashdata('trans_uploaded')) : ?>
            <?php echo $this->session->flashdata('trans_uploaded'); ?>
        <?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <input type="hidden" name="transid" id="transid" value="<?= !empty($transObject->trans_id) ? $transObject->trans_id : ''; ?>" />
                <div class="box-tools pull-right">
                    <div id="sentm" style="display:none"><input type="text" name="sendEmail" id="sendEmail" style="width: 350px; height: 35px;" /> | <input type="button" name="btnsendEmailbill" id="btnsendEmailbill" data-slug="<?= !empty($transObject->trans_slug) ? $transObject->trans_slug : ''; ?>" value="Send" class="btn btn-success" /> | <a href="javascript:;" id="emailcancel" class="btn btn-danger">Cancel</a></div> <div id="actonw" style="float:right"><a href="javascript:;" class="btn btn-primary" id="sendemailaction"><i class="fa fa-envelope"></i> Send Email</a> | <a href="javascript:;" class="btn btn-success" onclick="printDiv('print_area1')"><i class="fa fa-print"></i> Print Bill</a> | <a href="<?= ADMINC; ?>transport/add/<?= !empty($transObject->trans_slug) ? $transObject->trans_slug : ''; ?>" class="eaddmorecontent btn btn-info"><i class="fa fa-pencil"></i> Edit</a></div>

                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="print_area1">
                            <h2 style="text-align:center">KHUSHI LORRY BROKER</h2>
                            <p style="text-align:center"><b>Container Truck Commission Supplier Agent</b></p>
                            <p style="text-align:center">Chapra Road, Siwan (<?= ucwords($users1->cstate); ?>)<br/><b>PAN: </b> DTPPM5967P<br/><b>Mob:</b> +91-9470728961</p>

                            <table style="width:100%; border-color:black;" class="table table-bordered">
                                <tr>
                                    <td colspan="8"><b>Challan No.#  </b> <?= $transObject->trans_challan_no; ?></td>
                                    <td colspan="8"><b>Date </b> <?= $transObject->trans_date; ?>
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="8"><b>From: </b> <?= $transObject->trans_from; ?></td>
                                    <td colspan="8"><b>To: </b> <?= $transObject->trans_to; ?>
                                    </td> 
                                </tr>
                                <tr>
                                    <th colspan="8">Details of Consignor</th>
                                    <th colspan="8">Details of Consignee</th>
                                </tr>
                                <?php
                                $fetcconginee = array(
                                    'consig_id' => $transObject->trans_name_consignee,
                                    'consig_type' => '0'
                                );
                                $fetchconsignee = $this->consignee_model->viewRecordAny($fetcconginee);
                                
                               
                                
                                $fetcconginer = array(
                                    'consig_id' => $transObject->trans_name_consigntor,
                                    'consig_type' => '1'
                                );
                                $fetchconsigner = $this->consignee_model->viewRecordAny($fetcconginer);
                                 
                                ?>
                                <tr>
                                    <?php 
                                    if(!empty($fetchconsigner->consig_id)){
                                    ?>
                                    <td colspan="8"><b>Name: </b> <?= !empty($fetchconsigner->consig_name) ? $fetchconsigner->consig_name : ''; ?><br/><b>Address: </b> <?= !empty($fetchconsigner->consig_address) ? $fetchconsigner->consig_address : ''; ?><br/><b>GSTIN: </b> <?= !empty($fetchconsigner->consig_gstin) ? $fetchconsigner->consig_gstin : ''; ?><br/><b>State: </b> <?= !empty($fetchconsigner->consig_state) ? $fetchconsigner->consig_state : ''; ?></td>
                                    <?php }else{ ?>
                                    <td colspan="8"><b>Name: </b> Sonu Enterprises<br/><b>Address: </b> Siwan<br/><b>GSTIN: </b> 10CTEPS8418K1ZG<br/><b>State: </b> Bihar</td>
                                    <?php } ?>
                                    <td colspan="8"><b>Name: </b> <?= !empty($fetchconsignee->consig_name) ? $fetchconsignee->consig_name : ''; ?><br/><b>Address: </b> <?= !empty($fetchconsignee->consig_address) ? $fetchconsignee->consig_address : ''; ?><br/><b>GSTIN: </b> <?= !empty($fetchconsignee->consig_gstin) ? $fetchconsignee->consig_gstin : ''; ?><br/><b>State: </b> <?= !empty($fetchconsignee->consig_state) ? $fetchconsignee->consig_state : ''; ?></td>
                                </tr> 
                                <tr>
                                    <th colspan="6" style="text-align:left">Goods of Description</th>
                                    <th colspan="4" style="text-align:center">Qty/Weight</th>
                                    <th colspan="2" style="text-align:center">Unit</th>
                                    <th colspan="4" style="text-align:right">Rate</th>
                                </tr>

                                <?php
                                $fetchParticlearray1 = array(
                                    'itemTrans_trans' => $transObject->trans_id
                                );
                                $fetchParticle1 = $this->itemtypetrans_model->viewRecordAnyR($fetchParticlearray1);
                                $taotal = 0;
                                foreach ($fetchParticle1 as $tr):
                                    $fetchParticlearray = array(
                                        'item_id' => $tr->itemTrans_name
                                    );
                                    $fetchParticle = $this->itemtype_model->viewRecordAny($fetchParticlearray);
                                    ?>
                                    <tr>
                                        <td colspan="6" align="left"><?= $fetchParticle->item_particular_name; ?></td>
                                        <td colspan="2" align="center"><?= $tr->itemTrans_unit; ?></td>
                                        <td colspan="4" align="center"><?= $fetchParticle->item_unit; ?></td>

                                        <td colspan="4" align="right"> Fix</td>

                                    <?php endforeach; ?>
                                </tr>
                                <tr>
                                    <td colspan="15" align="right"><b>Fare</b> </td>
                                    <td colspan="4" align="right"> <?= $transObject->trans_amount == '0.00' ? 'Fix' : '<i class="fa fa-inr"></i> ' . number_format($transObject->trans_amount, 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="15" align="right"><b>Advance</b> </td>
                                    <td colspan="4" align="right"><i class="fa fa-inr"></i> <?= $transObject->trans_advance == '0.00' ? '0.00' : number_format($transObject->trans_advance, 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="15" align="right"><b>Balance</b> </td>
                                    <td colspan="4" align="right"> <?= $transObject->trans_amount == '0.00' ? 'PAID' : '<i class="fa fa-inr"></i> ' . number_format(($transObject->trans_amount - $transObject->trans_advance), 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="10" align="left">
                                        <b>Owner Name: </b> <?= $transObject->trans_owner_name; ?> <br/><b>Truck No.: </b> <?= $transObject->trans_truck_no; ?><br/><b>Address: </b> <?= $transObject->trans_owner_address; ?>
                                    </td>
                                    <td colspan="9" align="left">
                                        <b>Driver's Name: </b> <?= $transObject->trans_driver_name; ?><br/><b>Driver's L. No.: </b> <?= $transObject->trans_driver_licence; ?><br/><b>Address: </b> <?= $transObject->trans_driver_address; ?></td>
                                </tr>
                                <tr>
                                    <th colspan="7" align="left">
                                        Signature of Consignor
                                    </th>
                                    <th colspan="5" align="right">
                                        Driver's Signature
                                    </th>
                                    <th colspan="7" align="right">
                                        For <b>Khushi Lorry Broker</b>
                                    </th>
                                </tr>
                                <tr>
                                    <th  colspan="7" align="left">
                                        <p><br/><br/></p>
                                    </th>
                                    <td colspan="5" align="right">

                                    </td>
                                    <td colspan="9" align="right">

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="19" align="left">
                                        ** Note: During transit till the Driver of goods satisfactory the total risk whatever will be on truck owner to driver after the challan is signed by any of them.</td>
                                </tr>
                                <tr>
                                    <td colspan="19" align="left">
                                        1. Goods Carried at owner risk unless Insured with the company against risk.<br/>2. Freight will paid as per destination weight slip<br/>3. No. Tranship will be acceptable.<br/>4. GST paid by Consignor or Consignee.</td>
                                </tr>
                                <tr>
                                    <td colspan="19" align='center'>
                                        All Subject to Siwan Jurisdiction only</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class=""></div>
            <!-- box-body -->


        </div>

</div>
<!-- .box -->
</div>
<!-- .content-wrapper -->


