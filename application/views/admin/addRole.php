<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1><?= !empty($roleObject->role_id) ? 'Edit' : 'Add' ?> Role</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>role/topRole">Role</a></li>
	    <li class="active"><?= !empty($roleObject->role_id) ? 'Edit' : 'Add' ?> Role</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= !empty($roleObject->role_id) ? 'Edit' : 'Add' ?> Role</h3>
		<div class="box-tools pull-right">
		    <form  id="roleForm" name="roleForm" action="<?= base_url(); ?>role/add"  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnRole" id="btnRole" class="btn btn-info btn-sm" title="Collapse">
			    <i class="fa fa-save"></i> <?= !empty($roleObject->role_id) ? 'Update' : 'Save' ?> </button>
			<?php if (!empty($roleObject->role_id)) : ?>
    			<a href="<?= base_url(); ?>role/viewtopRole" title="Collapse" class="btn btn-info btn-sm" title="Cancel">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php else : ?>
    			<a href="" data-toggle="tooltip" title="Collapse" class="btn btn-info btn-sm" title="Collapse">
    			    <i class="fa fa-reply"></i> Cancel </a>
			<?php endif; ?>


		</div>
	    </div>
	    <div class="box-body">

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#General">General</a></li>
		</ul>
		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="roleName">Role Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input name="editName" id="editName" type="hidden" value="">
				    <input type="text" class="form-control" id="roleName" name="roleName" placeholder="Enter Role Name" value="">
				</div>
			    </div>
			</div>
		    </div>
		</div>
		</form>

	    </div>
	    <div class="box-body">


		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="roleName">Role List</label>
				<div class="col-sm-10">
				    <ul>
					<?php
					foreach ($roleObject as $ro):
					    ?>
    					<li><?= $ro->role_name; ?></li>
					<?php endforeach; ?>
				    </ul>

				</div>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
