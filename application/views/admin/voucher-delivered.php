  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   <!-- <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section> -->


<div class="row">
      <div class="col-md-12">
    <div class="box">
        
        <!-- /.box-header -->
        <div class="box-body">
		 
      <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
       <form id="billForm1" name="billForm1" action="<?= base_url();?>admin/sales/delivered" method="GET" enctype="multipart/form-data">
			<div class="row">
			

                   
				   <div class="col-md-2">

 <div class="form-group">
            <div id="example1_filter" class="dataTables_filter">
              <label>Filter Data:  
			  <select name="selParty" id="selParty2" class="form-control">
			  <option value="All">All Party</option>
			  <?php 
			  $userfill = array(
			  'view_status' => '1'
			  );
			  $fetch_sql = $this->user_m_f->viewRecordAnyR($userfill);
			  foreach($fetch_sql as $feach_s):
			    if($userid == $feach_s->u_id){
				  $selu = "selected";
			  }else{
				  $selu = "";
			  }
			  ?>
			  <option value="<?= $feach_s->u_id; ?>" <?= $selu; ?>><?= $feach_s->userName; ?></option>
			  <?php 
			  endforeach;
			  ?>
			  </select>
			  
			  
			 </label>
            </div>
			</div>
			
</div>
 <div class="col-sm-3">
		  <div class="form-group">
            <div id="example1_filter" class="dataTables_filter">
              <label>Filter Product:  
			  <select name="selProduct" id="selProduct" class="form-control">
			  <option value="All">All Prouct</option>
			  <?php 
			  $userfill = array(
				'view_status' => '1'
			  );
			  $fetch_sql = $this->product_model->viewRecordAnyR($userfill);
			  foreach($fetch_sql as $feach_s):
			  if($productid == $feach_s->p_product_id){
				  $selp = "selected";
			  }else{
				  $selp = "";
			  }
			  ?>
			  <option value="<?= $feach_s->p_product_id; ?>" <?= $selp; ?>><?= $feach_s->p_name; ?></option>
			  <?php 
			  endforeach;
			  ?>
			  </select>
			  
			  
			 </label>
            </div>
			</div>
			</div>

				   <div class="col-md-2">
				   <label class="control-label" for="product_name">Date From <b style="color:#f00;">*</b></label>
				    <input type="text" class="form-control" id="start_date" name="from" style="display:inline;" value="<?= !empty($from) ? $from : date('Y-m-d');?>" />
				   </div>
				   <div class="col-md-2">
	  <label class="control-label" for="product_name">Date to <b style="color:#f00;">*</b></label> 
	  <input type="text" class="form-control" id="end_date" name="to" style="display:inline" value="<?= !empty($to) ? $to : date('Y-m-d');?>" />
	  
				   </div>
				   
              
			
			 <div class="col-md-1" style="margin-top: 20px;">
				<button type="submit" class="btn btn-primary btn-md" title="seacrh">Search </button>
			 </div>
				</form>   


</div>

			
	<form id="billForm" name="billForm" action="<?= base_url();?>admin/sales/ajaxprintExprot" method="POST" enctype="multipart/form-data">		
			
			<div class="row">
			<input type="hidden" class="form-control" name="selParty" value="<?= $userid;?>" />
			  <input type="hidden" class="form-control" name="selProduct" value="<?= $productid;?>" />
			  <input type="hidden" class="form-control" id="start_date" name="start_date" style="display:inline;" value="<?= !empty($from) ? $from : date('Y-m-d');?>" />
			 <input type="hidden" class="form-control" id="end_date" name="end_date" style="display:inline" value="<?= !empty($to) ? $to : date('Y-m-d');?>" /> 
			 <div class="col-md-1" style="margin-top: 20px;">
			 <button type="button" name="btnReload" id="btnReload" class="btn btn-success btn-md" title="Reload">Refresh</button>
	  </div>
	  <?php 
	   if($productid == 'All'){
			$where .= " AND bill_product_id !='".$productid."'";
		}else{
			$where .= " AND bill_product_id='".$productid."'";
		}		
		if($userid == 'All'){
			$where .= " AND bill_consignee !='".$userid."'";
		}else{
			$where .= " AND bill_consignee='".$userid."'";
		}
	   $select_a = "bill_no from tbl_billing where (date_format(bill_date, '%Y-%m-%d') BETWEEN '".$from."' AND '".$to."') AND bill_status=3 ".$where."";
		$fetch_bill = $this->billing_model->viewRecordGCINR($select_a);
		if(count($fetch_bill) > 0){
			$selesty = "block";
		}else{
			$selesty = "none";
		}
	  ?>
			<div class="col-md-2" style="margin-top: 20px;">
			   <a href="javascript:;" class="btn btn-primary"><input type="checkbox" name="allcheck" id="allcheck" value="1" /> Select All</a> 
		    </div>
			<div class="col-md-2" style="margin-top: 20px;">
				<button type="submit" name="btnPrintExport" id="btnPrintExport" style="display:<?= $selesty; ?>" class="btn btn-danger btn-md" title="Export to PDF">Export to PDF </button>
            </div>
			</div>
			
			
			
			</div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
	
     </div>
      <!-- /.col -->
  </div>

    <!-- Main content -->
<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index?from=<?= $from; ?>&to=<?= $to; ?>&selParty=<?= $userid; ?>&selProduct=<?= $productid;?>"> Back</a></li>
          
            <li>Delivered</li>
        </ol>
    <section class="content">
	
	<div id="ajaxusers">
      <!-- Small boxes (Stat box) -->
      <div class="row">
	  <?php 
		$admin_type = $this->session->userdata('admin_type');
		//if($admin_type == 2):
		
		
		
		foreach($fetch_bill as $fetc_bill):
		$asd = array(
			'bill_product_id' => $fetc_bill->bill_product_id
		);
		$product_id = array();
		$total_ctn = 0;
		$fetch_bill_f = $this->billing_model->viewRecordAnyR($asd);
		foreach($fetch_bill_f as $fd){
			$product_id[] = $fd->bill_product_id;
			$total_ctn += $fd->bill_qty;
		}
		$ima = implode(',', $product_id);
		$aes_pro = $this->product_model->viewRecordGCIN('GROUP_CONCAT(p_name SEPARATOR ",") as product_name from tblproduct where p_product_id IN('.$ima.')');
		?>
		<div class="col-lg-3 col-xs-6 divcheck"> 
			
			<!-- small box -->
          <div class="small-box bg-blue" style="cursor: pointer">
		  <h6><label>
		  <div class="checkbox">
			<?php 
			 $asd_dev = array(
			'bs_billing_id' => $fetc_bill->bill_no,
			'bs_status' => '3'
		);
			$fetc_devey = $this->billingstatus_model->viewRecordAny($asd_dev);
			
			?>
				<label><input type="checkbox" name="chkselectp[]" class="chkselectp" value="<?= $fetc_bill->bill_id;?>"></label>
			</div></label></h6>
			<div class="inner">
				<h6><?= ucfirst($this->user_m_f->userbyId($fetc_bill->bill_consignee)); ?></h6>
				<h6><?= $aes_pro->product_name; ?> </h6>
				<h6><?php
				$fet = explode(',', $aes_pro->product_name);
				echo $total_ctn; ?> CTN</h6>
				
				<h6>Bill No.#<?= $fetc_bill->bill_id;?></h6>
				<h6>Delivery BY: <?= $this->devl_model->devlbyId($fetc_bill->bill_assigned_to);?></h6>
				<h6>Delivered on : <?= $fetc_devey->bs_created_on; ?></h6>
			</div>
           
            
          </div>
        </div>
		<?php 
		endforeach;
	//	endif;
		?>
      </div>
	  </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
	 </form>
  </div>
  <!-- /.content-wrapper -->
  