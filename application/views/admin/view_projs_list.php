<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Project List</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12"> 
                <div class="box">
                    <div class="box-header"> 
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Sr.</th>
                                    <th>Client Name</th>
                                    <th>Project Title</th>
                                    <th style="text-align:center">Project Price</th>
                                    <th>Project Url</th>
                                    <th style="text-align:center">Start - End Date</th>
                                    <th style="text-align:center">Duration</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $today_date = date('Y-m-d');
                                foreach ($projObjects as $pO) :
                                    ?>
                                    <tr class="deleteInvoicetrV<?= $pO->id; ?>">
                                        <td><?= $i++; ?></td> 
                                        <td><?php
                                            $fetad = array(
                                                'consig_id' => $pO->proj_client
                                            );
                                            $fetchany = $this->consignee_model->viewRecordAny($fetad);
                                            $party_name =  $fetchany->consig_name . ', ' . $fetchany->consig_phone;
                                            ?>
                                            <a title="View Client Details"  href="<?= ADMINC; ?>consignee/consignee_show_list/<?= $fetchany->consig_slug; ?>"><?= $party_name; ?></a>
                                            </td>
                                        <td><?= $pO->proj_title; ?></td>
                                        <td style="text-align:center"><?= $pO->proj_price; ?></td>
                                        <td><?= $pO->proj_development_url; ?></td>
                                        <td style="text-align:center"><?= dateMonth($pO->proj_start_date) . '<span style="display:block;padding:5px;border-top:2px solid black;">'. dateMonth($pO->proj_end_date); ?></td>
                                        <td style="text-align:center"><?= dateTwoDiff($pO->proj_start_date, $pO->proj_end_date) . '<span style="display:block;padding:5px;border-top:2px solid black;">'. dateTwoDiff($pO->proj_end_date, $today_date); ?></td>
                                        <td>
                                            <?php
                                            $checkm = checkpermission('172');
                                            ?>
                                            <?php if ($checkm->is_deleted == '1'): ?>
                                                <a href="javascript:;" title="Delete Invoice" class="btn btn-danger deleteRcordV" data-id="<?= $pO->id; ?>"><i class="fa fa-trash btn-xs"></i></a> |
                                            <?php endif; ?>
                                            <?php if ($checkm->is_edited == '1'): ?>
                                                <a title="Edit Project Details" class="btn btn-warning btn-xs" href="<?= ADMINC; ?>invoices/createinvoice/<?= $pO->proj_slug; ?>"><i class="fa fa-edit"></i></a> |
                                            <?php endif; ?>
                                            <?php if ($checkm->is_viewed == '1'): ?>
                                                <a title="View Project Details" class="btn btn-primary btn-xs" href="<?= ADMINC; ?>project/proj_show_list/<?= $pO->proj_slug; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <p></p>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
