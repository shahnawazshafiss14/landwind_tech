<!-- Content Wrapper. Contains apply content -->
<div class="content-wrapper">
    <!-- Content Header (Apply header) -->
    <section class="content-header">
	<h1>All Applys List<small>Manage View Apply</small></h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Apply</a></li>
	    <li class="active">Apply List</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>Number</th>
				    <th>Apply For</th>
				    <th>Date</th>
				    <th>Action</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($applyObject as $pO) :
				    ?>
    				<tr class="deletetrapply<?= $pO->apply_id; ?>">
    				    <td><?= $i++; ?></td>
					<?php
					$arrcareetp = array(
					    '1' => 'Accounting',
					    '2' => 'Event Assistant',
					    '3' => 'Dev Ops Engineer',
					    '4' => 'Engineering Manager',
					    '5' => 'Marketing Manager',
					    '6' => 'Product Specialists'
					);
					?>
    				    <td><?= $arrcareetp[$pO->categoryApply]; ?></td>
    				    <td><?= $pO->applyDate; ?></td>
    				    <td>
    					<a href="<?= ADMINC; ?>apply/apply_show_list/<?= $pO->apply_id; ?>" id=""><i class="fa fa-book"></i></a></td>
    				</tr>
				<?php endforeach; ?>
				</tfoot>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
