<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= ADMINC; ?>consignee/consignee_list">Consignee</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('consignee_uploaded')) : ?>
	    <?php echo $this->session->flashdata('consignee_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form id="consigneeForm" name="consigneeForm" action="<?= ADMINC; ?>consignee/addConsignee"  method="POST" enctype="multipart/form-data">
		<?php
                        $checkm = checkpermission('168');
                        if ($checkm->is_edited == '1' || $checkm->is_inserted == '1'):
                            ?>
                        <button type="submit" name="btnSubmitconsignee" id="<?= !empty($consigneeObject->consig_id) ? 'ubtnConsignee' : 'ubtnConsignee'; ?>" title="Save" class="btn btn-success btn-lg" >
			    <i class="fa fa-save"></i> <?= !empty($consigneeObject->consig_id) ? 'Update' : 'Save'; ?> </button>
                        <a href="<?= ADMINC; ?>consignee/consignee_list" title="Back to List" class="btn btn-info btn-lg" >
                            <i class="fa fa-reply"></i> Cancel </a>
                            <?php endif;?>
                </div>
            </div>
            <div class="box-body">

                <ul class="nav nav-tabs">
                    <li class="active">
			<a data-toggle="tab" href="#General">General</a>
		    </li>
                    <!--li><a data-toggle="tab" href="#Image">Image</a></li-->
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">

			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_name">Consignee Name <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="hidden" name="consigneeid" id="consigneeid" value="<?= !empty($consigneeObject->consig_id) ? $consigneeObject->consig_id : ''; ?>"/>
				    <input type="text" class="form-control" id="consignee_name" name="consignee_name" placeholder="Enter Consignee Name" value="<?= !empty($consigneeObject->consig_name) ? $consigneeObject->consig_name : '';
	?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_add">Consignee Address <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" class="form-control" id="consignee_address" name="consignee_address" placeholder="Enter Consignee Address" value="<?= !empty($consigneeObject->consig_address) ? $consigneeObject->consig_address : '';
	?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_type">Type </label>
				<div class="col-sm-5" id="consignee_type">
				    <label class="control-label" for="consignee_type">Details of Consignee(Shipped to) </label>
				    <input type="radio" name="consignee_type" value="<?= !empty($consigneeObject->consig_type) == 0 ? 0 : '0'; ?>">
				
				</div>
				<div class="col-sm-5">
				    <label class="control-label" for="consignee_type">Details of Receiver(Billed to) </label>
				    <input type="radio" name="consignee_type" value="<?= !empty($consigneeObject->consig_type) == 1 ? 1 : '1'; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_email">Email </label>
				<div class="col-sm-10">
				    <input type="text" id="consignee_email" name="consignee_email" class="form-control" value="<?= !empty($consigneeObject->consig_email) ? $consigneeObject->consig_email : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_phone">Phone </label>
				<div class="col-sm-10">
				    <input type="text" id="consignee_phone" name="consignee_phone" class="form-control number" maxlength="10" value="<?= !empty($consigneeObject->consig_phone) ? $consigneeObject->consig_phone : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_phone">GSTIN <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" id="consignee_gstin" name="consignee_gstin" class="form-control" value="<?= !empty($consigneeObject->consig_gstin) ? $consigneeObject->consig_gstin : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
                                <label class="control-label col-sm-2" for="consignee_state">State Name <b style="color:#f00;">*</b></label>
                                <div class="col-sm-10">

				    <input name="consignee_state" id="consignee_state" class="form-control" value="<?= !empty($consigneeObject->consig_state) ? $consigneeObject->consig_state : '' ?>"/>

				</div>
			    </div>
			    <div class="form-group">

				<label class="control-label col-sm-2" for="consignee_city">City <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" id="consignee_city" name="consignee_city" class="form-control" value="<?= !empty($consigneeObject->consig_city) ? $consigneeObject->consig_city : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_zipcode">Zip Code </label>
				<div class="col-sm-10">
				    <input type="text" id="consignee_zipcode" name="consignee_zipcode" class="form-control number" maxlength="6" value="<?= !empty($consigneeObject->consig_zipcode) ? $consigneeObject->consig_zipcode : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_contact_person">Contact Person </label>
				<div class="col-sm-10">
				    <input type="text" id="consignee_contact_person" name="consignee_contact_person" class="form-control" value="<?= !empty($consigneeObject->consig_contact_person) ? $consigneeObject->consig_contact_person : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<div class="form-group">
				    <label class="control-label col-sm-2" for="Description">Description </label>
				    <div class="col-sm-10">
					<textarea class="form-control" id="consignee_description" name="consignee_description" placeholder="Description"> <?= !empty($consigneeObject->consig_description) ? $consigneeObject->consig_description : ''; ?></textarea>
				    </div>
				</div>

			    </div>
			    <!--div class="form-group">
				<label class="control-label col-sm-2" for="consignee_Status">Status</label>
				<div class="col-sm-10">
				    <select class="form-control" name="consignee_status" id="consignee_status">
					<option value="">--select--</option>
					<option value="1">Approved</option>
					<option value="0">Request</option>

				    </select>
				</div>
			    </div-->
			</div>
			<!--div id="Image" class="tab-pane fade">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="consignee_images">Image <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <?php
				    if (!empty($consigneeObject->consignee_image)):
					?>
    				    <img src="<?= BASEURLFRONTASSET; ?>consignee/<?= $consigneeObject->consignee_image; ?>" alt="" width="200px" height="200px">
    				    <input type="hidden" id="consignee_images" name="consignee_images" value="<?= $consigneeObject->consignee_image; ?>">
				    <?php else: ?>
    				    <input type="file" id="consignee_images" name="consignee_images">
				    <?php endif; ?>

				</div>
			    </div>
			</div-->
		    </div>
		    </form>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
		</div>
		<!-- /.box-footer-->
	    </div>

	    <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
