<?php
$getEamil = $this->session->userdata('userName_sess');
$dataArraym = array('userEmail' => $getEamil);
$users1 = $this->user_m_f->viewRecordAny($dataArraym);
$getval = $this->uri->segment(4);
if (!empty($getval)) {
    $adf = $this->eventextracon_model->viewRecordId($getval);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    $userfetch = $this->user_m_f->viewRecordAny($dataArraym);
    ?>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('event_uploaded')) : ?>
	    <?php echo $this->session->flashdata('event_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
	    <input name="editName" id="editName" type="hidden" value="<?= !empty($cateObject->category_id) ? $cateObject->category_id : ''; ?>">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
		<input type="hidden" name="eventid" id="eventid" value="<?= !empty($cateObject->category_id) ? $cateObject->category_id : ''; ?>" />
		<input type="hidden" name="eextraid" id="eextraid" value="<?= !empty($getval) ? $getval : ''; ?>" />
                <div class="box-tools pull-right">

		    <a href="javascript:;" class="eaddmorecontent" name="eaddmorecontent" id="eaddmorecontent" data-eventid="<?= !empty($cateObject->category_id) ? $cateObject->category_id : ''; ?>" data-toggle="modal" data-target="#myModalevent">Add More Content</a>
		</div>
	    </div>
	    <div class="box-body">

		<div class="row">
		    <div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">

			<!--
						<div class="col-sm-9">
						    <b>Category Image</b>
						    <input type="file" id="eventImage" name="eventImage" />
						</div>
						<div class="col-sm-3">
						    <input type="button" name="uploadeventImage" id="uploadeventImage" value="Upload" class="btn btn-primary" />
						</div>

						<img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
						     src="<?= base_url(); ?>assests/event/<?= $cateObject->cateimage; ?>" class="img" /-->

			<?php
			$get_idu = $this->uri->segment(4);
			if (!empty($get_idu)):
			    ?>
    			<div class="form-group">
    			    <label class="control-label col-sm-2" for="UpdateImage">Update Image</label>
    			    <div class="col-sm-10">
    				<div class="col-sm-5">
    				    <img src="<?= BASEURLFRONTASSET; ?><?= !empty($cateObject->cateimage) ? 'banner/' . $cateObject->cateimage : 'demo.jpg' ?>" alt="" width="300" height="250">
    				</div>
    				<div class="col-sm-5">
    				    <input class="form-control" type="file" id="cateimagechange" name="cateimagechange">
    				    <input type="button" class="btn btn-primary" name="uploadimagechange" id="uploadimagechange" value="Upload">

    				</div>

    			    </div>
    			</div>
			    <?php
			endif;
			?>

		    </div>
		    <div class="col-sm-6">
			<b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px; display:block">Category Details</b>
			<div class="row detailsprodod">
			    <p><span class="col-sm-5"><b>Category Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $cateObject->category_name; ?></span></p><br/>
			    <p><span class="col-sm-5"><b>Category Title</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $cateObject->category_title; ?></span></p><br/>

			</div>
		    </div>
		</div>
		<div class="row">
		    <div class="col-md-12">
			<div class="panel panel-primary">

			    <!-- .panel-body -->
			    <div class="row">
				<div class="col-md-12">

				    <div class="panel panel-default">
					<!-- Modal content-->    <div class="panel-heading">
					    Add Bottom Extra Content
					</div>
					<div class="panel-body">
					    <div class="row">

						<div class="form-group">
						    <label class="control-label col-sm-2" for="Title">Video Url  <b style="color:#f00;">*</b></label>
						    <div class="col-sm-10">
							<input class="form-control" id="cateVediourl" name="cateVediourl" placeholder="Title" value="<?= !empty($cateObject->cateVediourl) ? $cateObject->cateVediourl : ''; ?>">
						    </div>
						    <br/>
						    <br/>
						</div>
						<div class="form-group">
						    <label class="control-label col-sm-2" for="Description">Description <b style="color:#f00;">*</b></label>
						    <div class="col-sm-10">
							<textarea class="form-control ckeditor" id="cateDescription" name="cateDescription" placeholder="Description"> <?= !empty($cateObject->cateDescription) ? $cateObject->cateDescription : ''; ?>
							</textarea>
						    </div>
						</div>
						<div class="form-group">
						    <label class="control-label col-sm-2" for="Button"></label>
						    <div class="col-sm-10">
							<input type="button" name="categoryupdated" id="categoryupdated" value="Update" class="btn btn-success"/>
						    </div>
						</div>

					    </div>
					</div>
				    </div>
				</div>
			    </div>
			</div>
			<!-- /.panel -->
		    </div>

		</div>
		<div class="row">

		    <div class="col-lg-12">
			<a href="javascript:;" name="addcategory" id="addcategory" class="btn btn-success">+ Add Extra Images</a>
			<div id="formhinde" style="display: none">

			    <div class="panel panel-info">

				<div class="panel panel-body">
				    <form name="formCategory" id="formCategory" action="<?= base_url(); ?>admin/category/extraimages" method="post" enctype="multipart/form-data">
					<div class="form-group">
					    <div class="col-md-12 ">
						<div class="col-md-3">
						    <label class="form-control">Choose Photo</label>
						</div>
						<div class="col-md-9">

						    <input type="file" name="category_image" id="category_image" class="form-control"/>
						</div>
					    </div>
					    <input type="hidden" name="category_ide" id="category_ide" value="<?= !empty($cateObject->category_id) ? $cateObject->category_id : ''; ?>" />
					</div>
					<div class="form-group">
					    <div class="col-md-12 ">
						<div class="col-md-3">
						    <input type="submit" class="btn btn-primary" name="btnCategoryextraimg" id="btnCategoryextraimg" value="Save" />
						</div>
					    </div>
					</div>
				    </form>
				</div>
			    </div>
			</div>

		    </div>
		</div>

		<div class="row">
		    <div class="col-md-12">
			<div class="panel panel-warning">
			    <div class="panel-heading">
				Category Extra Images
			    </div>
			    <!-- .panel-heading -->
			    <div class="panel-body">

				<div class="panel-group" id="accordion">
				    <div class="panel-heading">
					<h4 class="panel-title">
					    <a data-toggle="collapse" data-parent="#accordion" href="#collapsecategory" aria-expanded="false" class="collapsed">View Image: </a>
					</h4>
				    </div>
				    <div id="collapsecategory" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
					<?php
					$condition = array(
					    'pid' => $cateObject->category_id,
					    'category' => 'category'
					);
					$speckers = $this->extraimages_model->viewRecordAnyR($condition);
					foreach ($speckers as $specker):
					    ?>
    					<div class="col-md-3">

    					    <p><img src="<?= base_url(); ?>assests/banner/<?= $specker->fiename; ?>" alt="" width="200" height="150"/></p>
    					    <a href="javascript:;" class="btn btn-danger categorydeleteextra" data-delid="<?= !empty($specker->imagesid) ? $specker->imagesid : ''; ?>">Delete</a>
    					</div>

					<?php endforeach; ?>
				    </div>
				</div>
			    </div>
			    <!-- .panel-body -->

			</div>
		    </div>
		    <!-- /.panel -->
		</div>

	    </div>
	    <div class=""></div>
	    <!-- box-body -->


	</div>

</div>
<!-- .box -->
</section>



</div>
<!-- .content-wrapper -->


