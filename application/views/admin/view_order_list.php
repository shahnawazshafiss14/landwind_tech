<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Orders New
	</h1>
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="#">Orders</a></li>
	    <li class="active">Orders New</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">

		<div class="box">
		    <div class="box-header">

		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<div class="table-responsive">
			    <table class="table table-hover table-bordered table-striped">
				<thead>
				    <tr>
					<th>Number</th>
					<th>Images</th>
					<th>Product Name</th>
					<th>Selling Price</th>
					<th>Quantity</th>
					<th>Order Date</th>
					<th>Status</th>
					<th>Action</th>
				    </tr>
				</thead>
				<tbody>
				    <?php
				    $i = 1;
				    foreach ($productObjects as $pOm) :
					$daarr = array(
					    'p_product_id' => $pOm->order_product_id
					);
					$pO = $this->product_model->viewRecordAny($daarr);
					?>
    				    <tr class="deleteOrdertrO">
    					<td><?= $i++; ?></td>
    					<td><a href="" target="blank">
    						<img src="<?= BASEURLFRONTASSET . 'product/' . $pO->p_image; ?>" alt="100%50" width="100" height="50"></a></td>
    					<td><?= $pO->p_title; ?></td>
    					<td><?= $pOm->order_price; ?></td>
    					<td><?= $pOm->order_qty; ?></td>
    					<td><?= $pOm->oUdatedDate; ?></td>
    					<td>

						<?php
						$starray = array('1' => 'Processing', '2' => 'Completed');
						echo $starray[$pOm->oStatus];
						?>

    					</td>
    					<td><a href="javascript:;" class="orderPro" data-stut="dispatched" data-opro="<?= $pOm->order_id; ?>" data-pid="<?= $pO->p_product_id; ?>" data-qty="<?= $pOm->order_qty; ?>"><i class="fa fa-edit"></i> Invoice</a><br/></td>
    				    </tr>
				    <?php endforeach; ?>
				    </tfoot>
			    </table>
			</div>
		    </div>
		    <p><?php echo $links; ?></p>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>

	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
