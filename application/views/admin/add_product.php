<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>product/productList">Products</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>
    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('product_uploaded')) : ?>
	    <?php echo $this->session->flashdata('product_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">
                    <form  id="productForm" name="productForm" action="<?= base_url(); ?>admin/product/ajax_upload"  method="POST" enctype="multipart/form-data">
			<button type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-success btn-lg">
			    <i class="fa fa-save"></i> <?= !empty($productObject->p_product_id) ? 'Update' : 'Save'; ?> </button>
                        <a href="<?= ADMINC; ?>product/productLists"  class="btn btn-info btn-lg">
                            <i class="fa fa-reply"></i> Cancel </a>
                </div>
            </div>
            <div class="box-body">

                <ul class="nav nav-tabs">
                    <li class="active">
			<a data-toggle="tab" href="#General">General</a>
		    </li>
                    <li><a data-toggle="tab" href="#Image">Image</a></li>
                </ul>
                <div class="form-horizontal">
                    <div class="tab-content">
                        <div id="General" class="tab-pane fade in active">

			    <div class="form-group">
				<label class="control-label col-sm-2" for="product_name">Product Title <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="hidden" name="productEdit" id="productEdit" value="<?= !empty($productObject->p_product_id) ? $productObject->p_product_id : ''; ?>"/>
				    <input type="text" class="form-control strnum" id="product_name" name="product_name" placeholder="Enter Product Name" value="<?= !empty($productObject->p_title) ? $productObject->p_title : '';
	?>">
				</div>
			    </div>
			    <div class="form-group" id="p_category">
				<label class="control-label col-sm-2" for="prdouct_Status">Select Categories</label>
				<div class="col-sm-10">
				    <div >
					<?php
					echo $categories;
					?>
				    </div>
				    <input type="hidden" name="categoryParent" id="categoryParent" value="<?= !empty($productObject->p_category) ? $productObject->p_category : '';
					?>"/>

				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_price">Price <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" id="prdouct_price" name="prdouct_price" class="form-control price" value="<?= !empty($productObject->p_price) ? $productObject->p_price : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_price">Shipping Price <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" id="prdouct_ship_price" name="prdouct_ship_price" class="form-control price" value="<?= !empty($productObject->p_ship_price) ? $productObject->p_ship_price : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_price">Tax <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" id="prdouct_ship_price" name="prdouct_tax_price" class="form-control price" value="<?= !empty($productObject->p_tax_price) ? $productObject->p_tax_price : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_qty">Qty <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <input type="text" id="prdouct_qty" name="prdouct_qty" class="form-control number" value="<?= !empty($productObject->p_qty) ? $productObject->p_qty : ''; ?>">
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="Description">Description <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <textarea class="form-control ckeditor" id="product_description" name="product_description" placeholder="Description"> <?= !empty($productObject->p_discription) ? $productObject->p_discription : ''; ?></textarea>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_Status">Status</label>
				<div class="col-sm-10">
				    <select class="form-control" name="prdouct_Status" id="prdouct_Status">
					<option value="">--select--</option>
					<option value="1">Enable</option>
					<option value="0">Disabled</option>

				    </select>
				</div>
			    </div>
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_Status">Discount</label>
				<div class="col-sm-10">
				    <select class="form-control" name="prdouct_discount" id="prdouct_discount">
					<?php
					if (!empty($productObject->p_discount)):
					    ?>
    					<option value="<?= $productObject->p_discount; ?>"><?= $productObject->p_discount; ?>%</option>
					<?php endif; ?>
					<option value=""> -- Discount -- </option>
					<option value="5">5%</option>
					<option value="10">10%</option>
					<option value="15">15%</option>
					<option value="20">20%</option>
					<option value="25">25%</option>
					<option value="30">30%</option>
					<option value="35">35%</option>
					<option value="40">40%</option>
					<option value="45">45%</option>
					<option value="50">50%</option>
					<option value="55">55%</option>
					<option value="60">60%</option>
					<option value="65">65%</option>
					<option value="70">70%</option>
					<option value="75">75%</option>
					<option value="80">80%</option>
					<option value="85">85%</option>
					<option value="90">90%</option>
				    </select>
				</div>
			    </div>
			</div>
			<div id="Image" class="tab-pane fade">
			    <div class="form-group">
				<label class="control-label col-sm-2" for="prdouct_images">Image <b style="color:#f00;">*</b></label>
				<div class="col-sm-10">
				    <?php
				    if (!empty($productObject->p_image)):
					?>
    				    <img src="<?= ADMINPRODUCTIMG . $productObject->p_image ?>" alt="" width="200px" height="200px">
    				    <input type="hidden" id="product_images" name="product_images" value="<?= $productObject->p_image; ?>">
				    <?php else: ?>
    				    <input type="file" id="product_images" name="product_images">
				    <?php endif; ?>

				</div>
			    </div>
			</div>
		    </div>

		    </form>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
		</div>
		<!-- /.box-footer-->
	    </div>

	    <!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
