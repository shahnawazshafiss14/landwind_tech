<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $head_title ?> - <?= COMPANYNAME; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="shortcut icon" href="<?= base_url(); ?>assests/images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/bootstrap-select.min.css" type="text/css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/Ionicons/css/ionicons.min.css">
        <!-- daterange picker -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/plugins/iCheck/all.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
         
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/components/select2/dist/css/select2.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/skins/_all-skins.min.css">
        <!-- fileuploader -->

        <link rel="stylesheet" href="<?= base_url(); ?>assests/dist/css/multifile.css" type="text/css">
        <link href="<?= base_url(); ?>assests/dist/css/alertify.core.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url(); ?>assests/dist/css/alertify.default.css" rel="stylesheet" type="text/css"/>

        <link href="<?= base_url(); ?>assests/dist/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- Google Font -->
        <link rel="stylesheet"     href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php
            $this->load->view('admin/include/topmenu');
            $this->load->view('admin/include/sidemenu');
            ?>
            <?php $this->load->view($contentView); ?>

            <?php $this->load->view('admin/include/footer'); ?>

            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?= base_url(); ?>assests/components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url(); ?>assests/components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?= base_url(); ?>assests/components/select2/dist/js/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="<?= base_url(); ?>assests/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?= base_url(); ?>assests/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?= base_url(); ?>assests/plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- date-range-picker -->
        <script src="<?= base_url(); ?>assests/components/moment/min/moment.min.js"></script>
        <script src="<?= base_url(); ?>assests/components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?= base_url(); ?>assests/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?= base_url(); ?>assests/components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="<?= base_url(); ?>assests/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?= base_url(); ?>assests/components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?= base_url(); ?>assests/plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="<?= base_url(); ?>assests/components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?= base_url(); ?>assests/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url(); ?>assests/dist/js/demo.js"></script>
        <!-- CK Editor -->

        <script src="<?= base_url(); ?>assests/components/ckeditor/ckeditor.js"></script>
        <script src="<?= base_url(); ?>assests/dist/js/script.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/custom.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/alertify.min.js" type="text/javascript"></script>

        <script src="<?= base_url(); ?>assests/dist/js/notify.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/dist/js/jquery-ui.js" type="text/javascript"></script>
        <script src="<?= base_url(); ?>assests/js/sweetalert2.js" type="text/javascript"></script>
        <?php $this->load->view('admin/script12'); ?>
        <script>

            $(function () {
                $('.selectpicker1').selectpicker();

                //Initialize Select2 Elements
                $('.select2').select2()

                //Datemask dd/mm/yyyy
                $('#datemask').inputmask('yyyy/mm/dd', {'placeholder': 'yyyy/mm/dd'})
                //Datemask2 mm/dd/yyyy
                $('#datemask2').inputmask('yyyy/mm/dd', {'placeholder': 'yyyy/mm/dd'})
                //Money Euro
                $('[data-mask]').inputmask()

                //Date range picker
                $('#reservation').daterangepicker()
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'})
                $('#time').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'h:mm A'})
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        function (start, end) {
                            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                        }
                )

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                })
                $('#prdouct_discount_start').datepicker({
                    autoclose: true
                })
                $('#prdouct_discount_end').datepicker({
                    autoclose: true
                })

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                })
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                })
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                })

                //Colorpicker
                $('.my-colorpicker1').colorpicker()
                //color picker with addon
                $('.my-colorpicker2').colorpicker()

                //Timepicker
                $('.timepicker').timepicker({
                    showInputs: false
                });
                $('.timepicker1').timepicker();


                $('#event_start_date_a').datepicker({
                    format: 'yyyy-mm-dd',
                    startDate: '-3d'
                });
                $('.date').datepicker();
                $('#invoice_dos').datepicker({
                dateFormat: 'yy-mm-dd'});
				 $('#expn_date').datepicker({
                dateFormat: 'yy-mm-dd'});

            });
            function findcat(val_id) {
                var val_id = val_id;
                $.ajax({
                    url: base_url + "admin/category/findpartcat",
                    method: "POST",
                    data: {val_id: val_id},
                    success: function (data) {
                        //alert(data);
                        $('#pardiv1').html(data);
                    }
                });
            }
        </script>
        <script type="text/javascript">

            var placeSearch, autocomplete;

            var componentForm = {
                street_number: 'short_name', //house no

                route: 'long_name', // area

                locality: 'long_name', // city

                administrative_area_level_1: 'long_name', // state

                country: 'long_name',
                postal_code: 'short_name'

            };


            function initialize() {

                var input = document.getElementById('workshopplaces');

                var autocomplete = new google.maps.places.Autocomplete(input);


                google.maps.event.addListener(autocomplete, 'place_changed', function () {

                    var place = autocomplete.getPlace();

                    document.getElementById('workshoplatitude').value = place.geometry.location.lat();

                    document.getElementById('workshoplongitude').value = place.geometry.location.lng();

                    document.getElementById('place_idhealersearch').value = place.place_id;
                    for (var i = 0; i < place.address_components.length; i++) {

                        var addressType = place.address_components[i].types[0];

                        if (componentForm[addressType]) {

                            var val = place.address_components[i][componentForm[addressType]];

                            document.getElementById(addressType).value = val;

                        }

                    }

                })
            }

            google.maps.event.addDomListener(window, 'load', initialize);

        </script>

    </body>
</html>
