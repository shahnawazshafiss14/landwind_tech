<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= $head_title; ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>admin/sales/index">Vouchar</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol>
    </section>

    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('user_uploaded')) : ?>
	    <?php echo $this->session->flashdata('user_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> <?= $head_title; ?></h3>
                <div class="box-tools pull-right">

                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;">Vouchar Details</b>
                        <div class="row detailsprodod">
                            <p><span class="col-sm-5"><b>Vouchar Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $this->user_m_f->userbyId($billObjects->bill_consignee); ?></span></p><br/> 
                        </div>
                    </div>
                </div>
				<div class="">
					<table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">Sr.</th>
      <th scope="col">Item name</th>
      <th scope="col">Unit</th>
      <th scope="col">Qty</th>
      <th scope="col">Price</th>
      <th scope="col">Discount</th>
      <th scope="col">Total Price</th>
    </tr>
  </thead>
  <tbody>
  <?php 
  
  $i = 1;
  $total = 0;
		$array_product = array(
			'view_status' => '1',
			'bill_no' => $billObjects->bill_no
		);
		$bills = $this->billing_model->viewRecordAnyR($array_product);
		foreach($bills as $fetc_b):
  ?>
    <tr>
      <th scope="row"><?= $i++; ?></th>
      <td>
	  <?php 
		
		$array_product = array(
			'view_status' => '1',
			'p_product_id' => $fetc_b->bill_product_id
		);
		$fet_product =$this->product_model->viewRecordAny($array_product);
		echo $fet_product->p_name;	
		?>
	  </td>
      <td>
	  <?php 
		 
		echo $this->unit_model->unitbyId($fet_product->p_unit);
		?>
		</td>
      <td><?= $total_qty = ($fetc_b->bill_qty * $fet_product->p_qty); ?></td>
      <td><?= $fetc_b->bill_price; ?></td>
      <td><?= $total_discount = $fetc_b->bill_discount; ?></td>
      <td><?php
	  $totalpri = ($fetc_b->bill_price * $total_qty);
	  $total += $totalpri; 
	  $total1 = $totalpri - ($totalpri * $total_discount / 100); 
	  echo $total1;
	  ?></td>
	  
    </tr>
	<?php 
	endforeach;
	?>
	 <tr>
	 <td align="right" colspan="6">Total</td>
	 <td>
	 <?=  $total; ?>
	 </td>
	 </tr>
  </tbody>
</table>	
				</div>
                <div class="">
                </div><!-- box-body -->

                <div class="box-footer">

                </div><!-- box-footer -->

            </div>
            <!-- .box -->
    </section>


</div>
<!-- .content-wrapper -->
