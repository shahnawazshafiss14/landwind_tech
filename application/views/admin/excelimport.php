<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>Import / Export</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>admin/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li class="active">Emport / Export</li>
	</ol>
    </section>
    <section class="content" id="ppg">
	<div class="callout callout-success hidepop" id="rock">
	    <p id="product_success"> </p>
	</div>
	<!-- Default box -->
	<div class="box box-info">
	    <div class="box-body">

		<div class="form-horizontal">
		    <div class="tab-content">
			<div id="General" class="tab-pane fade in active">
			    <?php if ($this->session->flashdata('message')) { ?>
    			    <div align="center" class="alert alert-success">
				    <?php echo $this->session->flashdata('message') ?>
    			    </div>
			    <?php } ?>

			    <br><br>

			    <div class="row">
				<div class="col-md-10">
				    <form action="<?php echo base_url(); ?>admin/uploadcsv/import" method="post" name="upload_excel" enctype="multipart/form-data">
					<input type="file" name="file" id="file">
					<button type="submit" id="submit" name="import" class="btn btn-primary button-loading">Import</button>
				    </form>
				</div>
				<div class="col-md-2">
				    <a class="btn btn-success button-loading text-right" href="<?= base_url(); ?>admin/uploadcsv/ExportCSV">Export</a>
				</div>
			    </div>
			    <br>
			    <br>
			    <a href="<?php echo base_url(); ?>category_list.xlsx"> Category file </a>
			    <br>
			    <br>
			    <a href="<?php echo base_url(); ?>tblproduct.xlsx"> Sample file </a>
			    <br><br>

			    <div align="center">
				<table id="t01">
				    <tr>
					<th>  </th>
				    </tr>
				    <?php
				    if (isset($view_data) && is_array($view_data) && count($view_data)): $i = 1;
					foreach ($view_data as $key => $data) {
					    ?>
					    <tr>
						<td><?php echo $data['p_title'] ?></td>
					    </tr>
					<?php } endif; ?>
				</table>
			    </div>


			</div>
		    </div>
		</div>
		</form>
	    </div>
	    <!-- /.box-body -->
	    <div class="box-footer"></div>
	    <!-- /.box-footer-->
	</div>
	<!-- /.box -->
    </section>
</div>
<!-- /.content-wrapper -->
