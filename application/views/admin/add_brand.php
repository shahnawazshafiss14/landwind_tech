<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   


	 <div class="row" style="padding: 3px 0px; background-color: #fff" >
	 
      <div class="col-md-12">
   
      <h3 class="text-left" style="margin-left: 18px"><?= $head_title; ?></h3>
        
        
        
    </div>
    
      </div>
      
  
            <div class="row" style="background-color: #f5f5f5">
			<div class="col-md-4">
			<ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>admin/dashboard/index"> Back</a></li>
			<li><a href="<?= base_url(); ?>admin/brand/listbrand"> List Brand</a></li>
          
           
        </ol>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-7">
			<ol class="breadcrumb">
			<li>Go to</li>
            
			<li><a href="<?= base_url(); ?>admin/sales/newvouchers"> All Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/index"> List Vouchers</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/print1"> Print</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/delivered"> Delivered</a></li>
			<li><a href="<?= base_url(); ?>admin/sales/undelivered"> Undelivered</a></li>
          
            
        </ol>
		</div>
		</div>
	
    <form action="" id="brandForm" method="post">


 <div class="row" style="margin-top: 50px;">
						<div class="col-sm-1"></div>
						<div class="col-sm-10" style="background-color: antiquewhite; border-radius: 10px">
						<div class="container" style="margin-top: 30px">
						<div class="row">
		
		
		<div class="col-sm-3">
			    <div class="form-group">
				<label class="control-label" for="BrandName">Brand Name <b style="color:#f00;">*</b></label>
				
				    <input name="edit" type="hidden" value="<?= !empty($brandObject->b_id) ? $brandObject->b_id : ''; ?>">
				    <input type="text" class="form-control" id="b_name" name="b_name" placeholder="Enter Brand Name" value="<?= !empty($brandObject->b_name) ? $brandObject->b_name : '' ?>">
				</div>
			    </div>
			
		 </div>
							</div>
							</div>
							</div>
		
		
	
	
	
	<div class="row" style="margin-top: 12px">
			
                
                <div class="col-md-3 pull-right">
                   
			<button type="submit" name="btnBrand" id="btnBrand" class="btn btn-primary btn-md" title="Collapse">
			     <?= !empty($brandObject->b_id) ? 'Update' : 'Add Brand'; ?> </button>
                        
                </div>
            
			</div>
	
	
		
		
		</form>			   
	    </div>
	    <!-- /.box-body -->
	  