<div class="row">
      <div class="col-xs-12">
    <div class="box">
        
        <!-- /.box-header -->
        <div class="box-body">
      <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
       
			<div class="row">
			
<form id="billForm" name="billForm" action="<?= base_url();?>admin/dashboard/index" method="GET" enctype="multipart/form-data">
                  
				   
				   <div class="col-md-2">
				   <label class="control-label" for="product_name">Date From <b style="color:#f00;">*</b></label>
				    <input type="text" class="form-control" id="start_date" name="from" style="display:inline;" value="<?= $from; ?>" />
				   </div>
				   <div class="col-md-2">
	  <label class="control-label" for="product_name">Date to <b style="color:#f00;">*</b></label> <input type="text" class="form-control" id="end_date" name="to" style="display:inline" value="<?= $to;?>" />
	  
				   </div>
				   
              <input type="hidden" name="selParty" value="All" /> 
			  <input type="hidden" name="selProduct" value="All" /> 
			
			 <div class="col-md-2" style="margin-top: 20px;">
				<button type="submit" id="btnSearchDasboard" class="btn btn-primary btn-md" title="seacrh">Search </button>
			 </div>
				   
				   <div class="col-md-2" style="margin-top: 20px;">
                    
      
      <button type="button" name="btnReload" id="btnReload" class="btn btn-success btn-md" title="Reload">Refresh</button>
	  </div>
	   
 </form>
</div>

			
			
			</div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
	
      </div>
      <!-- /.col -->
  </div>
<div class="row">
	  <?php 
		$admin_type = $this->session->userdata('admin_type');
		//if($admin_type == 2):
		?>
	<div id="dis_t_top">
		<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <?php 
			 $data_arry_bil = array(
					'view_status' => '1',
					'bill_date >=' => $from,
					'bill_date <=' => $to
				);
			$fetch_bill = $this->billing_model->viewRecordAnyR($data_arry_bil);
			  ?>  
              <h3><?= count($fetch_bill);?></h3>

              <p>All Vouchers</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?= base_url(); ?>admin/sales/newvouchers?from=<?= $from; ?>&to=<?= $to; ?>&selParty=<?= $userid; ?>&selProduct=<?= $productid;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
               <?php 
			   $data_arry_bil = array(
					'view_status' => '1',
					'bill_status' => '1',
					'bill_date >=' => $from,
					'bill_date <=' => $to
				);
			 
			$fetch_bill = $this->billing_model->viewRecordAnyR($data_arry_bil);
			  ?>  
              <h3><?= count($fetch_bill);?></h3>

              <p>Undelivered</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?= base_url(); ?>admin/sales/undelivered?from=<?= $from; ?>&to=<?= $to; ?>&selParty=<?= $userid; ?>&selProduct=<?= $productid;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
                
              <?php 
			  $data_arry_bil = array(
					'view_status' => '1',
					'bill_status' => '2',
					'bill_date >=' => $from,
					'bill_date <=' => $to
				);
			$fetch_bill = $this->billing_model->viewRecordAnyR($data_arry_bil);
			  ?>  
              <h3><?= count($fetch_bill);?></h3>

              <p>Print</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?= base_url(); ?>admin/sales/print1?from=<?= $from; ?>&to=<?= $to; ?>&selParty=<?= $userid; ?>&selProduct=<?= $productid;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php 
			  $data_arry_bil = array(
					'view_status' => '1',
					'bill_status' => '3',
					'bill_date >=' => $from,
					'bill_date <=' => $to
				);
			$fetch_bill = $this->billing_model->viewRecordAnyR($data_arry_bil);
			  ?>  
              <h3><?= count($fetch_bill);?></h3>
              <p>Delivered</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?= base_url(); ?>admin/sales/delivered?from=<?= $from; ?>&to=<?= $to; ?>&selParty=<?= $userid; ?>&selProduct=<?= $productid;?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
</div>




		<?php 
	//	endif;
		?>
		 