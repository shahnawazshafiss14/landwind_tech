<?php
$users = $this->customer_model->viewRecordAll();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>All Users List</h1>
	<ol class="breadcrumb">
	    <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
	    <li><a href="<?= base_url(); ?>users/index">users</a></li>
	    <li class="active">List Users</li>
	</ol>
    </section>
    <section class="content">
	<div class="row">
	    <div class="col-xs-12">
		<div class="box">
		    <div class="box-header">
			<div><p id="msg"></p></div>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
			<table id="example1" class="table table-hover table-bordered table-striped">
			    <thead>
				<tr>
				    <th>Number</th>
				    <th>Full Name </th>
				    <th>Email</th>
				    <th>Mobile</th>
				    <th>History</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach ($users as $tcategory):
				    ?>
    				<tr class="deleteUsertrU<?= $tcategory->customer_id; ?>">
    				    <td><?= $i++; ?></td>
    				    <td><?= $tcategory->customerName . ' ' . $tcategory->customerlastName; ?></td>
    				    <td><?= $tcategory->customerEmail; ?></td>

    				    <td><?= $tcategory->customerMobile; ?></td>
    				    <td><a href="<?= ADMINC; ?>users/details/<?= $tcategory->customer_id; ?>"><i class="fa fa-book"></i></a></td>
    				</tr>
				<?php endforeach; ?>
			    </tbody>
			</table>
		    </div>
		    <!-- /.box-body -->
		</div>
		<!-- /.box -->
	    </div>
	    <!-- /.col -->
	</div>
	<!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- page script -->
