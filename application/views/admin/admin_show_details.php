<?php
$dataUser1 = $userObjects->u_id;
$dataArraym1 = array('user_parent' => $dataUser1);
$usersRetailer = $this->user_m_f->viewRecordAnyR($dataArraym1);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Admin</h1>
        <!--ol class="breadcrumb">
            <li><a href="<?= base_url(); ?>dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url(); ?>users/index">Users</a></li>
            <li class="active"><?= $head_title; ?></li>
        </ol-->
    </section>

    <section class="content" id="ppg">
	<?php if ($this->session->flashdata('user_uploaded')) : ?>
	    <?php echo $this->session->flashdata('user_uploaded'); ?>
	<?php endif; ?>
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Admin Details</h3>
                <div class="box-tools pull-right">
                    <?php 
                    if($userObjects->role_id == 1):
                    ?>
		    <a href="<?= ADMINC ?>users/manage/<?= $userObjects->uUrl; ?>" class="btn btn-warning"><i class="fa fa-user"></i> Manage Users</a> |
                    <?php endif;?>
                    <a href="<?= ADMINC ?>users/addUsers/<?= $userObjects->uUrl; ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
		    <?php
		    if (!empty($userObjects->u_id)):
			?>
    		    <div class="col-sm-12">
    			<b style="margin-bottom: 10px;border-bottom: 1px solid #000;letter-spacing: 0px;padding-bottom: 3px;">Admin Details</b>
    			<div class="row detailsprodod">
    			    <p><span class="col-sm-5"><b>User Name</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= htmlspecialchars_decode($userObjects->userName); ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Last Name</b><b class="pull-right">:</b></span> <span class="col-sm-7"><?= $userObjects->lastName; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Email</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->userEmail; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Mobile</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->userMobile; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Password</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->userPwd; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User Address</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->cAddress; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User State</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->cstate; ?></span></p><br/>
    			    <p><span class="col-sm-5"><b>User City</b><b class="pull-right">:</b></span>  <span class="col-sm-7"><?= $userObjects->cCity; ?></span></p><br/>

    			</div>
    		    </div>
			<?php
		    else :
			?>
    		    <div class="col-sm-12">
    			<h3>User Details Not Found</h3>
    		    </div>
		    <?php endif; ?>
                </div>
                <div class=""></div>
		
		<!-- .box -->
	    </div>
	    <!-- box-body -->

	    <div class="box-footer">

	    </div><!-- box-footer -->

	</div>
	<!-- .box -->
    </section>


</div>
<!-- .content-wrapper -->
