<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en" class="loading">
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="favicon.png">
        <title><?= COMPANYNAME; ?></title>
        <link href="<?= base_url(); ?>assests/css/bootstrap.css"" rel="stylesheet" type="text/css"/>
        <style>
            body{    background-color: #ece3e3;}
            .box{
                background: #fbfbfb;
                box-shadow: 0px 1px 13px 1px rgba(0, 0, 0, 0.2);
                margin-top: 100px;
                border-radius: 10px;
                padding: 20px;
            }
            .box h2 {font-size: 23px;
                     text-align: center;}
            .box h4{text-align: center;}
            .box input[type="submit"]{border: 0px;
                                      border-radius: 5px;
                                      letter-spacing: 1px;
                                      background: #0ebd1d;
                                      cursor: pointer;
                                      margin-top: 25px;
                                      transition: all 0.1s ease;
                                      padding: 8px 30px;
            }
            .box input[type="submit"]:hover{
                box-shadow: 0 62px 0 0 #FF6600 inset;
                transition: all 0.1s ease;}
            </style>
        </head>
        <body>
            <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 box">
                    <img src="<?= COMPANYLOGO; ?>" class="center-block"/>
                    <form action="<?php echo $paypal_link; ?>" method="post">
                        <input type="hidden" name="business" value="<?php echo $paypal_username; ?>">
                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">
                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_name" value="<?php echo $item_name; ?>">
                        <input type="hidden" name="item_number" value="<?php echo $item_number; ?>">
                        <input type="hidden" name="amount" value="<?php echo $amount; ?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <!-- Specify URLs -->
                        <input type='hidden' name='cancel_return' value='<?= base_url(); ?>cancel'>
                        <input type="hidden" name="rm" value="2" />
                        <input type='hidden' name='return' value='<?= base_url(); ?>success'>
                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                               src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
                        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
