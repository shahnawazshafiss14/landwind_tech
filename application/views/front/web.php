<?php $this->load->view("front/header"); ?>
<!-- End Header-->

<div class="banner innerpages otherpage" style="background:url(<?= BASEURLFRONTASSET; ?>images/header_bg_1.jpg) no-repeat;background-size: cover;    position: relative;">
    <div class="saiful_overlay"></div>
    <div class="bannertext">
     <h1>Website Designing & <br> Development</h1>
     </div>
</div>



<!-- -->
<!-- <div class='container'>
    <div class='row'>
        <article>
            <div class="content">
                <h1>Website Designing & Application Development</h1>
                <p> Web development being the core foundation of the company, we help client's business by providing best in class web application development services, followed by great support. With agile web development methodology, we speed up the process of project development and deploy the project as per planned schedule.</p>
                <p>As a process oriented web application development company, we have defined process from the start to project deployment. At the project commencement, our team of business analysts have detailed meeting with the client and draft the comprehensive scope of project, which is then shared with the project development team with the module definitions and milestones. Our creative team works closely with client and designs the layout, which is 100% in accordance to client needs. After the design approval, HTML is done and files are shared to development team to start the programming.</p>
                <p>We offer custom web application development services including PHP development, wordpress development, joomla development and drupal development.</p>
                <p>Our web application development services include:</p>
                <ul class="list-unstyled">
                    <li>Web Application Development</li>
                    <li>Web Portal Development</li>
                    <li>eCommerce Development</li>    
                </ul>
            </div>
        </article>
    </div>
</div> -->


<div class="what_offer_box">
 <div class="container">
 <div class="row">
<div class="col-md-12 ">    
<h2 class="section-title text-center color-3 mb-40 wow fadeInUp"><span>What</span> We Offer? </h2>    
 </div>    
</div>
  </div>    
</div>



<div class="website-designer-services">
  <ul>
  

    <li>
      <div class="img-box"><img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/responsive-website-designing-company-in-delhi.gif" alt="Responsive Website Designing Company In Delhi" title="Responsive Website Designing Company In Delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Responsive Designing</p>
                <p></p><p>We use advanced web technology so that our clients can stand out from the crowd. We use responsive web templates so that you get visitors from the laptops as well as from the mobile gadgets. We offer you the website that can easily get accessed from mobile phones, tablets &amp; other gadgets. We offer&nbsp;responsive web designing services at an affordable rate.</p>

      </div>
    </li>

    <li>
      <div class="img-box"><img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/static-website-designing-company-in-delhi.gif" alt="Static Website Designing Company In Delhi" title="Static Website Designing Company In Delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Cake PHP Development</p>
                <p>It is perfect for those who don’t want to change their content for a long time. It is very easy to load and attract more customers. We are a leader in designing static websites for your business.</p>

      </div>
    </li>

    <li>
      <div class="img-box"><img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/e-commerce-website-designing-company-in-delhi.gif" alt="E-commerce Website Designing Company In Delhi" title="E-commerce Website Designing Company In Delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">E-commerce Website Development</p>
                <p>It’s a big thing for the e-store owners because their sale totally depends upon it. And at Landwindtech Pvt. Ltd. we offer secure and responsive e-commerce designing services to our valuable clients.</p>

      </div>
    </li>

    <li>
      <div class="img-box"><img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/dynamic-web-designing-company-in-delhi.gif" alt="Dynamic Web Designing Company In Delhi" title="Dynamic Web Designing Company In Delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Dynamic Web Designing   </p>
                <p>Nowadays, the whole world goes for dynamic and wants something new every time that can easily change according to the requirement of the client and display different content every time. We are the right choice for Dynamic web designing services.</p>

      </div>
    </li>

    <li>
      <div class="img-box"><img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/flash-website-designing-company-in-delhi.gif" alt="Flash Website Designing Company In Delhi" title="Flash Website Designing Company In Delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Flash Website Designing</p>
                <p>This is mostly used to develop interactive images, animations and many more. This will help to give multimedia experience to your users and definitely enhance your sale. For flash web designing services, there is no better place then  Landwindtech Pvt. Ltd..</p>

      </div>
    </li>
    
     
  </ul>
</div>

<?php $this->load->view("front/brand"); ?>
<div class="upper-footer"> </div>

<?php $this->load->view("front/footer"); ?> 