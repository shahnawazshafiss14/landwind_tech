<?php $this->load->view("front/header"); ?>
<!-- End Header-->
<div class="banner innerpages">
    <img src="<?= BASEURLFRONTASSET; ?>images/bannerinner.png" alt="">
</div>
<div class='fluid-container graybg'>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:;">Services</a></li>
            <li class="active">Web development philosophy</li>
        </ul>
    </div>
</div>
<!-- -->
<div class='container'>
    <div class='row'>
        <aside class="sidenav hidden-sm hidden-xs">
            <ul>
                <li><a href="<?= base_url(); ?>about-us">About Landwind Tech</a></li>
                <li ><a href="javascript:void();">Website & Application Development</a></li>
                <li><a href="<?= base_url(); ?>search-engine-optimisation">Search Engine Optimisation</a></li>
                <li><a href="<?= base_url(); ?>mobile-application">Mobile Application</a></li>
                <li><a href="<?= base_url(); ?>social-media-marketing">Social Media Marketing</a></li>
                <li><a href="<?= base_url(); ?>our-mission">Our Mission</a></li>
                 <li class="active"><a href="<?= base_url(); ?>web-development-philosophy">Web development philosophy</a></li>
                <li><a href="<?= base_url(); ?>business-application-development">Business Application Development</a></li>
                <li><a href="<?= base_url(); ?>training">Training</a></li>
                 
            </ul>
        </aside>
        <article>
            <div class="content">
                <h1>Web development philosophy</h1>
                <p> we believe in making a qualityfull website and giving all the facility which requires for our clients regarding It.our vision is making website for growth of business , which needs to be well-planned, well-executed, well-managed and well-supported. That’s why, when partnering with our client we take the solid approach of laying a rock-solid foundation for growth,</p>
                <p>Our philosophy  stands to be beneficial and coaprative for clients.we firmly believe in  team work. and pursuit of excellence across the entire work ethic. It's the motivating force that empowers us to push boundaries beyond previous limits – our own limits, and those set by our competitors.</p>
                <p>It's a vision-based philosophy that inspires us to create practical, rewarding relationships with our clients while delivering rich, dynamic web solutions .                 </p>
                <ul class="list-unstyled">
                    <li>Web Application Development</li>
                    <li>Web Portal Development</li>
                    <li>eCommerce Development</li>    
                </ul>
            </div>
        </article>
    </div>
</div>

<?php $this->load->view("front/footer"); ?> 