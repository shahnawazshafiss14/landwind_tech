
<?php $this->load->view("front/header"); ?>

<!-- End Header-->

<div class="banner innerpages otherpage" style="background:url(<?= BASEURLFRONTASSET; ?>images/header_bg_1.jpg) no-repeat;background-size: cover;    position: relative;">
    <div class="saiful_overlay"></div>
    <div class="bannertext">
     <h1>Our Mission</h1>
  </div>
</div>


<!-- -->


<!--        <p>OUR MISSION is giving best services to clients to enhancing the business growth of our customers with creative Design and Development to deliver market-defining high-
quality solutions that create value and consistent competitive advantage for our clients around the world.</p>
       <p>Our Vision is very different to other,we believe in providing quality Web, Print and Software solutions in the competitive global market place. </p>
       <p>Our Commitment We take pride in our on time delivery and ability to meet quick turn around requests while exceeding customer quality demands.
Customer Satisfaction continues to be of utmost importance to CWS, as do Consistent quality, Constant innovation, Technology enhancement, Process improvement and
Customer orientation.</p>

        <p>We have developed our core competence and aligning objectives at all levels so as to realize synergy in operations. It is our collaborative approach, creative input,
and emphasis on economical solutions that has allowed us to develop an impressive and diverse client list.
</p> -->



<div class="what_offer_box">
 <div class="container">
 <div class="row">
<div class="col-md-12">    
<h2 class="section-title text-center color-3 mb-40 wow fadeInUp"><span>Our </span>Vision </h2>    
 </div>    
</div>
  </div>    
</div>


<div class="clearfix"></div>

<div class="container">

    <div class="row-box">
    <div class="row clearfix">
                    <div class="col-md-6 col-sm-5 padd-left0">
                        <div class="image">
                            <img src="<?= BASEURLFRONTASSET; ?>images/our-mision1.jfif" class="img-responsive" alt="">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-7">
                        <div class="vision-txt">
                            <h2 class="aheto-heading__title">Our <span class="moovit-dot dot-primary">Mission</span></h2>
                     <p>OUR MISSION is giving best services to clients to enhancing the business growth of our customers with creative Design and Development to deliver market-defining high-
quality solutions that create value and consistent competitive advantage for our clients around the world. Our Vision is very different to other,we believe in providing quality Web, Print and Software solutions in the competitive global market place.</p>
       
                            </div>
                        </div>
                    </div>
                    </div>



<div class="center-brief">   
       <div class="homeport-hd wow fadeInUp" data-wow-duration="1s">
          <h5 class="aheto-heading__subtitle">A SHORT BRIEF</h5>
          <h2 class="aheto-heading__title">Here to Help Your Business <span class="moovit-dot dot-primary">Need</span></h2>
          <h6>Stop worrying about any shipping problems. Focus on your business. Let us provide the support you deserve.</h6>
       </div>
       
       <div class="clear"></div>      
  </div>



    <div class="row-box">
    <div class="row clearfix">
                    <div class="col-md-6 col-sm-4 padd-left0 pull-right">
                        <div class="image">
                            <img src="<?= BASEURLFRONTASSET; ?>images/our-mision2.jpg" class="img-responsive" alt="">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-7">
                        <div class="vision-txt">
                    <h2 class="aheto-heading__title">The <span class="moovit-dot dot-primary">Goal</span></h2>
       <p>Our Commitment We take pride in our on time delivery and ability to meet quick turn around requests while exceeding customer quality demands.
Customer Satisfaction continues to be of utmost importance to CWS, as do Consistent quality, Constant innovation, Technology enhancement, Process improvement and
Customer orientation.</p>
                            </div>
                        </div>
                    </div>

</div>

</div>


<?php $this->load->view("front/brand"); ?>
<div class="upper-footer"> </div>



<!-- -->

<!-- Footer-->


<?php $this->load->view("front/footer.php"); ?>