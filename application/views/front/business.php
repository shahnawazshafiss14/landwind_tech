<?php $this->load->view("front/header"); ?>
<!-- End Header-->
<div class="banner innerpages">

    <img src="images/bannerinner.png" alt="">

</div>

<div class='fluid-container graybg'>

    <div class="container">

        <ul class="breadcrumb">

            <li><a href="">Services</a></li>

            <li class="active">Business Application Development</li>

        </ul>

    </div>

</div>

<!-- -->
<div class='container'>
    <div class='row'>
        <aside class="sidenav hidden-sm hidden-xs">
            <ul>
                <li><a href="<?= base_url(); ?>about-us">About Landwind Tech</a></li>
                <li ><a href="javascript:void();">Website & Application Development</a></li>
                <li><a href="<?= base_url(); ?>search-engine-optimisation">Search Engine Optimisation</a></li>
                <li><a href="<?= base_url(); ?>mobile-application">Mobile Application</a></li>
                <li><a href="<?= base_url(); ?>social-media-marketing">Social Media Marketing</a></li>
                <li><a href="<?= base_url(); ?>our-mission">Our Mission</a></li>
                <li><a href="<?= base_url(); ?>business-application-development">Business Application Development</a></li>
                <li><a href="<?= base_url(); ?>web-development-philosophy">Web development philosophy</a></li>
                <li class="active"><a href="<?= base_url(); ?>business-application-development">Business Application Development</a></li>
                <li><a href="<?= base_url(); ?>training">Training</a></li>
                 
            </ul>
        </aside>
        <article>
            <div class="content">
                <h1>Business Application Development</h1>
                <p> Every organization strives hard to maintain a close customer relationship in order to acquire profitable return on investment (ROI). In this fast moving world, a custom-made application or software can serve best to improve the efficiency of your business. Thus any application that best suits your business can streamline your work processes and amplify the productivity</p>
                <p>we develop customized Business Applications considering the unique requirements of your organization; either you require an end user business application or an enterprise business application, we deliver flexible and highly secure solution. The developers with immense knowledge can provide customized solutions for large, medium and small business requirements of both internal and external processes of various domain industries.
</p>
     
                <ul class="list-unstyled">
                    <li>Web Application Development</li>
                    <li>Web Portal Development</li>
                    <li>eCommerce Development</li>    
                </ul>
            </div>
        </article>
    </div>
</div>

<?php $this->load->view("front/footer"); ?> 