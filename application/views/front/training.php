
<?php $this->load->view("front/header"); ?>

<!-- End Header-->






<div class="clearfix"></div>
<section class="fw-main-row" style="padding-top: 80px;">
   <div class="fw-container-fluid">
      <div class="fw-row">
         <div class="fw-col-xs-12">
            <div class="ti_section ti_expertise_wrapper">
               <div class="container">
                  <div class="row">
                     <div class="ti_flex_wrapper">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="ti_heading_wraper">
                              <span><img src="<?= BASEURLFRONTASSET; ?>images/heading_icon.png" alt=""></span>
                              <h1>Our  <br> Expertise</h1>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="ti_expertise_box text-center ti_shadow_box">
                              <img src="<?= BASEURLFRONTASSET; ?>images/1-101x80.png" alt="Learn courses online">
                              <h2 class="ti_subheading">Learn courses online</h2>
                              <p>Aliquam erat volutpat. Donec laoreet iaculis [...]</p>
                              <div class="ti_overlay_box">
                                 <h2 class="ti_subheading">Learn courses online</h2>
                                 <p>Aliquam erat volutpat. Donec laoreet iaculis [...]</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="ti_expertise_box text-center ti_shadow_box">
                              <img src="<?= BASEURLFRONTASSET; ?>images/2.png" alt="Best industry experts">                     
                              <h2 class="ti_subheading">Best industry experts</h2>
                              <p>Nunc nec nisi vitae ipsum pharetra tincidunt. [...]</p>
                              <div class="ti_overlay_box">
                                 <h2 class="ti_subheading">Best industry experts</h2>
                                 <p>Nunc nec nisi vitae ipsum pharetra tincidunt. [...]</p>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                           <div class="ti_expertise_box text-center ti_shadow_box">
                              <img src="<?= BASEURLFRONTASSET; ?>images/3-101x80.png" alt="See our best skills">                        
                              <h2 class="ti_subheading">See our best skills</h2>
                              <p>Lorem ipsum dolor sit amet, consectetur [...]</p>
                              <div class="ti_overlay_box">
                                 <h2 class="ti_subheading">See our best skills</h2>
                                 <p>Lorem ipsum dolor sit amet, consectetur [...]</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>





<section class="fw-main-row" style="background-color:#f9f9fa;" >
   <div class="fw-container-fluid">
      <div class="fw-row">
         <div class="fw-col-xs-12">
            <div class="ti_section">
               <div class="container">
                  <div class="row">
                     <div class="ti_flex_wrapper">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-lg-push-9 col-md-push-9 col-sm-push-0 col-xs-push-0 ti_heading_center">
                           <div class="ti_heading_wraper">
                              <span><img src="<?= BASEURLFRONTASSET; ?>images/heading_icon.png" alt=""></span>
                              <h1>How  <br> We Teach</h1>
                           </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-pull-3 col-md-pull-3 col-sm-pull-0 col-xs-pull-0">
                           <div class="ti_counseling_slider">
                              <div class="ti_counseling_slide">
                                 <div class="owl-carousel owl-theme" id="single-slider">
                                    <div class="item">
                                       <div class="ti_flex_wrapper">
                                          <div class="ti_counseling_img">
                                             <img src="<?= BASEURLFRONTASSET; ?>images/crs1-3-250x180.jpg" alt="Student Guidance" class="img-responsive">
                                          </div>
                                          <div class="ti_counseling_inner">
                                             <h2>Student Guidance</h2>
                                             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into [&hellip;]</p>
                                             <a href="javascript:void(0);" class="ti_btn  theme-btn btn-style-two"><span class="btn-title"><span class="txt">Read more</span></a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="ti_flex_wrapper">
                                          <div class="ti_counseling_img">                                          
                                             <img src="<?= BASEURLFRONTASSET; ?>images/crs2-2-250x180.jpg" alt="Elementary School" class="img-responsive">
                                          </div>
                                          <div class="ti_counseling_inner">
                                             <h2>Elementary School</h2>
                                             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into [&hellip;]</p>
                                              <a href="javascript:void(0);" class="ti_btn  theme-btn btn-style-two"><span class="btn-title"><span class="txt">Read more</span></a>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="ti_flex_wrapper">
                                          <div class="ti_counseling_img">       
                                             <img src="<?= BASEURLFRONTASSET; ?>images/e1-250x180.jpg" alt="Vocational Counselling" class="img-responsive">
                                          </div>
                                          <div class="ti_counseling_inner">
                                             <h2>Vocational Counselling</h2>
                                             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into [&hellip;]</p>
                                             <a href="javascript:void(0);" class="ti_btn  theme-btn btn-style-two"><span class="btn-title"><span class="txt">Read more</span></a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="ti_counseling_gradient_border"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>









<section class="fw-main-row">
   <div class="fw-container-fluid">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom: 50px;">
               <div class="ti_heading_wraper">
                  <span><img src="<?= BASEURLFRONTASSET; ?>images/heading_icon.png" alt=""></span>
                  <h1>Recommended <br>Courses</h1>
               </div>
            </div>
         </div>

         <!-- course row start -->
         <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/web-design.png" alt="web designing" class="img-responsive">
                                 </div>
                                 <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">web designing</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 2500.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>

            <!-- col-2nd start -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/seo.png" alt="Writing skills" class="img-responsive">
                                 </div>
                                  <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">Seo</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 8000.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>
            <!-- col-2nd end -->

            <!-- col-3nd start -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/php.png" alt="UI/UX Designing" class="img-responsive">
                                 </div>
                                <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">Php</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 3500.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>
            <!-- col-3nd end -->

            <!-- col-4nd start -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/android.png" alt="Model Specificity" class="img-responsive">
                                 </div>
                              <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">Android</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 4000.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>
            <!-- col-4nd end -->
         </div>
         <!-- course row end -->
<br>
         <!-- course row start -->
         <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/js.png" alt="Management Prog." class="img-responsive">
                                 </div>
                              <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">Java Script</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 6000.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>

            <!-- col-2nd start -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/reactjs.png" alt="Javascript Campus" class="img-responsive">
                                 </div>
                                  <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">React Js</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 6000.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>
            <!-- col-2nd end -->

            <!-- col-3nd start -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/larvel.png" alt="Javascript Campus" class="img-responsive">
                                 </div>
                                  <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">Laravel</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 4000.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>
            <!-- col-3nd end -->

            <!-- col-4nd start -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-3">
               <div class="item swiper-slide">
                           <div class="ti_course_box ti_shadow_box">
                              <div class="ti_course_img_box">
                                 <div class="ti_course_img">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/flutter.png" alt="Javascript Campus" class="img-responsive">
                                 </div>
                                  <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i></a>
                              </div>
                              <div class="ti_course_detail">
                                 <h2 class="ti_subheading"><a href="javascript:void(0);">Flutter</a></h2>
                                 <ul>
                                    <li>
                                       <div class="ti_course_detail_inner ti_course_price">
                                          <h1>₹ 4000.00</h1>
                                       </div>
                                       <div class="ti_course_detail_inner">
                                          <p>rating</p>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
            </div>
            <!-- col-4nd end -->
            
         </div>
         <!-- course row end -->

      </div>
   </div>
</section>


<section class="fw-main-row   " style="background-color:#f9f9fa;" >
   <div class="fw-container-fluid">
      <div class="fw-row">
         <div class="fw-col-xs-12">
            <div class="ti_section ti_choose_wrapper">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <div class="ti_heading_wraper">
                           <span><img src="<?= BASEURLFRONTASSET; ?>images/heading_icon.png" alt=""></span>
                           <h1>Why<br>Choose Us</h1>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="ti_expertise_box ti_shadow_box ti_choose_box">
                           <div class="ti_choose_box_inner">
                              <div class="ti_choose_img">
                                 <img src="<?= BASEURLFRONTASSET; ?>images/1-1.png" alt="">
                              </div>
                              <div class="ti_choose_detail">
                                 <h2 class="timer" data-from="0" data-to="2500" data-speed="5000" data-refresh-interval="50">2500</h2>
                                 <p class="ti_subheading">
                                    Students Successive
                                 </p>
                              </div>
                           </div>
                           <div class="ti_overlay_box text-center">
                              <h2 class="ti_subheading">Students Successive</h2>
                              <p>Only competitions were the ones in the back of the magazines you find..</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="ti_expertise_box ti_shadow_box ti_choose_box">
                           <div class="ti_choose_box_inner">
                              <div class="ti_choose_img">
                                 <img src="<?= BASEURLFRONTASSET; ?>images/2-1.png" alt="">
                              </div>
                              <div class="ti_choose_detail">
                                 <h2 class="timer" data-from="0" data-to="4500" data-speed="5000" data-refresh-interval="50">4500</h2>
                                 <p class="ti_subheading">
                                    Competitions Won
                                 </p>
                              </div>
                           </div>
                           <div class="ti_overlay_box text-center">
                              <h2 class="ti_subheading">Competitions Won</h2>
                              <p>Only competitions were the ones in the back of the magazines you find..</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="ti_expertise_box ti_shadow_box ti_choose_box">
                           <div class="ti_choose_box_inner">
                              <div class="ti_choose_img">
                                 <img src="<?= BASEURLFRONTASSET; ?>images/3-1.png" alt="">
                              </div>
                              <div class="ti_choose_detail">
                                 <h2 class="timer" data-from="0" data-to="8530" data-speed="5000" data-refresh-interval="50">8530</h2>
                                 <p class="ti_subheading">
                                    Magazines Won
                                 </p>
                              </div>
                           </div>
                           <div class="ti_overlay_box text-center">
                              <h2 class="ti_subheading">Magazines Won</h2>
                              <p>Only competitions were the ones in the back of the magazines you find..</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>




<!-- Footer-->
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>
 
 <script type="text/javascript">
     $(document).ready(function () {
$('#single-slider').owlCarousel({
    items : 1, 
    loop:true,
    nav:false,
    dots:true,
    autoplay:true,
    autoplayTimeout:4000,
    responsive:{
        0:{
            items:1,
            dots:false,
        },
        600:{
            items:1,
            dots:false,

        },
        1000:{
            items:1
        }
    }
});
});   

  $(document).ready(function () {
$('#swiper-wrapper').owlCarousel({
    items: 5, 
    margin:30,
    loop:true,
    nav:false,
    dots:false,
    autoplay:true,
    autoplayTimeout:2000,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2
        },
        1200:{
            items:3
        },   
        1300:{
            items:5
        }
    }
});



});
 </script>