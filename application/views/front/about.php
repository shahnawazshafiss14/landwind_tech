<?php $this->load->view("front/header"); ?>
<!-- End Header-->

<div class="banner innerpages otherpage" style="background:url(<?= BASEURLFRONTASSET; ?>images/header_bg_1.jpg) no-repeat;background-size: cover;    position: relative;">
    <div class="saiful_overlay"></div>
    <div class="bannertext">
                            <h1>We're A Digital<br>Agency Company</h1>
                        </div>
</div>

<!-- -->
<section class="about-section">
        <div class="section-icons">
            <!-- Icon One -->
            <div class="icon-one" style="background-image:url(<?= BASEURLFRONTASSET; ?>images/icon-1.png)"></div>
            <!-- Icon Two -->
            <div class="icon-two" style="background-image:url(<?= BASEURLFRONTASSET; ?>images/icon-2.png)"></div>
            <!-- Icon Three -->
            <div class="icon-three" style="background-image:url(<?= BASEURLFRONTASSET; ?>images/icon-4.png)"></div>
        </div>

        <div class="auto-container">

            <div class="row clearfix">

                <!-- Title Column -->
                <div class="title-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">

                        <!-- Sec Title -->
                        <div class="sec-title">
                            <div class="title">About the Agency</div>
                            <div class="separator"><span></span></div>
                            <h2>Insights and Resources to <br> help drive your Business <br> Forward Faster.</h2>
                        </div>
                        <div class="text">we build results-oriented brand strategies and continually refine your canpaigns for the greatest outcome. from full-scale digital marketing and advertising strategy, right through to our precise execution and reporting that’s right,we’ve got you covered</div>
                        <!-- Author Box -->
                        <!--div class="author-box">
                            <div class="box-inner">
                                <div class="image"><img src="<?= BASEURLFRONTASSET; ?>images/images.jpg" alt=""></div>
                                <h3>Dntuli Frek</h3>
                                <div class="designation">Founden &amp; CEO</div>
                            </div>
                        </div>
                        <div class="signature"><img src="<?= BASEURLFRONTASSET; ?>images/signature.png" alt=""></div-->
                    </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInRight animated" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= BASEURLFRONTASSET; ?>images/about-1.png" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



<section class="quote-section">
  

        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title light centered">
                <div class="title">Ready to Grow</div>
                <div class="separator"><span></span></div>
                <h2>Get Your Free Quote Today</h2>

            </div>

            <!-- Quote Icon -->
            <div class="quote-icon">
                <img src="<?= BASEURLFRONTASSET; ?>images/quote-icon.png" alt="">
            </div>




            <!-- Counter Boxed -->
              <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                    <div class="location-box wow fadeInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms">
                     <div id="count-num"><span class="count">1000</span><small>+</small> <span class="text-num">Works Done</span> </div>
                    </div>
                </div>
             <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4">
                    <div class="location-box text-center wow fadeInLeft animated" data-wow-delay="300ms" data-wow-duration="1500ms">
                      <div id="count-num"><span class="count">2000</span><small>+</small> <span class="text-num">Happy Clients</span> </div>
                    </div>
                </div>
             <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                    <div class="location-box text-right wow fadeInLeft animated" data-wow-delay="600ms" data-wow-duration="1500ms">
                      <div id="count-num"><span class="count">25</span><small>+</small> <span class="text-num">Years Experience</span> </div>
                    </div>
                </div>
              </div>

        </div>

    </section>


<section class="work-section">


      <div class="section-icons">
            <!-- Icon One -->
            <div class="icon-one" style="background-image:url(<?= BASEURLFRONTASSET; ?>images/icon-1.png)"></div>
            <!-- Icon Two -->
            <div class="icon-two" style="background-image:url(<?= BASEURLFRONTASSET; ?>images/icon-2.png)"></div>
            <!-- Icon Three -->
            <div class="icon-three" style="background-image:url(<?= BASEURLFRONTASSET; ?>images/icon-4.png)"></div>
        </div>

    <div class="auto-container">
            <div class="row clearfix">

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="<?= BASEURLFRONTASSET; ?>images/work.png" alt="">
                        </div>
                    </div>
                </div>

                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <div class="title">Our Selected Works</div>
                            <div class="separator"><span></span></div>
                            <h2>Introducing Our Work</h2>
                        </div>
                        <div class="bold-text">We cater our campaigns to each client specifically </div>
                        <div class="text">
                            <p>Our creativity is driven by data in an effort to give you the best possible result and position your brand for success.</p>
                            <p>We’re on a mission to start a conversation with your customers in this fast connected world. Let’s discover, build and grow your digital business. We fuel business growth through innovative digital marketing and technology solu-tions.</p>
                        </div>
                        <a href="javascript:void(0);" class="theme-btn btn-style-two"><span class="btn-title"><span class="txt">More Services</span></span></a>
                    </div>
                </div>

            </div>
        </div>

    </section>






<!-- Footer-->
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>

<script type="text/javascript">
    $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};



let animateDone = false;
$(window).on('resize scroll load', function() {
  //check if in vp
  if($('.count').isInViewport()){
    $('.count').each(function () {
      if(!animateDone){
        $(this).prop('Counter',0).animate({
          Counter: $(this).text()
        }, {
          duration: 4000,
          easing: 'swing',
          step: function (now) {

            $(this).text(Math.ceil(now));
          }
        });
      }
    });
    animateDone = true;
  }
});
</script>