 
<?php $this->load->view("front/header"); ?>
    
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

 

<div class="clearfix"></div>




<div class="col-md-12  padd-top150">
	<div class="port_heading_wrapper text-center prt_bottompadder40">
		<div class="port_sub_heading_wrapper">
			<h2 class="port_sub_heading">Quality Work</h2>
		</div>
			<h1 class="port_heading">Our Projects</h1>
	</div>
</div>

 
<section class="blog-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img1.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- coloumn1 end -->

			<!-- column 2 start -->
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img2.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							<img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- column 2 end -->

			<!-- column 3 start -->
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img4.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- column 3 end -->
		</div>
<br>
		<!-- blog 2 row start -->
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img3.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- coloumn1 end -->

			<!-- column 2 start -->
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img5.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- column 2 end -->

			<!-- column 3 start -->
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img6.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- column 3 end -->
		</div>
		<!-- blog 2nd row end -->

		<br>
		<!-- blog 3rd row start -->
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img7.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- coloumn1 end -->

			<!-- column 2 start -->
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="blog-box">
					<div class="blog-img">
						<img src="<?= base_url('assests/images/blog-img8.png'); ?>">
					</div>
					<div class="blog-text">
						<h4>Lorem Ipsum- is simply dummy text</h4>
						<p>
							 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
							 <img src="<?= base_url('assests/images/blog-arrow.png'); ?>" class="right-arrow" alt="arrow Right" />
						</p>
						
					</div>
				</div>
			</div>
			<!-- column 2 end -->
		</div>
		<!-- blog 3rd row end -->
	</div>
</section>




          

<!-- --> 
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>
<script type="text/javascript">


	$('.ex_btn').click(function(){
		$(".more_content").not($(this).siblings(".more_content")).slideUp();
			  $(".ex_btn").not(this).text("Read More");
			  $(this).siblings('.more_content').slideToggle();
			  if ($(this).text() == "Read More") {
				$(this).text("Read Less")
			  } else {
				$(this).text("Read More")
			  }
	})
	/*------------------------------------------------------------------*/ 

        $('.portfolio-item').isotope({
         	itemSelector: '.item',
         	layoutMode: 'fitRows'
         });
         $('.portfolio-menu ul li').click(function(){
         	$('.portfolio-menu ul li').removeClass('active');
         	$(this).addClass('active');
         	
         	var selector = $(this).attr('data-filter');
         	$('.portfolio-item').isotope({
         		filter:selector
         	});
         	return  false;
         });
         $(document).ready(function() {
         var popup_btn = $('.popup-btn');
         popup_btn.magnificPopup({
         type : 'image',
         gallery : {
         	enabled : true
         }
         });
         });

  /* Demo purposes only */
  $("figure").mouseleave(
    function() {
      $(this).removeClass("hover");
    }
  );

</script>