 
<?php $this->load->view("front/header"); ?>
    
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

<!-- End Header-->
<!-- <div class="banner innerpages otherpage port_bannerbg_wrapper" style="background:url(https://lh3.googleusercontent.com/-xkIhhDSR4uQ/XhGPez7sqPI/AAAAAAAAFx4/r68DEvAybM0mRsVSxQ9WHwf8aT5CiSwxgCK8BGAsYHg/s0/marvin-meyer-SYTO3xs06fU-unsplash.jpg) no-repeat;background-size: cover;    position: relative;">

    <div class="bannertext">
                            <h1>BLOG Detail</h1>
                        </div>
    </div> -->

<!-- -->

<div class="clearfix"></div>



<section class="main-content blog-posts padd-top150">
        <div class="container">
            <div class="row">
                <div class="fixed-content clearfix">

         <div class="col-md-12">
                                <div class="port_heading_wrapper text-center prt_bottompadder40  wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="port_sub_heading_wrapper">
                                        <h2 class="port_sub_heading">Our Blog</h2>
                                    </div>
                                    <h1 class="port_heading">Recent News</h1>
                                </div>
                            </div>

                <div class="col-md-9">
                    <div class="blog-single">
                    <div class="post-wrap">
                        <article class="entry format-standard">
                            <div class="feature-post wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                                <div class="type-post">
                                </div>
                                <div class="entry-image">
                                    <img src="<?= BASEURLFRONTASSET; ?>images/blog-detail.jpg" alt="image">
                                </div>
                            </div>

                            <div class="main-post" id="myHeader">
                                <h2 class="entry-title wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                                Donec porttitor justo et odio dignissim.
                                </h2>
                                <div class="entry-meta wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">                                    
                                    <span class="date">Feb 21th, 2016</span>
                                    <span class="author"><a href="#">Bill Gates</a></span>
                                    <span class="comment"><a href="#">04</a></span>
                                    <span class="vote"><a href="#">20</a></span>
                                </div><!-- /.entry-meta -->

                                <div class="entry-content wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                                    <p><span class="drop-caps">L</span>
                                    orem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet luctus odio. Praesent vitae ex sit amet orci lacinia congue. Etiam venenatis mi nec dapibus tempus. Curabitur a mauris sit amet
                                    tellus pharetra placerat at eu est. In congue felis metus, non fringilla eros commodo quis. Donec velit nisi, bibendum quis ipsum sed, cursus vulputate felis. Donec elementum euismod leo eget ullamcorper. Cras id lobortis est, id convallis tortor. Pellentesque vel commodo velit, id tincidunt mi.</p> 
                                    <p>Phasellus vitae purus a leo consequat gravida non mattis mi. In iaculis est ut lorem tincidunt varius. Integer a congue purus. Vivamus dui velit, sagittis suscipit ac, pretium aliquet mi. Praesent euismod luctus eleifend. Morbi posuere est vel varius pharetra.</p>

                                    <blockquote class="alignleft">
                                        <div class="wrap-text">
                                            <div class="blockqoute-text">&nbsp;&nbsp;&nbsp;&nbsp;
                                            Cum sociis natoque penatibus et magnis parturient montes, nascetur ridiculus mus. Ut viverra purus id ornare arcu eleifend.</div>
                                            <div class="whisper">By: Bill Gates</div>
                                        </div>
                                    </blockquote>

                                    <p>
                                    Pellentesque faucibus auctor porttitor. Ut id bibendum est, vitae pretium diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam eu dui a quam aliquam cursus. Cras ac orci et magna placerat elementum sed ut nunc. Nam risus tortor, malesuada at eros vitae, bibendum ultrices felis.</p>
                                    <p>
                                    Aenean pretium, mauris imperdiet molestie feugiat, tortor nibh faucibus lorem, eu porta justo leo quis ante. Nulla accumsan aliquam purus ut convallis.
                                    </p>
                                </div><!-- /.entry-post -->
                                <div class="clearfix"></div>

                                <div class="wrap-share wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1500ms">
                                    <div class="share-post">                                   
                                        <ul class="flat-socials">
                                            <li class="facebook">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="twitter">
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="dribbble">
                                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                            </li>
                                            <li class="camera">
                                                <a href="#"><i class="fa fa-camera-retro"></i></a>
                                            </li>
                                            <li class="pinterest">
                                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            </li>
                                        </ul>
                                    </div>    

       
                                </div><!-- /.wrap-share -->
                            </div><!-- /.main-post -->
                        </article>                              

                        <div class="comment-post clearfix">
                            <div class="comment-list-wrap">
                                <h4 class="comment-title wow fadeInUp" data-wow-delay="1000ms" data-wow-duration="1500ms">COmments (04)</h4>
                                <ul class="comment-list clearfix">
                                    <li class="clearfix">
                                        <article class="comment wow fadeInUp" data-wow-delay="700ms" data-wow-duration="1500ms">
                                            <div class="comment-avatar">
                                                <img src="<?= BASEURLFRONTASSET; ?>images/comment-1.png" alt="image">
                                            </div>                  
                                            <div class="comment-detail">
                                                <div class="comment-meta">
                                                    <span class="comment-author"><a href="#">Bill Gates</a></span> 
                                                    <span class="comment-date">Feb 21,2016 at 09:00 pm</span> 
                                                </div>
                                                <p class="comment-body">Class aptent taciti sociosqu a litora torquent per conubia nostra, per inceptos himenaeos. In finibus mauris sed auctor tincidunt. Nunc finibus nisi quis ligula aliquet suscipit vel eget elit. In posuere mauris at metus imperdiet lacinia [...]</p>
                                            </div>
                                        </article><!-- /.comment -->
                                        <ul class="children clearfix">
                                            <li class="clearfix">
                                                <article class="comment wow fadeInUp" data-wow-delay="800ms" data-wow-duration="1500ms">
                                                <div class="comment-avatar">
                                                    <img src="<?= BASEURLFRONTASSET; ?>images/comment-2.png" alt="image">
                                                </div>                  
                                                <div class="comment-detail">
                                                    <div class="comment-meta">
                                                        <span class="comment-author"><a href="#">Steven gray</a></span> 
                                                        <span class="comment-date">Feb 21,2016 at 10:00 pm</span> 
                                                    </div>
                                                    <p class="comment-body">Class aptent taciti sociosqu a litora torquent per conubia nostra, per inceptos himenaeos. In finibus mauris sed auctor tincidunt. Nunc finibus nisi quis ligula aliquet suscipit vel eget elit metus imperdiet lacinia [...]</p>
                                                </div>
                                            </article><!-- /.comment -->
                                            </li>
                                        </ul><!-- /.children -->
                                    </li>

                                    <li class="clearfix">
                                        <article class="comment wow fadeInUp" data-wow-delay="900ms" data-wow-duration="1500ms">
                                            <div class="comment-avatar">
                                                <img src="<?= BASEURLFRONTASSET; ?>images/comment-3.png" alt="image">
                                            </div>                  
                                            <div class="comment-detail">
                                                <div class="comment-meta">
                                                    <span class="comment-author"><a href="#">Jenny Bada</a></span> 
                                                    <span class="comment-date">Feb 25,2016 at 11:00 pm</span> 
                                                </div>
                                                <p class="comment-body">Class aptent taciti sociosqu a litora torquent per conubia nostra, per inceptos himenaeos. In finibus mauris sed auctor tincidunt. Nunc finibus nisi quis ligula aliquet suscipit vel eget elit. In posuere mauris at metus imperdiet lacinia [...]</p>
                                            </div>
                                        </article><!-- /.comment -->
                                    </li>

                                    <li class="clearfix">
                                        <article class="comment wow fadeInUp" data-wow-delay="1000ms" data-wow-duration="1500ms">
                                            <div class="comment-avatar">
                                                <img src="<?= BASEURLFRONTASSET; ?>images/comment-4.png" alt="image">
                                            </div>                  
                                            <div class="comment-detail">
                                                <div class="comment-meta">
                                                    <span class="comment-author"><a href="#">Eva Grove</a></span> 
                                                    <span class="comment-date">Feb 28,2016 at 16:00 pm</span> 
                                                </div>
                                                <p class="comment-body">Class aptent taciti sociosqu a litora torquent per conubia nostra, per inceptos himenaeos. In finibus mauris sed auctor tincidunt. Nunc finibus nisi quis ligula aliquet suscipit vel eget elit. In posuere mauris at metus imperdiet lacinia [...]</p>
                                            </div>
                                        </article><!-- /.comment -->
                                    </li>
                                </ul><!-- /.comment-list -->
                            </div><!-- /.comment-list-wrap -->
                           
                           <div class="clearfix"></div>
                            <div id="respond" class="comment-respond wow fadeInUp" data-wow-delay="1200ms" data-wow-duration="1500ms">
                                <h4 class="comment-title">Leave a Comment</h4>
                                <form action="#" method="post" id="commentform" class="comment-form" novalidate="">
                                    <fieldset class="name-container">
                                        <input type="text" id="author" placeholder="Name*" class="tb-my-input" name="author" tabindex="1" value="" size="32" aria-required="true">
                                    </fieldset>
                                    <fieldset class="email-container">
                                        <input type="text" id="email" placeholder="Email*" class="tb-my-input" name="email" tabindex="2" value="" size="32" aria-required="true">
                                    </fieldset>

                                    <fieldset class="phone-container">
                                        <input type="text" id="phone" placeholder="Telephone*" class="tb-my-input" name="phone" tabindex="1" value="" size="32" aria-required="true">
                                    </fieldset>

                                    <fieldset class="website-container">
                                        <input type="text" id="website" placeholder="Website*" class="tb-my-input" name="website" tabindex="2" value="" size="32" aria-required="true">
                                    </fieldset>

                                    <fieldset class="message">  
                                            <textarea id="comment-message" name="comment" rows="8" tabindex="4">Comment*</textarea>
                                    </fieldset>
                                        <p class="form-submit">
                                        <input name="submit" type="submit" id="comment-reply" class="submit" value="send message"> <input type="hidden" name="comment_post_ID" value="27" id="comment_post_ID">
                                        <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                        </p>                
                                </form>
                            </div><!-- /#respond -->
                        </div><!-- /.comment-post -->
                                               
                    </div><!-- /.post-wrap -->
                    </div><!-- /.blog-single -->
                </div><!-- /.col-md-9 -->

                <div class="col-md-3">
                    <div class="sidebar">
                        <div class="widget widget-search wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Search The Blogs <span></span></h5>
                            <form action="#" id="searchform" method="get">
                                <div>
                                    <input type="text" id="s" class="sss" placeholder="Search blog ... ">
                                    <input type="submit" value="" id="searchsubmit">
                                </div>                                
                            </form>
                        </div><!-- /.widget-search -->

            

                        <div class="widget widget-categories wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Categories<span></span></h5>
                            <ul>
                                <li><a href="#">Articles<span class="pull-right">(12)</span></a></li>
                                <li><a href="#">Web Design<span class="pull-right">(35)</span></a></li>
                                <li><a href="#">Photography<span class="pull-right">(23)</span></a></li>   
                                <li><a href="#">Videos<span class="pull-right">(31)</span></a></li>                                
                            </ul>
                        </div><!-- /.widget-categories -->
 
                        <div class="widget widget-popular-news wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">latest posts <span></span></h5>
                            <ul class="popular-news clearfix">
                                <li>
                                    <div class="thumb">
                                       <img src="<?= BASEURLFRONTASSET; ?>images/post-tag.jpg" alt="image">
                                    </div>
                                    <div class="text">
                                        <p class="date-popular-news">Feb 25th, 2016</p>
                                        <h6>
                                            <a href="#">Class aptent ad litora torquent per.  </a>
                                        </h6>                                       
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                       <img src="<?= BASEURLFRONTASSET; ?>images/post-tag.jpg" alt="image">
                                    </div>
                                    <div class="text">
                                        <p class="date-popular-news">Jan 19th, 2016</p>
                                        <h6><a href="#">Proin sit amet tortor vitae lectus.</a></h6>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                       <img src="<?= BASEURLFRONTASSET; ?>images/post-tag.jpg" alt="image">
                                    </div>
                                    <div class="text">
                                        <p class="date-popular-news">Mar 13th, 2016</p>
                                        <h6><a href="#">Aliquam sed arcu et  accumsan.</a></h6>
                                    </div>
                                </li>
                            </ul><!-- /.popular-news -->
                        </div><!-- /.widget-popular-news -->

                        <div class="widget widget-archive wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Archive<span></span></h5>
                            <ul>
                                <li><a href="#">January  (18)</a></li>
                                <li><a href="#">February  (31)</a></li>
                                <li><a href="#">March  (22)</a></li>
                                <li><a href="#">April  (16)</a></li>
                                <li><a href="#">May  (07)</a></li>
                                <li><a href="#">June  (37)</a></li>
                            </ul>
                        </div><!-- /.widget-Archive -->

                        <div class="widget widget-tags wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Popular Tags<span></span></h5>
                            <div class="tag-list">
                                <a class="active" href="#">Fashion</a>
                                <a href="#">Photoshop</a>
                                <a href="#">Business</a>
                                <a href="#">Typography</a>
                                <a href="#">Bike</a>     
                            </div>
                        </div><!-- /.widget-tags -->
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
                </div><!-- fixed-content -->
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section>




        




          

<!-- --> 
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>
<!-- <script src="http://45.249.108.101/development/ish/bisk-farm/js/sticky-sidebar.min.js"></script>
<script type="text/javascript">
        var stickySidebar = new StickySidebar('#myHeader', {
            topSpacing: 65,
            bottomSpacing: 70,
            containerSelector: '.fixed-content',
            innerWrapperSelector: '.inner-wrapper-sticky'
        });
    
</script> -->