 
<?php $this->load->view("front/header"); ?>
    
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

<!-- End Header-->
<!-- <div class="banner innerpages otherpage port_bannerbg_wrapper" style="background:url(https://lh3.googleusercontent.com/-xkIhhDSR4uQ/XhGPez7sqPI/AAAAAAAAFx4/r68DEvAybM0mRsVSxQ9WHwf8aT5CiSwxgCK8BGAsYHg/s0/marvin-meyer-SYTO3xs06fU-unsplash.jpg) no-repeat;background-size: cover;    position: relative;">

    <div class="bannertext">
                            <h1>BLOG</h1>
                        </div>
    </div> -->

<!-- -->

<div class="clearfix"></div>



<section class="main-content blog-posts padd-top150">
        <div class="container">
            <div class="row">

         <div class="col-md-12">
                                <div class="port_heading_wrapper text-center prt_bottompadder40  wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="port_sub_heading_wrapper">
                                        <h2 class="port_sub_heading">Our Blog</h2>
                                    </div>
                                    <h1 class="port_heading">Recent News</h1>
                                </div>
                            </div>

                <div class="col-md-9">
                    <div class="post-wrap">
                 <div class="port_blog_setions">

                  
                 <div class="port_blog_mainbox right_content  wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                                    <div class="row no-gutters">
                                        <div class="col-md-5 col-sm-5 col-xs_img align-self-center">
                                            <div class="port_blog_imgbox">
                                                <img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/port-blog1.jpg" alt="blog-image">   
                                            </div>  
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs_text">
                                            <div class="port_blog_contentbox">
                                                <span class="date_span">January 28, 2019</span>
                                                <h4 class="blog_heading">
                                                    <a href="blog-detail">Unique ideas products are complited project.</a>
                                                </h4>
                                                <div class="blog_info">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i><a href="">By Admin</a> </li>
                                                        <li><i class="fa fa-comments"></i><a href="">50</a></li>
                                                    </ul>
                                                </div>
                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text of classical, Contrary to popular belief.</p>
                                                <div class="blog_readmore">
                                                    <a href="blog-detail" class="readmore_btn">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                                <div class="port_blog_mainbox right_content  wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                                    <div class="row no-gutters">
                                        <div class="col-md-5 col-sm-5 col-xs_img align-self-center">
                                            <div class="port_blog_imgbox">
                                                <img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/port-blog2.jpg" alt="blog-image">   
                                            </div>  
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs_text">
                                            <div class="port_blog_contentbox">
                                                <span class="date_span">January 28, 2019</span>
                                                <h4 class="blog_heading">
                                                    <a href="blog-detail">We are provide unique ideas we help clients.</a>
                                                </h4>
                                                <div class="blog_info">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i><a href="">By Admin</a> </li>
                                                        <li><i class="fa fa-comments"></i><a href="">50</a></li>
                                                    </ul>
                                                </div>
                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text of classical, Contrary to popular belief.</p>
                                                <div class="blog_readmore">
                                                    <a href="blog-detail" class="readmore_btn">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                                <div class="port_blog_mainbox right_content  wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                                    <div class="row no-gutters">
                                        <div class="col-md-5 col-sm-5 col-xs_img align-self-center">
                                            <div class="port_blog_imgbox">
                                                <img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/port-blog3.jpg" alt="blog-image">   
                                            </div>  
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs_text">
                                            <div class="port_blog_contentbox">
                                                <span class="date_span">January 28, 2019</span>
                                                <h4 class="blog_heading">
                                                    <a href="blog-detail">See Our most successful complited project.</a>
                                                </h4>
                                                <div class="blog_info">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i><a href="">By Admin</a> </li>
                                                        <li><i class="fa fa-comments"></i><a href="">50</a></li>
                                                    </ul>
                                                </div>
                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text of classical, Contrary to popular belief.</p>
                                                <div class="blog_readmore">
                                                    <a href="blog-detail" class="readmore_btn">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                                <div class="port_blog_mainbox right_content  wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                                    <div class="row no-gutters">
                                        <div class="col-md-5 col-sm-5 col-xs_img align-self-center">
                                            <div class="port_blog_imgbox">
                                                <img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/port-blog1.jpg" alt="blog-image">   
                                            </div>  
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs_text">
                                            <div class="port_blog_contentbox">
                                                <span class="date_span">January 28, 2019</span>
                                                <h4 class="blog_heading">
                                                    <a href="blog-detail">Unique ideas products are complited project.</a>
                                                </h4>
                                                <div class="blog_info">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i><a href="">By Admin</a> </li>
                                                        <li><i class="fa fa-comments"></i><a href="">50</a></li>
                                                    </ul>
                                                </div>
                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text of classical, Contrary to popular belief.</p>
                                                <div class="blog_readmore">
                                                    <a href="blog-detail" class="readmore_btn">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>


                                 <div class="port_blog_mainbox right_content  wow fadeInUp" data-wow-delay="800ms" data-wow-duration="1500ms">
                                    <div class="row no-gutters">
                                        <div class="col-md-5 col-sm-5 col-xs_img align-self-center">
                                            <div class="port_blog_imgbox">
                                                <img class="img-responsive" src="<?= BASEURLFRONTASSET; ?>images/port-blog2.jpg" alt="blog-image">   
                                            </div>  
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs_text">
                                            <div class="port_blog_contentbox">
                                                <span class="date_span">January 28, 2019</span>
                                                <h4 class="blog_heading">
                                                    <a href="blog-detail">We are provide unique ideas we help clients.</a>
                                                </h4>
                                                <div class="blog_info">
                                                    <ul>
                                                        <li><i class="fa fa-user"></i><a href="">By Admin</a> </li>
                                                        <li><i class="fa fa-comments"></i><a href="">50</a></li>
                                                    </ul>
                                                </div>
                                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text of classical, Contrary to popular belief.</p>
                                                <div class="blog_readmore">
                                                    <a href="blog-detail" class="readmore_btn">Read More <i class="fa fa-long-arrow-right"></i></a>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                               


                       </div>     
                    </div>
                </div><!-- /.col-md-9 -->

                <div class="col-md-3">
                    <div class="sidebar">
                        <div class="widget widget-search wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Search The Blogs <span></span></h5>
                            <form action="#" id="searchform" method="get">
                                <div>
                                    <input type="text" id="s" class="sss" placeholder="Search blog ... ">
                                    <input type="submit" value="" id="searchsubmit">
                                </div>                                
                            </form>
                        </div><!-- /.widget-search -->

            

                        <div class="widget widget-categories wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Categories<span></span></h5>
                            <ul>
                                <li><a href="#">Articles<span class="pull-right">(12)</span></a></li>
                                <li><a href="#">Web Design<span class="pull-right">(35)</span></a></li>
                                <li><a href="#">Photography<span class="pull-right">(23)</span></a></li>   
                            </ul>
                        </div><!-- /.widget-categories -->
 
                        <div class="widget widget-popular-news wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">latest posts <span></span></h5>
                            <ul class="popular-news clearfix">
                                <li>
                                    <div class="thumb">
                                       <img src="<?= BASEURLFRONTASSET; ?>images/post-tag.jpg" alt="image">
                                    </div>
                                    <div class="text">
                                        <p class="date-popular-news">Feb 25th, 2016</p>
                                        <h6>
                                            <a href="#">Class aptent ad litora torquent per.  </a>
                                        </h6>                                       
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                       <img src="<?= BASEURLFRONTASSET; ?>images/post-tag.jpg" alt="image">
                                    </div>
                                    <div class="text">
                                        <p class="date-popular-news">Jan 19th, 2016</p>
                                        <h6><a href="#">Proin sit amet tortor vitae lectus.</a></h6>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                       <img src="<?= BASEURLFRONTASSET; ?>images/post-tag.jpg" alt="image">
                                    </div>
                                    <div class="text">
                                        <p class="date-popular-news">Mar 13th, 2016</p>
                                        <h6><a href="#">Aliquam sed arcu et  accumsan.</a></h6>
                                    </div>
                                </li>
                            </ul><!-- /.popular-news -->
                        </div><!-- /.widget-popular-news -->

                        <div class="widget widget-archive wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Archive<span></span></h5>
                            <ul>
                                <li><a href="#">January  (18)</a></li>
                                <li><a href="#">February  (31)</a></li>
                                <li><a href="#">March  (22)</a></li>
                            </ul>
                        </div><!-- /.widget-Archive -->

                        <div class="widget widget-tags wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1500ms">
                            <h5 class="widget-title">Popular Tags<span></span></h5>
                            <div class="tag-list">
                                <a class="active" href="#">Fashion</a>
                                <a href="#">Photoshop</a>
                                <a href="#">Business</a>
                                <a href="#">Typography</a>
                            </div>
                        </div><!-- /.widget-tags -->
                    </div><!-- /.sidebar -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section>




        




          

<!-- --> 
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>

<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>
