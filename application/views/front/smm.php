 
<?php $this->load->view("front/header.php"); ?>

<!-- End Header-->
<div class="banner innerpages otherpage" style="background:url(<?= BASEURLFRONTASSET; ?>images/header_bg_1.jpg) no-repeat;background-size: cover;    position: relative;">
    <div class="saiful_overlay"></div>
    <div class="bannertext">
     <h1>Social Media & <br> Marketing</h1>
  </div>
</div>


<div class="what_offer_box">
 <div class="container">
 <div class="row">
<div class="col-md-12 ">    
<h2 class="section-title text-center color-3 mb-40 wow fadeInUp"><span>Digital </span>Marketing </h2>    
 </div>    
</div>
  </div>    
</div>


<div class="website-designer-services">
  <ul>
  

    <li>
      <div class="img-box"><img src="<?= BASEURLFRONTASSET; ?>images/smo-services-in-india.gif" alt="seo-services" title="seo-services" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Social Media Marketing</p>
         <p>Needless to say, in today’s time, it is the easiest place to target a wider audience. It helps you to build your brand image among various channels like Facebook, Twitter, LinkedIn, Instagram, Pinterest, Tumblr, etc. Over the past few years, this type of digital marketing is at its peak.</p>
      </div>
    </li>

    <li>
      <div class="img-box"><img src="<?= BASEURLFRONTASSET; ?>images/email-marketing-services.gif" alt="seo-company-in-delhi" title="seo-company-in-delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Email Marketing</p>
         <p>It is one of the oldest forms of digital marketing, which includes targeted messages at the correct time, which open the doors to drive a huge traffic to your website.</p>
      </div>
    </li>

      <li>
      <div class="img-box"><img src="<?= BASEURLFRONTASSET; ?>images/seo-company-in-india.gif" alt="seo-services" title="seo-services" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Social Media Marketing</p>
         <p>Needless to say, in today’s time, it is the easiest place to target a wider audience. It helps you to build your brand image among various channels like Facebook, Twitter, LinkedIn, Instagram, Pinterest, Tumblr, etc. Over the past few years, this type of digital marketing is at its peak.</p>
      </div>
    </li>

    
     
  </ul>
</div>

<?php $this->load->view("front/brand"); ?>
<div class="upper-footer"> </div>



<!-- -->

<!-- --> 
<?php $this->load->view("front/footer.php"); ?>
 