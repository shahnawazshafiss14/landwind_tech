<?php $this->load->view("front/header"); ?>

<!-- End Header-->
<div class="banner innerpages">
    <img src="<?= BASEURLFRONTASSET; ?>images/bannerinner.png" alt="">
</div>
<div class='fluid-container graybg'>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="javascript:;">Services</a></li>
            <li class="active">Branding and Identity</li>
        </ul>
    </div> 
</div>
<!-- -->
<div class='container'>
    <div class='row'>
        <aside class="sidenav hidden-sm hidden-xs">
            <ul>
                <li><a href="<?= base_url(); ?>about-us">ABOUT Landwind Tech</a></li>
                <li><a href="<?= base_url(); ?>website-design-development">Website Design & Development</a></li>
                <li><a href="<?= base_url(); ?>search-engine-optimisation">Search Engine Optimisation</a></li>
                <li><a href="<?= base_url(); ?>landwind-tech-management">Landwind Tech Management</a></li>
                <li><a href="<?= base_url(); ?>social-media-marketing">Social Media Marketing</a></li>
                <li><a href="<?= base_url(); ?>our-mission">Our Mission</a></li>
                <li><a href="<?= base_url(); ?>web-development-philosophy">Web development philosophy</a></li>
                <li><a href="<?= base_url(); ?>business-application-development">Business Application Development</a></li>
                <li><a href="<?= base_url(); ?>customer-review-management">Customer Review Management</a></li>
                <li class="active"><a href="javascript:void();">Branding and Identity</a></li>
            </ul>
        </aside>
        <article>
            <div class="content">
                <h1>Branding and Identity</h1>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,</p>

                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut,</p>
            </div>
        </article>
    </div>
</div>
<!-- -->

<?php $this->load->view("front/footer.php"); ?>
