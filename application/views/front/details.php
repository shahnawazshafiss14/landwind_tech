<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>A Complete Online Applications Solution :: Landwind Tech </title>
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.png" type="image/x-icon">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<script src="js/jquery-3.2.1.min.js"></script>
		<link rel="stylesheet" href="css/owl.carousel.min.css">
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/custom.js"></script>

<link rel="stylesheet" href="js/gallery/foundation.min.css"  type="text/css">
<link rel="stylesheet" type="text/css" href="js/gallery/set1.css" />
<!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<![endif]-->
		<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
		<link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">
		<link rel="stylesheet" href="css/hamburgers.css">
		<link rel="stylesheet" href="css/style.css" type="text/css">
	</head>
	<body>
		<div class="wrapper">
			<header>
				<?php include("include/header.php"); ?>
			</header>
				<!-- End Header-->
			<div class="banner innerpages">
				<img src="images/bannerinner.png" alt="">
			</div>
			<div class='fluid-container graybg'>
				<div class="container">
					<ul class="breadcrumb">
						<li><a href="portfolio.php">portfolio</a></li>
						<li class="active">Details</li>
					</ul>
				</div>
			</div>
			<!-- -->
			<div class='container'>
				<div class='row'>
					<main class="main-wrapper-inner" id="container">
            	<div class="container">
                	<div class="wrapper-inner">
                    	<!-- details-image -->
                    	<figure class="details-image">
                        	<img src="images/details-image-3.jpg" alt="" class="img-responsive"/>
                        </figure>
                        <!-- details-image -->
                        <!-- content -->
                        <div class="details-content">
                        	<!-- left -->
                        	
                            <!-- left -->
                            <!-- right -->
                            <section class="inner-right">
<p></p><br>
                            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat eu nibh ultricies semper. Vivamus porta, felis vitae facilisis sodales, felis est iaculis orci, et ornare sem mauris ut turpis. Pellentesque vitae tortor nec tellus hendrerit aliquam. Donec condimentum leo eu ull pellentesque urna rhoncus.</p>
                                <p>elis est iaculis orci, et ornare sem mauris ut turpis. Pellentesque vitae tortor nec tellus hendrerit aliquam. Donec condimentum leo eu ullamcorper scelerisque pellentes rhoncus.</p>
<p></p><br>
                            </section>
                            <!-- right -->
                        </div>
                        <!-- content -->
                        
                    </div>
                </div>
            </main>
					
				</div>
			</div>
			<!-- -->
			<!-- Footer-->
					<!-- Footer-->

	
<?php $this->load->view("front/footer.php"); ?>