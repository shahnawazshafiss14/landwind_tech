 
<?php $this->load->view("front/header"); ?>

<div class="banner innerpages otherpage" style="background:url(<?= BASEURLFRONTASSET; ?>images/header_bg_1.jpg) no-repeat;background-size: cover;    position: relative;">
    <div class="saiful_overlay"></div>
    <div class="bannertext">
     <h1>Search Engine &<br>Optimisation</h1>
  </div>
</div>



<!-- -->

<!-- <div class='container'>

    <div class='row'>
        <article>
            <div class="content">
                <h1>Search Engine Optimisation</h1>
                <p>In the field of Web Solutions and Services, we understood the significance of being the top most or the most preferred one in the search engines. Being found easily in the Search engines creates the brand value and brand recognition among the users and clients.</p> 
                <p>SEO Professionals with utmost interest and strong knowledge deliver outstanding results. We are prompt and powerful in optimizing the websites by getting updated with latest SEO trends and by doing intense research on keywords and other seo related stuffs.</p>
                <p>
                    Being a pioneer in SEO, our team provides end to end comprehensive solutions required by your Website to be listed in the top search results of SERPs (Search Engine Ranking Page). Our result oriented SEO services, increase visitors, generate leads and amplify profits by immense.
                </p>
                <p>Reasons to choose SEO professionals from landwind tech</p>
                <ul>
                    <li>Highly qualified SEO professionals with years of experience</li>
                    <li>Profound expertise in link building, off-line and on-line Search engine optimization</li>
                    <li>Consistent track records in sustaining keywords thereby generating leads</li>
                    <li>Renowned for maintaining the web pages in top 10 results</li>
                    <li>Successfully handled numerous projects from various domain industries across the globe</li>
                    <li>Our professionals can analyze your website and suggest the best suit SEO solutions. And so contact us,get all your doubts cleared about optimizing your website and discuss your SEO requirements in detail.</li>
                </ul>

            </div>

        </article>

    </div>

</div> -->

<!-- -->



<div class="what_offer_box">
 <div class="container">
 <div class="row">
<div class="col-md-12 ">    
<h2 class="section-title text-center color-3 mb-40 wow fadeInUp"><span>SEO </span>Services </h2>    
 </div>    
</div>
  </div>    
</div>



<div class="website-designer-services">
  <ul>
  

    <li>
      <div class="img-box"><img src="<?= BASEURLFRONTASSET; ?>images/seo-services.gif" alt="seo-services" title="seo-services" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Trusted SEO Company In Delhi</p>
                <p>SEO Professionals with utmost interest and strong knowledge deliver outstanding results. We are prompt and powerful in optimizing the websites by getting updated with latest SEO trends and by doing intense research on keywords and other seo related stuffs.</p>
                <p>
                                    <p>Reasons to choose SEO professionals from landwind tech</p>

      </div>
    </li>

    <li>
      <div class="img-box"><img src="<?= BASEURLFRONTASSET; ?>images/seo-company-in-delhi.gif" alt="seo-company-in-delhi" title="seo-company-in-delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">Advanced SEO Strategies</p>
          
                <p>SEO Professionals with utmost interest and strong knowledge deliver outstanding results. We are prompt and powerful in optimizing the websites by getting updated with latest SEO trends and by doing intense research on keywords and other seo related stuffs.</p>
                <p>
                    Being a pioneer in SEO, our team provides end to end comprehensive solutions required by your Website to be listed in the top search results of SERPs (Search Engine Ranking Page). Our result oriented SEO services, increase visitors, generate leads and amplify profits by immense.
                </p>
      </div>
    </li>

    <li>
      <div class="img-box"><img src="<?= BASEURLFRONTASSET; ?>images/top-seo-agency-in-delhi.gif" alt="E-commerce Website Designing Company In Delhi" title="E-commerce Website Designing Company In Delhi" class="img-responsive wow fadeInLeftBig"></div>
      <div class="design-content design-content-desc">    
        <p class="design-content-title">SEO professionals</p>
              <p>In the field of Web Solutions and Services, we understood the significance of being the top most or the most preferred one in the search engines. Being found easily in the Search engines creates the brand value and brand recognition among the users and clients.</p> 
      </div>
    </li>

    
     
  </ul>
</div>


<?php $this->load->view("front/brand"); ?>
<div class="upper-footer"> </div>




<!-- Footer-->

<?php $this->load->view("front/footer.php"); ?>
  