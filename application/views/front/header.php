<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

        <title>Complete Web Solutions - Landwind Tech</title>
        <link rel="shortcut icon" href="<?= BASEURLFRONTASSET; ?>images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= BASEURLFRONTASSET; ?>images/favicon.png" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/animate.css">
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/font-awesome.min.css">
        <script src="<?= BASEURLFRONTASSET; ?>js/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/owl.carousel.min.css">
        <script src="<?= BASEURLFRONTASSET; ?>js/owl.carousel.min.js"></script>
        <script src="<?= BASEURLFRONTASSET; ?>js/bootstrap.min.js"></script>
        <script src="<?= BASEURLFRONTASSET; ?>js/custom.js"></script>
        <script src="<?= BASEURLFRONTASSET; ?>js/notify.js"></script>
        <script src="<?= BASEURLFRONTASSET; ?>js/alertify.min.js"></script>
        <script src="<?= BASEURLFRONTASSET; ?>js/sweetalert2.js"></script>
        <script src="<?= BASEURLFRONTASSET; ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/hamburgers.css">
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>css/style.css" type="text/css">
        <link rel="stylesheet" href="<?= BASEURLFRONTASSET; ?>js/gallery/foundation.min.css"  type="text/css">
        <link rel="stylesheet" type="text/css" href="<?= BASEURLFRONTASSET; ?>js/gallery/set1.css" />

    </head>
    <body>
        <div class="wrapper">
            <header>
                <nav>
                    <div class="logo">
                        <a class="" href="<?= base_url(); ?>"><img src="<?= BASEURLFRONTASSET; ?>images/logo1.png" alt="Parag Logo" /></a>
                    </div>
                    <div class="smvisible bars">
                        <span></span>
                    </div>
                    <div class="navbar">
                        <div class="preenav">
                            <ul>
                                <li><a href="tel:+919871769780"><i class="fa fa-phone"></i>  +91-9560203969</a></li>
                                <li><a href="mailto:info@landwindtech.com"><i class="fa fa-envelope"></i> info@landwindtech.com</a></li>
                                <li>
                                    <ol>
                                        <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                    </ol>
                                </li>
                            </ul>
                        </div>
                        <div class="navmenu">
                            <ul>
                                <li><a href="<?= base_url(); ?>">Home</a></li>
                                <li><a href="<?= base_url(); ?>about-us">About Us</a></li>
                                <li  class="menu-item-has-children clearfix"><a href="<?= base_url(); ?>services">Services</a> <span class="down-angle ds-lg"><i class="fa fa-angle-down"></i></span>
                                    <div class="submenu">
                                        <div class="submenuinner">
                                                <ul class="dropnavulabout">
                                                    <li><a href="<?= base_url(); ?>website-design-development">Website Design & Development</a></li>
                                                    <li><a href="<?= base_url(); ?>search-engine-optimisation">Search Engine Optimisation</a></li>
                                                    <li><a href="<?= base_url(); ?>mobile-application">Mobile Application</a></li>
                                                    <li><a href="<?= base_url(); ?>social-media-marketing">Social Media Marketing</a></li>
                                                    <li><a href="<?= base_url(); ?>our-mission">Our Mission</a></li>
                                                    <!-- <li><a href="<?= base_url(); ?>web-development-philosophy">Web development philosophy</a></li> -->
                                                    <!-- <li><a href="<?= base_url(); ?>business-application-development">Application Development</a></li> -->
                                                    <li><a href="<?= base_url(); ?>training">Training</a></li>  
                                                    <!-- <li><a href="<?= base_url(); ?>landwind-tech-management">Landwind Tech Management</a></li> -->
                                                </ul>
                                            <!-- -->
                                        </div>
                                    </div>
                                </li>
                                <li><a href="<?= base_url(); ?>portfolio">Portfolio</a></li>
                                <li><a href="<?= base_url(); ?>training">Training</a></li>
                                 <li><a href="<?= base_url(); ?>blog">Blog</a></li>  
                                <li><a href="<?= base_url(); ?>contact-us">Contact Us</a></li>
                                <!--<li><a href="#">Careers</a></li>-->
                                <?php
                                $fetchurl = $this->uri->segment(1);
                                if($fetchurl != 'contact-us'): 
                                ?>
                                    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#callbackModal">Get In Touch</a></li>
                                <?php 
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>