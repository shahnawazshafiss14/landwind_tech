<?php
$this->load->view('front/header');
?>

<!-- End Header-->
<div class="banner">
    <div class="bannerimage" style="background:url(<?= BASEURLFRONTASSET; ?>images/blx-d-p15.jpg) no-repeat;background-size: cover;    position: relative;">
        <div class="bannertext">
            <h1>Complete Web Solutions</h1>
            <!-- <ul><li>Web Development</li><li>Search Engine Optimisation</li><li>Training</li></ul> -->
        </div>
    </div>
</div>
<div class="grey-bg" style="background:url(<?= BASEURLFRONTASSET; ?>images/grey-pattren.jpg) left top repeat;">
  <div class="container-fluid">   
       <div class="homeport-hd wow fadeInUp" data-wow-duration="1s">
          <h3>We Expertise in<br>
          <span>All Web &amp; Digital Services</span></h3>
          <p>We are a creative and committed web design, development and digital marketing agency in India, known for more than a decade to build the most beautiful &amp; engaging UI designs for distinct businesses. Our experts deliver a balance of creativeness and user experience with their result-oriented innovations and our clients get more than just a well-designed website, as we make sure to make users visit the site again &amp; again.</p>
       </div>
       
       <div class="clear"></div>
       <div class="homeserv-white">
               <div class="col-sm-6 col-md-6 col-lg-4">
                   <div class="home-serv wow fadeInUp" data-wow-duration="1s">
                        <h4 class="wow fadeInUp" data-wow-duration="1s">SEO/SMO &amp; Digital <br> <span>Marketing</span></h4>
                        <div class="col-sm-9 col-lg-9 pull-right">
                            <ul class="wow fadeInUp" data-wow-duration="1s">
                               <li><a>Search Engine Optimization</a></li>
                               <li><a>Content Optimization</a></li>
                               <li><a>Blogs and Articles</a></li>
                               <li><a>Social Optimization</a></li>
                            </ul>
                        </div>
                        <div class="serviceicon"> <img src='<?= BASEURLFRONTASSET; ?>images/service-icon4.png' alt="service-icon"></div>
                        <div class="servicemore"><a href="<?= base_url(); ?>social-media-marketing" title="View More"><img src='<?= BASEURLFRONTASSET; ?>images/servicemore.png' alt="service-icon"></a></div>
                        
                        <div class="clear"></div>                   
                   </div>
               
               </div>
               <div class="col-sm-6 col-md-6 col-lg-4">
                   <div class="home-serv  wow fadeInUp" data-wow-duration="1s">
                        <h4 class="wow fadeInUp" data-wow-duration="1s">Web Application<br> <span>Development</span></h4>
                        <div class="col-sm-9 col-lg-9 pull-right">
                            <ul class="wow fadeInUp" data-wow-duration="1s">
                               <li><a>Website Development</a></li>
                               <li><a>Content Management Systems</a></li>
                               <li><a>Software Development</a></li>
                               <li><a>Systems Integration</a></li>
                            </ul>
                        </div>
                           <div class="serviceicon"><img src='<?= BASEURLFRONTASSET; ?>images/service-icon.png' alt="service-icon"></div>
                           <div class="servicemore"><a href="<?= base_url(); ?>website-design-development" title="View More Services"><img src='<?= BASEURLFRONTASSET; ?>images/servicemore.png' alt="service-icon"></a></div>
                        
                        <div class="clear"></div>                   
                   </div>
                   
                   
               </div>
               <div class="col-sm-6 col-md-6 col-lg-4">
                   <div class="home-serv  wow fadeInUp" data-wow-duration="1s">
                        <h4 class="wow fadeInUp" data-wow-duration="1s">Mobile App<br> <span>Development</span></h4>
                        <div class="col-sm-9 col-lg-9 pull-right">
                            <ul class="wow fadeInUp" data-wow-duration="1s">
                               <li><a>Interspire Development</a></li>
                               <li><a>Iphone App Development</a></li>
                               <li><a>Android App Development</a></li>
                               <li><a>Window App solution</a></li>
                            </ul>
                        </div>
                        <div class="serviceicon"><img src='<?= BASEURLFRONTASSET; ?>images/service-icon3.png' alt="service-icon"></div>
                        <div class="servicemore"><a href="<?= base_url(); ?>mobile-application" title="View More Services"><img src='<?= BASEURLFRONTASSET; ?>images/servicemore.png' alt="service-icon"></a></div>
                        
                        <div class="clear"></div>                   
                   </div>
                   
               </div>
               
               <div class="col-sm-6 col-md-6 col-lg-4">
                 <div class="home-serv  wow fadeInUp" data-wow-duration="1s">
                        <h4 class="wow fadeInUp" data-wow-duration="1s">E-commerce <br> <span>Solution</span></h4>
                        <div class="col-sm-9 col-lg-9 pull-right">
                            <ul class="wow fadeInUp" data-wow-duration="1s">
                               <li><a>E-commerce / Shopping</a></li>
                               <li><a>Competitive Analysis</a></li>
                               <li><a>Mobile Capabilities</a></li>
                               <li><a>Business Goals</a></li>
                            </ul>
                        </div>
                        <div class="serviceicon"><img src='<?= BASEURLFRONTASSET; ?>images/service-icon2.png' alt="service-icon"></div>
                        <div class="servicemore"><a href="<?= base_url(); ?>social-media-marketing" title="View More Services"><img src='<?= BASEURLFRONTASSET; ?>images/servicemore.png' alt="service-icon"></a></div>
                        
                        <div class="clear"></div>                   
                   </div>
                   
               </div>
               
               <div class="col-sm-6 col-md-6 col-lg-4">
                   <div class="home-serv  wow fadeInUp" data-wow-duration="1s">
                        <h4 class="wow fadeInUp" data-wow-duration="1s">Responsive Web<br> <span>Designings</span></h4>
                        <div class="col-sm-9 col-lg-9 pull-right">
                            <ul class="wow fadeInUp" data-wow-duration="1s">
                                <li><a>Websites Design</a></li>
                                <li><a>Logo Design</a></li>
                                <li><a>Emailer Design</a></li>
                                <li><a>Brochure Design</a></li>
                            </ul>
                        </div>
                        <div class="serviceicon"><img src='<?= BASEURLFRONTASSET; ?>images/service-icon5.png' alt="service-icon"></div>
                        <div class="servicemore"><a href="<?= base_url(); ?>website-design-development" title="View More Services"><img src='<?= BASEURLFRONTASSET; ?>images/servicemore.png' alt="service-icon"></a></div>
                        
                        <div class="clear"></div>                   
                   </div>
               </div>

               <div class="col-sm-6 col-md-6 col-lg-4">
                   <div class="home-serv  wow fadeInUp" data-wow-duration="1s">
                        <h4 class="wow fadeInUp" data-wow-duration="1s">All Other Web Related<br> <span>Services</span></h4>
                        <div class="col-sm-9 col-lg-9 pull-right">
                            <ul class="wow fadeInUp" data-wow-duration="1s">
                               <li><a>Website development</a></li>
                               <li><a>Software development</a></li>
                               <li><a>ASP.net customised</a></li>
                               <li><a>Using MVC language</a></li>
                            </ul>
                        </div>
                        <div class="serviceicon"><img src='<?= BASEURLFRONTASSET; ?>images/service-icon6.png' alt="service-icon"></div>
                        <div class="servicemore"><a href="<?= base_url(); ?>services" title="View More Services">
                          <img src='<?= BASEURLFRONTASSET; ?>images/servicemore.png' alt="service-icon"></a></div>
                        
                        <div class="clear"></div>                   
                   </div>
               </div>               
       </div>          
  </div>
</div>

<!-- Footer-->
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>
