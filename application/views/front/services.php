
<?php $this->load->view("front/header"); ?>

<!-- End Header-->
<!-- <div class="banner innerpages otherpage" style="background:url(<?= BASEURLFRONTASSET; ?>images/header_bg_1.jpg) no-repeat;background-size: cover;    position: relative;">
    <div class="saiful_overlay"></div>
    <div class="bannertext">
                            <h1>Our Services</h1>
                        </div>
</div> -->




<div class="clearfix"></div>

<section style="padding-top: 80px;">
        <div class="container">
            <div class="features-area-box wow fadeInUp">
                <div class="row text-center">
                    <div class="col-md-4 col-sm-4">
                        <div class="single-feature">
                            <i class="fa fa-television"></i>
                            <h6>Awesome Design</h6>
                            <div class="fetu-line"></div>
                            <p>Our pro designers design &amp; develop every product that looks and feels as great as it functions.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="single-feature">
                            <i class="fa fa-clock-o"></i>
                            <h6>Fast Development</h6>
                            <div class="fetu-line"></div>
                            <p>Our engineers write flexible, high-quality code using proven methodologies within the project deadline.</p>
                        </div>
                    </div>

                    <div class="col-sm-3"></div>
                    <div class="col-md-4 col-sm-4">
                        <div class="single-feature">
                            <i class="fa fa-cloud-download"></i>
                            <h6>Advanced Support</h6>
                            <div class="fetu-line"></div>
                            <p>Our Rack Server come with up to 64 GB of RAM and 2 TB hard disk space, providing capacity for a variety of applications.</p>
                        </div>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
        </div>
    </section>




<section id="about" class="about-section wow fadeInLeft">
        <div class="row">
            <div class="container">
                <div class="col-md-3">
                    <div class="about-right"><div class="about-phone"><img src="<?= BASEURLFRONTASSET; ?>images/05.png" class="" alt="aboutus"></div></div>
                </div>
                <div class="col-md-8">

                    <div class="section-about">
                        <h3>Web Development Service</h3>
                        <p>Landwindtech is a leading web designing company which provides the best service with excellent results. We know the importance of the website and how to make it attractive so that it can bring lots of traffic. </p>
                        <p>Our company ensures that you will fulfil all your wishes and gets high pay audiences. For running a business with growing speed the website should be professional and clean. The website should look beautiful as this is the first step which attracts the audiences and service provides with proficiency makes it more reliable. </p>
                        <p>
                            We provide all types of services whether it is designing or maintaining in an effective manner. Our main motive is to make your site effective among users and engage them so that sight audiences get converted into buying one.
                        </p>
                        <p>It can be achieved when your website provides them satisfied result with the commitment of a long run without any hassle. We make sure that every client could get connected with their target audiences and could make the site full of traffic. </p>
                    </div>


                </div>
            </div>
        </div>
    </section>




<section id="services" class="servcies-section">
        <div class="container">
            <div class="row wow slideInUp">
                <div class="col-sm-12">
                    <div class="section_title text-center pT80">
                        <h1>Our Services</h1>
                    </div>
                    <div class="col-md-12">
                        <div class="section-header section-space">
                            <div class="row text-center">
                                
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                                        <div class="gallery-section">
                                            <div class="fa fa-2x icon-circle"><i class="fa fa-file-code-o"></i></div>
                                            <div class="mT10 ourservices-content">
                                                <h4>Website Design & Development</h4>
                                                <p>We make customized and unique website design according to the needs of the client. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>      

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                                        <div class="gallery-section">
                                            <div class="fa fa-2x icon-circle"><i class="fa fa-search"></i></div>
                                            <div class="mT10 ourservices-content">
                                                <h4>Search Engine Optimisation</h4>
                                                <p>We make customized and unique website design according to the needs of the client. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="300ms">
                                        <div class="gallery-section">
                                            <div class="fa fa-2x icon-circle"><i class="fa fa-mobile"></i></div>
                                            <div class="mT10 ourservices-content">
                                                <h4>Mobile Application</h4>
                                                <p>With the help of our expert, we create a rich and excellent performance of the website. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">
                                        <div class="gallery-section">
                                            <div class="fa fa-2x icon-circle"><i class="fa fa-share-square-o"></i></div>
                                            <div class="mT10 ourservices-content">
                                                <h4>Social Media Marketing</h4>
                                                <p>We create a brochure with a perfect balance of visuals and fonts for engaging readers. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">
                                        <div class="gallery-section">
                                            <div class="fa fa-2x icon-circle"><i class="fa fa-home"></i></div>
                                            <div class="mT10 ourservices-content">
                                                <h4>Our Mission</h4>
                                                <p>We first illustrate the layout according to the target audience with the depth functioning. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms">
                                        <div class="gallery-section">
                                            <div class="fa fa icon-circle"><i class="fa fa-2x fa-paint-brush"></i> </div>
                                            <div class="mT10 ourservices-content">
                                                <h4>Training</h4>
                                                <p>We use artificial design for creating a unique and effective logo with perfect fonts and icons.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




<section class="clients wow fadeIn">
  <div class="buble">
    <div class="bottom-particles" id="bubbletext"><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div><div class="bubble"></div></div>
  </div>
  <div class="left-img wow slideInLeft"> <img src="<?= BASEURLFRONTASSET; ?>images/boy.png" class="hidden-sm" alt="100% client satisfaction - Sabka Atoot Vishwas" title="100% client satisfaction - Sabka Atoot Vishwas"> </div>
  <div class="container">
    <div class="col-lg-8 pull-right text-right wow slideInRight">
      <h3>AN ARMY OF CLIENTS CANNOT BE WRONG! </h3>
      <p></p><p>As the top 5 website designing company in India, we are passionate, dedicated, committed to achieve only one goal - 100% client satisfaction. We are chasing this dream by designing and developing flawless digital solutions with sophisticated strategies to bring you on top of the search engines.</p>
      <ul class="counter">
        <li> <span class="count">100</span><i class="fa fa-percent" aria-hidden="true"></i>
          <p>websites optimised</p>
        </li>
        <li> <span class="count">100</span><i class="fa fa-percent" aria-hidden="true"></i>
          <p>worldwide clients</p>
        </li>
        <li> <span class="count">100</span><i class="fa fa-percent" aria-hidden="true"></i>
          <p>appreciations won</p>
        </li>
      </ul>
    </div>
  </div>
</section>




<!-- Footer-->
<?php $this->load->view("front/brand"); ?>
<?php $this->load->view("front/footer"); ?>
 