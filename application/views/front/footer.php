<div class="footer" data-background="images/footer-bg.jpg" style="background:url(<?= BASEURLFRONTASSET; ?>images/footer-bg.jpg) no-repeat;background-size: cover;">
        <div class="content-wrap">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-3">
                        <div class="footer-item">
                              <div class="footer-title">
                               About
                            </div>
                            <p>We are on a mission to start a conversation with your customers in this fast connected world. Let’s discover, build and grow your digital business. We fuel business growth through innovative digital marketing and technology solutions.</p>
                              <div class="footer_logo">
                            <img src="<?= BASEURLFRONTASSET; ?>images/logo-footer.png" alt="" class="logo-bottom">
                           </div>
                              <div class="clearfix"></div>
                            <a href="<?= base_url();?>about-us"><i class="fa fa-angle-right"></i> Read More</a>
                        </div>
                    </div>                  

                    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-3">
                        <div class="footer-item">
                            <div class="footer-title">
                                Contact Info
                            </div>
                            <ul class="list-info">
                                <li>
                                    <div class="info-icon">
                                        <span class="fa fa-map-marker"></span>
                                    </div>
                                    <div class="info-text">Abul Fazal Enclave II, Jamia Nager, Okhla,</div> </li>
                                <li>
                                    <div class="info-icon">
                                        <span class="fa fa-phone"></span>
                                    </div>
                                    <div class="info-text"><a href="tel:+919560203969">+91-9560203969</a></div>
                                </li>
                                <li>
                                    <div class="info-icon">
                                        <span class="fa fa-envelope"></span>
                                    </div>
                                    <div class="info-text"><a href="mailto:info@landwindtech.com">info@landwindtech.com</a></div>
                                </li>
                                <li>
                                    <div class="info-icon">
                                        <span class="fa fa-clock-o"></span>
                                    </div>
                                    <div class="info-text">Mon - Sat 09:00 - 17:00</div>
                                </li>
                                <?php
                                $fetchurl = $this->uri->segment(1);
                                if($fetchurl != 'contact-us'): 
                                ?>
                                 <a href="javascript:void(0);" data-toggle="modal" data-target="#callbackModal" class="btn callback">REQUEST CALLBACK</a>
                                 <?php 
                                endif;
                                ?>
                            </ul>

                        </div>
                    </div>

                    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-3">
                        <div class="footer-item">
                            <div class="footer-title">
                                Useful Links
                            </div>
                            
                            <ul class="list">
                              <li><a href="<?= base_url(); ?>website-design-development">Website Design & Development</a></li>
                        <li><a href="<?= base_url(); ?>search-engine-optimisation">Search Engine Optimisation</a></li>
                        <li><a href="<?= base_url(); ?>mobile-application">Mobile Application</a></li>
                        <li><a href="<?= base_url(); ?>social-media-marketing">Social Media Marketing</a></li>
                        <li><a href="<?= base_url(); ?>our-mission">Our Mission</a></li>
                        <li><a href="<?= base_url(); ?>training">Training</a></li>
                        <!-- <li><a href="<?= base_url(); ?>web-development-philosophy">Web development philosophy</a></li>
                        <li><a href="<?= base_url(); ?>business-application-development">Business Application Development</a></li>
                        <li><a href="<?= base_url(); ?>landwind-tech-management">Landwind Tech Management</a></li> -->
                            </ul>
                                
                        </div>
                    </div>
                    
                    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-3">
                        <div class="footer-item">
                            <div class="footer-title">
                                Get in Touch
                            </div>
                            <p>F-149, Ground Floor, Shaheen Bagh, Abul Fazal Enclave II, Jamia Nager, Okhla, New Delhi-110014</p>
                            <div class="sosmed-icon d-inline-flex">
                                <a href="#" class="fb"><i class="fa fa-facebook"></i></a> 
                                <a href="#" class="tw"><i class="fa fa-twitter"></i></a> 
                                <a href="#" class="ig"><i class="fa fa-instagram"></i></a> 
                                <a href="#" class="in"><i class="fa fa-linkedin"></i></a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="fcopy text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                     <p>Copyright © 2018-2019 Landwind Tech  All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


    <div class="modal fade custom_form" id="callbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document"> 
            <div class="modal-content">
                <div class="modal-header" style="    background: #122348;color: #fff;">
                    <h5 class="modal-title" id="exampleModalLabel" style="color: #fff;">REQUEST CALLBACK</h5>
                </div>
                <form action="" class="" role="form" method="post" id="frmStudent">
                    <div class="modal-body col-xs-12">
                        <div class="form-group">
                            <input type="text" name="contName" id="contName" placeholder="Enter Your Name*" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="email" name="contEmail" id="contEmail" placeholder="Enter Your email*" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="contPhone" id="contPhone" maxlength="10" placeholder="Enter Your Phone*" class="form-control number">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="contMessage" id="contMessage" placeholder="Message"></textarea>
                        </div>
                        <div class="pull-right form-group">
                            <button type="button" class="btn btn-success" name="btnRequestCall" id="btnRequestCall" style="background-color: #091224;"> SEND <i class="fa fa-send-o"></i></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"> CLOSE <i class="fa fa-times"></i></button>
                        </div>
                    </div> 
                    <div class="clearfix"></div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
</div>
<?php
    $fetchurl = $this->uri->segment(1);
    if($fetchurl != 'contact-us'): 
?>
<div class="requestcallbackstickey">
    <a href="javascript:void(0);" data-toggle="modal" data-target="#callbackModal" class="hidden-lg hidden-md hidden-sm">Contact Us</a>
    <a href="javascript:void(0);" data-toggle="modal" data-target="#callbackModal"><span>REQUEST CALLBACK</span><i class="fa fa-phone"></i></a>
</div>
<?php 
endif;
?>
<script src="<?= BASEURLFRONTASSET; ?>js/gallery/masonry.pkgd.min.js" type="text/javascript"></script> 
<script src="<?= BASEURLFRONTASSET; ?>js/gallery/imagesloaded.pkgd.min.js" type="text/javascript"></script> 
<script src="<?= BASEURLFRONTASSET; ?>js/wow.min.js"></script>
<script src="<?= BASEURLFRONTASSET; ?>js/gallery/main.js" type="text/javascript"></script> 
    <script> new WOW().init();  </script>

</body>
</html>