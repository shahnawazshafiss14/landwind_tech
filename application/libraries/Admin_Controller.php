<?php

class Admin_Controller extends MY_Controller {

    public function __construct() {
	parent::__construct();
	$this->acc_id = $this->session->userdata('account_id');
	if ($this->session->userdata('userName_sess') == '') {
	    $dashboard = 'admin/index/index';
	    redirect($dashboard);
	}
    }

}

?>