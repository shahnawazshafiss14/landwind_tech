<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'index';
$route['404_override'] = '';
//$route['index'] = 'index'; 
$route['blog-detail'] = 'index/blog_detail'; 
$route['blog'] = 'index/blog'; 
$route['about-us'] = 'index/about'; 
$route['services'] = 'index/services'; 
$route['website-design-development'] = 'index/website'; 
$route['search-engine-optimisation'] = 'index/search_engine_optimisation';
$route['our-mission'] = 'index/our_mission';
$route['web-development-philosophy'] = 'index/webphilosophy';
$route['business-application-development'] = 'index/business';
$route['mobile-application'] = 'index/mobileapp';
$route['landwind-tech-management'] = 'index/landwind_tech_management'; 
$route['social-media-marketing'] = 'index/social_media_marketing'; 
$route['customer-review-management'] = 'index/customer_review_management'; 
$route['branding-and-identity'] = 'index/branding_and_identity'; 
$route['callbackMessage'] = 'index/callbackM'; 
$route['portfolio'] = 'index/portfolio'; 
$route['training'] = 'index/training'; 
$route['contact-us'] = 'index/contact'; 
$route['payment'] = 'index/payment'; 
$route['translate_uri_dashes'] = FALSE;
